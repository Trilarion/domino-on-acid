# Domino On Acid

## Development 2002 - 2009

In this time period, Domino On Acid was conceived and realized by Matthias S. Benkmann on
[Sourceforge](https://sourceforge.net/projects/nddomino) as well as on [winterdrache.de](http://winterdrache.de/freeware/domino/index.html).
It was written in Java and published under the GPL license version 2 (this version only). 

It was advertised as a natural deduction visualization in the form of a game of unusual dominoes.

## Continuation in 2021

Even though the binary release from 2009 was still runnable in 2021 on an JRE, the project was continued on [Gitlab](https://gitlab.com/Trilarion/domino-on-acid)
by Trilarion. The aim was to adapt the code base to recent Java language developments, streamline it and improve the app with a reasonable effort, while
keeping the game mechanics exactly equal.
