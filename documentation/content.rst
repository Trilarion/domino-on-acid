.. TODO all in one file for now, need to split later

#############
Domino Manual
#############

********
Overview
********

The following instructions may seem complicated but the game is really very simple and intuitive once you start playing.
It is a matching game that could be described as dominoes on acid.

.. figure:: /images/different_sizes_and_structures.gif

   The tiles in this game have different sizes and structures.

Each domino tile consists of one or more blocks. The function of a block is determined by the color of its sides,
which can be gray, blue, green or red. We will call these **gray blocks**, **blue blocks**, **green blocks** and **red blocks**
respectively.

This terminology obviously ignores the color patterns found on the top of most blocks. However, aside from the usual
dominoes rule that adjacent blocks of different tiles must show the same pattern, there is not much to say about
these patterns.

Most blocks have a pattern on top. The usual domino rule applies that adjacent blocks of different tiles must show the
same pattern.

The game starts with a single domino tile on the board and the goal of the game is to connect a **red** block or a **blue**
block to every **green** block on the board.

For this purpose there is a set of tiles freely available to the player. All of these tiles have one red block for
connecting to a green block. Unfortunately, all of these tiles have at least one green block, too. So for every green
block you finish by attaching a red block to it, you get at least one new green block that wants to be connected.
The only way to finish the game is by using blue blocks. A blue block connects to a green block without adding a
new green block. But unlike the tiles with red blocks that are freely available to the player for use everywhere,
the blue blocks are bonus blocks that have to be earned and can be used only with restrictions.

.. figure:: /images/connecting_red_to_green.gif

   Connecting this red block finishes one green block but adds two.

.. figure:: /images/connecting_blue_to_green.gif

   Connecting this blue block finishes the green block for good.

**************
Basic Controls
**************

To get you started right away, here are the basic controls of the game. The complete list of controls will be presented
later.

*****
Tiles
*****

The following paragraphs describe the tiles you will encounter during the game.

The Start Tile
==============

.. image:: /images/start_tile.gif

At the beginning of each level, the start tile is the only tile on the board and unlike other tiles the start tile can
not be removed. The start tile is also the only tile that has a gray block with a pattern. The gray block can not be
connected to other tiles.

Depending on the level there may be one or more blue bonus blocks on the start tile for use anywhere on the board.

Freely Available Tiles
======================

The freely available tiles may be used at any time anywhere on the board. All freely available tiles have one red block
for connecting to a green block and one or more green blocks. Some freely available tiles also have bonus blocks that
are discussed further below. The main characteristic of the freely available tiles is that most of their blocks have
patterns with rectangles colored in shades of gray. Every gray rectangle is a joker. When connecting the red block of
one of the freely available tiles to a green block, each gray rectangle on the red block will assume color and pattern
of the corresponding rectangle on the green block, with the following limitations:

* A gray rectangle can match a group of smaller rectangles but not part of a larger rectangle.
* Rectangles with the same shade of gray can only match the same pattern.

The Redness Inducer
-------------------

.. image:: /images/redness_inducer.gif

Redness inducer tiles are special, because they are the only freely available tiles that have a non-joker color,
the color red, in their patterns. Another specialty of the redness inducer is that the pattern of its green block
is independent of the pattern of its red block.

.. TODO ?? This is difficult to understand.

The Longish Simplifier
----------------------

.. image:: /images/longish_simplifier.gif
.. image:: /images/simplifier_use_example.gif

Simplifiers are useful for breaking complex patterns into simpler ones. At the same time a simplifier gives you a
blue block. Use the simplifier whenever you can.

The Branching Complexifier
--------------------------

.. image:: /images/branching_complexifier.gif

The branching complexifier is in a way the opposite of the longish simplifier. You attach it it to a green block and
what you get is a new green block whose bottom half corresponds to the pattern of the green block the complexifier
is attached to. The complexifier adds another green block whose pattern matches the top half of the other new green block.

.. figure:: /images/complexifier_use_example.gif

    Complexifiers are useful if you have a bonus block available that has in its bottom right corner the same pattern
    as an unconnected green block.

The Boring Elongator
--------------------

.. image:: /images/boring_elongator.gif

This is a very unremarkable tile that is not used very often. Its only purpose is to provide a way out when space
around a green block gets too narrow for placing larger tiles.

Bonus Blocks
============

.. image:: /images/several_bonus_blocks.gif

The bonus blocks are the blue blocks that lie on top of some tiles. Bonus blocks are single-block tiles that can be
connected to green blocks. There is an important restriction that applies to all bonus blocks: A bonus block may only
be attached

* to a green block of the tile that provides the bonus block or
* to a tile attached to a green block of the tile that provides the bonus block or
* to a tile attached to a tile attached to a green block of the tile that provides he bonus block, and so on.

In other words: If you were to walk on the domino tiles from the bonus block to the green
block you want to attach it to, then you must not cross the red block of the tile that provides the bonus block.

Custom Tiles
============

.. image:: /images/custom_tile_example.gif

Custom tiles are created by connecting some freely available tiles and compressing the result into a single tile.
The compressed tile has the same red and green blocks and provides the same bonus blocks, but has a more compact shape.

See the game controls for information on how to create custom tiles.

*************
Game Controls
*************

There are two control modes. The first mode has just the normal mouse pointer. The second mode has a domino tile
"hanging" at the mouse pointer and following its movements. This tile is referred to as the term active tile.
Some controls are dependent on the current mode.

In the following list, *left-click* means to press the left mouse button and to release it again
immediately, without moving the mouse between pressing and releasing. *left-drag* on the other hand
means to press the left mouse button and to move the mouse while holding it pressed.
The drag ends when the mouse button is released. The same terminology is used for the right mouse
button and the middle mouse button (Note that most mice have a scroll wheel that functions as a middle mouse
button when pressed). term{Ctrl-left-click} means to press and hold the Ctrl key while performing
a left-click. The same applies to other Ctrl-actions and Shift- actions. In every case the Ctrl
key or Shift key must be pressed before initiating the action.

Note, that on systems with only one mouse button, this button is regarded as the left button.
The right button can be simulated by holding the Alt key while pressing the (left) mouse button.

* right-click or left-click (no active tile), spacebar (no active tile)
  Creates a new tile under the mouse pointer that will become the active tile. What tile is
  created depends on the mouse pointer's location. If there is no domino block at the click
  location, the new tile will be of the same type as the previously active tile. If the click
  is over a blue block, a copy of this blue block becomes the active tile. If any other block
  is clicked, a custom tile will be created.
* right-drag (no active tile)
  Removes domino tiles from the board. The tiles removed will be the tile under the mouse pointer
  at the initial button press and all tiles connected to green blocks of this tile, to green
  blocks of tiles connected to green tiles of this block and so on. The tiles that are removed
  will be compressed into a custom tile that will become the new active tile.
* right-click (with active tile)
  Switches to the next available active tile. The click location does not matter.
* 0,1,2,3,4,... (with or without active tile)
  Activate a certain tile directly.
* right-drag (with active tile)
  Rotate the active tile.
  To perform the rotation, press the right mouse button anywhere, hold it pressed and move the
  mouse pointer at least 1cm away from the location of the initial button press. You will notice
  that the active tile does not follow the mouse cursor. Now move the mouse pointer in a circle
  around the point of the initial button press (i.e. around the location where the active tile is
  fixed). Press the right mouse button and move the mouse as if drawing a circle around the active
  tile.
* left-click (with active tile), spacebar (with active tile)
  Places the active tile on the board at the current mouse pointer location, if the location is
  permissible. If the location is not allowed, the active tile will disappear and you will be back
  in normal mode without an active tile.
  The active tile may only be placed on the board if the following conditions are met:
  *  One and only one block of the active tile must be horizontally or vertically adjacent to a block of another tile.
  *  The adjacent blocks must show the same pattern.
  *  One of the adjacent blocks must be green and the other must be either red or blue.
* Ctrl-left-click or Shift-left-click (with active tile), Ctrl-spacebar or Shift-spacebar (with active tile)
  Like plain left-click with the difference that the location is also permissible if no block is
  adjacent to the active tile. This function can be used to try out some tile connections without
  messing with the main part of the domino.
* middle-click (with active tile), W,E or R (US keyboard layout)
  Rotates the active tile 90 degrees clockwise.
* Q (US keyboard layout)
  Rotates the active tile 90 degrees counter-clockwise.
* left-drag, arrow keys
  Scrolls the board.
* Ctrl-left-drag
  Zoom in.
  The point of the game board that was under the mouse pointer during the initial button press
  will remain under the mouse pointer while everything is enlarged. This allows you to specify
  the point you want to zoom in on.
* Shift-left-drag
  Zoom out.
  The mouse pointer location does not matter.

**********
Game Menus
**********

File Menu
=========

Show Proof
----------

Solving a level corresponds to proving a tautology. This option opens a window that displays the
natural deduction proof corresponding to your arrangement of domino tiles. If you have no idea
what all this means, just ignore this function.

Quit
----

Exits the program.

Level Menu
==========

About
-----

Pops up a window with a description of the current level.

Restart
-------

Restarts the level from the beginning.

Advance
-------

Advances to the next level.

Select New
----------

Prompts you for the level you want to play and then starts this level. You may enter either a
level number or a tautology (actually any formula is allowed but you won't be able to solve the
level if it's not a tautology). If you don't know  what a tautology is, just enter a level number.

Auto-Simplify
-------------

When selected, the game will automatically apply as many simplifiers to the start tile as possible, when the level is started.

Tile Menu
=========

The tile menu contains checkboxes to enable and disable the freely available tiles (except for the
Boring Elongator).


Add Custom Tile
---------------

Adds the current custom tile (if any) to the list of freely available tiles.

Load Tiles
----------

Loads a set of tiles from a file.

Save Tiles
----------

Saves the current set of tiles to a file.

Remove Unselected
-----------------

Removes all tiles from the list whose checkboxes are not checked.

Restore Default
---------------

Restores the default list of freely available tiles.

Help Menu
=========

Short Reference
---------------

Opens the help window with a shortened version of the manual.

Walkthrough
-----------

Opens the help window with a walkthrough that presents solutions for some levels and uses these
to explain important tips and tricks. It is recommended that you read this document.

Complete Manual
---------------

Opens the help window with the manual of this game.

About
-----

Pops up a window with information about this game.

###########
Walkthrough
###########

************
Introduction
************

This text will walk you through some of the levels of the Domino game. It explains how to approach
a level and provides useful tips and tricks. It is recommended that you read this whole
walkthrough before playing the game (unless of course you like the challenge of discovering
the tricks yourself). Note, that if a certain level is used in this walkthrough to present a
tip, this does not mean the tip can not be useful in earlier levels. The controls are not
explained here, so if you don't know them already you should at least read the Short Reference
(accessible from the Help menu).

Level 1
=======

.. image:: /images/level1.gif

This is the easiest possible level. Remember that to solve a level, all blocks with green sides
(called "green blocks", regardless of the pattern on top) must be connected to another block.
Two kinds of blocks can be connected to a green block. The freely available tiles have a red
block that can be connected to a green block, but they always have at least one new green block,
which means that adding a freely available tile can never finish a level. In order to finish a
level you have to attach blue bonus blocks to the green blocks. In Level 1 this is very easy
because the start tile already provides a blue bonus block that has the same pattern (a solid
cyan rectangle) on top as the only green block. To solve the level, just attach this blue block
to the green block.

.. figure:: /images/level1_solved.gif

   Attaching the blue block to the green block solves the level.

Level 2
=======

.. image:: /images/level2.gif

This level is more difficult than the first. The start tile provides two blue bonus blocks but
neither has the same pattern as the green block. This means that you can't finish this level
right away. You need to use freely available tiles first. If you look at the two bonus blocks
you will notice that one of them has a pattern that is split horizontally in two rectangles.
The bottom rectangle has the same pattern (a solid cyan rectangle) as the green block.
This is an important clue to look out for. Whenever an available bonus block has a lower half
that has the same pattern as a green block, you can use a Complexifier tile as shown in the
following picture. Notice that only a pattern in the lower half of a bonus block provides this
clue. If a pattern in the upper half of a bonus block corresponds to the pattern on a green block,
this does not help at all.

.. figure:: /images/level2_complexifier.gif

   Attaching a Complexifier creates a green block that the split bonus block can be attached to.

After attaching a Complexifier it looks like no progress has been made at first. Instead of one
green block, you now have two that you need to connect. However, one of these green blocks can be
finished right away. The most important property of the Complexifier is that it takes the pattern
on the green block it is attached to (here: the solid cyan rectangle) and puts it in the lower
half of one of the new green blocks. This is why you have to look out for bonus blocks whose
lower half corresponds to a green block.

The next step is straightforward. You attach the bonus block whose lower half contains the cyan
rectangle to the green block of the Complexifier whose lower half contains the cyan rectangle.
This works because the upper half of the Complexifier's green block is gray, meaning it is a
wildcard rectangle that will match anything.

.. figure:: /images/level2_bonus.gif

   After using the Complexifier, one of the bonus blocks can be used.

Notice how attaching the bonus block has not only changed the wildcard rectangle in the pattern of
the first green block, but also in the second block's pattern. Whenever a gray wildcard rectangle
is matched, all rectangles with the same shade of gray will assume the same pattern. Finishing the
level is easy now. The pattern on the only unconnected green block remaining (a dark blue rectangle)
is the same as that on the as yet unused bonus block, so it can be attached to the green block.

.. figure:: /images/level2_solved.gif

   The other bonus block finishes the level.

Level 3
=======

.. image:: /images/level3.gif

This level introduces the color red. It is special because it is the only non-wildcard color that
occurs in a freely available tile, the Redness Inducer. Whenever you encounter a pattern with red
in it, this is a hint that you may need the Redness Inducer. An interesting property of the
Redness Inducer is that it can be attached to any pattern and its green block is independent of
the pattern it is attached to.

.. figure:: /images/level3_red.gif

   The color red is a hint that you should use a Redness Inducer here.

You could easily finish the level now by attaching the bonus block with the solid red rectangle
pattern to the green block of the Redness Inducer. However, before you do this you should learn
about another interesting property of the Redness Inducer. Not only does it attach to anything,
it can be used to just give you a bonus block without changing the patterns you have to finish.
To achieve this, you first attach a Complexifier to the Redness Inducer and then immediately
attach the bonus block provided by the Redness Inducer to that branch of the Complexifier that
has the split pattern.

.. figure:: / images/level3_bonus_for_free.gif

   A Redness Inducer combined with a Complexifier can give you a bonus block without changing
   the branch it is attached to.

Look at the result of this combination. The one branch of the Complexifier is already finished
with a bonus block. The only green block that still has to be finished is the other branch of
the Complexifier. Interestingly, it has exactly the same pattern (a solid cyan rectangle) as
the block that you initially attached the Redness Inducer to. So this combination of Redness
Inducer and Complexifier has not changed the green blocks that you have to finish. But it has
given you another bonus block, the block with the cyan/red split pattern. So you have won
something without losing anything.

Because this Inducer/Complexifier combination is so powerful, you may want to create a custom
tile for it and add it to your freely available tiles and possibly even save it via the Tile menu.
See the Short Reference or Complete Manual (accessible from the Help menu) on how to create
custom tiles. See the Complete Manual for a description of the Tile menu.

.. figure:: /images/level3_custom.gif

   The Redness Inducer/Complexifier combination can be compressed into a custom tile that will
   just give you a bonus block without changing the branch you attach it to.

Now that you've learned about the properties of the Redness Inducer, either detach the unneeded
Complexifier or attach a new Redness Inducer. Then use the bonus block with the solid red pattern
to finish the level.

.. figure:: /images/level3_solved.gif

   After attaching the Redness Inducer, the level is easily solved.

Level 22
========

.. image:: /images/level22.gif

When level 2 was discussed, you were told to look out for bonus blocks whose lower half pattern
corresponds to the pattern of a green block, because this indicates that you should use a
Complexifier. As a matter of fact this is only a special case of a more general rule. Whenever
you encounter a bonus block that has in its lower right corner a pattern that corresponds to the
pattern of a green block, you can use a Complexifier cascade to make use of this bonus block. If
you look at level 22, you'll see that one of the two initially available bonus blocks has in its
lower right corner a yellow rectangle. This fits with the green block whose pattern is a yellow
rectangle. Attach a cascade of 3 complexifiers as shown in the next picture.

.. figure:: /images/level22_complexifier_cascade.gif

   Attaching 3 Complexifiers creates a green block that the bonus block with the yellow rectangle
   in the bottom right corner can be attached to.

Now attach the bonus block with the yellow corner to the split branch of the 3rd Complexifier to
finish it. The remaining branches of the 1st and 2nd Complexifier can be finished using the
unhealthy green bonus block, which leaves only one remaining green block to be finished. Note,
that this is not the case for all Complexifier cascades. Sometimes, a Complexifier cascade creates
as many new branches as it has Complexifiers.

.. figure:: /images/level22_bonus.gif

   3 branches can be finished with bonus blocks.

Because there's no obvious way to finish the remaining branch, it is a good idea to use Simplifiers on it.

.. figure:: /images/level22_simplified.gif

   After attaching two Simplifiers, the level is almost solved.

After attaching two Simplifiers, all that remains is to attach the bonus block provided by the first
Simplifier.

.. figure:: /images/level22_solved.gif

   The bonus block provided by the first Simplifier solves the level.

Level 27
========

.. image:: /images/level27.gif

This level looks simple but without the right trick you could work on it for hours without solving it.
If you remember what you learned about the Complexifier in the walkthrough of levels 2 and 22, you would
typically start by attaching a Complexifier, so that you can use the bonus square.

.. figure:: /images/level27_complexifier_bonus.gif

   Most people will begin this level with a Complexifier and the bonus block.

The next logical step is to use a Simplifier.

.. figure:: /images/level27_simplifier.gif

   The next logical step is to use a Simplifier.

Another Simplifier can not be attached, so the choice is between a Complexifier and a Redness Inducer.
The Complexifier would result in a green block with a split pattern that has magenta in its lower
half. However, the only available bonus block that has magenta in its pattern has it in the upper
half, and there exists no combination of tiles that can bring a pattern from the upper half into
the lower half. So attaching a Complexifier does not make sense here. This leaves the Redness Inducer
as the only choice.

.. figure:: /images/level27_redness.gif

   Only a Redness Inducer makes sense as the next step.

It is obvious that attaching another Redness Inducer is not helpful, so a Complexifier is the only
sensible choice now.

.. figure:: /images/level27_complexifier.gif

   Now a Complexifier is the only sensible choice.

The next obvious move would be to attach the only available bonus block with red in the lower half
of its pattern to the split green block of the Complexifier. This corresponds to the trick
described in the walkthrough for level 3. Unfortunately, it would not achieve anything. Even
worse, no matter what tiles you attach you will not be able to finish the level. This is because
one crucial bonus block is missing. You can see this if you attach the bonus block with the
solid cyan rectangle pattern to the Complexifier.

.. figure:: /images/level27_cyan.gif

   Attaching the bonus block with the cyan rectangle pattern gives a clue.

If you had a bonus block with a cyan upper half and a red lower half, you could finish the level
right away. But you don't have this block and there is no way to get such a block by attaching
more tiles. However, if you think back to the Redness Inducer/Complexifier combination trick
explained in the walkthrough for level 3, you will see that you could have gained such a block
at the very beginning of the level.

In order to insert the Redness Inducer/Complexifier combination at the beginning, detach all the
tiles you have placed. They will be transformed into a custom tile. Store this custom tile
somewhere on the board (don't forget that you have to press Ctrl to drop a tile without it being
connected to another tile), then attach the Redness Inducer Complexifier combination to the
start tile.

.. figure:: /images/level27_insert.gif

   Detach all tiles placed so far and store the custom tile on the board, then attach the Redness
   Inducer/Complexifier combination to the start tile.

Now that you have gained the desired bonus block with the cyan upper half and the red lower half,
attach the custom tile (which corresponds to the detached tiles) and the 2 remaining bonus blocks
to finish the level.

.. figure:: /images/level27_solved.gif

   Reattach the detached part and finish the level with the appropriate bonus blocks.

Further Tips
============

* Use Elongators when an area gets too crowded to place tiles.
* Detach a part of the tree and reattach the custom tile that is created to compress the part to
  take less space.
* Use custom tile creation (without detaching) to copy parts of the tree somewhere else.
* Zoom out to get a better look when the level gets large.