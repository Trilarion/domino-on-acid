.. Domino On Acid documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Domino On Acid's documentation
==============================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   content


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
