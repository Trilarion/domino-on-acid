"""
  Use Google machine translation to translate language content
"""

root_path = os.path.realpath(os.path.join(os.path.dirname(__file__), os.path.pardir))


def translate_message_resource_bundle(overwrite_existing_content=False):
    """

    :param overwrite_existing_content:
    :return:
    """
    # import message bundle


def translate_sphinx_documentation(overwrite_existing_content=False):
    """

    :param overwrite_existing_content:
    :return:
    """
    pass


def translate_markdown(overwrite_existing_content=False):
    """

    :param overwrite_existing_content:
    :return: 
    """
    pass


if __name__ == '__main__':

    # how man target languages do we want?
    # target_languages = ('de', 'es', 'it', 'fr', 'pt', 'ru', 'ja', 'zh-CN', 'pt', 'ta', 'hi', 'ko', 'vi')
    target_languages = ('de', 'es', 'ru') # the small test set


    # translate messages resource bundle
    message_ressource_bundle_folder = ''
    translate_message_resource_bundle()

    # translate sphinx documentation
    translate_sphinx_documentation()

    # translate markdown
    translate_markdown()