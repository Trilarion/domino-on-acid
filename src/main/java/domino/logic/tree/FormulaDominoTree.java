/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.tree;


import domino.logic.IllegalNodeTypeException;
import domino.logic.WantedCloneMap;
import domino.logic.fsn.FSNIterator;
import domino.logic.rules.SquareDoesNotExistException;
import domino.logic.square.BonusFormulaDominoSquare;
import domino.logic.square.DominoSquare;
import domino.logic.square.LeafFormulaDominoSquare;
import domino.logic.square.RootFormulaDominoSquare;
import domino.logic.syntaxnet.ConflictingBindException;
import domino.logic.syntaxnet.FormulaSyntaxNet;

import java.util.*;

/**
 * <p>This is a class for {@link DominoTree}s that represent proofs in propositional
 * logic.</p>
 */
public class FormulaDominoTree implements DominoTree {
    /**
     * <p>The methods of this class all assume that the {@link FormulaSyntaxNet}
     * {@link #root_} and those contained in {@link #leaves_} and {@link #bonus_}
     * don't contain forward nodes that have not been created by a call to
     * {@link #attachTo(DominoTree, int)} or {@link #useBonusSquare(int, int)}.</p>
     */
    public FormulaSyntaxNet root_;

    /**
     * <p>The methods of this class assume that leaves_.length==0 characterizes a
     * bonus square. Normal {@code FormulaDominoTree}s without leaves are not
     * possible.</p>
     */
    public FormulaSyntaxNet[] leaves_;

    /**
     * <p>NOTE: Right now every FormulaDominoTree may have several bonus squares but does
     * not allow restricting them to certain leaves only. In order to create
     * FormulaDominoTrees like one for exists-elimination where leaf-dependent bonus
     * squares are needed, the FormulaDominoTree can be assembled from 3
     * FormulaDominoTrees.</p>
     */
    public FormulaSyntaxNet[] bonus_;
    public FormulaDominoTree[] leafConnections_;
    public FormulaDominoTree rootConnection_;
    /**
     * <p>{@code bonusSquaresInAction_[i]} is a Collection of
     * {@link BonusFormulaDominoTree}s that are
     * unified with {@link #bonus_ bonus_[i]}.</p>
     */
    protected Collection[] bonusSquaresInAction_; // TODO want to generify, convert array to arraylist, but difficult because may contain different things
    /**
     * <p>{@code rootConnection_.leafConnections_[rootConnectionLeafIndex_]==this}</p>
     * <p>If {@code rootConnection_==null} then {@code rootConnectionLeafIndex_==-1}</p>
     */
    protected int rootConnectionLeafIndex_ = -1;
    private int numOfOpenLeavesCache_;
    private int numOfLeavesCache_;
    private int[] numOfOpenLeavesCacheForConnection_;
    private int[] numOfLeavesCacheForConnection_;

    /**
     * <p>Creates a new {@code FormulaDominoTree} initialized with the respective
     * {@link FormulaSyntaxNet}s.</p>
     * <p>For internal use only. None of the arrays may be {@code null} and none
     * of the elements may be {@code null}.</p>
     */
    protected FormulaDominoTree(FormulaSyntaxNet root, FormulaSyntaxNet[] leaves, FormulaSyntaxNet[] bonus) {
        init(root, leaves, bonus);
    }

    /**
     * <p>Creates an uninitialized {@code FormulaDominoTree}.
     * For internal use only.</p>
     */
    protected FormulaDominoTree() {
    }

    /**
     * <p>Returns a copy of the {@link FormulaSyntaxNet} rooted at {@code n}
     * treating forward connections as non-existent.</p>
     * <p>When a
     * forward node is encountered it is copied as an unbound leaf and its forward
     * connections are not followed. Because of this
     * the returned {@link FormulaSyntaxNet} is always a proper tree with only branch
     * and leaf nodes.</p>
     *
     * @param copyowner the {@link FormulaSyntaxNet#owner() owner} of the returned
     *                  {@link FormulaSyntaxNet}.
     * @param net       the node to start copying at.
     * @param m         If you pass a {@link Map} that is empty, then
     *                  after this function has finished
     *                  {@code m} will map {@link FormulaSyntaxNet} nodes in the original net
     *                  to their corresponding nodes in the copy.
     *                  By passing the same {@link Map} (without clearing it) to later calls of
     *                  this function you can carry over the mapping which is useful when
     *                  copying several {@link FormulaSyntaxNet}s that share nodes.
     */
    protected static FormulaSyntaxNet noForwardCrossingCopy(Object copyowner, FormulaSyntaxNet net, Map<FormulaSyntaxNet, FormulaSyntaxNet> m) {
        FormulaSyntaxNet node = m.get(net);
        if (node == null) {
            if (net.isBound()) {
                node = FormulaSyntaxNet.newLeaf(copyowner, net.id());
            } else if (net.isUnboundOrForward()) {
                node = FormulaSyntaxNet.newLeaf(copyowner, null);
            } else if (net.isBranch()) {
                node = FormulaSyntaxNet.newBranch(copyowner,
                        noForwardCrossingCopy(copyowner, net.left(), m),
                        noForwardCrossingCopy(copyowner, net.right(), m)
                );
            }

            m.put(net, node);
        }
        return node;
    }

    /**
     * <p>Creates and returns a new {@code FormulaDominoTree} based on
     * infix formula {@link String}s.</p>
     * <p>A formula may contain "-&gt;", bound and unbound propositional variables.
     * Unbound propositional variables have the form X&lt;num&gt;, where &lt;num&gt;
     * is any non-negative integer. Bound propositional variables can be anything
     * (that doesn't include "(", ")" or "-&gt;").</p>
     * <p>No associativity is assumed for -&gt;, so A-&gt;B-&gt;C must be
     * written as either (A-&gt;B)-&gt;C or A-&gt;(B-&gt;C).</p>
     * <p>Note that there are no predefined symbols for false and true. Use any
     * bound propositional variable(s) you like.</p>
     * <p>If the same identifier for a propositional variable is used in more than
     * one place (in the same formula or in different formulas), it will represent the
     * same variable. In the case of an unbound
     * propositional variable, any binding of one occurrence will automatically bind
     * all other occurrences. This works across all formulas passed in a single
     * invocation of this method but not across different invocations.<br>
     * Example (pseudo-code):
     * <pre>
     *    a=newFromInfix("X0",{"X0"},{});
     *    b=newFromInfix("X0",{"X0"},{});
     * </pre>
     * The 2 {@link DominoSquare}s of tree {@code a} will always be identical.
     * If one is
     * bound, the other one will be bound with it. The same is true for tree
     * {@code b}. However, there is no connection between tree {@code a} and
     * tree {@code b}. Binding a square in one of the 2 trees will not
     * automatically bind a square in the other tree.
     *
     * @param root   the formula for the root {@link DominoSquare}.
     * @param leaves array of {@link String}s that may not be null or empty
     *               and must not contain
     *               {@code null}
     *               elements. Every element will add a corresponding
     *               {@link DominoSquare} to the result tree as a leaf.
     * @param bonus  array of {@link String}s that may be empty or {@code null}
     *               but must not contain
     *               {@code null} elements.
     *               Every element will add a corresponding bonus square
     *               that may be {@link DominoTree#useBonusSquare(int, int) used}
     *               in subtrees attached to any leaves of the result tree.
     * @throws IllegalArgumentException if {@code leaves} is empty or
     *                                  {@code null} or
     *                                  an element of any of the arrays is {@code null}
     *                                  or there is a syntax error in any of the strings.
     */
    public static FormulaDominoTree newFromInfix(String root, String[] leaves, String[] bonus) {
        //having no leaves characterizes a bonus square and this test is used
        //in several places, so we can not allow it for regular FormulaDominoTrees
        if (leaves == null || leaves.length == 0) throw new IllegalArgumentException();

        int blen = 0;
        if (bonus != null) blen = bonus.length;
        String[] formulas = new String[1 + leaves.length + blen];
        formulas[0] = root;
        System.arraycopy(leaves, 0, formulas, 1, leaves.length);
        if (blen > 0) System.arraycopy(bonus, 0, formulas, 1 + leaves.length, blen);

        FormulaDominoTree tree = new FormulaDominoTree();

        FormulaSyntaxNet[] fsns = FormulaSyntaxNet.newFromInfixFormulaStrings(tree, formulas);
        FormulaSyntaxNet rootFSN = fsns[0];
        FormulaSyntaxNet[] leavesFSN = new FormulaSyntaxNet[leaves.length];
        System.arraycopy(fsns, 1, leavesFSN, 0, leaves.length);
        FormulaSyntaxNet[] bonusFSN = new FormulaSyntaxNet[blen];
        if (blen > 0) System.arraycopy(fsns, 1 + leavesFSN.length, bonusFSN, 0, blen);

        tree.init(rootFSN, leavesFSN, bonusFSN);

        return tree;
    }

    /**
     * <p>Unifies all {@link FormulaSyntaxNet}s in {@code unbound} with new
     * leaf nodes with ids from
     * {@code ids}. The order in which they are unified is unspecified.</p>
     * <p>All elements of {@code unbound} must be unbound or forward nodes.
     * The owner of every new bound node will be the same as that of
     * the unbound node it is unified with.</p>
     *
     * @throws IllegalArgumentException if {@code ids==null} or
     *                                  {@code ids.length<unbound.size()}. If
     *                                  this exception is thrown some variables may already be bound and will
     *                                  remain so.
     */
    protected static void bindUnbound(Collection<FormulaSyntaxNet> unbound, String[] ids) {
        if (ids == null || ids.length < unbound.size()) throw new IllegalArgumentException();

        int i = 0;
        for (FormulaSyntaxNet unode : unbound) {
            FormulaSyntaxNet.unify(unode, FormulaSyntaxNet.newLeaf(unode.owner(), ids[i]));
            i++;
        }
    }

    /**
     * <p>Returns the parent tree of {@code this} tree, if there is one, or
     * {@code null} if {@code this} tree does not have a parent.</p>
     *
     * @see #parentSubtreeIndex()
     */
    public DominoTree parent() {
        return rootConnection_;
    }

    /**
     * <p>If {@code this} tree has a {@link #parent()},
     * {@code parent().subtree(parentSubtreeIndex())==this}, otherwise -1 is
     * returned.</p>
     */
    public int parentSubtreeIndex() {
        return rootConnectionLeafIndex_;
    }

    /**
     * <p>Returns the number of open leaves connected with index {@code i}.</p>
     * <p>If {@code leafConnections_[i]==null}, this function returns 1.</p>
     * <p>If {@code leafConnections_[i]!=null}, this function returns
     * {@code leafConnections_[i].numberOfOpenLeaves()} .</p>
     *
     * <p>Constraints for {@code i}:
     * {@code 0<=i<leaves_.length}.</p>
     */
    protected int numOfOpenLeavesForConnection(int i) //NOT cache-clean (meaning this function must not be called when caches are in an inconsistent state), TESTED
    {
        if (numOfOpenLeavesCacheForConnection_[i] < 0) {
            if (leafConnections_[i] != null) {
                numOfOpenLeavesCacheForConnection_[i] = leafConnections_[i].numberOfOpenLeaves();
            } else //if (leafConnections_[i]==null)
            {
                numOfOpenLeavesCacheForConnection_[i] = 1;
            }
        }

        return numOfOpenLeavesCacheForConnection_[i];
    }

    /**
     * <p>Returns the number of leaves connected with index {@code i}.</p>
     * <p>If {@code leafConnections_[i]==null}, this function returns 1.</p>
     * <p>If {@code leafConnections_[i]!=null}, this function returns
     * {@code max{leafConnections_[i].numberOfLeaves(),1}} .</p>
     *
     * <p>Constraints for {@code i}:
     * {@code 0<=i<leaves_.length}.</p>
     */
    protected int numOfLeavesForConnection(int i) //NOT cache-clean, TESTED
    {
        if (numOfLeavesCacheForConnection_[i] < 0) {
            if (leafConnections_[i] != null) {
                int j = leafConnections_[i].numberOfLeaves();
                if (j == 0) j = 1;
                numOfLeavesCacheForConnection_[i] = j;
            } else //if (leafConnections_[i]==null)
            {
                numOfLeavesCacheForConnection_[i] = 1;
            }
        }

        return numOfLeavesCacheForConnection_[i];
    }

    /**
     * <p>Returns the {@link LeafMapping#index index} in {@code leafConnections_}
     * to find the subtree
     * that knows {@code leaf} under index {@link LeafMapping#subindex subindex}.</p>
     * <p>If no subtree is attached at {@code index} or if the attached subtree
     * has no leaves (i.e. it is a bonus square), {@code subindex}
     * is set to -1.</p>
     *
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     */
    protected LeafMapping mapLeaf(int leaf) //NOT cache-clean, TESTED
    {
        if (leaf >= 0) {
            for (int i = 0; i < leafConnections_.length; ++i) {
                int n = numOfLeavesForConnection(i);
                if (leaf < n) {
                    if (leafConnections_[i] == null || leafConnections_[i].leaves_.length == 0) leaf = -1;
                    return new LeafMapping(i, leaf);
                }
                leaf -= n;
            }
        }
        throw new SquareDoesNotExistException();
    }

    /**
     * <p>Returns the {@link LeafLocation#tree tree} that has
     * {@code leaf} under index {@link LeafLocation#index index}.</p>
     * <p>If the returned {@link LeafLocation#index} is -1, this means that
     * {@link LeafLocation#tree}
     * is already the desired leaf (i.e. the leaf is a bonus square).
     * Otherwise {@code tree.leafConnections_[index]==null}.</p>
     *
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     */
    protected LeafLocation locateLeaf(int leaf) //NOT cache-clean, TESTED
    {
        FormulaDominoTree tree = this;
        LeafMapping l = tree.mapLeaf(leaf);
        while (l.subindex >= 0) {
            tree = tree.leafConnections_[l.index];
            leaf = l.subindex;
            l = tree.mapLeaf(leaf);
        }

        if (tree.leafConnections_[l.index] != null) {
            tree = tree.leafConnections_[l.index];
            l.index = -1;
        }

        return new LeafLocation(tree, l.index);
    }

    /**
     * <p>Returns {@code true} if {@code leafConnections_[i]==null}.</p>
     *
     * <p>Constraints for {@code i}:
     * {@code 0<=i<leaves_.length}.</p>
     */
    protected boolean isOpenConnection(int i) //cache-clean
    {
        return (leafConnections_[i] == null);
    }

    /**
     * <p>Returns {@code true} if {@code leaf} is open.</p>
     *
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<numberOfLeaves()}.</p>
     *
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     */
    public boolean isOpenLeaf(int leaf) //NOT cache-clean, TESTED
    {
        LeafLocation l = locateLeaf(leaf);

        return l.index >= 0;
    }

    /**
     * <p>Returns the index of the {@code n}-th open leaf.</p>
     *
     * <p>Constraints for {@code n}:
     * {@code 0<=n<numberOfOpenLeaves()}.</p>
     *
     * @throws SquareDoesNotExistException if {@code n} is not valid.
     */
    public int indexOfOpenLeaf(int n) //NOT cache-clean, TESTED
    {
        if (n >= 0) {
            int index = 0;
            for (int i = 0; i < leafConnections_.length; ++i) {
                int num = numOfOpenLeavesForConnection(i);
                if (n < num) {
                    if (leafConnections_[i] == null)//case leafConnections_[i].leaves_.length==0 does not have to be tested because in this case num would be 0 so that n<num would be false
                        return index;
                    else return index + leafConnections_[i].indexOfOpenLeaf(n);
                }
                n -= num;
                index += numOfLeavesForConnection(i);
            }
        }
        throw new SquareDoesNotExistException();
    }

    /**
     * <p>Returns the index of the first leaf found in subtree {@code i}.</p>
     *
     * <p>Constraints for {@code i}:
     * {@code 0<=i<numberOfSubtreeConnections()}.</p>
     *
     * @throws SquareDoesNotExistException if {@code i} is not a valid subtree
     *                                     connection index.
     */
    public int leafIndexForSubtreeConnection(int i) //NOT cache-clean
    {
        if (i < 0 || i >= numberOfSubtreeConnections()) throw new SquareDoesNotExistException();

        int idx = 0;
        for (int j = 0; j < i; ++j) idx += numOfLeavesForConnection(j);
        return idx;
    }

    /**
     * <p>Removes all forward nodes and forward connections from all
     * {@link FormulaSyntaxNet}s in {@code this} and all of its subtrees
     * (but not from parents and siblings unless their
     * {@link FormulaSyntaxNet}s are unified with those of {@code this}).</p>
     *
     * @see #recreateAllForwardsDownwards()
     */
    protected void removeAllForwardsDownwards() {
        FormulaSyntaxNet.removeAllForwards(root_);
        for (FormulaSyntaxNet syntaxNet : leaves_) {
            FormulaSyntaxNet.removeAllForwards(syntaxNet);
        }

        for (FormulaSyntaxNet formulaSyntaxNet : bonus_) {
            FormulaSyntaxNet.removeAllForwards(formulaSyntaxNet);
        }

        for (FormulaDominoTree formulaDominoTree : leafConnections_) {
            if (formulaDominoTree != null) formulaDominoTree.removeAllForwardsDownwards();
        }
    }

    /**
     * <p>Recursively reunifies the appropriate {@link FormulaSyntaxNet}s
     * of {@code this} and
     * of its subtrees (but not those of parents and siblings).</p>
     * <p>Old forward connections are <b>not</b> removed.</p>
     *
     * @see #removeAllForwardsDownwards()
     */
    protected void recreateAllForwardsDownwards() {
        for (int i = 0; i < leaves_.length; ++i) {
            if (leafConnections_[i] != null) FormulaSyntaxNet.unify(leaves_[i], leafConnections_[i].root_);
        }

        for (int i = 0; i < bonus_.length; ++i) {
            for (FormulaDominoTree bonusInstance : (Collection<FormulaDominoTree>) bonusSquaresInAction_[i]) {
                FormulaSyntaxNet.unify(bonus_[i], bonusInstance.root_);
            }
        }

        for (FormulaDominoTree formulaDominoTree : leafConnections_) {
            if (formulaDominoTree != null) formulaDominoTree.recreateAllForwardsDownwards();
        }
    }

    /**
     * <p>Removes all forward connections and then recursively reunifies the whole
     * tree.</p>
     * <p>This function need not be called on the top-level root. It will automatically
     * ascend as far as possible before beginning operation.</p>
     */
    protected void reunifyWholeTree() {
        FormulaDominoTree tree = this;
        while (tree.rootConnection_ != null) tree = tree.rootConnection_;
        tree.removeAllForwardsDownwards();
        tree.recreateAllForwardsDownwards();
    }

    /**
     * <p>Finds and detaches all bonus squares that originate in a
     * {@code FormulaDominoTree} contained in {@code parents}.</p>
     * <p>This function does not deunify or reunify the tree or
     * invalidate any caches.
     * This means that after calling this function the {@code FormulaDominoTree}
     * is likely to be in an inconsistent state.</p>
     */
    protected void detachAllBonusSquaresOriginatingIn(Collection<FormulaDominoTree> parents) {
    /*
      This function is overridden in BonusFormulaDominoTree to do the actual
      detaching. Non-bonus squares just pass the command on to their children.
    */
        for (FormulaDominoTree formulaDominoTree : leafConnections_)
            if (formulaDominoTree != null) formulaDominoTree.detachAllBonusSquaresOriginatingIn(parents);
    }

    /**
     * <p>If the {@code DominoTree} is a subtree of a larger tree, it is
     * detached from the parent tree. If the tree is not a subtree, nothing happens.</p>
     * <p>All bonus squares in the subtree to be detached, that originate from
     * {@code DominoTree}s not part of the detached subtree will be detached
     * from the subtree and cannot be reattached.</p>
     *
     * @return {@code true} if subtree was detached; {@code false} if tree
     * had no parent tree to begin with.
     * @see #attachTo(DominoTree, int)
     */
    public boolean detachFromParent() {
        if (rootConnection_ == null) return false;

        //collect all parent nodes
        Collection<FormulaDominoTree> parents = new HashSet<>();
        FormulaDominoTree parent = rootConnection_;
        FormulaDominoTree masterRoot = parent;
        while (parent != null) {
            masterRoot = parent;
            parents.add(parent);
            parent = parent.rootConnection_;
        }

        //now find and detach all bonus squares whose originating DominoTree is in
        //the collection of parent nodes
        detachAllBonusSquaresOriginatingIn(parents);

        FormulaDominoTree oldRootConnection = rootConnection_;
        rootConnection_ = null;
        oldRootConnection.leafConnections_[rootConnectionLeafIndex_] = null;
        rootConnectionLeafIndex_ = -1;

        //it would be great if we could just use FormulaSyntaxNet.extractByOwner() to
        //detach the tree. However (see doc for extractByOwner()) there are some
        //unifications that might remain. So we have no choice but to reunify the whole
        //tree.
        masterRoot.reunifyWholeTree(); //reunify parent part
        reunifyWholeTree();            //reunify detached part

        //because of the call to detachAllBonusSquaresOriginatingIn(parents) above
        //we have no choice but to invalidate all caches related to open leaves in
        //the whole tree.
        masterRoot.invalidateOpenLeavesCachesWholeTreeDownwards(); //invalidate in parent part
        invalidateOpenLeavesCachesWholeTreeDownwards();   //invalidate in detached part

        //because leaf counts are wrong in parents of the detached tree we have to
        //invalidate related caches.
        oldRootConnection.invalidateLeavesCachesUpwards();

        return true;
    }

    /**
     * <p>Invalidates leaf count caches (but not open leaves counting caches)
     * of {@code this} and all parents.</p>
     */
    protected void invalidateLeavesCachesUpwards() {
        FormulaDominoTree tree = this;
        do {
            tree.numOfLeavesCache_ = -1;
            Arrays.fill(tree.numOfLeavesCacheForConnection_, -1);
            tree = tree.rootConnection_;
        }
        while (tree != null);
    }

    /**
     * <p>Invalidates open leaves counting caches
     * of {@code this} and all parents.</p>
     */
    protected void invalidateOpenLeavesCachesUpwards() {
        FormulaDominoTree tree = this;
        do {
            tree.numOfOpenLeavesCache_ = -1;
            Arrays.fill(tree.numOfOpenLeavesCacheForConnection_, -1);
            tree = tree.rootConnection_;
        }
        while (tree != null);
    }

    /**
     * <p>Invalidates leaf count caches (but not open leaves counting caches)
     * in {@code this} and all of
     * its subtrees (but not in parents
     * and siblings of {@code this}).</p>
     */
    protected void invalidateLeavesCachesWholeTreeDownwards() {
        numOfLeavesCache_ = -1;
        Arrays.fill(numOfLeavesCacheForConnection_, -1);

        for (FormulaDominoTree formulaDominoTree : leafConnections_) {
            if (formulaDominoTree != null) formulaDominoTree.invalidateLeavesCachesWholeTreeDownwards();
        }
    }

    /**
     * <p>Invalidates caches related to open leaves in {@code this} and all of
     * its subtrees (but not in parents
     * and siblings of {@code this}).</p>
     */
    protected void invalidateOpenLeavesCachesWholeTreeDownwards() {
        numOfOpenLeavesCache_ = -1;
        Arrays.fill(numOfOpenLeavesCacheForConnection_, -1);

        for (FormulaDominoTree formulaDominoTree : leafConnections_) {
            if (formulaDominoTree != null) formulaDominoTree.invalidateOpenLeavesCachesWholeTreeDownwards();
        }
    }

    /**
     * <p>Invalidates caches related to open leaves in the whole tree (including parents
     * and siblings of {@code this}).</p>
     */
    protected void invalidateOpenLeavesCachesWholeTree() {
        FormulaDominoTree tree = this;
        while (tree.rootConnection_ != null) tree = tree.rootConnection_;
        tree.invalidateOpenLeavesCachesWholeTreeDownwards();
    }

    /**
     * <p>Invalidates all caches in the whole tree (including parents
     * and siblings of {@code this}).</p>
     */
    protected void invalidateAllCachesWholeTree() {
        FormulaDominoTree tree = this;
        while (tree.rootConnection_ != null) tree = tree.rootConnection_;

        //not very efficient, the following could be done in one merged function but
        //that would add maintenance overhead due to code duplication
        tree.invalidateOpenLeavesCachesWholeTreeDownwards();
        tree.invalidateLeavesCachesWholeTreeDownwards();
    }

    /**
     * <p>Connects the root of {@code this} to {@code leaf}
     * of {@code DominoTree} {@code to}.</p>
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<to.numberOfLeaves() && to.isOpenLeaf(leaf)}.</p>
     *
     * @throws IllegalArgumentException    if {@code to} is a subtree of
     *                                     {@code this}.
     *                                     The trees remain unchanged.
     * @throws IllegalNodeTypeException    if {@code this} is already connected, or is
     *                                     a bonus square that has been detached or if the leaf to attach to is not
     *                                     open.
     *                                     The trees remain unchanged.
     * @throws SquareDoesNotExistException if {@code to==null} or
     *                                     {@code leaf} is not a valid leaf
     *                                     number. The trees remain unchanged.
     * @throws ConflictingBindException    if the root square does not match the leaf to
     *                                     connect with. The trees remain unchanged.
     * @throws ClassCastException          if {@code this} and {@code to}
     *                                     have incompatible types. The trees remain unchanged.
     * @see #attachToSubtreeConnection(DominoTree, int)
     * @see #detachFromParent()
     * @see #numberOfLeaves()
     * @see #isOpenLeaf(int)
     */
    public void attachTo(DominoTree to, int leaf) //NOT cache-clean
    {
        if (to == null) throw new SquareDoesNotExistException();

        FormulaDominoTree fto = (FormulaDominoTree) to;
        LeafLocation l = fto.locateLeaf(leaf);

        if (l.index < 0) throw new IllegalNodeTypeException();

        attachToSubtreeConnection(l.tree, l.index);
    }

    /**
     * <p>Like {@link #attachTo(DominoTree, int)} with the difference that instead of a leaf
     * index, a subtree connection index is used.</p>
     * <p>Constraints for {@code i}:
     * {@code 0<=i<to.numberOfSubtreeConnections() &&
     * to.isOpenLeaf(leafIndexForSubtreeConnection(i))}.</p>
     *
     * @throws IllegalArgumentException    if {@code to} is a subtree of
     *                                     {@code this}.
     *                                     The trees remain unchanged.
     * @throws IllegalNodeTypeException    if {@code this} is already connected, or is
     *                                     a bonus square that has been detached or if the leaf to attach to is not
     *                                     open.
     *                                     The trees remain unchanged.
     * @throws SquareDoesNotExistException if {@code to==null} or
     *                                     {@code i} is not a valid subtree connection index.
     *                                     The trees remain unchanged.
     * @throws ConflictingBindException    if the root square does not match the leaf to
     *                                     connect with. The trees remain unchanged.
     * @throws ClassCastException          if {@code this} and {@code to}
     *                                     have incompatible types. The trees remain unchanged.
     * @see #attachTo(DominoTree, int)
     * @see #detachFromParent()
     * @see #numberOfLeaves()
     * @see #isOpenLeaf(int)
     */
    public void attachToSubtreeConnection(DominoTree to, int i) //NOT cache-clean
    {
        if (to == null || i < 0 || i >= to.numberOfSubtreeConnections()) throw new SquareDoesNotExistException();

        FormulaDominoTree tree = (FormulaDominoTree) to;

        FormulaDominoTree masterTop = tree;
        while (masterTop.rootConnection_ != null) masterTop = masterTop.rootConnection_;
        if (masterTop == this) throw new IllegalArgumentException();

        if (rootConnection_ != null || !tree.isOpenConnection(i)) throw new IllegalNodeTypeException();

        //ATTENTION! The following may throw and we promise that the trees are unchanged
        //if an exception is thrown. So before this line there must not be any modifying
        //instructions.
        FormulaSyntaxNet.unify(root_, tree.leaves_[i]);

        //because attaching a new tree messes up the leaves count we have to invalidate
        //all related caches
        tree.invalidateLeavesCachesUpwards();
        tree.invalidateOpenLeavesCachesUpwards();

        tree.leafConnections_[i] = this;
        rootConnection_ = tree;
        rootConnectionLeafIndex_ = i;
    }

    /**
     * <p>Returns the number of subtrees that may be connected to {@code this}.</p>
     * <p>Every {@code DominoTree} consists of a root tree with connections
     * for subtrees. This function returns the number of connections regardless of
     * whether actual subtrees are connected to them or not.</p>
     *
     * @see #subtree(int)
     */
    public int numberOfSubtreeConnections() {
        return leafConnections_.length;
    }

    /**
     * <p>If a subtree is connected to subtree connection {@code i}, then that
     * subtree is returned. Otherwise this function returns {@code null}.</p>
     *
     * <p>Constraints for {@code i}:
     * {@code 0<=i<to.numberOfSubtreeConnections()}.</p>
     *
     * @throws IllegalArgumentException if {@code i} has an illegal value.
     * @see #numberOfSubtreeConnections()
     */
    public DominoTree subtree(int i) {
        if (i < 0 || i >= leafConnections_.length) throw new IllegalArgumentException();
        return leafConnections_[i];
    }

    /**
     * <p>Returns the number of leaves this tree has.</p>
     */
    public int numberOfLeaves() //NOT cache-clean, TESTED
    {
        if (numOfLeavesCache_ < 0) {
            numOfLeavesCache_ = 0;
            for (int i = 0; i < leafConnections_.length; ++i)
                numOfLeavesCache_ += numOfLeavesForConnection(i);
        }

        return numOfLeavesCache_;
    }

    /**
     * <p>Returns the number of leaves this tree has that are not bonus squares.</p>
     */
    public int numberOfOpenLeaves() //NOT cache-clean, TESTED
    {
        if (numOfOpenLeavesCache_ < 0) {
            numOfOpenLeavesCache_ = 0;
            for (int i = 0; i < leafConnections_.length; ++i)
                numOfOpenLeavesCache_ += numOfOpenLeavesForConnection(i);
        }

        return numOfOpenLeavesCache_;
    }

    /**
     * <p>Returns the number of new bonus {@link DominoSquare}s the player may use in a
     * subtree attached to subtree connection {@code i}. This does not count bonus
     * squares coming from parent nodes of {@code this}.
     * <p>A {@code FormulaDominoTree} may provide the same bonus square multiple
     * times and the same bonus square may be provided by {@code this} and/or
     * several parents at the same time, so the count returned by this function may
     * include squares that are also provided by parent trees.</p>
     *
     * <p>Constraints for {@code i}:
     * {@code 0<=i<numberOfSubtreeConnections()}.</p>
     *
     * @throws IllegalArgumentException if {@code i} is not a valid number.
     * @see #numberOfSubtreeConnections()
     * @see #subtree(int)
     */
    public int numberOfNewBonusSquaresForSubtreeConnection(int i) {
        return bonus_.length;
    }

    /**
     * <p>Returns the number of bonus {@link DominoSquare}s the player may use in a
     * subtree attached to {@code leaf}. This counts bonus squares coming from
     * parent nodes of {@code this} as well as new bonus squares not available in
     * parent nodes.</p>
     * <p>A {@code FormulaDominoTree} may provide the same bonus square multiple
     * times and the same bonus square may be provided by {@code this} and/or
     * several parents at the same time. These duplicates will be included in the count
     * returned by this function!</p>
     * <p>This method may be called even on non-open leaves although bonus squares
     * cannot be used on them. In this case the return value is the number of bonus
     * squares that could be used if the leaf were open.</p>
     *
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<numberOfLeaves()}.</p>
     *
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     */
    public int numberOfBonusSquares(int leaf) //NOT cache-clean, TESTED
    {
        LeafLocation l = locateLeaf(leaf);
        FormulaDominoTree tree = l.tree;
        int n = tree.bonus_.length;
        while (tree.rootConnection_ != null) {
            tree = tree.rootConnection_;
            n += tree.bonus_.length;
        }
        return n;
    }

    /**
     * <p>Attaches to {@code leaf} the {@code num}-th bonus square that
     * may be used in subtrees attached to {@code leaf}.</p>
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<numberOfLeaves()}.</p>
     * <p>Constraints for {@code num}:
     * {@code 0<=num<numberOfBonusSquares(leaf)}.</p>
     *
     * @return the {@link DominoTree} that was attached.
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     * @throws ConflictingBindException    if the bonus square and the leaf are connected
     *                                     in a way that would cause recursion or are
     *                                     otherwise incompatible.
     * @throws IllegalNodeTypeException    {@code leaf} is not open.
     * @throws IllegalArgumentException    if {@code num} has an illegal value.
     * @see #rootSquare()
     * @see #leafSquare(int)
     * @see #bonusSquare(int, int)
     */
    public DominoTree useBonusSquare(int leaf, int num) //NOT cache-clean
    {
        int n = numberOfBonusSquares(leaf);
        if (num < 0 || num >= n) throw new IllegalArgumentException();

        LeafLocation l = locateLeaf(leaf);

        if (l.index < 0) throw new IllegalNodeTypeException();

        FormulaDominoTree bonustree = l.tree;
        while (true) {
            n -= bonustree.bonus_.length;
            if (num >= n) break;
            bonustree = bonustree.rootConnection_;
        }

        num -= n;

        //passing 0 for subtree because it doesn't matter as this class doesn't support
        //different bonuses for different subtrees
        return l.tree.useBonusSquare(l.index, bonustree, 0, num);
    }

    /**
     * <p>Attaches to subtree connection {@code i} the bonus square returned by
     * {@code tree.newBonusSquareForSubtreeConnection(subtree,newBonusForSubtreeIndex)}.</p>
     * <p>{@code tree} must be {@code this} or a direct or indirect parent of
     * {@code this}.</p>
     * <p>Constraints for {@code i}:
     * {@code 0<=i<numberOfSubtreeConnections()}.</p>
     * <p>Constraints for {@code subtree}:
     * {@code 0<=subtree<tree.numberOfSubtreeConnections()}.</p>
     * <p>Constraints for {@code newBonusForSubtreeIndex}:
     * {@code 0<=newBonusForSubtreeIndex<tree.numberOfNewBonusSquaresForSubtreeConnection(subtree)}.</p>
     *
     * @return the {@link DominoTree} that was attached.
     * @throws SquareDoesNotExistException if {@code i} is not a valid subtree
     *                                     connection index or
     *                                     {@code newBonusForSubtreeIndex}
     *                                     has an illegal value.
     * @throws ConflictingBindException    if the bonus square and the leaf are connected
     *                                     in a way that would cause recursion or are
     *                                     otherwise incompatible, or if {@code tree}
     *                                     is not a direct or indirect parent of
     *                                     {@code leaf}.
     * @throws IllegalNodeTypeException    a subtree is connected to connection
     *                                     {@code i} (i.e. it is not open).
     * @throws IllegalArgumentException    if {@code subtree} is not a valid index.
     * @see #rootSquare()
     * @see #leafSquare(int)
     * @see #bonusSquare(int, int)
     * @see #newBonusSquareForSubtreeConnection(int, int)
     */
    public DominoTree useBonusSquare(int i, DominoTree tree, int subtree, int newBonusForSubtreeIndex) { //NOT cache-clean

        //subtree is checked for validity but it is not used as this class doesn't support
        //different bonuses for different subtrees
        if (subtree < 0 || subtree >= tree.numberOfSubtreeConnections()) throw new IllegalArgumentException();
        if (newBonusForSubtreeIndex < 0 || newBonusForSubtreeIndex >= tree.numberOfNewBonusSquaresForSubtreeConnection(subtree))
            throw new SquareDoesNotExistException();

        if (i < 0 || i >= numberOfSubtreeConnections()) throw new SquareDoesNotExistException();

        if (!isOpenConnection(i)) throw new IllegalNodeTypeException();

        FormulaDominoTree bonustree = this;
        while (bonustree != tree && bonustree.rootConnection_ != null)
            bonustree = bonustree.rootConnection_;
        if (bonustree != tree) throw new ConflictingBindException();

        FormulaSyntaxNet bsNet = bonustree.bonus_[newBonusForSubtreeIndex];

        BonusFormulaDominoTree bsTree = new BonusFormulaDominoTree();
        FormulaSyntaxNet newBsNet = noForwardCrossingCopy(bsTree, bsNet, new HashMap<>());

        try {
            //because bonus squares may be wholly or partially unbound, we have to
            //unify with the original so that the new one keeps in sync, rather than
            //leaving or newBsNet independent.
            FormulaSyntaxNet.unify(bsNet, newBsNet);
        } catch (Exception x) {
            throw new RuntimeException("Unexpected exception: " + x);
        }

        bsTree.init(bonustree, newBonusForSubtreeIndex, newBsNet);

        try {
            FormulaSyntaxNet.unify(newBsNet, leaves_[i]);
        } catch (RuntimeException x) {
            Set<BonusFormulaDominoTree> owners = new HashSet<>();
            owners.add(bsTree);
            //using extractByOwner is safe here because the above unify() does not
            //leave traces if it fails so that the only thing we undo here is the earlier
            //unify of the newBsNet with bsNet. And that unify() couldn't add connections
            //that this extractByOwner would not remove.
            FormulaSyntaxNet.extractByOwner(newBsNet, owners);
            throw x;
        }

        //ATTENTION! If an exception-throwing command is added after the following line,
        //the exception will have to be caught so that bsTree can be removed again
        //from bonusSquaresInAction_ before propagating the exception.
        bonustree.bonusSquaresInAction_[newBonusForSubtreeIndex].add(bsTree);

        leafConnections_[i] = bsTree;
        bsTree.rootConnection_ = this;
        bsTree.rootConnectionLeafIndex_ = i;

        //because attaching bonus square messes up open leaves counts, we have to
        //invalidate the caches.
        invalidateOpenLeavesCachesUpwards();

        return bsTree;
    }

    /**
     * <p>Creates and returns an incomplete copy of {@code this} and its subtrees.</p>
     * <p>The {@link FormulaSyntaxNet}s of the resulting tree will not be unified and the
     * caches are uninitialized (the cache arrays have the proper size but undefined
     * values).</p>
     * <p><b>This function does not work on {@link BonusFormulaDominoTree}s.</b></p>
     *
     * @param cloneTreeRootConnection          the {@code rootConnection_} the copy should get.
     * @param cloneTreeRootConnectionLeafIndex the rootConnectionLeafIndex_ the copy
     *                                         should get.
     * @param mapBonusSquareToClone            used internally. Pass {@code null}.
     * @param wcmap                            When {@code wcmap.orginal} is cloned, a reference to the clone
     *                                         is stored in {@code wcmap.clone}.
     * @return the clone.
     */
    protected FormulaDominoTree recursiveNonUnifiedClone(FormulaDominoTree cloneTreeRootConnection, int cloneTreeRootConnectionLeafIndex, Map<BonusFormulaDominoTree, BonusFormulaDominoTree> mapBonusSquareToClone, WantedCloneMap wcmap) {
        FormulaDominoTree cloneTree = new FormulaDominoTree();

        if (wcmap.original == this) wcmap.clone = cloneTree;

        Map<FormulaSyntaxNet, FormulaSyntaxNet> copyMap = new HashMap<>();
        cloneTree.root_ = noForwardCrossingCopy(cloneTree, root_, copyMap);
        cloneTree.leaves_ = new FormulaSyntaxNet[leaves_.length];
        for (int i = 0; i < leaves_.length; ++i) {
            cloneTree.leaves_[i] = noForwardCrossingCopy(cloneTree, leaves_[i], copyMap);
        }

        cloneTree.bonus_ = new FormulaSyntaxNet[bonus_.length];
        for (int i = 0; i < bonus_.length; ++i) {
            cloneTree.bonus_[i] = noForwardCrossingCopy(cloneTree, bonus_[i], copyMap);
        }

        //mapBonusSquareToClone is used to map a bonus square from the original tree to
        //its copy. This is necessary because we copy bonus squares when we encounter
        //the originating FormulaDominoTree and we need to get a reference to the copy
        //later when we're copying a leaf.
        if (mapBonusSquareToClone == null) mapBonusSquareToClone = new HashMap<>();

        cloneTree.bonusSquaresInAction_ = new Collection[bonusSquaresInAction_.length];
        for (int i = 0; i < bonusSquaresInAction_.length; ++i) {
            Collection<BonusFormulaDominoTree> cloneColl = new HashSet<>();
            cloneTree.bonusSquaresInAction_[i] = cloneColl;

            for (BonusFormulaDominoTree bsTree : (Collection<BonusFormulaDominoTree>) bonusSquaresInAction_[i]) {
                BonusFormulaDominoTree bsTreeClone = new BonusFormulaDominoTree();
                cloneColl.add(bsTreeClone);

                //do not use copyMap in the following call to noForwardCrossingCopy()
                //because we want an independent copy
                bsTreeClone.init(cloneTree, i, noForwardCrossingCopy(bsTreeClone, cloneTree.bonus_[i], new HashMap<>()));
                mapBonusSquareToClone.put(bsTree, bsTreeClone);
            }
        }

        cloneTree.numOfOpenLeavesCacheForConnection_ = new int[numOfOpenLeavesCacheForConnection_.length];
        cloneTree.numOfLeavesCacheForConnection_ = new int[numOfLeavesCacheForConnection_.length];

        cloneTree.rootConnection_ = cloneTreeRootConnection;
        cloneTree.rootConnectionLeafIndex_ = cloneTreeRootConnectionLeafIndex;

        cloneTree.leafConnections_ = new FormulaDominoTree[leafConnections_.length];
        for (int i = 0; i < leafConnections_.length; ++i) {
            FormulaDominoTree leafCon = leafConnections_[i];
            if (leafCon == null) continue;
            if (leafCon.leaves_.length == 0) //if bonus square
            {
                FormulaDominoTree leafConClone = mapBonusSquareToClone.get(leafCon);
                // debugging test
                if (leafConClone == null) throw new RuntimeException("Huh? This is not possible.");

                if (wcmap.original == leafCon) wcmap.clone = leafConClone;
                leafConClone.rootConnection_ = cloneTree;
                leafConClone.rootConnectionLeafIndex_ = i;
                cloneTree.leafConnections_[i] = leafConClone;
            } else
                cloneTree.leafConnections_[i] = leafCon.recursiveNonUnifiedClone(cloneTree, i, mapBonusSquareToClone, wcmap);
        }

        return cloneTree;
    }


    /**
     * <p>Returns a copy of the {@code DominoTree}. The copy will have the same
     * structure as the original but will not share any physical data, nor will it
     * be connected with the original in any way. This means that the clone can be
     * used independent of the orginal as if the clone had been synthesized directly
     * with no involvement of the original tree.</p>
     * <p>Note that if this function is called on a subtree of a larger tree, the
     * whole tree will be cloned and the return value of this function will be the
     * subtree of the large clone tree that corresponds to the tree being cloned.</p>
     */
    public DominoTree cloneDT() {
        //find absolute root of tree
        FormulaDominoTree tree = this;
        while (tree.rootConnection_ != null) tree = tree.rootConnection_;

        WantedCloneMap wcmap = new WantedCloneMap(this);

        FormulaDominoTree cloneTree = tree.recursiveNonUnifiedClone(null, -1, null, wcmap);
        cloneTree.recreateAllForwardsDownwards();
        cloneTree.invalidateAllCachesWholeTree();

        // debugging test
        if (wcmap.clone == null) throw new RuntimeException("Failed to clone desired tree!");

        return wcmap.clone;
    }

    /**
     * <p>See {@link #cloneDT()}.</p>
     */
    public Object clone() {
        return cloneDT();
    }

    /**
     * <p>Returns the root square.</p>
     *
     * @see #leafSquare(int)
     * @see #bonusSquare(int, int)
     */
    public DominoSquare rootSquare() {
        return new RootFormulaDominoSquare(this);
    }

    /**
     * <p>Returns the square for {@code leaf}.</p>
     *
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<numberOfLeaves()}.</p>
     *
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     * @see #rootSquare()
     * @see #bonusSquare(int, int)
     */
    public DominoSquare leafSquare(int leaf) //NOT cache-clean, TESTED
    {
        LeafLocation l = locateLeaf(leaf);
        if (l.index < 0) return l.tree.rootSquare();
        return l.tree.subtreeConnectionSquare(l.index);
    }

    /**
     * <p>Returns the {@link DominoSquare} for subtree connection {@code i}.</p>
     * <p>This method works even for subtree connections with no attached subtree.</p>
     *
     * <p>Constraints for {@code i}:
     * {@code 0<=i<numberOfSubtreeConnections()}.</p>
     *
     * @throws SquareDoesNotExistException if {@code i} is not a valid
     *                                     subtree connection index.
     */
    public DominoSquare subtreeConnectionSquare(int i) {
        if (i < 0 || i >= numberOfSubtreeConnections()) throw new SquareDoesNotExistException();
        return new LeafFormulaDominoSquare(this, i);
    }

    /**
     * <p>Returns {@code num}-th bonus square usable for {@code leaf}.</p>
     *
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<numberOfLeaves()}.</p>
     * <p>Constraints for {@code num}:
     * {@code 0<=num<numberOfBonusSquares(leaf)}.</p>
     *
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     * @throws IllegalArgumentException    if {@code num} has an illegal value;
     * @see #rootSquare()
     * @see #leafSquare(int)
     */
    public DominoSquare bonusSquare(int leaf, int num) //NOT cache-clean, TESTED
    {
        int n = numberOfBonusSquares(leaf);
        if (num < 0 || num >= n) throw new IllegalArgumentException();

        LeafLocation l = locateLeaf(leaf);
        FormulaDominoTree bonustree = l.tree;
        while (true) {
            n -= bonustree.bonus_.length;
            if (num >= n) break;
            bonustree = bonustree.rootConnection_;
        }

        num -= n;
        return bonustree.bonusFormulaDominoSquare(num);
    }

    /**
     * <p>Returns {@code num}-th new bonus square usable for subtree connection
     * {@code i}.</p>
     *
     * <p>Constraints for {@code i}:
     * {@code 0<=i<numberOfSubtreeConnections()}.</p>
     * <p>Constraints for {@code num}:
     * {@code 0<=num<numberOfNewBonusSquaresForSubtreeConnection(i)}.</p>
     *
     * @throws IllegalArgumentException    if {@code i} has an illegal value;
     * @throws SquareDoesNotExistException if {@code num} is not a valid number
     *                                     for subtree connection {@code i}.
     * @see #rootSquare()
     * @see #leafSquare(int)
     * @see #bonusSquare(int, int)
     * @see #subtreeConnectionSquare(int)
     * @see #numberOfSubtreeConnections()
     * @see #numberOfNewBonusSquaresForSubtreeConnection(int)
     */
    public DominoSquare newBonusSquareForSubtreeConnection(int i, int num) {
        if (i < 0 || i >= numberOfSubtreeConnections()) throw new IllegalArgumentException();
        if (num < 0 || num >= numberOfNewBonusSquaresForSubtreeConnection(i)) throw new SquareDoesNotExistException();
        return bonusFormulaDominoSquare(num);
    }

    /**
     * <p>Returns bonus square for {@code bonus_[bonusnum]}.</p>
     */
    protected DominoSquare bonusFormulaDominoSquare(int bonusnum) {
        // debugging test
        if (bonusnum < 0 || bonusnum >= bonus_.length) throw new RuntimeException();

        return new BonusFormulaDominoSquare(this, bonusnum);
    }

    /**
     * <p>Initializes {@code this} with the respective
     * {@link FormulaSyntaxNet}s.</p>
     * <p>For internal use only. None of the arrays may be {@code null} and none
     * of the elements may be {@code null}.</p>
     * <p>This function may only be called on a {@code FormulaDominoTree} that
     * is not yet initialized.</p>
     */
    protected void init(FormulaSyntaxNet root, FormulaSyntaxNet[] leaves, FormulaSyntaxNet[] bonus) {
        root_ = root;
        leaves_ = leaves;
        bonus_ = bonus;
        leafConnections_ = new FormulaDominoTree[leaves_.length];
        bonusSquaresInAction_ = new Collection[bonus_.length];
        numOfLeavesCacheForConnection_ = new int[leaves_.length];
        numOfOpenLeavesCacheForConnection_ = new int[leaves_.length];

        for (int i = 0; i < bonusSquaresInAction_.length; ++i) {
            bonusSquaresInAction_[i] = new HashSet();
        }
        invalidateAllCachesWholeTree();
    }

    /**
     * <p>Returns {@code leaf}'s {@link FormulaSyntaxNet}.</p>
     *
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     */
    protected FormulaSyntaxNet leafNet(int leaf) //NOT cache-clean, TESTED
    {
        LeafLocation l = locateLeaf(leaf);
        if (l.index < 0) return l.tree.root_;
        else return l.tree.leaves_[l.index];
    }

    /**
     * <p>Returns all the unbound nodes that occur in {@link FormulaSyntaxNet}s of
     * leaves as a {@link Set} of {@link FormulaSyntaxNet}s.</p>
     */
    public Set<FormulaSyntaxNet> unboundVariablesInLeaves() //NOT cache-clean, TESTED
    {
        Set<FormulaSyntaxNet> unbound = new HashSet<>();
        for (int leaf = 0; leaf < numberOfLeaves(); ++leaf) {
            FormulaSyntaxNet n = leafNet(leaf);
            FSNIterator iter = n.multiorderIterator(false, false, false);
            while (iter.hasNext()) {
                FormulaSyntaxNet node = iter.next();
                if (node.isUnboundOrForward()) unbound.add(node);
            }
        }
        return unbound;
    }

    /**
     * <p>Returns all the unbound nodes that occur in the root
     * {@link FormulaSyntaxNet}.</p>
     */
    public Set<FormulaSyntaxNet> unboundVariablesInRoot() {
        Set<FormulaSyntaxNet> unbound = new HashSet<>();
        FSNIterator iter = root_.multiorderIterator(false, false, false);
        while (iter.hasNext()) {
            FormulaSyntaxNet node = iter.next();
            if (node.isUnboundOrForward()) unbound.add(node);
        }
        return unbound;
    }

    /**
     * <p>Returns the number of unbound variables that occur in formulas in leaves.</p>
     */
    public int numberOfUnboundVariablesInLeaves() //NOT cache-clean, TESTED
    {
        return unboundVariablesInLeaves().size();
    }

    /**
     * <p>Returns the number of unbound variables that occur in the root formula.</p>
     */
    public int numberOfUnboundVariablesInRoot() {
        return unboundVariablesInRoot().size();
    }

    /**
     * <p>Binds all unbound variables that occur in formulas in leaves to
     * identifiers from {@code ids}. The order in which unbound variables are
     * bound is unspecified. The owner of every new bound node will be the same as that of
     * the unbound node it is unified with.</p>
     *
     * @throws IllegalArgumentException if {@code ids==null} or
     *                                  {@code ids.length<numberOfUnboundVariablesInLeaves()}. If
     *                                  this exception is thrown some variables may already be bound and will
     *                                  remain so.
     */
    public void bindUnboundVariablesInLeaves(String[] ids) //NOT cache-clean, TESTED
    {
        bindUnbound(unboundVariablesInLeaves(), ids);
    }

    /**
     * <p>Binds all unbound variables that occur in the root formula to
     * identifiers from {@code ids}. The order in which unbound variables are
     * bound is unspecified. The owner of every new bound node will be the same as that of
     * the unbound node it is unified with.</p>
     *
     * @throws IllegalArgumentException if {@code ids==null} or
     *                                  {@code ids.length<numberOfUnboundVariablesInRoot()}. If
     *                                  this exception is thrown some variables may already be bound and will
     *                                  remain so.
     */
    public void bindUnboundVariablesInRoot(String[] ids) //NOT cache-clean, TESTED
    {
        bindUnbound(unboundVariablesInRoot(), ids);
    }

}

