/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.tree;


import domino.logic.IllegalNodeTypeException;
import domino.logic.WantedCloneMap;
import domino.logic.syntaxnet.FormulaSyntaxNet;

import java.util.Collection;
import java.util.Map;

/**
 * <p>This class is a specialized variant of {@link FormulaDominoTree}.</p>
 */
public class BonusFormulaDominoTree extends FormulaDominoTree {
    /**
     * <p>{@code originator_.bonusSquaresInAction[bonusIndex_].contains(this)==true}.</p>
     */
    private FormulaDominoTree originator_;
    /**
     * <p>{@code originator_.bonusSquaresInAction[bonusIndex_].contains(this)==true}.</p>
     */
    private int bonusIndex_;

    /**
     * <p>Creates an uninitialized {@code BonusFormulaDominoTree}.
     * For use by {@link FormulaDominoTree} only.</p>
     */
    protected BonusFormulaDominoTree() {
    }

    /**
     * <p>Initializes an uninitialized {@code BonusFormulaDominoTree}.</p>
     * <p>This function must not be called on an already initialized tree.</p>
     * <p>This function does not add {@code this} to {@code originator}'s
     * {@code bonusSquaresInAction_} list, nor do other changes in
     * {@code originator} take place.</p>
     *
     * @see FormulaDominoTree#init(FormulaSyntaxNet, FormulaSyntaxNet[], FormulaSyntaxNet[])
     */
    protected void init(FormulaDominoTree originator, int bonusIndex, FormulaSyntaxNet root) {
        FormulaSyntaxNet[] leaves = new FormulaSyntaxNet[0];
        FormulaSyntaxNet[] bonus = new FormulaSyntaxNet[0];
        init(root, leaves, bonus);
        originator_ = originator;
        bonusIndex_ = bonusIndex;
    }

    /**
     * <p>Throws {@link IllegalNodeTypeException}.</p>
     */
    public void attachTo(DominoTree to, int leaf) {
        throw new IllegalNodeTypeException();
    }

    /**
     * <p>Returns the {@link FormulaDominoTree} thanks to which this bonus
     * square was used.</p>
     */
    public FormulaDominoTree originator() {
        return originator_;
    }

    /**
     * <p>Finds and detaches all bonus squares that originate in a
     * {@code FormulaDominoTree} contained in {@code parents}.</p>
     * <p>This function does not deunify or reunify the tree or
     * invalidate any caches.
     * This means that after calling this function the {@code FormulaDominoTree}
     * is likely to be in an inconsistent state.</p>
     */
    protected void detachAllBonusSquaresOriginatingIn(Collection<FormulaDominoTree> parents) {
        if (parents.contains(originator())) {
            doDetach();
        }
    }

    /**
     * <p>Clears the pointers between {@code this} and its parent, and removes
     * {@code this} from {@link #originator()}'s
     * {@link FormulaDominoTree#bonusSquaresInAction_ bonusSquaresInAction_} list.</p>
     */
    protected void doDetach() {
        // debugging test
        if (rootConnection_ == null) throw new RuntimeException("This call should not have happened!");
        if (!originator().bonusSquaresInAction_[bonusIndex_].contains(this))
            throw new RuntimeException("*sniff* *sniff* My father has disowned me :~-(");

        FormulaDominoTree oldRootConnection = rootConnection_;
        rootConnection_ = null;
        oldRootConnection.leafConnections_[rootConnectionLeafIndex_] = null;
        rootConnectionLeafIndex_ = -1;
        originator().bonusSquaresInAction_[bonusIndex_].remove(this);
    }

    /**
     * <p>If the {@code DominoTree} is a subtree of a larger tree, it is
     * detached from the parent tree. If the tree is not a subtree, nothing happens.</p>
     * <p>All bonus squares in the subtree to be detached, that originate from
     * {@code DominoTree}s not part of the detached subtree will be detached
     * from the subtree and cannot be reattached.</p>
     *
     * @return {@code true} if subtree was detached; {@code false} if tree
     * had no parent tree to begin with.
     * @see #attachTo(DominoTree, int)
     */
    public boolean detachFromParent() {
        if (rootConnection_ == null) return false;

        FormulaDominoTree oldConnection = rootConnection_;
        doDetach();

        //it would be great if we could just use FormulaSyntaxNet.extractByOwner() to
        //detach the tree. However, (see doc for extractByOwner()) there are some
        //unifications that might remain. So we have no choice but to reunify the whole
        //tree.
        oldConnection.reunifyWholeTree(); //reunify parent part
        //the child part doesn't need to be reunified as the above will automatically
        //kill all forwards to this and there are none that need to be recreated

        //because detaching bonus square messes up open leaves caches, we have to
        //invalidate.
        oldConnection.invalidateOpenLeavesCachesUpwards(); //invalidate in parent part

        //no need to invalidate leaves caches in parent because a bonus square counts
        //as one leaf just as an open connection

        //no need to invalidate caches in this because this does not have any leaves
        return true;
    }

    /**
     * <p>Throws {@link RuntimeException}.</p>
     */
    protected FormulaDominoTree recursiveNonUnifiedClone(FormulaDominoTree cloneTreeRootConnection, int cloneTreeRootConnectionLeafIndex, Map<BonusFormulaDominoTree, BonusFormulaDominoTree> mapBonusSquareToClone, WantedCloneMap wcmap) {
        throw new RuntimeException("This method should not be called on BonusFormulaDominoTrees");
    }

}
