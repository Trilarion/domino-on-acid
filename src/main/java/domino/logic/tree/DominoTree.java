/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.tree;

import domino.logic.IllegalNodeTypeException;
import domino.logic.rules.SquareDoesNotExistException;
import domino.logic.square.DominoSquare;
import domino.logic.syntaxnet.ConflictingBindException;

/**
 * <p>This interface is implemented by classes that represent domino tiles connected
 * to form a tree.</p>
 */
public interface DominoTree {
    /**
     * <p>If the {@code DominoTree} is a subtree of a larger tree, it is
     * detached from the parent tree. If the tree is not a subtree, nothing happens.</p>
     * <p>All bonus squares in the subtree to be detached, that originate from
     * {@code DominoTree}s not part of the detached subtree will be detached
     * from the subtree and cannot be reattached.</p>
     *
     * @return {@code true} if subtree was detached; {@code false} if tree
     * had no parent tree to begin with.
     * @see #attachTo(DominoTree, int)
     */
    boolean detachFromParent();

    /**
     * <p>Connects the root of {@code this} to {@code leaf}
     * of {@code DominoTree} {@code to}.</p>
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<to.numberOfLeaves() && to.isOpenLeaf(leaf)}.</p>
     *
     * @throws IllegalArgumentException    if {@code to} is a subtree of
     *                                     {@code this}.
     *                                     The trees remain unchanged.
     * @throws IllegalNodeTypeException    if {@code this} is already connected, or is
     *                                     a bonus square that has been detached or if the leaf to attach to is not
     *                                     open.
     *                                     The trees remain unchanged.
     * @throws SquareDoesNotExistException if {@code to==null} or
     *                                     {@code leaf} is not a valid leaf
     *                                     number. The trees remain unchanged.
     * @throws ConflictingBindException    if the root square does not match the leaf to
     *                                     connect with. The trees remain unchanged.
     * @throws ClassCastException          if {@code this} and {@code to}
     *                                     have incompatible types. The trees remain unchanged.
     * @see #attachToSubtreeConnection(DominoTree, int)
     * @see #detachFromParent()
     * @see #numberOfLeaves()
     * @see #isOpenLeaf(int)
     */
    void attachTo(DominoTree to, int leaf);

    /**
     * <p>Like {@link #attachTo(DominoTree, int)} with the difference that instead of a leaf
     * index, a subtree connection index is used.</p>
     * <p>Constraints for {@code i}:
     * {@code 0<=i<to.numberOfSubtreeConnections() &&
     * to.isOpenLeaf(leafIndexForSubtreeConnection(i))}.</p>
     *
     * @throws IllegalArgumentException    if {@code to} is a subtree of
     *                                     {@code this}.
     *                                     The trees remain unchanged.
     * @throws IllegalNodeTypeException    if {@code this} is already connected, or is
     *                                     a bonus square that has been detached or if the leaf to attach to is not
     *                                     open.
     *                                     The trees remain unchanged.
     * @throws SquareDoesNotExistException if {@code to==null} or
     *                                     {@code i} is not a valid subtree connection index.
     *                                     The trees remain unchanged.
     * @throws ConflictingBindException    if the root square does not match the leaf to
     *                                     connect with. The trees remain unchanged.
     * @throws ClassCastException          if {@code this} and {@code to}
     *                                     have incompatible types. The trees remain unchanged.
     * @see #attachTo(DominoTree, int)
     * @see #detachFromParent()
     * @see #numberOfLeaves()
     * @see #isOpenLeaf(int)
     */
    void attachToSubtreeConnection(DominoTree to, int i);

    /**
     * <p>Returns the parent tree of {@code this} tree, if there is one, or
     * {@code null} if {@code this} tree does not have a parent.</p>
     *
     * @see #parentSubtreeIndex()
     */
    DominoTree parent();

    /**
     * <p>If {@code this} tree has a {@link #parent()},
     * {@code parent().subtree(parentSubtreeIndex())==this}, otherwise -1 is
     * returned.</p>
     */
    int parentSubtreeIndex();

    /**
     * <p>Returns the number of subtrees that may be connected to {@code this}.</p>
     * <p>Every {@code DominoTree} consists of a root tree with connections
     * for subtrees. This function returns the number of connections regardless of
     * whether actual subtrees are connected to them or not.</p>
     *
     * @see #subtree(int)
     */
    int numberOfSubtreeConnections();

    /**
     * <p>If a subtree is connected to subtree connection {@code i}, then that
     * subtree is returned. Otherwise this function returns {@code null}.</p>
     *
     * <p>Constraints for {@code i}:
     * {@code 0<=i<to.numberOfSubtreeConnections()}.</p>
     *
     * @throws IllegalArgumentException if {@code i} has an illegal value.
     * @see #numberOfSubtreeConnections()
     */
    DominoTree subtree(int i);

    /**
     * <p>Returns the number of leaves this tree has that are not bonus squares.</p>
     */
    int numberOfOpenLeaves();

    /**
     * <p>Returns the number of leaves this tree has.</p>
     */
    int numberOfLeaves();

    /**
     * <p>Returns {@code true} if {@code leaf} is open.</p>
     *
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<numberOfLeaves()}.</p>
     *
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     */
    boolean isOpenLeaf(int leaf);

    /**
     * <p>Returns the index of the {@code n}-th open leaf.</p>
     *
     * <p>Constraints for {@code n}:
     * {@code 0<=n<numberOfOpenLeaves()}.</p>
     *
     * @throws SquareDoesNotExistException if {@code n} is not valid.
     */
    int indexOfOpenLeaf(int n);

    /**
     * <p>Returns the index of the first leaf found in subtree {@code i}.</p>
     *
     * <p>Constraints for {@code i}:
     * {@code 0<=i<numberOfSubtreeConnections()}.</p>
     *
     * @throws SquareDoesNotExistException if {@code i} is not a valid subtree
     *                                     connection index.
     */
    int leafIndexForSubtreeConnection(int i);

    /**
     * <p>Returns the number of bonus {@link DominoSquare}s the player may use in a
     * subtree attached to {@code leaf}. This counts bonus squares coming from
     * parent nodes of {@code this} as well as new bonus squares not available in
     * parent nodes.</p>
     * <p>A {@code FormulaDominoTree} may provide the same bonus square multiple
     * times and the same bonus square may be provided by {@code this} and/or
     * several parents at the same time. These duplicates will be included in the count
     * returned by this function!</p>
     * <p>This method may be called even on non-open leaves although bonus squares
     * cannot be used on them. In this case the return value is the number of bonus
     * squares that could be used if the leaf were open.</p>
     *
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<numberOfLeaves()}.</p>
     *
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     */
    int numberOfBonusSquares(int leaf);

    /**
     * <p>Returns the number of new bonus {@link DominoSquare}s the player may use in a
     * subtree attached to subtree connection{@code i}. This does not count bonus
     * squares coming from parent nodes of {@code this}.
     * <p>A {@code FormulaDominoTree} may provide the same bonus square multiple
     * times and the same bonus square may be provided by {@code this} and/or
     * several parents at the same time, so the count returned by this function may
     * include squares that are also provided by parent trees.</p>
     *
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<numberOfSubtreeConnections()}.</p>
     *
     * @throws IllegalArgumentException if {@code i} is not a valid number.
     * @see #numberOfSubtreeConnections()
     * @see #subtree(int)
     */
    int numberOfNewBonusSquaresForSubtreeConnection(int i);

    /**
     * <p>Attaches to {@code leaf} the {@code num}-th bonus square that
     * may be used in subtrees attached to {@code leaf}.</p>
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<numberOfLeaves()}.</p>
     * <p>Constraints for {@code num}:
     * {@code 0<=num<numberOfBonusSquares(leaf)}.</p>
     *
     * @return the DominoTree that was attached.
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     * @throws ConflictingBindException    if the bonus square and the leaf are connected
     *                                     in a way that would cause recursion or are
     *                                     otherwise incompatible.
     * @throws IllegalNodeTypeException    {@code leaf} is not open.
     * @throws IllegalArgumentException    if {@code num} has an illegal value.
     * @see #rootSquare()
     * @see #leafSquare(int)
     * @see #bonusSquare(int, int)
     */
    DominoTree useBonusSquare(int leaf, int num);

    /**
     * <p>Attaches to subtree connection {@code i} the bonus square returned by
     * {@code tree.newBonusSquareForSubtreeConnection(subtree,newBonusForSubtreeIndex)}.</p>
     * <p>{@code tree} must be {@code this} or a direct or indirect parent of
     * {@code this}.</p>
     * <p>Constraints for {@code i}:
     * {@code 0<=i<numberOfSubtreeConnections()}.</p>
     * <p>Constraints for {@code subtree}:
     * {@code 0<=subtree<tree.numberOfSubtreeConnections()}.</p>
     * <p>Constraints for {@code newBonusForSubtreeIndex}:
     * {@code 0<=newBonusForSubtreeIndex<tree.numberOfNewBonusSquaresForSubtreeConnection(subtree)}.</p>
     *
     * @return the DominoTree that was attached.
     * @throws SquareDoesNotExistException if {@code i} is not a valid subtree
     *                                     connection index or
     *                                     {@code newBonusForSubtreeIndex}
     *                                     has an illegal value.
     * @throws ConflictingBindException    if the bonus square and the leaf are connected
     *                                     in a way that would cause recursion or are
     *                                     otherwise incompatible, or if {@code tree}
     *                                     is not a direct or indirect parent of
     *                                     {@code leaf}.
     * @throws IllegalNodeTypeException    a subtree is connected to connection
     *                                     {@code i} (i.e. it is not open).
     * @throws IllegalArgumentException    if {@code subtree} is not a valid index.
     * @see #rootSquare()
     * @see #leafSquare(int)
     * @see #bonusSquare(int, int)
     * @see #newBonusSquareForSubtreeConnection(int, int)
     */
    DominoTree useBonusSquare(int i, DominoTree tree, int subtree, int newBonusForSubtreeIndex);

    /**
     * <p>Returns a copy of the {@code DominoTree}. The copy will have the same
     * structure as the original but will not share any physical data, nor will it
     * be connected with the original in any way. This means that the clone can be
     * used independent of the original as if the clone had been synthesized directly
     * with no involvement of the original tree.</p>
     * <p>Note that if this function is called on a subtree of a larger tree, the
     * whole tree will be cloned and the return value of this function will be the
     * subtree of the large clone tree that corresponds to the tree being cloned.</p>
     */
    DominoTree cloneDT();

    /**
     * <p>See {@link #cloneDT()}.</p>
     */
    Object clone();

    /**
     * <p>Returns the root square.</p>
     *
     * @see #leafSquare(int)
     * @see #bonusSquare(int, int)
     * @see #newBonusSquareForSubtreeConnection(int, int)
     * @see #subtreeConnectionSquare(int)
     */
    DominoSquare rootSquare();

    /**
     * <p>Returns the square for {@code leaf}.</p>
     *
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<numberOfLeaves()}.</p>
     *
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     * @see #rootSquare()
     * @see #bonusSquare(int, int)
     * @see #newBonusSquareForSubtreeConnection(int, int)
     * @see #subtreeConnectionSquare(int)
     */
    DominoSquare leafSquare(int leaf);

    /**
     * <p>Returns {@code num}-th bonus square usable for {@code leaf}.</p>
     *
     * <p>Constraints for {@code leaf}:
     * {@code 0<=leaf<numberOfLeaves()}.</p>
     * <p>Constraints for {@code num}:
     * {@code 0<=num<numberOfBonusSquares(leaf)}.</p>
     *
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf number.
     * @throws IllegalArgumentException    if {@code num} has an illegal value;
     * @see #rootSquare()
     * @see #leafSquare(int)
     * @see #newBonusSquareForSubtreeConnection(int, int)
     * @see #subtreeConnectionSquare(int)
     */
    DominoSquare bonusSquare(int leaf, int num);

    /**
     * <p>Returns the {@link DominoSquare} for subtree connection {@code i}.</p>
     * <p>This method works even for subtree connections with no attached subtree.</p>
     *
     * <p>Constraints for {@code i}:
     * {@code 0<=i<numberOfSubtreeConnections()}.</p>
     *
     * @throws SquareDoesNotExistException if {@code i} is not a valid
     *                                     subtree connection index.
     */
    DominoSquare subtreeConnectionSquare(int i);

    /**
     * <p>Returns {@code num}-th new bonus square usable for subtree connection
     * {@code i}.</p>
     *
     * <p>Constraints for {@code i}:
     * {@code 0<=i<numberOfSubtreeConnections()}.</p>
     * <p>Constraints for {@code num}:
     * {@code 0<=num<numberOfNewBonusSquaresForSubtreeConnection(i)}.</p>
     *
     * @throws IllegalArgumentException    if {@code i} has an illegal value;
     * @throws SquareDoesNotExistException if {@code num} is not a valid number
     *                                     for subtree connection {@code i}.
     * @see #rootSquare()
     * @see #leafSquare(int)
     * @see #bonusSquare(int, int)
     * @see #subtreeConnectionSquare(int)
     * @see #numberOfSubtreeConnections()
     * @see #numberOfNewBonusSquaresForSubtreeConnection(int)
     */
    DominoSquare newBonusSquareForSubtreeConnection(int i, int num);
}


