/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.rules;

import domino.logic.IllegalNodeTypeException;
import domino.logic.syntaxnet.ConflictingBindException;
import domino.logic.tree.DominoTree;

import java.util.List;

public class CloseRule implements DeductionRule {

    /**
     * <p>Returns 1.</p>
     */
    public int requiredBonusSquares() {
        return 1;
    }

    public DominoTree applyTo(DominoTree tree, int leaf, List<Integer> bsIndex) {
        try {
            return tree.useBonusSquare(leaf, bsIndex.get(0));
        } catch (SquareDoesNotExistException | IllegalNodeTypeException | ConflictingBindException ex) {
            throw ex;
        } catch (IllegalArgumentException ex) {
            throw new ConflictingBindException();
        }
    }

    public String name(String conclusion) {
        return "[" + conclusion + "]";
    }

}
