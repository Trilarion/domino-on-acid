/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.rules;

import domino.logic.syntaxnet.ConflictingBindException;
import domino.logic.tree.DominoTree;

import java.util.List;

/**
 * <p>Classes that implement this interface are rules that can be applied to
 * a {@link DominoTree}'s leaf.</p>
 */
public interface DeductionRule {
    /**
     * <p>The number of bonus squares required to apply this rule.</p>
     */
    int requiredBonusSquares();

    /**
     * <p>Applies the rule to {@code leaf} of {@code tree} using the
     * bonus squares whose indices are in {@code bsIndex}.</p>
     * <p>{@code bs} must contain {@link #requiredBonusSquares()} elements.</p>
     *
     * @return the new subtree attached to {@code leaf} if the rule was applied
     * successfully. You can undo the application of the rule by calling
     * this tree's {@link DominoTree#detachFromParent()} function.
     * @throws ConflictingBindException    if the rule is not applicable. The tree
     *                                     remains unchanged.
     * @throws SquareDoesNotExistException if {@code leaf} is not a valid leaf
     *                                     number. The tree remains unchanged.
     * @throws ClassCastException          if {@code this} rule can not be applied to
     *                                     {@code tree} because of an unsupported {@link DominoTree} subtype.
     */
    DominoTree applyTo(DominoTree tree, int leaf, List<Integer> bsIndex);

    /**
     * <p>Returns the name of the rule if the conclusion is {@code conclusion}.</p>
     */
    String name(String conclusion);
}
