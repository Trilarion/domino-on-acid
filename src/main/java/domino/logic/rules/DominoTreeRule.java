/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.rules;

import domino.logic.tree.DominoTree;

import java.util.List;

public class DominoTreeRule implements DeductionRule {
    protected DominoTree tree_;

    protected DominoTreeRule() {
    }

    /**
     * <p>{@code t} is copied so changing {@code t} after calling this
     * function has no effect on the {@code DominoTreeRule} object.</p>
     */
    protected void init(DominoTree t) {
        tree_ = t.cloneDT();
    }

    /**
     * <p>Returns 0.</p>
     */
    public int requiredBonusSquares() {
        return 0;
    }

    /**
     * @param bsIndex is not used.
     */
    public DominoTree applyTo(DominoTree tree, int leaf, List<Integer> bsIndex) {
        DominoTree newTree = tree_.cloneDT();
        newTree.attachTo(tree, leaf);
        return newTree;
    }

    public String name(String conclusion) {
        throw new UnsupportedOperationException("Can only be called for derived classes");
    }

}
