/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/*
NOTES:
- seed=137982274663266409L is a real killer for versions compiled with FormulaSyntaxNet
  up to (and including) 2002-07-05.
  TautologyGenerator 4 1 2 3 4  will cause Out Of Memory as will a lot of other
  parameters.

*/

package domino.logic;

import domino.logic.square.FormulaDominoSquare;
import domino.logic.syntaxnet.ConflictingBindException;
import domino.logic.tree.DominoTree;
import domino.logic.tree.FormulaDominoTree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * <p>This class creates propositional logic tautologies.</p>
 */
public class TautologyGenerator {
    private static final boolean debugRules = false;

    protected final DominoTree tndTile = createTNDTile();
    protected final DominoTree peTile = createPETile();
    protected final DominoTree mpTile = createMPTile();
    protected final Random random;

    /**
     * <p>Creates a new TautologyGenerator with a new random number generator.</p>
     */
    public TautologyGenerator() {
        //Math.random() is multiplied with the initial seed not because it adds
        //more randomness (it doesn't), but because this makes it more likely
        //that different TautologyGenerators get different seeds, even when started
        //in the same millisecond.
        random = new Random((long) (Math.random() * Long.MAX_VALUE) * System.currentTimeMillis());
    }

    /**
     * <p>Creates a new TautologyGenerator using random number generator
     * {@code rand}.</p>
     */
    public TautologyGenerator(Random rand) {
        random = rand;
    }

    protected static DominoTree createTNDTile() {
        String[] leaves = {"_|_"};
        String[] bonus = {"X0->_|_"};
        return FormulaDominoTree.newFromInfix("X0", leaves, bonus);
    }

    protected static DominoTree createPETile() {
        String[] leaves = {"X1"};
        String[] bonus = {"X0"};
        return FormulaDominoTree.newFromInfix("X0->X1", leaves, bonus);
    }

    protected static DominoTree createMPTile() {
        String[] leaves = {"X0", "X0->X1"};
        return FormulaDominoTree.newFromInfix("X1", leaves, null);
    }

    /**
     * <p>Outputs a random tautology on stdout.</p>
     */
    public static void main(String[] args) { // TODO similar to dominore.core.LevelGenerator, as a test ...?
        if (args.length != 5) {
            System.err.println("USAGE: numVars TND ->I ->E []");
            System.exit(1);
        }
        Random rand0 = new Random();
        long seed = rand0.nextLong();

//    seed=-7636458461345605413L; System.out.println("Fixed seed!");

        System.err.println("Random seed: " + seed);
        rand0 = new Random(seed);
        TautologyGenerator tgen = new TautologyGenerator(rand0);
        List<Double> prob = new ArrayList<>();
        prob.add(Double.parseDouble(args[1]));
        prob.add(Double.parseDouble(args[2]));
        prob.add(Double.parseDouble(args[3]));
        prob.add(Double.parseDouble(args[4]));
        FormulaDominoTree randomTautProof = tgen.randomTautologyProof(Integer.parseInt(args[0]), prob, true);
        FormulaDominoSquare root = (FormulaDominoSquare) randomTautProof.rootSquare();

        String[] diag = ProofFormatter.naturalDeductionDiagram(randomTautProof);
        for (String s : diag) System.out.println(s);
        System.out.println();
    }

    /**
     * <p>Returns the indices of the elements in {@code prob}
     * in a random order based on the contents of
     * {@code prob}.</p>
     * <p>{@code prob} must contain {@link Double}s.</p>
     * <p>The returned {@link List} will contain the numbers {@code 0} to
     * {@code prob.size()-1} (i.e. the indices of the elements of {@code prob})
     * in random order.</p>
     * <p>Every element of
     * {@code prob} is the relative weight of the index of the element within
     * {@code prob}. The chance that index {@code i} is returned as element
     * 0 of the result {@link List} is {@code prob.get(i)} divided by the sum of
     * all weights. The other elements of the result list are determined the same way
     * with the restriction that the same index may not be contained in the result
     * {@link List} twice.</p>
     *
     * @return {@link List} of {@link Integer}s.
     * @throws IllegalArgumentException if {@code prob} is empty or
     *                                  {@code null} or contains any element {@code <0.0000001}.
     */
    public List<Integer> randomPick(List<Double> prob) {
        final double THRESH = 0.0000001;
        if (prob == null || prob.isEmpty()) throw new IllegalArgumentException();

        List<Integer> result = new ArrayList<>(prob.size());

        List<Double> temp = new ArrayList<>(prob);
        double sum = 0;
        for (Double o : temp) {
            double d = o;
            if (d < THRESH) throw new IllegalArgumentException();
            sum += d;
        }

        double rnum = random.nextDouble();

        for (int j = 0; j < temp.size(); ++j) {
            double tmpsum = 0;
            for (int index = 0; index < temp.size(); ++index) {
                double d = temp.get(index);
                if (d < 0) continue;
                tmpsum += d / sum;
                if (rnum > tmpsum) continue;
                temp.set(index, (double) -1);
                sum -= d;
                result.add(index);
                break;
            }
        }

        return result;
    }

    /**
     * <p>Similar to {@link #randomPick(List)} but returns index pairs.</p>
     * <p>{@code prob} must contain {@link Double}s.</p>
     * <p>The returned {@link List} will contain the pairs {@code (0,1),...,
     * (0,prob.size()-1), (1,0), (1,2),...}.</p>
     * <p>Element 0 of the result {@link List} is the 1st index of the 1st pair.
     * Element 1 is the 2nd index of the 1st pair. Element 2 is the 1st index of the 2nd
     * pair,... </p>
     * <p>The result {@link List} will have {@code 2*prob.size()*(prob.size()-1)}
     * elements.</p>
     *
     * @return {@link List} of {@link Integer}s.
     * @throws IllegalArgumentException if {@code prob} contains fewer than 2
     *                                  elements or
     *                                  {@code null} or contains any element {@code <0.0000001}.
     */
    public List<Integer> randomPairPick(List<Double> prob) {
        final double THRESH = 0.0000001;
        if (prob == null || prob.size() < 2) throw new IllegalArgumentException();

        int s = prob.size();

        Iterator<Double> iterator = prob.iterator();
        double sum = 0;
        while (iterator.hasNext()) {
            double d = iterator.next();
            if (d < THRESH) throw new IllegalArgumentException();
            sum += d;
        }

        List<Double> pairprob = new ArrayList<>(s * (s - 1));
        for (int i = 0; i < s; ++i)
            for (int j = 0; j < s; ++j) {
                if (j == i) continue;
                double p = (prob.get(i) / sum) * (prob.get(j) / sum);
                pairprob.add(p);
            }

        List<Integer> result = new ArrayList<>(2 * s * (s - 1));

        for (int index : randomPick(pairprob)) {
            int i = index / (s - 1);

            int j = index % (s - 1);
            if (j >= i) ++j;

            result.add(i);
            result.add(j);
        }

        return result;
    }

    /**
     * <p>Creates a random tautology with a random proof.</p>
     *
     * @param numVars is the maximum number of assumptions to use in the proof.
     * @param prob    must contain 4 positive {@link Double}s. These values determine by
     *                their relative values the relative probabilities of the 4 rules used to
     *                create the proof. Element 0 is the weight for TND. Element 1 is the weight
     *                for {@literal ->I}. Element 2 is the weight for {@literal ->E}. Element 3 is the weight for
     *                the rule that uses a free assumption to remove a leaf.
     * @param bound   if {@code true}, the returned tautology will have bound
     *                leaves with names consisting only of uppercase letters from the
     *                range A-Z.
     * @return a random proof for a random tautology. The symbol used for false is
     * "_|_".
     * @throws IllegalArgumentException if {@code prob} is empty or
     *                                  {@code null} or contains any element {@code <0.0000001} or
     *                                  if {@code numVars<=0}.
     */
    public FormulaDominoTree randomTautologyProof(int numVars, List<Double> prob, boolean bound) {
        if (prob == null || prob.size() != 4 || numVars <= 0) throw new IllegalArgumentException();

        List<DominoTree> trees = new ArrayList<>(numVars);
        for (int i = 0; i < numVars; ++i) {
            String[] leaves = {"X0"};
            DominoTree var = FormulaDominoTree.newFromInfix("X0", leaves, null);
            trees.add(var);
        }

        while (trees.size() > 1 || trees.get(0).numberOfOpenLeaves() > 0) {
            List<Double> treeprob = new ArrayList<>(trees.size());
            for (int i = 0; i < trees.size(); ++i) treeprob.add(1.0);

            Iterator<Integer> ruleToTry = randomPick(prob).iterator();
            boolean success = false;
            while (!success && ruleToTry.hasNext()) {
                int rule = ruleToTry.next();

                if (rule < 2) //TND or ->I
                {
                    DominoTree ruleTile;
                    if (rule == 0) ruleTile = tndTile.cloneDT();
                    else ruleTile = peTile.cloneDT();

                    for (Integer o : randomPick(treeprob)) {
                        int treeidx = o;
                        DominoTree tryMe = trees.get(treeidx);
                        try {
                            tryMe.attachTo(ruleTile, 0);
                            trees.set(treeidx, ruleTile);
                            success = true;
                            if (debugRules) System.out.println(rule + "/" + treeidx);
                            break;
                        } catch (ConflictingBindException ignored) {
                        }
                    }
                } else if (rule == 2 && trees.size() >= 2) //MP
                {
                    DominoTree ruleTile = mpTile.cloneDT();
                    Iterator<Integer> treeToTry = randomPairPick(treeprob).iterator();
                    while (treeToTry.hasNext()) {
                        int treeidx1 = treeToTry.next();
                        int treeidx2 = treeToTry.next();
                        DominoTree tryMe1 = trees.get(treeidx1);
                        DominoTree tryMe2 = trees.get(treeidx2);
                        try {
                            //attach to leaf 1 first because attaching changes leaf indices
                            //so if we attached to 0 first we couldn't use index 1 to refer to
                            //the 2nd leaf.
                            tryMe1.attachTo(ruleTile, 1);
                            try {
                                tryMe2.attachTo(ruleTile, 0);
                                trees.set(treeidx1, ruleTile);
                                trees.remove(treeidx2);
                                success = true;
                                if (debugRules) System.out.println(rule + "/" + treeidx1 + "/" + treeidx2);
                                break;
                            } catch (ConflictingBindException x) {
                                tryMe1.detachFromParent();
                            }
                        } catch (ConflictingBindException ignored) {
                        }
                    }
                } else if (rule == 3) //attach a bonus square to a leaf
                {
                    Iterator<Integer> treeToTry = randomPick(treeprob).iterator();
                    tryTrees:
                    while (treeToTry.hasNext()) {
                        int treeidx = treeToTry.next();
                        DominoTree tryMe = trees.get(treeidx);
                        int lnum = tryMe.numberOfOpenLeaves();
                        if (lnum == 0) continue;

                        List<Double> leafprob = new ArrayList<>(lnum);
                        for (int i = 0; i < lnum; ++i) leafprob.add(1.0);
                        for (Integer value : randomPick(leafprob)) {
                            int leaf = tryMe.indexOfOpenLeaf(value);
                            int bsnum = tryMe.numberOfBonusSquares(leaf);
                            if (bsnum == 0) continue;

                            List<Double> bsprob = new ArrayList<>(bsnum);
                            for (int i = 0; i < bsnum; ++i) bsprob.add(1.0);
                            for (Integer o : randomPick(bsprob)) {
                                int bs = o;

                                try {
                                    tryMe.useBonusSquare(leaf, bs);
                                    success = true;
                                    if (debugRules)
                                        System.out.println(rule + "/" + treeidx + "/" + leaf + "/" + bs);
                                    break tryTrees;
                                } catch (ConflictingBindException ignored) {
                                }
                            }
                        }
                    }
                }
            }

            if (!success) throw new RuntimeException("Can't find applicable rule");
        }

        FormulaDominoTree result = (FormulaDominoTree) trees.get(0);

        if (bound)
            try {
                String zstr = "Z";
                byte[] zdat = zstr.getBytes("ISO8859_1");
                final byte z = zdat[0];
                String astr = "A";
                byte[] adat = astr.getBytes("ISO8859_1");
                final byte a = adat[0];
                byte[] dat = {a};
                int unboundNum = result.numberOfUnboundVariablesInRoot();
                int unboundNum2 = unboundNum + result.numberOfUnboundVariablesInLeaves();
                String[] ids = new String[unboundNum2];
                for (int i = 0; i < unboundNum2; ++i) {
                    ids[i] = new String(dat, "ISO8859_1");
                    boolean append = true;
                    for (int j = 0; j < dat.length; ++j) {
                        if (dat[j] < z) {
                            dat[j]++;
                            append = false;
                            break;
                        }
                        dat[j] = a;
                    }
                    if (append) {
                        byte[] newdat = new byte[dat.length + 1];
                        System.arraycopy(dat, 0, newdat, 0, dat.length);
                        dat = newdat;
                        dat[dat.length - 1] = a;
                    }
                }

                result.bindUnboundVariablesInRoot(ids);
                unboundNum2 = result.numberOfUnboundVariablesInLeaves();
                String[] ids2 = new String[unboundNum2];
                System.arraycopy(ids, unboundNum, ids2, 0, unboundNum2);
                result.bindUnboundVariablesInLeaves(ids2);

            } catch (Exception x) {
                throw new RuntimeException("Unexpected exception: " + x);
            }

        return result;
    }

}

