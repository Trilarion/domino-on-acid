/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic;

import domino.logic.fsn.FSNIterator;
import domino.logic.syntaxnet.FormulaSyntaxNet;

import java.util.NoSuchElementException;

/**
 * <p>Iterator that never returns an element.
 */
public class EmptyFSNIterator implements FSNIterator {
    /**
     * <p>Returns {@code false}.</p>
     */
    public boolean hasNext() {
        return false;
    }

    /**
     * <p>Throws {@link NoSuchElementException}.</p>
     */
    public FormulaSyntaxNet next() {
        throw new NoSuchElementException();
    }

    /**
     * <p>Returns {@code false}.</p>
     */
    public boolean preorder() {
        return false;
    }

    /**
     * <p>Returns {@code false}.</p>
     */
    public boolean postorder() {
        return false;
    }

    /**
     * <p>Returns {@code false}.</p>
     */
    public boolean inorder() {
        return false;
    }

    /**
     * <p>Throws {@link IllegalStateException} .</p>
     *
     * @throws IllegalStateException
     */
    public void remove() {
        throw new IllegalStateException();
    }

}

