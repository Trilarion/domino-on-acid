/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic;

import domino.logic.square.FormulaDominoSquare;
import domino.logic.tree.FormulaDominoTree;

import java.util.ArrayList;
import java.util.HashSet;

public class BranchStringTree {
    public final String[] branch;
    /**
     * bonus[i] contains bonus Strings for branch[i]
     */
    public final ArrayList<HashSet<String>> bonus;

    public BranchStringTree(FormulaDominoTree tree) {
        int num = tree.numberOfOpenLeaves();
        branch = new String[num];
        bonus = new ArrayList<>(num);

        UnboundIdAssignerImpl assigner = UnboundIdAssigner.newAssigner();

        int bi = 0;
        for (int i = 0; i < tree.numberOfLeaves(); ++i) {
            if (tree.isOpenLeaf(i)) {
                branch[bi] = ((FormulaDominoSquare) tree.leafSquare(i)).toInfixRepresentation(assigner);
                bonus.set(bi, new HashSet<>());
                for (int j = 0; j < tree.numberOfBonusSquares(i); ++j) {
                    bonus.get(bi).add(((FormulaDominoSquare) tree.bonusSquare(i, j)).toInfixRepresentation(assigner));
                }
                bi++;
            }
        }
    }

    protected boolean hasBranchWorseThan(String bra, HashSet<String> bon) {
        for (int i = 0; i < branch.length; ++i) {
            if (branch[i].equals(bra)) {
                if (bon.containsAll(bonus.get(i))) return true;
            }
        }
        return false;
    }

    //-1 this is better, 0 incomparable, +1 bst2 is better
    public int compareTo(BranchStringTree bst2) {
        boolean thisIsBetter = true;
        for (int i = 0; i < branch.length; ++i) {
            if (!bst2.hasBranchWorseThan(branch[i], bonus.get(i))) {
                thisIsBetter = false;
                break;
            }
        }
        boolean bst2IsBetter = true;
        for (int i = 0; i < bst2.branch.length; ++i) {
            if (!hasBranchWorseThan(bst2.branch[i], bst2.bonus.get(i))) {
                bst2IsBetter = false;
                break;
            }
        }

        if (!thisIsBetter && !bst2IsBetter) return 0;
        if (thisIsBetter == bst2IsBetter)
            return bst2.branch.length < branch.length ? +1 : -1;
        return thisIsBetter ? -1 : +1;
    }

}
