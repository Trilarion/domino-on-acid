/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic;

/**
 * {@code IllegalNodeTypeException} is thrown when a method that only
 * works for some node types is called on a node that is unsupported by the
 * method.
 */
public class IllegalNodeTypeException extends RuntimeException {
    private static final long serialVersionUID = 5351012925959256326L;

    /**
     * Constructs an {@code IllegalNodeTypeException} with no detail message.
     */
    public IllegalNodeTypeException() {
    }

    /**
     * Constructs an {@code IllegalNodeTypeException} with the specified
     * detail message
     */
    public IllegalNodeTypeException(String s) {
        super(s);
    }

}

