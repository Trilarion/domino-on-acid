/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic;

import domino.logic.diagram.Diagram;
import domino.logic.diagram.FormulaDiagram;
import domino.logic.diagram.SeparatedDiagram;
import domino.logic.diagram.SeparatorDiagram;
import domino.logic.square.FormulaDominoSquare;
import domino.logic.tree.FormulaDominoTree;

/**
 * <p>Formats proof for output.</p>
 */
public final class ProofFormatter {
    /**
     * <p>Spaces between 2 formulas on the same line.</p>
     */
    public static final int SPACING = 4;

    private ProofFormatter() {
    }

    /**
     * <p>Converts {@code proof} into an array of strings to be printed
     * in ascending order to get a nice ASCII diagram.</p>
     */
    public static String[] naturalDeductionDiagram(FormulaDominoTree proof) {
        UnboundIdAssignerImpl assigner = UnboundIdAssigner.newAssigner();
        return naturalDeductionDiagramD(proof, assigner).toStringArray();
    }

    /**
     * <p>Converts {@code proof} into an array of strings to be printed
     * in ascending order to get a nice ASCII diagram.</p>
     * <p>{@code assigner} is used to assign names to unbound nodes.</p>
     */
    public static String[] naturalDeductionDiagram(FormulaDominoTree proof, UnboundIdAssignerImpl assigner) {
        return naturalDeductionDiagramD(proof, assigner).toStringArray();
    }

    private static Diagram naturalDeductionDiagramD(FormulaDominoTree proof, UnboundIdAssignerImpl assigner) {
        FormulaDominoSquare root = (FormulaDominoSquare) proof.rootSquare();

        int nsub = proof.numberOfSubtreeConnections();

        String rootInfixRep = root.toInfixRepresentation(assigner);

        if (nsub == 0) {
            return new FormulaDiagram("[" + rootInfixRep + "]");
        }

        Diagram[] subs = new Diagram[nsub];
        String[][] bonusStrings = new String[nsub][];
        for (int i = 0; i < nsub; ++i) {
            FormulaDominoTree subtree = (FormulaDominoTree) proof.subtree(i);
            if (subtree == null) {
                FormulaDominoSquare unconn = (FormulaDominoSquare) proof.subtreeConnectionSquare(i);
                subs[i] = new FormulaDiagram(unconn.toInfixRepresentation(assigner));
            } else {
                subs[i] = naturalDeductionDiagramD(subtree, assigner);
            }
            int nbon = proof.numberOfNewBonusSquaresForSubtreeConnection(i);
            bonusStrings[i] = new String[nbon];
            for (int j = 0; j < nbon; ++j) {
                FormulaDominoSquare newbon = (FormulaDominoSquare) proof.newBonusSquareForSubtreeConnection(i, j);
                bonusStrings[i][j] = "[" + newbon.toInfixRepresentation(assigner) + "]";
            }
        }

        return new SeparatedDiagram(rootInfixRep, new SeparatorDiagram(subs, bonusStrings));
    }

}


