/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.diagram;


import domino.logic.ProofFormatter;

public class SeparatorDiagram extends Diagram {
    private final Diagram[] subdiagrams_;
    private final int nsubs_;
    private final String[][] bonusStrings_;
    private boolean allHaveSameBonus_;
    private int lengthcache = -1;
    private int separatorbarminlengthcache = -1;

    public SeparatorDiagram(Diagram[] subs, String[][] bonusStrings) {
        subdiagrams_ = subs;
        nsubs_ = subs.length;
        bonusStrings_ = bonusStrings;

        allHaveSameBonus_ = true;
        for (int i = 1; i < nsubs_; ++i) {
            if (bonusStrings[0].length != bonusStrings[i].length) {
                allHaveSameBonus_ = false;
                break;
            }

            for (int j = 0; j < bonusStrings[0].length; ++j)
                if (!bonusStrings[0][j].equals(bonusStrings[i][j])) {
                    allHaveSameBonus_ = false;
                    break;
                }
        }

        for (int i = 0; i < nsubs_; ++i) subs[i].setParent(this);
    }

    protected int separatorBarMinLength() {
        if (separatorbarminlengthcache < 0) {
            int l = 0;
            for (int i = 0; i < nsubs_; ++i) {
                l += subdiagrams_[i].length() + ProofFormatter.SPACING;
            }
            l -= ProofFormatter.SPACING; //no spacing after last

            int l2 = parent_.minLength();
            if (l2 > l) l = l2;
            separatorbarminlengthcache = l;
        }
        return separatorbarminlengthcache;
    }

    protected int length() {
        if (lengthcache < 0) {
            int l = separatorBarMinLength();

            if (allHaveSameBonus_) {
                for (int i = 0; i < bonusStrings_[0].length; ++i)
                    l += bonusStrings_[0][i].length();
            } else {
                throw new RuntimeException("not implemented");
            }
            lengthcache = l;
        }

        return lengthcache;
    }

    protected int minLength() {
        return length();
    }

    protected String line() {
        if (allHaveSameBonus_)  // ----------------[f1][f2]
        {
            int l = separatorBarMinLength();
            StringBuilder s = new StringBuilder(l);
            while (l > 0) {
                l--;
                s.append('-');
            }
            l--;

            for (int i = 0; i < bonusStrings_[0].length; ++i) s.append(bonusStrings_[0][i]);

            return s.toString();
        } else  // ---[f11][f12]---[f21]---[f31][f32]---
        {
            throw new RuntimeException("not implemented");
        }
    }

    protected String[] subdiagramToStringArray() {
        String[][] sub = new String[nsubs_][];
        int[] len = new int[nsubs_];

        for (int i = 0; i < nsubs_; ++i) {
            sub[i] = subdiagrams_[i].toStringArray();
            len[i] = subdiagrams_[i].length() + ProofFormatter.SPACING;
        }
        len[nsubs_ - 1] -= ProofFormatter.SPACING; //no spacing after last

        int maxarraylen = sub[0].length;
        for (int i = 1; i < nsubs_; ++i)
            if (sub[i].length > maxarraylen) maxarraylen = sub[i].length;

        String[] res = new String[maxarraylen];
        for (int line = 0; line < maxarraylen; ++line) {
            StringBuilder sbuf = new StringBuilder();
            for (int i = 0; i < nsubs_; ++i) {
                int space = len[i];
                int subline = line + sub[i].length - maxarraylen;
                if (subline >= 0) {
                    String s = sub[i][subline];
                    sbuf.append(s);
                    space -= s.length();
                    // debugging test
                    if (space < 0)
                        throw new RuntimeException("subdiagrams_[i] has line that is longer than subdiagrams_[i].length()");
                }
                while (space > 0) {
                    space--;
                    sbuf.append(' ');
                }
                space--;
            }
            res[line] = sbuf.toString();
        }
        return res;
    }

}
