/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.diagram;

public class FormulaDiagram extends Diagram {
    private final String formula_;

    public FormulaDiagram(String formula) {
        formula_ = formula;
    }

    protected String line() {
        return formula_;
    }

    protected int minLength() {
        return formula_.length();
    }

    protected int length() {
        return formula_.length();
    }

    protected String[] subdiagramToStringArray() {
        return new String[0];
    }

}
