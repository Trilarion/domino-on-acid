/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic;

import domino.logic.syntaxnet.FormulaSyntaxNet;

/**
 * Connection between 2 {@code FormulaSyntaxNets}.
 */
public class ForwardConnection {
    private final FormulaSyntaxNet from_;
    private final FormulaSyntaxNet to_;

    /**
     * Creates a {@code ForwardConnection} for connecting {@code from}
     * with {@code to}. Creating the {@code ForwardConnection} object
     * does not actually make a connection between the 2
     * {@code FormulaSyntaxNet}s. Use {@link FormulaSyntaxNet#addForwardUnsafe(FormulaSyntaxNet)}
     * for that.
     *
     * @see FormulaSyntaxNet#addForwardUnsafe(FormulaSyntaxNet)
     */
    public ForwardConnection(FormulaSyntaxNet from, FormulaSyntaxNet to) {
        from_ = from;
        to_ = to;
    }

    /**
     * Returns the target node of the connection.
     */
    public FormulaSyntaxNet to() {
        return to_;
    }

    /**
     * Returns the source node of the connection.
     */
    public FormulaSyntaxNet from() {
        return from_;
    }

}
