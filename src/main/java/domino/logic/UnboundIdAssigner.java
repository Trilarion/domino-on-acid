/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic;

import domino.logic.tree.FormulaDominoTree;

/**
 * <p>Used to assign numbers to unbound leaves of formulas.
 * {@code UnboundIdAssigner}s are used to make sure ids assigned to
 * unbound leaves are unique in the whole tree and the same ids are used for the
 * same nodes for independent calls to functions that generate a representation
 * of the whole tree or a part of it for displaying.</p>
 */
public class UnboundIdAssigner {
    /**
     * <p>{@code UnboundIdAssigner} may not be instantiated directly.</p>
     */
    protected UnboundIdAssigner() {
    }

    /**
     * <p>Returns a new {@code UnboundIdAssigner}.</p>
     */
    public static UnboundIdAssignerImpl newAssigner() {
        return new UnboundIdAssignerImpl();
    }


    /**
     * <p>Clears all mappings.</p>
     */
    public void reset() {
    }


    /**
     * <p>Removes mappings for the unbound nodes in formulas in {@code tree}.</p>
     * <p>If {@code tree} is attached to another tree, mappings in that tree
     * that affect {@code tree} through unification, will also be removed.
     * If this is not desirable, {@link #removeMappingsStrictlyFor(FormulaDominoTree)}
     * should be used instead.</p>
     */
    public void removeMappingsFor(FormulaDominoTree tree) {
    }

    /**
     * <p>Removes mappings for the unbound nodes in formulas in {@code tree} if
     * they are unneeded because other mappings or unifications with bound or
     * branch nodes supercede them.</p>
     * <p>If {@code tree} is attached to another tree, mappings in that tree
     * that affect {@code tree} through unification, will also be removed.</p>
     */
    public void removeUnneededMappingsFor(FormulaDominoTree tree) {
    }

    /**
     * <p>Removes mappings for the unbound nodes in formulas in {@code tree}.</p>
     * <p>Only mappings for nodes connected through branch connections will be affected.
     * Mappings for nodes connected through forward connections, even if they affect
     * {@code tree}, will not be touched.
     * If these mappings should be removed, too,
     * {@link #removeMappingsFor(FormulaDominoTree)} should be used instead.</p>
     */
    public void removeMappingsStrictlyFor(FormulaDominoTree tree) {
    }

    // TODO WHY DID I NOT MAKE THIS CLASS ABSTRACT ????  I AM SURE I HAD A REASON.

}
