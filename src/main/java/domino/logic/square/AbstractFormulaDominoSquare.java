/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.square;

import domino.logic.UnboundIdAssignerImpl;
import domino.logic.fsn.FSNIterator;
import domino.logic.syntaxnet.FormulaSyntaxNet;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Parent class of {@link FormulaDominoSquare}s.</p>
 */
public abstract class AbstractFormulaDominoSquare implements FormulaDominoSquare {
    /**
     * <p>Returns this square's {@link FormulaSyntaxNet}.</p>
     */
    protected abstract FormulaSyntaxNet net();

    public Object toDefaultRepresentation() {
        return toInfixRepresentation(null);
    }

    /**
     * <p>Returns an infix formula string for the square's formula.</p>
     */
    public String toInfixRepresentation() {
        return toInfixRepresentation(null);
    }

    /**
     * <p>Returns an infix formula string for the square's formula.</p>
     *
     * @param assign If this is {@code null}, unbound nodes will be numbered
     *               starting from 0. Otherwise, the assigner will be used to determine the
     *               numbers. The assigner stores the assignments so that by reusing the
     *               same assigner in different calls to this function you can achieve
     *               a consistent numbering of unbound nodes.
     */
    public String toInfixRepresentation(UnboundIdAssignerImpl assign) {
        StringBuilder s = new StringBuilder();
        FSNIterator i = net().multiorderIterator(true, true, true);
        Map<FormulaSyntaxNet, String> map = null;
        if (assign == null) map = new HashMap<>(); //maps FormulaSyntaxNets to String names
        int unboundCount = 0; //number of unbound leaves encountered so far
        while (i.hasNext()) {
            FormulaSyntaxNet node = i.next();
            if (node.isBranch()) {
                if (i.preorder()) s.append('(');
                if (i.inorder()) s.append("->");
                if (i.postorder()) s.append(')');
            } else if (node.isBound()) s.append(node.id());
            else if (node.isUnboundOrForward()) {
                String id;
                if (assign != null)
                    id = "X" + assign.getIdFor(node);
                else {
                    id = map.get(node);
                    if (id == null) {
                        id = "X" + (unboundCount);
                        unboundCount++;
                        map.put(node, id);
                    }
                }
                s.append(id);
            } else throw new RuntimeException("Huh? What's this node?"); //just to be safe
        }
        return s.toString();
    }

    /**
     * <p>Returns the prefix representation of the {@code FormulaDominoSquare}.</p>
     */
    public String toPrefixRepresentation() {
        return toPrefixRepresentation(null);
    }

    /**
     * <p>Returns a prefix formula string for the square's formula.</p>
     *
     * @param assign If this is {@code null}, unbound nodes will be numbered
     *               starting from 0. Otherwise, the assigner will be used to determine the
     *               numbers. The assigner stores the assignments so that by reusing the
     *               same assigner in different calls to this function you can achieve
     *               a consistent numbering of unbound nodes.
     */
    public String toPrefixRepresentation(UnboundIdAssignerImpl assign) {
        return toPrePostfixRepresentation(assign, net().preorderIterator());
    }

    /**
     * <p>Returns the postfix representation of the {@code FormulaDominoSquare}.</p>
     */
    public String toPostfixRepresentation() {
        return toPostfixRepresentation(null);
    }

    /**
     * <p>Returns a postfix formula string for the square's formula.</p>
     *
     * @param assign If this is {@code null}, unbound nodes will be numbered
     *               starting from 0. Otherwise, the assigner will be used to determine the
     *               numbers. The assigner stores the assignments so that by reusing the
     *               same assigner in different calls to this function you can achieve
     *               a consistent numbering of unbound nodes.
     */
    public String toPostfixRepresentation(UnboundIdAssignerImpl assign) {
        return toPrePostfixRepresentation(assign, net().postorderIterator());
    }

    /**
     * <p>If {@code i} is a preorder iterator, this function performs as
     * {@link #toPrefixRepresentation(UnboundIdAssignerImpl)} and if
     * {@code i} is a postorder iterator, it performs as
     * {@link #toPostfixRepresentation(UnboundIdAssignerImpl)}.</p>
     */
    protected String toPrePostfixRepresentation(UnboundIdAssignerImpl assign, FSNIterator i) {
        StringBuilder s = new StringBuilder();
        Map<FormulaSyntaxNet, String> map = null;
        if (assign == null) map = new HashMap<>(); //maps FormulaSyntaxNets to String names
        int unboundCount = 0; //number of unbound leaves encountered so far
        while (i.hasNext()) {
            FormulaSyntaxNet node = i.next();
            if (node.isBranch())
                s.append("->");
            else if (node.isBound())
                s.append("(").append(node.id()).append(")");
            else if (node.isUnboundOrForward()) {
                String id;
                if (assign != null)
                    id = "X" + assign.getIdFor(node);
                else {
                    id = map.get(node);
                    if (id == null) {
                        id = "X" + (unboundCount);
                        unboundCount++;
                        map.put(node, id);
                    }
                }
                s.append("(").append(id).append(")");
            } else throw new RuntimeException("Huh? What's this node?"); //just to be safe
        }
        return s.toString();
    }

}
