/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.square;

import domino.logic.syntaxnet.FormulaSyntaxNet;
import domino.logic.tree.FormulaDominoTree;

public class BonusFormulaDominoSquare extends AbstractFormulaDominoSquare {
    private final FormulaDominoTree formulaDominoTree;
    private final int bonusindex_;

    public BonusFormulaDominoSquare(FormulaDominoTree formulaDominoTree, int bonusindex) {
        this.formulaDominoTree = formulaDominoTree;
        bonusindex_ = bonusindex;
    }


    protected FormulaSyntaxNet net() {
        return formulaDominoTree.bonus_[bonusindex_];
    }

}
