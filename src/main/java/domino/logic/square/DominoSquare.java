/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.square;

import domino.logic.tree.DominoTree;

/**
 * <p>This interface is implemented by the classes that deal with single domino
 * squares.</p>
 * <p>As {@code DominoSquare}s are publicly exposed through the
 * {@link DominoTree} interface, they should only
 * deal with (textual or visual) representation issues but should not allow access
 * to the underlying low-level structures.</p>
 */
public interface DominoSquare {
    /**
     * <p>Returns the default representation of the {@code DominoSquare}.</p>
     */
    Object toDefaultRepresentation();
}



