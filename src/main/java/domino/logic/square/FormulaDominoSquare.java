/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.square;

import domino.logic.UnboundIdAssignerImpl;
import domino.logic.tree.FormulaDominoTree;

/**
 * <p>This interface is implemented by the classes that deal with single domino
 * squares part of a {@link FormulaDominoTree}.</p>
 */
public interface FormulaDominoSquare extends DominoSquare {
    /**
     * <p>Returns the default representation of the {@code FormulaDominoSquare}.</p>
     */
    Object toDefaultRepresentation();

    /**
     * <p>Returns the infix representation of the {@code FormulaDominoSquare}.</p>
     */
    String toInfixRepresentation();

    /**
     * <p>Returns an infix formula string for the square's formula.</p>
     *
     * @param assign If this is {@code null}, unbound nodes will be numbered
     *               starting from 0. Otherwise, the assigner will be used to determine the
     *               numbers. The assigner stores the assignments so that by reusing the
     *               same assigner in different calls to this function you can achieve
     *               a consistent numbering of unbound nodes.
     */
    String toInfixRepresentation(UnboundIdAssignerImpl assign);

    /**
     * <p>Returns the prefix representation of the {@code FormulaDominoSquare}.</p>
     */
    String toPrefixRepresentation();

    /**
     * <p>Returns a prefix formula string for the square's formula.</p>
     *
     * @param assign If this is {@code null}, unbound nodes will be numbered
     *               starting from 0. Otherwise, the assigner will be used to determine the
     *               numbers. The assigner stores the assignments so that by reusing the
     *               same assigner in different calls to this function you can achieve
     *               a consistent numbering of unbound nodes.
     */
    String toPrefixRepresentation(UnboundIdAssignerImpl assign);

    /**
     * <p>Returns the postfix representation of the {@code FormulaDominoSquare}.</p>
     */
    String toPostfixRepresentation();

    /**
     * <p>Returns a postfix formula string for the square's formula.</p>
     *
     * @param assign If this is {@code null}, unbound nodes will be numbered
     *               starting from 0. Otherwise, the assigner will be used to determine the
     *               numbers. The assigner stores the assignments so that by reusing the
     *               same assigner in different calls to this function you can achieve
     *               a consistent numbering of unbound nodes.
     */
    String toPostfixRepresentation(UnboundIdAssignerImpl assign);
}



