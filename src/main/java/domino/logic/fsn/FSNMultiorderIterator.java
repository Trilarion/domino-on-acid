/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.fsn;

import domino.logic.stackElem;
import domino.logic.syntaxnet.FormulaSyntaxNet;

import java.util.ArrayDeque;
import java.util.ConcurrentModificationException;
import java.util.Deque;
import java.util.NoSuchElementException;

/**
 * <p>Iterates over the nodes of a {@link FormulaSyntaxNet}. It visits bound
 * and unbound leaves and branch nodes
 * in pre/in/postorder sequence.</p>
 * <p>Forward nodes will not be visited unless all nodes
 * equivalent to the forward node are also forward nodes. In that case,
 * the forward node that is visited is the one with the lowest {@link FormulaSyntaxNet#uid()}.
 * </p>
 * <p>For every set of equivalent nodes at the same position in the net
 * only one representative will be visited. Equivalent nodes in different
 * branches of the net will be visited multiple times.</p>
 * <p>
 * An {@code FSNMultiorderIterator} fails with a
 * {@link ConcurrentModificationException} if the underlying
 * {@link FormulaSyntaxNet} is modified.
 *
 * @see FormulaSyntaxNet#preorderIterator()
 * @see FormulaSyntaxNet#inorderIterator()
 * @see FormulaSyntaxNet#postorderIterator()
 * @see FormulaSyntaxNet#multiorderIterator(boolean, boolean, boolean)
 */
public class FSNMultiorderIterator implements FSNIterator {
    private final Deque<stackElem> branchStack_ = new ArrayDeque<>();
    private final boolean preorderStop_;
    private final boolean postorderStop_;
    private final boolean inorderStop_;
    /**
     * <p>0: visiting current node for the 1st time <br>
     * 1: visiting again after visiting left child  <br>
     * 2: visiting again after visiting right child <br>
     * 3: node is leaf node .<p>
     */
    private int order_;

    /**
     * <p>Creates an iterator that visits bound
     * and unbound leaves and branch nodes
     * in pre/in/postorder sequence.</p>
     * <p>Forward nodes will not be visited unless all nodes
     * equivalent to the forward node are also forward nodes. In that case,
     * the forward node that is visited is the one with the lowest {@link FormulaSyntaxNet#uid()}.
     * </p>
     * <p>For every set of equivalent nodes at the same position in the net
     * only one representative will be visited. Equivalent nodes in different
     * branches of the net will be visited multiple times.</p>
     *
     * <p>{@code pre}, {@code in} and {@code post} determine
     * whether and when the iterator will return branch nodes. If all 3
     * values are {@code false}, only leaf nodes will be returned. If
     * more than one is {@code true}, branch nodes will be visited
     * multiple times.</p>
     *
     * @param net  the {@link FormulaSyntaxNet} to iterate over. May be
     *             {@code null}.
     * @param pre  stop at branch node before visiting left child
     * @param in   stop at branch node before visiting right child
     * @param post stop at branch node after visiting right child
     * @see FormulaSyntaxNet#preorderIterator()
     * @see FormulaSyntaxNet#inorderIterator()
     * @see FormulaSyntaxNet#postorderIterator()
     * @see FormulaSyntaxNet#multiorderIterator(boolean, boolean, boolean)
     */
    public FSNMultiorderIterator(FormulaSyntaxNet net, boolean pre, boolean in, boolean post) {
        preorderStop_ = pre;
        inorderStop_ = in;
        postorderStop_ = post;
        if (net != null) branchStack_.push(new stackElem(net, 0, 0));
    }

    /**
     * <p>Returns true if the iteration has more elements.</p>
     */
    public boolean hasNext() {
        return !branchStack_.isEmpty();
    }

    /**
     * <p>Returns the next element in the iteration.</p>
     *
     * @return the next element in the iteration.
     * @throws NoSuchElementException if the iteration has no more elements.
     */
    public FormulaSyntaxNet next() {
        FormulaSyntaxNet next_ = null;
        boolean getNextFSN = true;
        while (getNextFSN) {
            getNextFSN = false;

            if (!hasNext()) throw new NoSuchElementException();
            stackElem e = branchStack_.pop();

            next_ = e.node;

            if (next_.isBranch()) {
                if (e.order == 0) {
                    branchStack_.push(new stackElem(next_, 1, next_.dirtyNumberForwards()));
                    branchStack_.push(new stackElem(next_.left(), 0, 0));
                    order_ = 0;
                    if (!preorderStop_) getNextFSN = true;
                } else {
                    if (e.dirtyNumber != next_.dirtyNumberForwards())
                        throw new ConcurrentModificationException();

                    if (e.order == 1) {
                        if (postorderStop_) branchStack_.push(new stackElem(next_, 2, e.dirtyNumber));
                        branchStack_.push(new stackElem(next_.right(), 0, 0));
                        order_ = 1;
                        if (!inorderStop_) getNextFSN = true;
                    } else //if (e.order==2)
                    {
                        order_ = 2;
                        //next_=next_;
                    }
                }
            } else if (next_.isForward()) {
      /* equivalent forward nodes and the nodes they point to are always
      fully connected by unify(), so if there's any non-forward node
      equivalent to next_, it's in next_'s forwards() list */

                FormulaSyntaxNet equivalentNodeWithLowestUID = next_;
                long lowestUID = next_.uid();
                FSNIterator i = next_.forwards();
                while (i.hasNext()) {
                    FormulaSyntaxNet n = i.next();
                    if (!n.isForward()) {
                        next_ = n;
                        break;
                    }
                    if (n.uid() < lowestUID) {
                        equivalentNodeWithLowestUID = n;
                        lowestUID = n.uid();
                    }
                }

                //if we found a non-forward node, we have to process it in place
                //of the current forward node
                if (!next_.isForward()) {
                    branchStack_.push(new stackElem(next_, 0, 0));
                    getNextFSN = true;
                } else //otherwise we have to pick the forward node with lowest uid.
                {
                    next_ = equivalentNodeWithLowestUID;
                    order_ = 3;
                }
            } else //if (next_.isUnbound() || next_.isBound())
            {
                order_ = 3;
            }
        }

        return next_;
    }

    /**
     * <p>If the node returned by the most recent call to
     * {@link #next()} is a branch, this method returns
     * true if it has to be
     * processed now in a preorder iteration. If the node returned is a leaf,
     * this method always returns true.</p>
     * <p>If {@link #next()} was not called prior to
     * calling this method, its return value is unspecified.</p>
     */
    public boolean preorder() {
        return (order_ == 0 || order_ == 3);
    }

    /**
     * <p>If the node returned by the most recent call to
     * {@link #next()} is a branch, this method returns
     * true if it has to be
     * processed now in a postorder iteration. If the node returned is a leaf,
     * this method always returns true.</p>
     * <p>If {@link #next()} was not called prior to
     * calling this method, its return value is unspecified.</p>
     */
    public boolean postorder() {
        return (order_ == 2 || order_ == 3);
    }

    /**
     * <p>If the node returned by the most recent call to
     * {@link #next()} is a branch, this method returns
     * true if it has to be
     * processed now in an inorder iteration. If the node returned is a leaf,
     * this method always returns true.</p>
     * <p>If {@link #next()} was not called prior to
     * calling this method, its return value is unspecified.</p>
     */
    public boolean inorder() {
        return (order_ == 1 || order_ == 3);
    }

    /**
     * <p>Throws {@link UnsupportedOperationException} .</p>
     *
     * @throws UnsupportedOperationException
     */
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
