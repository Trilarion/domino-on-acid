/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.fsn;

import domino.logic.syntaxnet.FormulaSyntaxNet;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * <p>Iterates over the {@link FormulaSyntaxNet}s iterated over by
 * an {@link Iterator Iterator}</p>
 */
public class FSNIteratorFromIterator implements FSNIterator {
    private final Iterator<FormulaSyntaxNet> iterator_;

    /**
     * <p>Creates an iterator to iterate over the {@code FormulaSyntaxNet}s
     * iterated over by {@code i}.</p>
     *
     * <p>Note that operations on the iterator {@code i} affect the
     * {@code FSNIteratorFromIterator}.</p>
     */
    public FSNIteratorFromIterator(Iterator<FormulaSyntaxNet> i) {
        iterator_ = i;
    }

    /**
     * <p>Returns true if the iteration has more elements.</p>
     */
    public boolean hasNext() {
        return iterator_.hasNext();
    }

    /**
     * <p>Returns the next element in the iteration.</p>
     *
     * @return the next element in the iteration.
     * @throws NoSuchElementException if the iteration has no more elements.
     */
    public FormulaSyntaxNet next() {
        return iterator_.next();
    }

    /**
     * <p>Throws {@link UnsupportedOperationException} .</p>
     *
     * @throws UnsupportedOperationException
     */
    public void remove() {
        throw new UnsupportedOperationException();
    }

    /**
     * Always returns {@code true}.
     */
    public boolean inorder() {
        return true;
    }

    /**
     * Always returns {@code true}.
     */
    public boolean preorder() {
        return true;
    }

    /**
     * Always returns {@code true}.
     */
    public boolean postorder() {
        return true;
    }

}
