/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.fsn;

import domino.logic.syntaxnet.FormulaSyntaxNet;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

/**
 * Iterates over a collection of {@code FormulaSyntaxNets}.
 * An {@code FSNIterator} fails with a
 * {@link ConcurrentModificationException} if the underlying collection is
 * modified. Note that this does not prevent all modifications anywhere in
 * the {@code FormulaSyntaxNet}. Only modifications that add to or remove
 * from the list of {@code FormulaSyntaxNet}s that the
 * {@code FSNIterator} iterates over will invalidate the iterator.
 */
public interface FSNIterator extends Iterator<FormulaSyntaxNet> {
    /**
     * Returns true if the iteration has more elements.
     */
    boolean hasNext();

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration.
     * @throws java.util.NoSuchElementException if the iteration has no more elements.
     */
    FormulaSyntaxNet next();

    /**
     * <p>If the node returned by the most recent call to
     * {@link #next()} is a branch, this method returns
     * true if it has to be
     * processed now in a preorder iteration. If the node returned is a leaf,
     * this method always returns true.</p>
     * <p>If {@link #next()} was not called prior to
     * calling this method, its return value is unspecified.</p>
     */
    boolean preorder();

    /**
     * <p>If the node returned by the most recent call to
     * {@link #next()} is a branch, this method returns
     * true if it has to be
     * processed now in a postorder iteration. If the node returned is a leaf,
     * this method always returns true.</p>
     * <p>If {@link #next()} was not called prior to
     * calling this method, its return value is unspecified.</p>
     */
    boolean postorder();

    /**
     * <p>If the node returned by the most recent call to
     * {@link #next()} is a branch, this method returns
     * true if it has to be
     * processed now in an inorder iteration. If the node returned is a leaf,
     * this method always returns true.</p>
     * <p>If {@link #next()} was not called prior to
     * calling this method, its return value is unspecified.</p>
     */
    boolean inorder();
}
