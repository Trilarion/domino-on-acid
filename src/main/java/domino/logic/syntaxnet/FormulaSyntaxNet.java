/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.syntaxnet;


import domino.logic.*;
import domino.logic.fsn.FSNIterator;
import domino.logic.fsn.FSNIteratorFromFConnIterator;
import domino.logic.fsn.FSNIteratorFromIterator;
import domino.logic.fsn.FSNMultiorderIterator;

import java.util.*;

/**
 * This class represents logical formulae with shared propositional variables
 * as a net (a forest of interconnected trees).
 * <p><b>Neither this class nor its iterators are thread-safe! Note that
 * synchronizing on a FormulaSyntaxNet does not help, because it will only
 * protect one node of the net. You will need an external semaphore to control
 * access to the whole net, if you want to use it from multiple threads.
 * </b></p>
 * <p>
 * The following node
 * types may be present in a {@code FormulaSyntaxNet}:
 * <dl>
 * <dt>bound leaf</dt>
 *   <dd>
 *     A bound leaf is a leaf node (a node with no outgoing edges) tagged with
 *     the name of a propositional variable.
 *   </dd>
 *
 * <dt>unbound leaf</dt>
 *   <dd>
 *     An unbound leaf is a placeholder for a subformula. When a
 *     {@code FormulaSyntaxNet} is unified with another one, unbound leaves are linked
 *     to the corresponding nodes in the other {@code FormulaSyntaxNet}.
 *   </dd>
 *
 * <dt>branch</dt>
 *   <dd>
 *     A branch is a node with 2 outgoing edges (left and right). The branch
 *     node represents an implication. The left edge points at the premise and
 *     the right edge at the conclusion.
 *   </dd>
 *
 * <dt>forward</dt>
 *   <dd>
 *     A forward node is created during unification when an unbound leaf is
 *     identified with a node in another {@code FormulaSyntaxNet}. A forward node
 *     points to one or more {@code FormulaSyntaxNet}s that are all equivalent. For
 *     the purpose of processing the formula(e) represented by a given
 *     {@code FormulaSyntaxNet}, a forward node is to be replaced by one of the
 *     {@code FormulaSyntaxNet}s it points to that is not a forward node.<br>
 *     Note that a forward node never points to itself.
 *   </dd>
 * <dt></dt>
 * </dl>
 * <p><b>{@code FormulaSyntaxNet}s must never be recursive (i.e. no branch node
 * may point to a node that is equivalent to a direct or indirect parent node of
 * the branch node). For such a {@code FormulaSyntaxNet}, all methods have
 * undefined behaviour.</b><p>
 */
public final class FormulaSyntaxNet {
    /**
     * The number that will be assigned to the {@code uid_} of the next
     * {@code FormulaSyntaxNet} that is created.
     */
    private static long uidsource_ = 0; // TODO atomic long to make safe for multi-threading
    /**
     * A unique identifier. No 2 {@code FormulaSyntaxNet}s have the same
     * {@code uid_} value.
     */
    private final long uid_ = newUID();
    /**
     * The {@code backwards_} list stores references to all
     * {@code FormulaSyntaxNet}s
     * that point to this {@code FormulaSyntaxNet}.
     *
     * @see #forwards_
     * @see #left_
     * @see #right_
     */
    private final List<FormulaSyntaxNet> backwards_ = new LinkedList<>();
    /**
     * Points to the {@code FormulaSyntaxNet} that specifies the
     * premise of an implication (branch node).
     *
     * <p>See {@link #forwards_} for details on how this variable influences the
     * type of a {@code FormulaSyntaxNet} node.</p>
     * <p>
     * p>
     * b>Invariants:</b>
     * <dl>
     * <dt>{@code  if (left_!=null)        }</dt>
     * <dd><code>  right_!=null &&   		<br>
     *             left_!=right_ &&               <br>
     *             id_==null && forwards_==null   </code></dd>
     * </dl>
     * </p>
     *
     * @see #forwards_
     * @see #id_
     * @see #right_
     * @see FormulaSyntaxNet {@code FormulaSyntaxNet} description
     */
    private final FormulaSyntaxNet left_;
    /**
     * Points to the {@code FormulaSyntaxNet} that specifies the
     * conclusion of an implication (branch node).
     *
     * <p>See {@link #forwards_} for details on how this variable influences the
     * type of a {@code FormulaSyntaxNet} node.</p>
     * <p>
     * p>
     * b>Invariants:</b>
     * <dl>
     * <dt>{@code  if (right_!=null)        }</dt>
     * <dd><code>  left_!=null &&   		<br>
     *             left_!=right_ &&               <br>
     *             id_==null && forwards_==null   </code></dd>
     * </dl>
     * </p>
     *
     * @see #forwards_
     * @see #id_
     * @see #left_
     * @see FormulaSyntaxNet {@code FormulaSyntaxNet} description
     */
    private final FormulaSyntaxNet right_;
    /**
     * Name of the propositional variable a bound leaf represents.
     *
     * <p>See {@link #forwards_} for details on how this variable influences the
     * type of a {@code FormulaSyntaxNet} node.</p>
     * <p>
     * p>
     * b>Invariants:</b>
     * <dl>
     * <dt>{@code  if (id_!=null)        }</dt>
     * <dd>{@code  left_==null && right_==null && forwards_==null   }</dd>
     * </dl>
     * </p>
     *
     * @see #forwards_
     * @see #left_
     * @see #right_
     * @see FormulaSyntaxNet {@code FormulaSyntaxNet} description
     */
    private final String id_;
    /**
     * The owner of this {@code FormulaSyntaxNet}. Every
     * {@code FormulaSyntaxNet} has an owner. The owner can be any
     * {@link Object}.
     *
     * <p><b>Invariants:</b> {@code owner_!=null}</p>
     *
     * @see #owner()
     */
    private final Object owner_;
    /**
     * A list of {@link ForwardConnection} objects that point to {@code FormulaSyntaxNet}s
     * that are all equivalent, one of which (that is not a forward node) must
     * be picked and substituted for the forward node, when processing
     * a {@code FormulaSyntaxNet}.
     *
     * <p>
     * Together with the variables {@link #id_}, {@link #left_} and {@link #right_},
     * {@code forwards_} determines the type of node
     * (see {@link FormulaSyntaxNet class description}) as follows:
     * </p>
     * <ul>
     *   <li>A node with {@code id_!=null} is a propositional variable with
     *     name id_  (a bound leaf). </li>
     *   <li>A node with {@code forwards_!=null} is a forward node. </li>
     *   <li>A node with {@code left_!=null && right_!=null} is a branch. </li>
     *   <li>A node with all 4 variables {@code ==null} is an unbound leaf. </li>
     * </ul>
     *
     *  <p>
     *  <b>Invariants:</b>
     *  <dl>
     *  <dt>{@code	if (forwards_!=null)                }</dt>
     *  <dd>{@code	forwards_.isEmpty()==false &&            }</dd>
     *  <dd>{@code	id_==null && left_==null && right_==null	}</dd>
     *  </dl>
     *
     * @see #id_
     * @see #left_
     * @see #right_
     * @see FormulaSyntaxNet {@code FormulaSyntaxNet} description
     */
    protected List<ForwardConnection> forwards_;
    /**
     * <p>Incremented by 1 for every modification that invalidates iterators over
     * {@link #forwards_} (for forward nodes) and iterators over
     * {@link #left_} and {@link #right_} (for branch nodes).</p>
     *
     * <p><b>This variable is not used by all iterators. Some rely on the
     * underlying java.util.* collection for throwing
     * {@link ConcurrentModificationException}</b></p>
     */
    private int forwardsDirty_;

    /**
     * <p>Incremented by 1 for every modification that invalidates iterators over
     * {@link #backwards_}.</p>
     *
     * <p><b>This variable is not used by all iterators. Some rely on the
     * underlying java.util.* collection for throwing
     * {@link ConcurrentModificationException}</b></p>
     */
    private int backwardsDirty_;

    /**
     * <p>Constructor for internal use only. Use the {@code new*()} methods
     * to create {@code FormulaSyntaxNet}s.</p>
     *
     * <p><b>Does not call {@link #checkInvariant()}</b>.</p>
     *
     * @see #newBranch(Object, FormulaSyntaxNet, FormulaSyntaxNet)
     * @see #newLeaf(Object, String)
     * @see #newFromInfixFormulaString(Object, String)
     */
    private FormulaSyntaxNet(String id, FormulaSyntaxNet left, FormulaSyntaxNet right, Object owner) {
        id_ = id;
        left_ = left;
        right_ = right;
        owner_ = owner;
    }

    /**
     * <p>Creates and returns a new unique identifier.</p>
     */
    private static synchronized long newUID() {
        long l = uidsource_;
        uidsource_++;
        return l;
    }

    /**
     * <p>Tests whether {@code a} and {@code b} are equivalent.</p>
     * <p>This method always walks through the whole net, even if {@code a}
     * and {@code b} are the same {@link Object}. This allows using this
     * function to check the consistency of a {@code FormulaSyntaxNet}.</p>
     *
     * @return {@code true} if {@code a} and {@code b} are
     * equivalent.
     * @see #checkForwardConsistency()
     */
    public static boolean isEquivalent(FormulaSyntaxNet a, FormulaSyntaxNet b) {
        return isEquivalent(a, b, new HaveVisitedPair());
    }

    /**
     * <p>See {@link #isEquivalent(FormulaSyntaxNet, FormulaSyntaxNet)}.</p>
     *
     * @param a           {@code FormulaSyntaxNet} to check for equivalence
     * @param b           {@code FormulaSyntaxNet} to check for equivalence
     * @param haveVisited used internally - pass {@code new HaveVisitedPair()}
     * @return {@code true} if {@code a} and {@code b} are
     * equivalent.
     */
    private static boolean isEquivalent(FormulaSyntaxNet a, FormulaSyntaxNet b, HaveVisitedPair haveVisited) {
        //do !!NOT!! short-cut if a==b (see documentation)
        if (haveVisited.includesUnordered(a, b)) return true;

    /* Note regarding haveVisited: Infinite loops can only occur if a
       group of equivalent forward nodes point to each other. All of these
       forward nodes are located on the same level of the net (i.e. the same
       distance to the tree roots if only branch nodes are counted). Because
       forward nodes on different levels can never point to each other, we
       only need to keep track of the forward nodes visited since the last
       pair of branch/bound nodes.
       So with every pair of branch/bound nodes we encounter on our recursive
       walk through the net we reset haveVisited and we only add pairs to
       haveVisited that have at least one forward or unbound node.
    */

        //if we have at least one bound node or branch, make sure it is a
        if (b.isBoundOrBranch()) {
            FormulaSyntaxNet c = a;
            a = b;
            b = c;
        }

        if (a.isBoundOrBranch()) {
            if (b.isBoundOrBranch()) {
                if ((a.isBound() != b.isBound()) ||
                        (a.isBound() && !a.id().equals(b.id()))
                ) return false;

                if (a.isBranch()) {
                    return isEquivalent(a.left(), b.left(), new HaveVisitedPair()) &&
                            isEquivalent(a.right(), b.right(), new HaveVisitedPair());
                }
            } else //if b.isUnboundOrForward()
            {
                haveVisited.addUnordered(a, b);
                FSNIterator i = b.forwards();
                boolean foundNonUnboundForward = false;
                while (i.hasNext()) {
                    FormulaSyntaxNet n = i.next();
                    if (!n.isUnboundOrForward()) foundNonUnboundForward = true;
                    if (!isEquivalent(a, n, haveVisited)) return false;
                }

                //Because of haveVisited, all isEquivalent() calls above would return true
                //if all of the forward connections are just to other forward nodes. Therefore
                //we test if there was at least one real forward.
                //This also catches the error that b is an unbound node with no forward
                //connections.
                return foundNonUnboundForward;
            }
        } else if (a.isUnboundOrForward()) { //because of the swapping at the beginning of this method, b is neither
            //bound nor branch node, so b.isUnboundOrForward() is true.

            haveVisited.addUnordered(a, b);

            //if we have at least one forward node, make sure it is a
            if (b.isForward()) {
                FormulaSyntaxNet c = a;
                a = b;
                b = c;
            }

            if (b.isUnbound()) {
                //unbound nodes are only equivalent if they are the same Object
                //(because otherwise they could get out of sync)
                return a == b;
            } else //if (b.isForward())
            {
                FSNIterator i = a.forwards();
                FSNIterator j = b.forwards();
                if (!(i.hasNext() && j.hasNext())) return false; //check for buggy iterators
                while (j.hasNext()) if (!isEquivalent(a, j.next(), haveVisited)) return false;
                while (i.hasNext()) {
                    FormulaSyntaxNet c = i.next();
                    if (!isEquivalent(c, b, haveVisited)) return false;
                    j = b.forwards();
                    while (j.hasNext())
                        if (!isEquivalent(c, j.next(), haveVisited)) return false;
                }
            }
        }
        return true;
    }

    /**
     * Checks {@code a} and {@code b} for compatibility and crosslinks
     * unbound and forward nodes with their corresponding nodes from the respective
     * other net.
     *
     * <p>After a successful call to {@code unify()},
     * {@code a} and {@code b} are equivalent.</p>
     *
     * <p>{@code unify} either succeeds completely or throws an exception and
     * leaves {@code a} and {@code b} (and other nodes visited during
     * the unification) unchanged.</p>
     *
     * <p>This method may invalidate iterators related to {@code this} and
     * to nodes reachable from {@code this} by following forward connections.
     * Do not use any such iterators after calling this method.</p>
     *
     * <p>Unifying a {@code FormulaSyntaxNet} with one of its
     * subnets would create an infinite recursion in the formula represented by the
     * net, so it is disallowed.</p>
     *
     * @throws ConflictingBindException if {@code a} and {@code b}
     *                                  can not be unified because at least one bound leaf in {@code a}
     *                                  or {@code b} does not match a corresponding (bound or unbound)
     *                                  leaf in the other one, or because a node would have to be unified with
     *                                  a direct or indirect parent.
     */
    public static void unify(FormulaSyntaxNet a, FormulaSyntaxNet b) {
        ConflictingBindException conflict = null;

        List<FormulaSyntaxNet> madeConnections = new ArrayList<>();
        try {
            unify(a, b, madeConnections, new HaveVisitedPair());

      /*
        Check the new net for recursion by walking through net a and keeping track of
        all branches visited on every path. It is an error if the same branch is
        encountered twice on the same path (it's okay on different paths).
        It doesn't matter whether a or b is chosen as starting point as unify() makes
        them equivalent.
      */
            Collection<Long> haveVisitedOnThisPath = new HashSet<>();
            FSNIterator fsnIterator = a.multiorderIterator(true, false, true);
            while (fsnIterator.hasNext()) {
                FormulaSyntaxNet next = fsnIterator.next();

                if (next.isBranch()) {
                    if (fsnIterator.preorder()) {
                        Long uid = next.uid();
                        if (haveVisitedOnThisPath.contains(uid)) throw new ConflictingBindException();
                        haveVisitedOnThisPath.add(uid);
                    } else //if (iter.postorder())
                    {
                        Long uid = next.uid();
                        haveVisitedOnThisPath.remove(uid);
                    }
                }
            }
        } catch (ConflictingBindException x) {
            conflict = x;
            Iterator<FormulaSyntaxNet> iterator = madeConnections.iterator();
            while (iterator.hasNext()) {
                FormulaSyntaxNet from = iterator.next();
                FormulaSyntaxNet to = iterator.next();
                from.removeForwardUnsafe(to);
            }
        }

        // debugging test
        Iterator<FormulaSyntaxNet> i = madeConnections.iterator();
        while (i.hasNext()) {
            FormulaSyntaxNet from = i.next();
            i.next(); //throw away "to" because it either doesn't change
            //or reappears as "from" later.
            from.checkInvariant();
        }

        // expensive debugging test
        a.checkForwardConsistency();
        b.checkForwardConsistency();
        if (conflict == null && !isEquivalent(a, b)) throw new RuntimeException("unify() failed abnormally");

        if (conflict != null) throw conflict;
    }

    /**
     * <p>Like {@link #unify(FormulaSyntaxNet, FormulaSyntaxNet)} but leaves net in a
     * broken state in the case of an exception.</p>
     * <p><b>Unlike {@link #unify(FormulaSyntaxNet, FormulaSyntaxNet)}
     * this method does never perform debugging tests.</b></p>
     *
     * @param a           {@code FormulaSyntaxNet} to be unified
     * @param b           {@code FormulaSyntaxNet} to be unified
     * @param conns       must be initialized with an empty {@link List}.
     *                    On successful return {@code conns} contains the connections that were
     *                    made to make {@code a} and {@code b} equivalent.
     *                    If an exception is thrown
     *                    this list contains the connections that were made before the exception, i.e.
     *                    the connections that need to be undone to restore the original nets.
     *                    The 1st, 3rd, 5th,... entry is the {@code FormulaSyntaxNet}
     *                    (a forward or unbound node) where the connection originates and
     *                    the 2nd, 4th, 6th entry is the {@code FormulaSyntaxNet} the
     *                    connection points to.<br>
     *                    This list may contain duplicate entries.
     * @param haveVisited contains unordered
     *                    {@code (FormulaSyntaxNet,FormulaSyntaxNet)} pairs of nodes
     *                    that have already been unified. This is for internal use by this
     *                    method only and must be initialized with an empty
     *                    {@code HaveVisitedPair} object by the caller.
     * @throws ConflictingBindException if {@code a} and {@code b}
     *                                  can not be unified because at least one bound leaf in {@code a}
     *                                  or {@code b} does not match a corresponding (bound or unbound)
     *                                  leaf in the other one.
     * @see #unify(FormulaSyntaxNet, FormulaSyntaxNet)
     */
    private static void unify(FormulaSyntaxNet a, FormulaSyntaxNet b, List<FormulaSyntaxNet> conns, HaveVisitedPair haveVisited) {
        if (a == b || haveVisited.includesUnordered(a, b)) return;

    /* Note regarding haveVisited: Infinite loops can only occur if a
       group of equivalent forward nodes point to each other. All of these
       forward nodes are located on the same level of the net (i.e. the same
       distance to the tree roots if only branch nodes are counted). Because
       forward nodes on different levels can never point to each other, we
       only need to keep track of the forward nodes visited since the last
       pair of branch/bound nodes.
       So with every pair of branch/bound nodes we encounter on our recursive
       walk through the net we reset haveVisited and we only add pairs to
       haveVisited that have at least one forward or unbound node.

       UPDATE: The above is outdated now, because the code tests for already
       existing connections and doesn't recurse into them. I still keep haveVisited
       around because it could improve performance. However, this would probably
       require a hashing scheme in HaveVisited and would only have an effect on
       pathological cases with many crosslinked forward nodes.
       Anyway, keeping haveVisited doesn't hurt that much.

       UPDATE: Hashing scheme for HaveVisitedPair now implemented.
    */

        //if we have at least one bound node or branch, make sure it is a
        if (b.isBoundOrBranch()) {
            FormulaSyntaxNet c = a;
            a = b;
            b = c;
        }

        if (a.isBoundOrBranch()) {
            if (b.isBoundOrBranch()) {
                if ((a.isBound() != b.isBound()) ||
                        (a.isBound() && !a.id().equals(b.id()))
                ) throw new ConflictingBindException();

                if (a.isBranch()) {
                    unify(a.left(), b.left(), conns, new HaveVisitedPair());
                    unify(a.right(), b.right(), conns, new HaveVisitedPair());
                }
            } else //if b.isUnboundOrForward()
            {
                haveVisited.addUnordered(a, b);

                if (b.addForwardUnsafe(a))  //if a and b not yet unified
                {
                    conns.add(b);
                    conns.add(a);

                    FormulaSyntaxNet[] bforwards = new FormulaSyntaxNet[b.forwards_.size()];
                    FSNIterator i = b.forwards();
                    int j = 0;
                    while (i.hasNext()) {
                        bforwards[j] = i.next();
                        j++;
                    }
                    j--;
                    while (j >= 0) {
                        unify(a, bforwards[j], conns, haveVisited);
                        j--;
                    }
                }
            }
        } else //if (a.isUnboundOrForward())
        { //because of the swapping at the beginning of this method, b is neither
            //bound nor branch node, so b.isUnboundOrForward() is true.

            haveVisited.addUnordered(a, b);

            if (a.addForwardUnsafe(b))  //if a and b not yet unified
            {
                b.addForwardUnsafe(a);
                conns.add(a);
                conns.add(b);
                conns.add(b);
                conns.add(a);

                //Unifying a with the forward connections of b may lead to the adding of
                //new connections to b and/or a. The same is true for unifying b with the
                //forward connections of a. This will cause forwards() iterators to be
                //invalidated. For that reason, we have to add the necessary connections
                //to a and b before we call unify in iterator loops.
                List<FormulaSyntaxNet> aForwardsWithoutB = new ArrayList<>();
                List<FormulaSyntaxNet> bForwardsWithoutA = new ArrayList<>();

                FSNIterator i = a.forwards();
                while (i.hasNext()) {
                    FormulaSyntaxNet n = i.next();
                    if (n != b) aForwardsWithoutB.add(n);
                }
                i = b.forwards();
                while (i.hasNext()) {
                    FormulaSyntaxNet n = i.next();
                    if (n != a) bForwardsWithoutA.add(n);
                }

        /*
          The following block adds all of b's connections to a and all of a's
          connections to b. All of a's connections that are forward nodes will also
          get connected to b and all of b's connections that are forward nodes
          will also get connected to a.
        */
                final FormulaSyntaxNet[] fsna = {a, b};
                final List<List<FormulaSyntaxNet>> la = Arrays.asList(bForwardsWithoutA, aForwardsWithoutB);
                for (int idx = 0; idx < 2; ++idx) {
                    for (FormulaSyntaxNet value : la.get(idx)) {
                        if (fsna[idx].addForwardUnsafe(value)) {
                            conns.add(fsna[idx]);
                            conns.add(value);
                        }

                        if (value.isUnboundOrForward()) {
                            if (value.addForwardUnsafe(fsna[idx])) {
                                conns.add(value);
                                conns.add(fsna[idx]);
                            }

                            for (FormulaSyntaxNet o : la.get(1 - idx)) {
                                if (value.addForwardUnsafe(o)) {
                                    conns.add(value);
                                    conns.add(o);
                                }
                            }
                        }
                    }
                }

        /*
          At this point a, b and all of their forward connections are
          completely interconnected.
          All that has to be done now is to unify those connections.
          Because forward nodes are completely connected, we can limit the
          unification to bound and branch nodes.
        */
                for (FormulaSyntaxNet net1 : aForwardsWithoutB) {

                    if (net1.isUnboundOrForward()) continue;

                    for (FormulaSyntaxNet net2 : bForwardsWithoutA) {

                        if (net2.isUnboundOrForward()) continue;

                        unify(net1, net2, conns, new HaveVisitedPair());
                    }
                }
            }
        }
    }

    /**
     * <p>Extracts a {@code FormulaSyntaxNet} that contains
     * only nodes owned by {@link Object}s contained in {@code owners}.</p>
     * <p>If {@code n} is not owned by an {@link Object} found
     * in {@code owners}, then {@code null} is returned and nothing else
     * happens.</p>
     * <p>Otherwise, beginning with {@code n} all reachable
     * {@code FormulaSyntaxNet} nodes (not just children and forward connections of
     * {@code n} but also parents and backward connections) will be scanned
     * recursively for nodes with an owner found in {@code owners}. From these nodes
     * all connections (forward and backward) that are not branches are removed if they
     * connect with a node not owned by an {@link Object} from {@code owners}.</p>
     * <p>The scan stops at nodes owned by {@link Object}s not in {@code owners},
     * which means that after this function returns there may still remain some nodes
     * that are owned by {@link Object}s from {@code owners} and have non-branch
     * connections to nodes owned by {@link Object}s not in {@code owners}. The
     * nodes in question are not extracted because they are not reachable from
     * {@code n} through a path of nodes all owned by {@link Object}s from
     * {@code owners}.</p>
     * <p><b>Note that this method can not always be used to undo a
     * {@link #unify(FormulaSyntaxNet, FormulaSyntaxNet) unify()}.</b><br>
     * Example:</p>
     * <p><code>FormulaSyntaxNet a=newLeaf(owner1,null);<br>
     * FormulaSyntaxNet b=newLeaf(owner1,null);<br>
     * FormulaSyntaxNet c=newLeaf(owner2,null);<br>
     * unify(a,c); unify(c,b); <br>
     * HashSet owners=new HashSet(); owners.add(c);<br>
     * extractByOwner(c,owners);</code></p>
     * <p>{@code a} and {@code b} will be unified after the example commands.
     * </p>
     *
     * @param net    node where the scanning starts.
     * @param owners {@link Collection} of {@link Object}s permissible as owners of nodes
     *               in the extracted net.
     * @return {@code null} if {@code n}'s owner is not in {@code owners}.
     * Otherwise returns {@code n}.
     * @see #removeAllForwards(FormulaSyntaxNet)
     */
    public static FormulaSyntaxNet extractByOwner(FormulaSyntaxNet net, Collection owners) {
        if (!owners.contains(net.owner())) return null;
        Collection<FormulaSyntaxNet> haveVisited = new HashSet<>();
        splitByOwner(net, owners, haveVisited);

        // debugging test
        for (FormulaSyntaxNet o : haveVisited) o.checkInvariant();

        // expensive debugging test
        net.checkForwardConsistency();

        return net;
    }

    /**
     * <p>see {@link #extractByOwner(FormulaSyntaxNet, Collection)}.</p>
     * <p><b>This method does not call {@link #checkInvariant()}!</b></p>
     *
     * @param haveVisited an empty {@link Collection}. After {@code splitByOwner()} returns
     *                    this contains all nodes that have been visited by this method.
     */
    private static void splitByOwner(FormulaSyntaxNet net, Collection owners, Collection<FormulaSyntaxNet> haveVisited) {
        if (haveVisited.contains(net) || !owners.contains(net.owner())) return;
        haveVisited.add(net);

        /*
          Gather all nodes that point to n via forward connections and are not owned
          by a permissible owner. Ignore those that point here via branches because
          we can't chop off branches (would violate the left_!=null!=right_ invariant).
        */
        List<FormulaSyntaxNet> backForwards = new ArrayList<>();
        Iterator<FormulaSyntaxNet> iterator = net.backwards();
        while (iterator.hasNext()) {
            FormulaSyntaxNet back = iterator.next();
            if (back.isForward() && !owners.contains(back.owner())) backForwards.add(back);
        }

        /*
          Now remove the forward connections from the nodes determined above. We have
          to do this separately through iterating over backForwards instead of putting
          it directly in the above loop because removeForwardUnsafe() would invalidate
          our n.backwards() iterator.
        */
        iterator = backForwards.iterator();
        while (iterator.hasNext()) iterator.next().removeForwardUnsafe(net);

        if (net.isBranch()) {
            /* now split left and right child recursively */
            splitByOwner(net.left(), owners, haveVisited);
            splitByOwner(net.right(), owners, haveVisited);
        } else if (net.isForward()) {
            /* Remove all forward connections to nodes with no permissible owner. */
            FConnIterator j = net.forwardConns();
            while (j.hasNext()) {
                ForwardConnection conn = j.next();
                if (!owners.contains(conn.to().owner())) j.remove();
            }

          /*
            Now we split remaining forward connections recursively. Because n has a
            permissible owner, this recursive splitting can not cause any further removes
            from n.forwards_ that would invalidate iterator i.
          */
            iterator = net.forwards();
            while (iterator.hasNext()) splitByOwner(iterator.next(), owners, haveVisited);
        }

        /*
          Now split remaining backwards connections recursively. Because n has a
          permissible owner and all backwards connections to nodes with non-permissible
          owners have either been removed already or are (non-removable) branches
          the recursive split can not cause further removes from n.backwards_ that would
          invalidate iterator i.
        */
        iterator = net.backwards();
        while (iterator.hasNext()) splitByOwner(iterator.next(), owners, haveVisited);
    }

    /**
     * <p>Recursively removes all forward connections from {@code n} and all
     * nets connected to {@code n} via branches or forward connections,
     * backwards or forwards.</p>
     *
     * @see #extractByOwner(FormulaSyntaxNet, Collection)
     */
    public static void removeAllForwards(FormulaSyntaxNet net) {
        Collection<FormulaSyntaxNet> haveVisited = new HashSet<>();
        Deque<FormulaSyntaxNet> todo = new ArrayDeque<>();
        todo.add(net);
        removeAllForwards(todo, haveVisited);

        // debugging test
        //very incomplete test as removeAllForwards(Stack, haveVisited) only'
        //stores branch nodes in haveVisited.
        for (FormulaSyntaxNet o : haveVisited) o.checkInvariant();
    }

    /**
     * <p>See {@link #removeAllForwards(FormulaSyntaxNet)}.</p>
     *
     * @param todo        contains all FormulaSyntaxNets to process.
     * @param haveVisited Pass an empty {@link Collection} (preferably a {@link HashSet}).
     */
    private static void removeAllForwards(Deque<FormulaSyntaxNet> todo, Collection<FormulaSyntaxNet> haveVisited) {
        while (!todo.isEmpty()) {
            FormulaSyntaxNet n = todo.pop();
            if (haveVisited.contains(n)) continue;

            FSNIterator i = n.backwards();
            while (i.hasNext()) todo.add(i.next());

            if (n.isBranch()) {
                haveVisited.add(n);
                todo.add(n.left());
                todo.add(n.right());
            } else if (n.isForward()) {
                FConnIterator j = n.forwardConns();
                while (j.hasNext()) {
                    todo.add(j.next().to());
                    j.remove();
                }
            }
        }
    }

    /**
     * <p>Creates a new {@code FormulaSyntaxNet} that is a (bound or unbound)
     * leaf node.</p>
     *
     * @param owner the owner of the new object
     * @param id    the name of the bound leaf or {@code null} if the new leaf
     *              is to be unbound.
     * @return a new {@code FormulaSyntaxNet}
     */
    public static FormulaSyntaxNet newLeaf(Object owner, String id) {
        FormulaSyntaxNet newNode = new FormulaSyntaxNet(id, null, null, owner);
        // debugging test
        newNode.checkInvariant();
        return newNode;
    }

    /**
     * <p>Creates a new {@code FormulaSyntaxNet} that is a branch node.</p>
     *
     * @param owner the owner of the new object
     * @param left  the left branch
     * @param right the right branch
     * @return a new {@code FormulaSyntaxNet}
     */
    public static FormulaSyntaxNet newBranch(Object owner, FormulaSyntaxNet left, FormulaSyntaxNet right) {
        FormulaSyntaxNet newNode = new FormulaSyntaxNet(null, left, right, owner);
        ++left.backwardsDirty_; //invalidate iterators
        left.backwards_.add(newNode);
        if (left != right) {
            ++right.backwardsDirty_; //invalidate iterators
            right.backwards_.add(newNode);
        }

        // debugging test
        newNode.checkInvariant();
        left.checkInvariant();
        right.checkInvariant();

        return newNode;
    }

    /**
     * <p>Returns a new FormulaSyntaxNet that corresponds to {@code s},
     * which must be a formula string in infix format, like (A-&gt;B) .</p>
     * <p>Unbound leaves must be represented as X&lt;num&gt;, where &lt;num&gt;
     * is any non-negative integer.</p>
     * <p>The net that is returned will not contain forward nodes. Leaf nodes with
     * the same id will
     * be identical (i.e. the same object).</p>
     * <p>No associativity is assumed for -&gt;, so A-&gt;B-&gt;C must be
     * written as either (A-&gt;B)-&gt;C or A-&gt;(B-&gt;C).</p>
     *
     * @param owner the owner of the {@code FormulaSyntaxNet} to create.
     * @param s     the infix formula string to interpret
     * @throws IllegalArgumentException if there is a syntax error in {@code s} or
     *                                  {@code s==null}.
     * @see #newFromInfixFormulaStrings(Object, String[])
     */
    public static FormulaSyntaxNet newFromInfixFormulaString(Object owner, String s) {
        return newFromInfixFormulaString(owner, s, new HashMap<>());
    }

    /**
     * <p>Like {@link #newFromInfixFormulaString(Object, String)} but with an
     * extra parameter to take influence on leaf creation.</p>
     *
     * @param leaves must map {@link String}s to {@code FormulaSyntaxNet}s.
     *               Whenever a leaf is about to be created, this mapping is consulted. If
     *               a leaf already exists it is reused. Otherwise a new leaf is created and
     *               a mapping from its name to the newly created leaf is added to
     *               {@code leaves}. This parameter is useful when creating several
     *               {@code FormulaSyntaxNet}s that are to share leaves.
     */
    private static FormulaSyntaxNet newFromInfixFormulaString(Object owner, String s, Map<String, FormulaSyntaxNet> leaves) {
        if (s == null) throw new IllegalArgumentException();

        Deque<FormulaSyntaxNet> operandStack = new ArrayDeque<>(); //stores FormulaSyntaxNets
        Deque<Integer> operatorStack = new ArrayDeque<>(); //stores Integers that carry the
        //parentheses level of the operator

        boolean error = false; //if a syntax error is found, this is set to true and the while loop is broken
        boolean parseOperand = false; //if true, currentOperand will be converted to a FormulaSyntaxNet and put on the operandStack at the end of the while loop
        boolean applyOperator = false; //if true, the top operator will be applied at the end of the while loop
        int i = 0; //index of the current character to parse
        int state = 0; //0=>waiting for operand, 1=>parsing operand, 2=>waiting for ')' to apply operator
        int parentheses = 0; //current parentheses level
        int lastOperatorLevel = -1; //parentheses level of the top (most recent) unapplied operator
        StringBuilder currentOperand = new StringBuilder();
        while (i < s.length()) {
            if (s.charAt(i) == '(') {
                ++parentheses;
                if (state != 0) {
                    error = true;
                    break;
                }
            } else if (s.charAt(i) == ')') {
                if (state == 0) {
                    error = true;
                    break;
                }
                if (state == 1) parseOperand = true;
                if (parentheses == lastOperatorLevel) applyOperator = true;
                parentheses--;
                if (parentheses < 0) {
                    error = true;
                    break;
                }
                state = 2;
            } else if (s.regionMatches(i, "->", 0, 2)) {
                if (state == 0 || lastOperatorLevel == parentheses) {
                    error = true;
                    break;
                }
                if (state == 1) parseOperand = true;

                state = 0;
                lastOperatorLevel = parentheses;
                operatorStack.push(parentheses);

                ++i; //extra increment because "->" has 2 characters
            } else //if s.charAt(i) part of operand
            {
                if (state == 2) {
                    error = true;
                    break;
                }
                state = 1;
                currentOperand.append(s.charAt(i));
            }

            ++i;

            if (i == s.length()) {
                if (parentheses != 0) {
                    error = true;
                    break;
                }

                if (state == 1) parseOperand = true;

                //determine the number of operands expected to be on operandStack
                //we start with 1 because we expect the final result to be the only thing
                //on the stack
                int expectedOperands = 1;

                //if we still have to put one operand on the operand stack, we expect one
                //operand less to be there now
                if (parseOperand) --expectedOperands;

                if (!operatorStack.isEmpty()) {
                    //we expect one more operand per operator
                    expectedOperands += operatorStack.size();
                    applyOperator = true;
                }

                if (operandStack.size() != expectedOperands) {
                    error = true;
                    break;
                }
            }

            if (parseOperand) {
                String operand = currentOperand.toString();
                currentOperand.delete(0, currentOperand.length());
                FormulaSyntaxNet net = leaves.get(operand);
                if (net == null) {
                    boolean unbound = true;
                    if (operand.charAt(0) != 'X' || operand.length() < 2)
                        unbound = false;
                    else
                        for (int j = 1; j < operand.length(); ++j)
                            if (!Character.isDigit(operand.charAt(j))) {
                                unbound = false;
                                break;
                            }

                    if (unbound) net = newLeaf(owner, null);
                    else net = newLeaf(owner, operand);

                    leaves.put(operand, net);
                }
                operandStack.push(net);
                parseOperand = false;
            }

            while (applyOperator) {
        /*
          We don't need to test if operandStack has at least 2 elements.
          The i==s.length() block above tests explicitly for this before setting
          applyOperator=true and the if (s.charAt(i)==')') block tests this
          implicitly via the state variable.
        */
                FormulaSyntaxNet right = operandStack.pop();
                FormulaSyntaxNet left = operandStack.pop();
                operandStack.push(newBranch(owner, left, right));

                operatorStack.pop();

                if (operatorStack.isEmpty()) {
                    lastOperatorLevel = -1;
                } else {
                    lastOperatorLevel = operatorStack.peek();
                }

                if (i == s.length())
                    applyOperator = !operatorStack.isEmpty();
                else
                    applyOperator = false;
            }
        }

        if (error) throw new IllegalArgumentException(s.substring(0, i) + " ERROR=>" + s.substring(i));

        if (operandStack.isEmpty()) throw new IllegalArgumentException("empty expression");
        return operandStack.pop();
    }

    /**
     * <p>Like {@link #newFromInfixFormulaString(Object, String)} but creates several
     * nets at the same time.</p>
     * <p>Leaf nodes with the same id will
     * be identical (i.e. the same object) in all returned nets.</p>
     *
     * @param owner the owner of the {@code FormulaSyntaxNet} to create.
     * @param s     the infix formula strings to interpret.
     * @return array whose elements correspond to those of {@code s}, i.e. the 1st
     * element is the {@code FormulaSyntaxNet} that corresponds to the
     * 1st element of {@code s}.
     * @throws IllegalArgumentException if {@code s==null} or
     *                                  {@code s.length==0} or an element of {@code s} is
     *                                  {@code null} or
     *                                  there is a syntax error in an element of {@code s}.
     * @see #newFromInfixFormulaString(Object, String)
     */
    public static FormulaSyntaxNet[] newFromInfixFormulaStrings(Object owner, String[] s) {
        if (s == null || s.length == 0) throw new IllegalArgumentException();

        Map<String, FormulaSyntaxNet> leaves = new HashMap<>();
        FormulaSyntaxNet[] result = new FormulaSyntaxNet[s.length];
        for (int i = 0; i < result.length; ++i) {
            if (s[i] == null) throw new IllegalArgumentException();
            result[i] = newFromInfixFormulaString(owner, s[i], leaves);
        }

        return result;
    }

    /**
     * Iterators over {@link #forwards_} (for forward nodes) or
     * {@link #left_} and {@link #right_} (for branch nodes)
     * should store this number on their creation and
     * invalidate themselves when it changes.
     *
     * <p><b>This variable is not used by all iterators. Some rely on the
     * underlying java.util.* collection for throwing
     * {@link ConcurrentModificationException}</b></p>
     *
     * @see #dirtyNumberBackwards()
     * @see #forwards_
     */
    public int dirtyNumberForwards() {
        return forwardsDirty_;
    }

    /**
     * Iterators over {@link #backwards_} should store this number on their creation and
     * invalidate themselves when it changes.
     *
     * <p><b>This variable is not used by all iterators. Some rely on the
     * underlying java.util.* collection for throwing
     * {@link ConcurrentModificationException}</b></p>
     *
     * @see #dirtyNumberForwards()
     * @see #backwards_
     */
    public int dirtyNumberBackwards() {
        return backwardsDirty_;
    }

    /**
     * Returns the owner of this {@code FormulaSyntaxNet}. Every
     * {@code FormulaSyntaxNet} has an owner. The owner can be any
     * {@link Object}.
     */
    public Object owner() {
        return owner_;
    }

    /**
     * Tests the integrity of the object for which it is called.
     * If debugging is enabled, this method is called after every mutator method
     * and every constructor.
     *
     * @throws RuntimeException if any of the object's invariants are violated.
     * @see #checkForwardConsistency()
     */
    public void checkInvariant() { //next invariant violation count is 12
        if (forwards_ != null) {
            if (forwards_.isEmpty() || left_ != null || right_ != null || id_ != null)
                throw new InvariantViolationException("1");
        } else if (id_ != null) {
            if (left_ != null || right_ != null || forwards_ != null) throw new InvariantViolationException("2");
        } else if (left_ != null || right_ != null) {
            if (left_ == null || right_ == null || forwards_ != null || id_ != null)
                throw new InvariantViolationException("3");
            //hmm why did I forbid the following? fromInfixFormulaString() produces this : if (left_==right_) throw new InvariantViolation("9");
        }

        if (owner_ == null) throw new InvariantViolationException("4");

        // expensive debugging test
        try {
            if (isForward()) { //test if all forward connections have exactly 1 backwards connection
                //to this, also test if this has a forward connection to itself (illegal)
                FSNIterator i = forwards();
                while (i.hasNext()) {
                    FormulaSyntaxNet n = i.next();
                    if (n == this) throw new InvariantViolationException("11");
                    FSNIterator j = n.backwards();
                    int found = 0;
                    while (j.hasNext()) {
                        if (j.next() == this) ++found;
                    }
                    if (found != 1) throw new InvariantViolationException("10-" + found);
                }
            } else if (isBranch()) { //test if left and right branch both have exactly one backwards
                //connections to this
                int found = 0;
                for (int i = 0; i < 2; ++i) {
                    FormulaSyntaxNet n;
                    if (i == 0) n = left();
                    else n = right();
                    FSNIterator j = n.backwards();
                    while (j.hasNext()) {
                        if (j.next() == this) ++found;
                    }
                }
                if (found != 2) throw new InvariantViolationException("6-" + found);
            }

            //test if all backward connections have exactly 1 forward or
            //at least 1 branch connection to this
            FSNIterator i = backwards();
            while (i.hasNext()) {
                int found = 0;
                FormulaSyntaxNet n = i.next();
                if (n.isBranch()) {
                    if (n.left() == this || n.right() == this) ++found;
                } else {
                    FSNIterator j = n.forwards();
                    while (j.hasNext()) {
                        if (j.next() == this) ++found;
                    }
                }
                if (found != 1) throw new InvariantViolationException("7-" + found);
            }

            if (isForward()) { //test if all forward nodes pointed to by this forward node point to
                //the same set of nodes. This includes the test that a forward node
                //must not point to an unbound node (because that node should be a
                //forward node pointing back).
                Collection<Long> targetSet = new HashSet<>();
                i = forwards();
                while (i.hasNext()) targetSet.add(i.next().uid());
                targetSet.add(uid());

                i = forwards();
                while (i.hasNext()) {
                    FormulaSyntaxNet n = i.next();
                    if (n.isUnboundOrForward()) {
                        Set<Long> compareSet = new HashSet<>();
                        FSNIterator j = n.forwards();
                        while (j.hasNext()) compareSet.add(j.next().uid());
                        compareSet.add(n.uid());
                        if (!compareSet.equals(targetSet)) throw new InvariantViolationException("8");
                    }
                }
            }
        } catch (InvariantViolationException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("unexpected exception in FormulaSyntaxNet.checkInvariant(): " + e);
        }
    }

    /**
     * <p>Tests that all forward connections from forward nodes are equivalent (have
     * been properly unified).</p>
     * <p>This method is called after {@link #unify(FormulaSyntaxNet, FormulaSyntaxNet)} and
     * {@link #extractByOwner(FormulaSyntaxNet, Collection)}.</p>
     *
     * @throws RuntimeException if non-equivalent forward connections are found.
     * @see #checkInvariant()
     * @see #isEquivalent(FormulaSyntaxNet, FormulaSyntaxNet)
     */
    public void checkForwardConsistency() {
        if (!(isEquivalent(this, this))) throw new RuntimeException("non-equivalent forward connections detected");
    }

    /**
     * Returns {@code true} if the node is a forward node
     * (see FormulaSyntaxNet class description}).
     *
     * @see #isBound()
     * @see #isBoundOrBranch()
     * @see #isBranch()
     * @see #isUnbound()
     * @see #isUnboundOrForward()
     */
    public boolean isForward() {
        return forwards_ != null;
    }

    /**
     * Returns {@code true} if the node is a branch node
     * (see FormulaSyntaxNet class description).
     *
     * @see #isForward()
     * @see #isBound()
     * @see #isBoundOrBranch()
     * @see #isUnbound()
     * @see #isUnboundOrForward()
     */
    public boolean isBranch() {
        return left_ != null;
    }

    /**
     * Returns {@code true} if the node is an unbound leaf node
     * (see FormulaSyntaxNet class description).
     *
     * @see #isForward()
     * @see #isBound()
     * @see #isBoundOrBranch()
     * @see #isBranch()
     * @see #isUnboundOrForward()
     */
    public boolean isUnbound() {
        return !(isBranch() || isBound() || isForward());
    }

    /**
     * Returns {@code true} if the node is an unbound leaf node or a
     * forward node (see FormulaSyntaxNet class description).
     *
     * @see #isForward()
     * @see #isBound()
     * @see #isBoundOrBranch()
     * @see #isBranch()
     * @see #isUnbound()
     */
    public boolean isUnboundOrForward() {
        return isUnbound() || isForward();
    }

    /**
     * Returns {@code true} if the node is a bound leaf node or a
     * branch node (see FormulaSyntaxNet class description).
     *
     * @see #isForward()
     * @see #isBound()
     * @see #isBranch()
     * @see #isUnbound()
     * @see #isUnboundOrForward()
     */
    public boolean isBoundOrBranch() {
        return isBound() || isBranch();
    }

    /**
     * Returns {@code true} if the node is a bound leaf node
     * <p>
     * (see FormulaSyntaxNet class description).
     *
     * @see #isForward()
     * @see #isBoundOrBranch()
     * @see #isBranch()
     * @see #isUnbound()
     * @see #isUnboundOrForward()
     */
    public boolean isBound() {
        return id_ != null;
    }

    /**
     * Adds a forward connection to an unbound leaf node or a forward node.
     * <p>If {@code newForward} is already in the list of forward connections,
     * it will not be added a second time.</p>
     * <p>Calling this method will invalidate iterators that iterate over the
     * list of forward connections for {@code this} and iterators that
     * iterate over the list of backward connections of {@code newForward}.</p>
     *
     * <p><b>This method does not call {@link #checkInvariant()}, so the
     * {@code FormulaSyntaxNet} structure may be violated after calling
     * this method.</b></p>
     *
     * @param newForward the {@code FormulaSyntaxNet} to connect with.
     *                   {@link #unify(FormulaSyntaxNet, FormulaSyntaxNet) unify()} should be used to
     *                   make sure that {@code newForward} is equivalent to all existing
     *                   forward connections (which is required for proper forward nodes).
     * @return {@code true} if a new connection was added.
     * @throws IllegalNodeTypeException if {@code this} is neither unbound
     *                                  leaf nor forward node.
     */
    private boolean addForwardUnsafe(FormulaSyntaxNet newForward) {
        if (!isUnboundOrForward()) throw new IllegalNodeTypeException();
        if (isUnbound()) forwards_ = new ArrayList<>();
        FSNIterator i = forwards();
        while (i.hasNext()) {
            if (newForward == i.next()) return false;
        }
        ForwardConnection newCon = new ForwardConnection(this, newForward);
        ++forwardsDirty_; //invalidate iterators
        forwards_.add(newCon);
        ++newForward.backwardsDirty_; //invalidate iterators
        newForward.backwards_.add(this);
        return true;
    }

    /**
     * Removes a forward connection from an unbound leaf node or a forward node.
     * <p>If {@code oldForward} is not in the list of forward connections,
     * nothing is done.</p>
     * <p>Calling this method will invalidate iterators that iterate over the
     * list of forward connections for {@code this} and iterators that
     * iterate over the list of backward connections of {@code oldForward}.</p>
     *
     * <p><b>This method does not call {@link #checkInvariant()}, so the
     * {@code FormulaSyntaxNet} structure may be violated after calling
     * this method.</b></p>
     *
     * @param oldForward the {@code FormulaSyntaxNet} to remove connection to.
     * @return {@code true} if a connection was removed.
     * @throws IllegalNodeTypeException if {@code this} is neither unbound
     *                                  leaf nor forward node.
     */
    private boolean removeForwardUnsafe(FormulaSyntaxNet oldForward) {
        if (!isUnboundOrForward()) throw new IllegalNodeTypeException();

        if (isUnbound()) return false;

        FConnIterator i = forwardConns();
        while (i.hasNext()) {
            if (oldForward == i.next().to()) {
                i.remove();
                return true;
            }
        }

        return false;
    }

    /**
     * <p>Removes the forward connection {@code oldForward} which must be
     * the {@link ForwardConnection#to() to()} part of a {@link ForwardConnection} returned
     * by a preceding call to {@code i.next()}.</p>
     * <p>Calling this method will invalidate iterators that iterate over the
     * list of forward connections for {@code this} and iterators that
     * iterate over the list of backward connections of {@code oldForward}.</p>
     *
     * <p><b>This method does not call {@link #checkInvariant()}, so the
     * {@code FormulaSyntaxNet} structure may be violated after calling
     * this method.</b></p>
     *
     * @param i          an iterator that returned a {@link ForwardConnection} pointing to
     *                   {@code oldForward} on a preceding call to {@code next()}.
     * @param oldForward the {@code FormulaSyntaxNet} to remove connection to.
     * @throws IllegalStateException           if {@code i.remove()} would throw it.
     * @throws ConcurrentModificationException if {@code i.remove()} would throw it.
     * @throws UnsupportedOperationException   if {@code i.remove()} would throw it.
     */
    protected void removeForwardUnsafe(Iterator<ForwardConnection> i, FormulaSyntaxNet oldForward) {
        i.remove(); //must be before any other commands that modify the structure
        //because this iterator could throw IllegalState.. or ConcurrentMod..
        ++forwardsDirty_; //invalidate iterators
        if (forwards_.isEmpty()) forwards_ = null;
        ++oldForward.backwardsDirty_; //invalidate iterators
        oldForward.backwards_.remove(this);
    }

    /**
     * <p>Returns {@code true} if {@code this} is a descendant of or equivalent
     * to a descendant of {@code b}.</p>
     * <p>If {@code this==b} or {@code this} is equivalent to {@code b}
     * the return value is {@code false}.</p>
     */
    public boolean isDescendantOf(FormulaSyntaxNet b) {
        return (isDescendantOf(b, new HashSet<>(), true) > 0);
    }

    /**
     * <p>Determines whether {@code this} is a descendant of or equivalent
     * to a descendant of {@code b}.</p>
     *
     * @param b           ancestor to check for
     * @param haveVisited pass a new {@link HashSet}.
     * @param equiv       pass {@code true}.
     * @return {@code -1} if {@code this} is equivalent to {@code b},
     * {@code +1} if {@code this} is a descendant of {@code b},
     * {@code 0} if {@code this} is not a descendant of {@code b}.
     * @see #isDescendantOf(FormulaSyntaxNet)
     */
    private int isDescendantOf(FormulaSyntaxNet b, Collection<FormulaSyntaxNet> haveVisited, boolean equiv) {
        if (this == b) {
            if (equiv) return -1;
            else return +1;
        }

        if (!haveVisited.contains(this)) {
            haveVisited.add(this);

            FSNIterator i = backwards();
            while (i.hasNext()) {
                FormulaSyntaxNet n = i.next();
                int ret = n.isDescendantOf(b, haveVisited, equiv && n.isForward());
                if (ret != 0) return ret;
            }

            //the following part is not redundant because a forward node can point to a
            //node that is not a forward node, so that it will not be in backwards()
            if (isForward()) {
                i = forwards();
                while (i.hasNext()) {
                    FormulaSyntaxNet n = i.next();
                    int ret = n.isDescendantOf(b, haveVisited, equiv);
                    if (ret != 0) return ret;
                }
            }
        }

        return 0;
    }

    /**
     * <p>Traverses the whole net (forwards and backwards) and stores all of its nodes in
     * {@code nodes}.</p>
     * <p>After this method returns, {@code nodes} will contain {@code this}
     * and all
     * the nodes necessary so that all nodes in {@code nodes} point to and are
     * pointed to only by nodes in {@code nodes}.</p>
     *
     * @param nodes (should be a {@link Collection} with good
     *              {@link Collection#add(Object) add} and
     *              {@link Collection#contains(Object) contains} performance) collects all
     *              nodes of the whole {@code FormulaSyntaxNet}.
     *              {@code nodes} is cleared before the execution of this method, so it
     *              need not be initially empty.
     */
    public void getAllReachableNodes(Collection<FormulaSyntaxNet> nodes) {
        nodes.clear();
        getAllReachableNodesNoClear(nodes);
    }

    /**
     * <p>Like {@link #getAllReachableNodes(Collection)} but does not clear
     * {@code nodes}. Nodes contained in {@code nodes} will not be visited, i.e
     * nodes only reachable through these nodes will not be part of the final
     * collection.</p>
     */
    private void getAllReachableNodesNoClear(Collection<FormulaSyntaxNet> nodes) {
        if (nodes.contains(this)) return;
        nodes.add(this);

        if (isBranch()) {
            left().getAllReachableNodesNoClear(nodes);
            right().getAllReachableNodesNoClear(nodes);
        } else if (isForward()) {
            FSNIterator i = forwards();
            while (i.hasNext()) i.next().getAllReachableNodesNoClear(nodes);
        }

        FSNIterator i = backwards();
        while (i.hasNext()) i.next().getAllReachableNodesNoClear(nodes);
    }

    /**
     * <p>Returns {@code true} if {@code this} and {@code obj} are physically the same
     * {@link Object}.</p>
     */
    public boolean equals(Object obj) {
        return this == obj;
    }

    /**
     * <p>Returns the left branch of a branch node.</p>
     *
     * @throws IllegalNodeTypeException if {@code this} is not a branch node.
     * @see #right()
     */
    public FormulaSyntaxNet left() {
        if (!isBranch()) throw new IllegalNodeTypeException();
        return left_;
    }

    /**
     * <p>Returns the right branch of a branch node.</p>
     *
     * @throws IllegalNodeTypeException if {@code this} is not a branch node.
     * @see #left()
     */
    public FormulaSyntaxNet right() {
        if (!isBranch()) throw new IllegalNodeTypeException();
        return right_;
    }

    /**
     * Returns the unique id of this {@code FormulaSyntaxNet}.
     * No 2 {@code FormulaSyntaxNet}s return the same number for
     * {@code uid()}
     */
    public long uid() {
        return uid_;
    }

    /**
     * <p>Returns the id of the propositional variable for a bound node.</p>
     *
     * @throws IllegalNodeTypeException if {@code this} is not a bound node.
     */
    public String id() {
        if (!isBound()) throw new IllegalNodeTypeException();
        return id_;
    }

    /**
     * Returns an iterator to visit all the nodes that a forward node points to.
     * This function may be called on forward nodes and unbound leaves.
     *
     * @throws IllegalNodeTypeException if {@code this} is neither unbound
     *                                  leaf nor forward node.
     * @see #backwards()
     * @see #preorderIterator()
     * @see #inorderIterator()
     * @see #postorderIterator()
     * @see #multiorderIterator(boolean, boolean, boolean)
     */
    public FSNIterator forwards() {
        return new FSNIteratorFromFConnIterator(forwardConns());
    }

    /**
     * Returns an iterator to visit all the (branch and forward) nodes that
     * point to {@code this}.
     *
     * @see #forwards()
     * @see #preorderIterator()
     * @see #inorderIterator()
     * @see #postorderIterator()
     * @see #multiorderIterator(boolean, boolean, boolean)
     */
    public FSNIterator backwards() {
        return new FSNIteratorFromIterator(backwards_.iterator());
    }

    /**
     * <p>Returns an iterator that visits bound and unbound leaves and branch nodes
     * in preorder sequence.</p>
     * <p>Forward nodes will not be visited unless all nodes
     * equivalent to the forward node are also forward nodes. In that case,
     * the forward node that is visited is the one with the lowest {@link #uid()}.
     * </p>
     * <p>For every set of equivalent nodes at the same position in the net
     * only one representative will be visited. Equivalent nodes in different
     * branches of the net will be visited multiple times.</p>
     *
     * @see #forwards()
     * @see #inorderIterator()
     * @see #postorderIterator()
     * @see #multiorderIterator(boolean, boolean, boolean)
     */
    public FSNIterator preorderIterator() {
        return new FSNMultiorderIterator(this, true, false, false);
    }

    /**
     * <p>Returns an iterator that visits bound and unbound leaves and branch nodes
     * in inorder sequence.</p>
     * <p>Forward nodes will not be visited unless all nodes
     * equivalent to the forward node are also forward nodes. In that case,
     * the forward node that is visited is the one with the lowest {@link #uid()}.
     * </p>
     * <p>For every set of equivalent nodes at the same position in the net
     * only one representative will be visited. Equivalent nodes in different
     * branches of the net will be visited multiple times.</p>
     *
     * @see #forwards()
     * @see #preorderIterator()
     * @see #postorderIterator()
     * @see #multiorderIterator(boolean, boolean, boolean)
     */
    public FSNIterator inorderIterator() {
        return new FSNMultiorderIterator(this, false, true, false);
    }

    /**
     * <p>Returns an iterator that visits bound and unbound leaves and branch nodes
     * in postorder sequence.</p>
     * <p>Forward nodes will not be visited unless all nodes
     * equivalent to the forward node are also forward nodes. In that case,
     * the forward node that is visited is the one with the lowest {@link #uid()}.
     * </p>
     * <p>For every set of equivalent nodes at the same position in the net
     * only one representative will be visited. Equivalent nodes in different
     * branches of the net will be visited multiple times.</p>
     *
     * @see #forwards()
     * @see #inorderIterator()
     * @see #multiorderIterator(boolean, boolean, boolean)
     */
    public FSNIterator postorderIterator() {
        return new FSNMultiorderIterator(this, false, false, true);
    }

    /**
     * <p>Returns an iterator that visits bound and unbound leaves and branch nodes
     * in pre/in/postorder sequence.</p>
     * <p>Forward nodes will not be visited unless all nodes
     * equivalent to the forward node are also forward nodes. In that case,
     * the forward node that is visited is the one with the lowest {@link #uid()}.
     * </p>
     * <p>For every set of equivalent nodes at the same position in the net
     * only one representative will be visited. Equivalent nodes in different
     * branches of the net will be visited multiple times.</p>
     *
     * <p>{@code pre}, {@code in} and {@code post} determine
     * whether and when the iterator will return branch nodes. If all 3
     * values are {@code false}, only leaf nodes will be returned. If
     * more than one is {@code true}, branch nodes will be visited
     * multiple times.</p>
     *
     * @param pre  stop at branch node before visiting left child
     * @param in   stop at branch node before visiting right child
     * @param post stop at branch node after visiting right child
     * @see #forwards()
     * @see #inorderIterator()
     * @see #postorderIterator()
     * @see #preorderIterator()
     */
    public FSNIterator multiorderIterator(boolean pre, boolean in, boolean post) {
        return new FSNMultiorderIterator(this, pre, in, post);
    }

    /**
     * <p>Returns an iterator to visit all {@link ForwardConnection}s that a
     * forward node has. This function may be called on forward nodes and
     * unbound leaves.</p>
     *
     * @throws IllegalNodeTypeException if {@code this} is neither unbound
     *                                  leaf nor forward node.
     * @see #forwards()
     * @see #preorderIterator()
     * @see #inorderIterator()
     * @see #postorderIterator()
     * @see #multiorderIterator(boolean, boolean, boolean)
     */
    private FConnIterator forwardConns()//DO NOT MAKE this method public because this exposes the FSN to unchecked modification throw FConnIterator.remove()!
    {
        if (!isUnboundOrForward()) throw new IllegalNodeTypeException();
        if (isUnbound()) return new EmptyFConnIterator();
        return new FConnIteratorForFSN(this);
    }

    /**
     * <p>Returns the formula represented by the {@code FormulaSyntaxNet}
     * as a {@link String} in infix format, like (A-&gt;B) .</p>
     * <p>Unbound leaves will be represented as X0, X1,...</p>
     */
    public String toInfixFormulaString() {
        StringBuilder s = new StringBuilder();
        FSNIterator i = multiorderIterator(true, true, true);
        Map<Long, String> map = new HashMap<>(); // maps uid()s to String names
        int unboundCount = 0; //number of unbound leaves encountered so far
        while (i.hasNext()) {
            FormulaSyntaxNet node = i.next();
            if (node.isBranch()) {
                if (i.preorder()) s.append('(');
                if (i.inorder()) s.append("->");
                if (i.postorder()) s.append(')');
            } else if (node.isBound()) s.append(node.id());
            else if (node.isUnboundOrForward()) {
                String id = map.get(node.uid());
                if (id == null) {
                    id = "X" + (unboundCount);
                    unboundCount++;
                    map.put(node.uid(), id);
                }
                s.append(id);
            } else throw new RuntimeException("Huh? What's this node?"); //just to be safe
        }
        return s.toString();
    }

}

