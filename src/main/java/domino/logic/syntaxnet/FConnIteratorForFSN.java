/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.syntaxnet;

import domino.logic.ForwardConnection;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * <p>Iterates over the {@link ForwardConnection}s of a
 * {@link FormulaSyntaxNet}.</p>
 */
public class FConnIteratorForFSN implements FConnIterator {
    private final Iterator<ForwardConnection> iterator_;
    private final FormulaSyntaxNet formulaSyntaxNet;
    private ForwardConnection current;

    /**
     * <p>Creates an iterator to iterate over the {@code ForwardConnection}s
     * of {@code n}.</p>
     */
    public FConnIteratorForFSN(FormulaSyntaxNet net) {
        formulaSyntaxNet = net;
        iterator_ = formulaSyntaxNet.forwards_.iterator();
    }

    /**
     * <p>Returns true if the iteration has more elements.</p>
     */
    public boolean hasNext() {
        return iterator_.hasNext();
    }

    /**
     * <p>Returns the next element in the iteration.</p>
     *
     * @return the next element in the iteration.
     * @throws NoSuchElementException if the iteration has no more elements.
     */
    public ForwardConnection next() {
        current = iterator_.next();
        return current;
    }

    /**
     * <p>Removes from the underlying collection the last element returned by
     * the iterator (optional operation). This method can be called only once
     * per call to next. The behavior of an iterator is unspecified if the
     * underlying collection is modified while the iteration is in progress in
     * any way other than by calling this method.</p>
     *
     * @throws UnsupportedOperationException if the remove operation is not
     *                                       supported by this Iterator.
     * @throws IllegalStateException         if the next method has not yet been
     *                                       called, or the remove method has already been called after
     *                                       the last call to the next method.
     */
    public void remove() {
        formulaSyntaxNet.removeForwardUnsafe(iterator_, current.to());
    }

}
