/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.syntaxnet;

/**
 * {@code ConflictingBindException} is thrown when the unification
 * of two {@code FormulaSyntaxNet}s fails.
 */
public class ConflictingBindException extends RuntimeException {
    private static final long serialVersionUID = 8561915633331042366L;

    /**
     * Constructs a {@code ConflictingBindException} with no detail message.
     */
    public ConflictingBindException() {
    }

    /**
     * Constructs a {@code ConflictingBindException} with the specified
     * detail message
     */
    public ConflictingBindException(String s) {
        super(s);
    }

}
