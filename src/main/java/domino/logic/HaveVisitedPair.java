/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic;

import domino.logic.syntaxnet.FormulaSyntaxNet;
import domino.utils.Pair;

import java.util.HashSet;
import java.util.Set;

/**
 * A {@code HaveVisitedPair} object keeps a list of object pairs
 * that have already been visited by an algorithm.
 */
public class HaveVisitedPair {
    private final Set<Pair<FormulaSyntaxNet, FormulaSyntaxNet>> visited_ = new HashSet<>();

    /**
     * Returns true if the pair {@code (o1,o2)} or the pair
     * {@code (o2,o1)} has been visited.
     */
    public boolean includesUnordered(FormulaSyntaxNet o1, FormulaSyntaxNet o2) {
        return visited_.contains(new Pair<>(o1, o2));
    }

    /**
     * Adds the pairs {@code (o1,o2)} and {@code (o2,o1)} to the
     * list of visited pairs.
     */
    public void addUnordered(FormulaSyntaxNet o1, FormulaSyntaxNet o2) {
        visited_.add(new Pair<>(o1, o2));
    }
}
