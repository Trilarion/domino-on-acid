/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic;

import domino.logic.fsn.FSNIterator;
import domino.logic.syntaxnet.FormulaSyntaxNet;
import domino.logic.tree.FormulaDominoTree;

import java.util.*;

/**
 * <p>Implements {@link UnboundIdAssigner}. This is a separate class in order to
 * hide the {@link #getIdFor(FormulaSyntaxNet)} function from the user, so that
 * s/he can't affect the mappings directly.</p>
 */
public class UnboundIdAssignerImpl extends UnboundIdAssigner {
    private final Map<FormulaSyntaxNet, Integer> map_ = new HashMap<>();
    private final Set<Integer> freeIds_ = new HashSet<>();
    private int maxIdAssignedSoFar_ = -1;

    public void reset() {
        maxIdAssignedSoFar_ = -1;
        map_.clear();
        freeIds_.clear();
    }

    /**
     * <p>Returns the distance of {@code tree} to the root of the large
     * {@link FormulaDominoTree} it is part of.</p>
     * <p>A return value of 0 means that {@code tree} is the root.</p>
     * <p>The maximum number returned by this function will be
     * {@code max{0,maxdist}}. This can be used for optimization because
     * the search for the root will stop after {@code maxdist} parent levels
     * have been found.</p>
     */
    protected int distanceToRoot(FormulaDominoTree tree, int maxdist) {
        int n = 0;
        while (tree.rootConnection_ != null && n <= maxdist) {
            ++n;
            tree = tree.rootConnection_;
        }
        return n;
    }

    /**
     * <p>Returns the highest (=closest to the root) node in the
     * {@link FormulaDominoTree} that is equivalent to {@code node}.</p>
     * <p>node must me an unbound or forward node that is owned by a {@link FormulaDominoTree}.</p>
     */
    protected FormulaSyntaxNet highestEquivalent(FormulaSyntaxNet node) {
        FormulaDominoTree tree = (FormulaDominoTree) node.owner();
        int rootdist = distanceToRoot(tree, Integer.MAX_VALUE);

        FormulaSyntaxNet nodeToAssign = node;
        FSNIterator i = node.forwards();
        while (i.hasNext()) {
            FormulaSyntaxNet n = i.next();
            if (n.isUnboundOrForward()) {
                int rdist = distanceToRoot((FormulaDominoTree) n.owner(), rootdist);
                if (rdist < rootdist || (rdist == rootdist && n.uid() < nodeToAssign.uid())) {
                    rootdist = rdist;
                    nodeToAssign = n;
                }
            }
        }
        return nodeToAssign;
    }

    /**
     * <p>Returns the mapping for {@code node} after creating one if necessary.
     * {@code node} must be an unbound or forward node whose owner is
     * a {@link FormulaDominoTree}.</p>
     * <p>This function is not part of {@link UnboundIdAssigner}, because users
     * must not affect the mappings directly
     * (i.e. without {@link FormulaDominoTree}s being involved).</p>
     */
    public int getIdFor(FormulaSyntaxNet node) {
        FormulaSyntaxNet nodeToAssign = highestEquivalent(node);

        Integer id = map_.get(nodeToAssign);
        if (id == null) {
            if (freeIds_.isEmpty()) {
                ++maxIdAssignedSoFar_;
                freeIds_.add(maxIdAssignedSoFar_);
            }
            Iterator<Integer> iterator = freeIds_.iterator();
            id = iterator.next();
            iterator.remove();
            map_.put(nodeToAssign, id);
        }

        return id;
    }

    public void removeMappingsFor(FormulaDominoTree tree) {
        removeMappingsFor(tree, new HashSet<>(), true);
    }

    public void removeMappingsStrictlyFor(FormulaDominoTree tree) {
        removeMappingsFor(tree, new HashSet<>(), false);
    }

    protected void removeMappingsFor(FormulaDominoTree tree, Set<FormulaSyntaxNet> haveVisited, boolean followForwards) {
        removeMappingsFor(tree.root_, haveVisited, followForwards);
        for (int i = 0; i < tree.bonus_.length; ++i)
            removeMappingsFor(tree.bonus_[i], haveVisited, followForwards);

        for (int i = 0; i < tree.leaves_.length; ++i) {
            removeMappingsFor(tree.leaves_[i], haveVisited, followForwards);
            if (tree.leafConnections_[i] != null)
                removeMappingsFor(tree.leafConnections_[i], haveVisited, followForwards);
        }
    }

    protected void removeMappingsFor(FormulaSyntaxNet net, Set<FormulaSyntaxNet> haveVisited, boolean followForwards) {
        if (haveVisited.contains(net)) return;
        haveVisited.add(net);

        if (net.isUnboundOrForward()) {
            removeSingleMappingFor(net);
            if (followForwards) {
                FSNIterator i = net.forwards();
                while (i.hasNext()) removeMappingsFor(i.next(), haveVisited, followForwards);
            }
        } else if (net.isBranch()) {
            removeMappingsFor(net.left(), haveVisited, followForwards);
            removeMappingsFor(net.right(), haveVisited, followForwards);
        }
    }

    public void removeUnneededMappingsFor(FormulaDominoTree tree) {
        removeUnneededMappingsFor(tree, new HashSet<>());
    }

    protected void removeUnneededMappingsFor(FormulaDominoTree tree, Set<FormulaSyntaxNet> haveVisited) {
        removeUnneededMappingsFor(tree.root_, haveVisited);
        for (int i = 0; i < tree.bonus_.length; ++i)
            removeUnneededMappingsFor(tree.bonus_[i], haveVisited);

        for (int i = 0; i < tree.leaves_.length; ++i) {
            removeUnneededMappingsFor(tree.leaves_[i], haveVisited);
            if (tree.leafConnections_[i] != null) removeUnneededMappingsFor(tree.leafConnections_[i], haveVisited);
        }
    }

    protected void removeUnneededMappingsFor(FormulaSyntaxNet net, Set<FormulaSyntaxNet> haveVisited) {
        if (haveVisited.contains(net)) return;
        haveVisited.add(net);

        if (net.isUnboundOrForward()) {
            FormulaSyntaxNet nodeToAssign = highestEquivalent(net);

            if (net != nodeToAssign) removeSingleMappingFor(net);

            boolean foundBranchOrBound = false;
            FSNIterator i = net.forwards();
            while (i.hasNext()) {
                FormulaSyntaxNet n2 = i.next();
                if (n2.isUnboundOrForward()) {
                    if (n2 != nodeToAssign) removeSingleMappingFor(n2);
                } else {
                    foundBranchOrBound = true;
                    removeUnneededMappingsFor(n2, haveVisited);
                }
            }

            if (foundBranchOrBound) removeSingleMappingFor(nodeToAssign);
        } else if (net.isBranch()) {
            removeUnneededMappingsFor(net.left(), haveVisited);
            removeUnneededMappingsFor(net.right(), haveVisited);
        }
    }

    protected void removeSingleMappingFor(FormulaSyntaxNet net) {
        Integer i = map_.remove(net);
        if (i != null) {
            freeIds_.add(i);
            if (i == maxIdAssignedSoFar_) {
                while (freeIds_.contains(maxIdAssignedSoFar_)) {
                    freeIds_.remove(maxIdAssignedSoFar_);
                    maxIdAssignedSoFar_--;
                }
            }
        }
    }

}
