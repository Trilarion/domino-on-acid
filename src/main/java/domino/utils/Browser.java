/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.utils;

import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import java.awt.*;
import java.io.IOException;
import java.net.URL;

/**
 * Contains a JFrame that can show HTML content and the ability to set URLs. Mostly used for displaying help pages.
 */
public class Browser { // TODO implement model with history forwards and backwards

    private static final Dimension MINIMAL_SIZE = new Dimension(800, 600);
    private final JFrame browserFrame;
    private final JEditorPane htmlEditorPane;

    public Browser(@NotNull String title) {
        // initialize the model


        // initialize UI
        browserFrame = new JFrame(title);
        browserFrame.setMinimumSize(MINIMAL_SIZE);
        browserFrame.setLocationByPlatform(true);
        htmlEditorPane = new JEditorPane();
        htmlEditorPane.setContentType("text/html");
        htmlEditorPane.setEditable(false);
        htmlEditorPane.setBackground(Color.WHITE);
        htmlEditorPane.addHyperlinkListener(hyperlinkEvent -> {
            if (hyperlinkEvent.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                URL url = hyperlinkEvent.getURL();
                browse(url);
            }
        });

        // scroll pane (never scrolls horizontally)
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(htmlEditorPane);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        browserFrame.setContentPane(scrollPane);
    }

    public void browse(@NotNull URL url) {
        try {
            htmlEditorPane.setPage(url);
        } catch (IOException e) {
            throw new RuntimeException("Could not set URL");
        }
    }

    public void show() {
        browserFrame.setVisible(true);
        browserFrame.toFront();
    }

}
