/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.utils;

/**
 * Generic {@code Pair<A, B>} implementation to help when you really only need a simple
 * Pair, nothing else. In all examples where semantics would help coding it
 * might be better to use a meaningful naming convention. However for the cases,
 * where the use is simple and limited, Pair might be the better option.
 * <p>
 * Should be HashMap/Comparable/Iterable(because it's final) save.
 *
 * @param <A> Type of first element.
 * @param <B> Type of second element.
 */
public class Pair<A, B> {

    /**
     * Two final elements of two different types.
     */
    private final A a;
    private final B b;

    /**
     * Constructor setting the values for both elements.
     *
     * @param a First element.
     * @param b Second element.
     */
    public Pair(A a, B b) {
        this.a = a;
        this.b = b;
    }

    /**
     * @return First element.
     */
    public A getA() {
        return a;
    }

    /**
     * @return Second element.
     */
    public B getB() {
        return b;
    }

    /**
     * Calculates a combined hash code in a relatively simple but smart way.
     *
     * @return The hash code.
     */
    @Override
    public int hashCode() {
        int hashA = 0;
        if (a != null) {
            hashA = a.hashCode();
        }
        int hashB = 0;
        if (b != null) {
            hashB = b.hashCode();
        }
        return (hashA + hashB) * hashB + hashA;
    }

    /**
     * Implementation of an equals method.
     *
     * @param other Another object.
     * @return True if they are of the same class and not null and both elements
     * are equal.
     */
    @Override
    public boolean equals(Object other) {
        if (other != null && getClass() == other.getClass() && a != null && b != null) {
            @SuppressWarnings("unchecked")
            Pair<A, B> o = (Pair<A, B>) other;
            return a.equals(o.a) && b.equals(o.b);
        }
        return false;
    }

    /**
     * @return Combines the toString() methods of both elements.
     */
    @Override
    public String toString() {
        return "(" + a + ", " + b + ")";
    }

}
