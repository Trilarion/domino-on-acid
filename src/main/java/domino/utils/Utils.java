/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.utils;

public final class Utils {

    private Utils() {
    }

    public static String upperCaseNoSpace(String s) {
        StringBuilder sbuf = new StringBuilder(s.toUpperCase());
        for (int i = sbuf.length() - 1; i >= 0; --i)
            if (Character.isWhitespace(sbuf.charAt(i))) sbuf.deleteCharAt(i);

        return sbuf.toString();
    }
}
