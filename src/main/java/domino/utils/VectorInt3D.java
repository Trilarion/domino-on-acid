/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.utils;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * Immutable 3D Vector of ints.
 */
public class VectorInt3D implements Serializable {

    public static final VectorInt3D ZERO = new VectorInt3D(0, 0, 0);
    private static final long serialVersionUID = -4884989050786243407L;
    public final int x, y, z;

    public VectorInt3D(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static VectorInt3D add(@NotNull VectorInt3D a, @NotNull VectorInt3D b) {
        return new VectorInt3D(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static VectorInt3D subtract(@NotNull VectorInt3D a, @NotNull VectorInt3D b) {
        return new VectorInt3D(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    /**
     * @param v
     * @param x
     * @return
     */
    public static VectorInt3D newX(@NotNull VectorInt3D v, int x) {
        return new VectorInt3D(x, v.y, v.z);
    }

    /**
     * @param v
     * @param y
     * @return
     */
    public static VectorInt3D newY(@NotNull VectorInt3D v, int y) {
        return new VectorInt3D(v.x, y, v.z);
    }

    /**
     * @param v
     * @param z
     * @return
     */
    public static VectorInt3D newZ(@NotNull VectorInt3D v, int z) {
        return new VectorInt3D(v.x, v.y, z);
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof VectorInt3D)) return false;

        final VectorInt3D other = (VectorInt3D) obj;

        return x == other.x && y == other.y && z == other.z;
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        return (x * 29 + y) * 31 + z;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }
}
