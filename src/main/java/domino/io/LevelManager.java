/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;

/**
 * <p>This class manages infix tautology strings used as levels for the game.</p>
 */
public class LevelManager {
    private String[] levels;

    /**
     * <p>Creates a new level manager that reads its levels from {@code levelFileURL}.</p>
     * <p>Note that syntax checking is not done! The input has to be well-formed.
     * A well-formed line is a description followed by ':',
     * followed by an infix formula whose variables consist only of the 26 basic letters.
     * The description may be left out together with the colon.</p>
     */
    public LevelManager(URL levelFileURL) {
        InputStream instream = null;
        try {
            URLConnection ucon = levelFileURL.openConnection();
            ucon.setDoInput(true);
            ucon.setDoOutput(false);
            //ucon.connect(); should be done implicitly by getInputStream()
            instream = ucon.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(instream));

            LinkedList<String> levelList = new LinkedList<>();

            String line;
            while ((line = in.readLine()) != null) {
                if (line.lastIndexOf(':') >= 0) line = line.substring(line.lastIndexOf(':') + 1);

                line = line.toUpperCase();
                StringBuilder sbuf = new StringBuilder(line);
                for (int i = sbuf.length() - 1; i >= 0; --i)
                    if (Character.isWhitespace(sbuf.charAt(i))) sbuf.deleteCharAt(i);
                line = sbuf.toString();

                if (!line.isEmpty()) levelList.add(line);
            }

            levels = levelList.toArray(new String[0]);
            instream.close();
        } catch (IOException x) {
            try {
                if (instream != null) instream.close();
            } catch (Exception ignored) {
            }
        }
    }

    /**
     * <p>Returns the number of levels available.</p>
     */
    public int numberOfLevels() {
        return levels.length;
    }

    /**
     * <p>Returns the {@code n}-th level or {@code null} if a level with this
     * number does not exist.</p>
     */
    public String getLevel(int n) {
        if (n < 0 || n >= numberOfLevels()) return null;
        return levels[n];
    }

    /**
     * <p>Returns the level number for {@code level} or -1 if {@code level}
     * is not in the database.</p>
     * <p>Note that the level strings must match exactly. Logical equivalence is not
     * enough for the level to be found.</p>
     */
    public int getLevelNumberFor(String level) {
        for (int i = 0; i < numberOfLevels(); ++i) if (levels[i].equals(level)) return i;
        return -1;
    }
}