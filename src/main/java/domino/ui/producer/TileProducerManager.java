/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.producer;

import domino.logic.UnboundIdAssigner;
import domino.logic.tree.FormulaDominoTree;
import domino.model.ref.BonusRef;
import domino.model.tile.EnhancedDominoTile;
import domino.utils.Utils;

import java.util.*;

/**
 *
 */
public class TileProducerManager {

    /*
     * <p>contains ProducerMenuItems. The 1st entry is always the fixed producer.</p>
     */
    private final List<ProducerMenuItem> normalProducers = new ArrayList<>();
    private final int fixedProducerIndex;
    private ProducerMenuItem extraProducer;
    private int activeProducer;
    private int smallestSortOrderIndex;

    /**
     * <p>Creates a new {@code TileProducerManager} with the same fixed producer
     * as orig. Other producers are NOT copied.</p>
     */
    public TileProducerManager(TileProducerManager original) {
        this(original.fixedProducerIndex, original.getStringRepresentationForProducer(original.fixedProducerIndex));
    }

    /**
     * <p>Creates a new {@code TileProducerManager} with a given fixed producer.</p>
     * <p>The fixed producer can not be disabled or removed and it will always be
     * activated by {@code activateProducerIfEnabled(fixedProducerNum)} (if there
     * are less than {@code fixedProducerNum} producers, the fixed producer will
     * be available with other indices, too).</p>
     */
    public TileProducerManager(int fixedProducerNum, String fixedProducerStringRep) {
        fixedProducerIndex = fixedProducerNum;
        addProducerForStringRepresentation(fixedProducerStringRep);
        getProducer(fixedProducerNum).setFixed();
        activeProducer = fixedProducerNum;
    }

    /**
     * <p>Returns {@code true} if {@code formula} (!!after converting to
     * uppercase removing all whitespace from it!!) is safe for
     * further processing. This does not mean
     * it is free of syntax errors, but if the formula is accepted by
     * newFromInfix(), it can be used in this program. This includes a check whether
     * bound variables consist of only ASCII letters. If
     * unboundAllowed==true, unbound variables will not be reported as an error.</p>
     */
    public static boolean isFormulaSafe(String formula, boolean unboundAllowed) {
    /*
      The following syntax check is only to make sure that all bound variables
      are simple ASCII letter sequences without whitespace in between, because
      we can only generate color values for those. The test also makes sure
      that there is at least one operand.
      There may be other syntax
      errors in level but these are not detected by the following scan.
    */
        formula = formula.toUpperCase();
        StringBuilder operand = new StringBuilder();
        int i = 0;
        boolean foundOp = false;
        while (i < formula.length()) {
            while (i < formula.length() &&
                    (Character.isWhitespace(formula.charAt(i)) || formula.charAt(i) == '(')
            ) i++;

            operand.delete(0, Integer.MAX_VALUE);
            while (i < formula.length() &&
                    formula.charAt(i) != ')' &&
                    !formula.regionMatches(i, "->", 0, 2)
            ) {
                operand.append(formula.charAt(i));
                i++;
            }

            String opstr = operand.toString().trim();
            if (opstr.isEmpty()) return false;
            if (!"_|_".equals(opstr)) {
                boolean validOp = false;
                if (unboundAllowed && opstr.charAt(0) == 'X' && opstr.length() > 1) {
                    int j = 1;
                    while (j < opstr.length() && Character.isDigit(opstr.charAt(j))) ++j;
                    validOp = (j == opstr.length());
                }

                if (!validOp)
                    for (int j = 0; j < opstr.length(); ++j)
                        if (opstr.charAt(j) < 'A' || opstr.charAt(j) > 'Z') return false;

            }
            foundOp = true;

            if (formula.regionMatches(i, "->", 0, 2)) {
                i += 2;
                continue;
            }

            while (i < formula.length() &&
                    (Character.isWhitespace(formula.charAt(i)) || formula.charAt(i) == ')')
            ) i++;

            if (i < formula.length() && !formula.regionMatches(i, "->", 0, 2)) return false;
            i += 2;
        }
        return foundOp;
    }

    /**
     * <p>Removes all producers except the fixed producer.</p>
     */
    public void removeAllProducers() {
        setExtraProducer(null);
        normalProducers.removeIf(prod -> !prod.isFixed());
    }

    public void removeDisabledProducers() {
        normalProducers.removeIf(prod -> !(prod.isFixed() || prod.isEnabled()));
    }

    /**
     * <p>Turns the extra producer into a regular producer.
     */
    public void extraProducerToRegularProducerIfPossible(String name) {
        if (extraProducer == null) return;
        String srep = getStringRepresentationForProducer(-1);
        if (srep != null && srep.isEmpty()) return;
        addProducerForStringRepresentation(name + ": " + srep);
    }

    /**
     * <p>sreps with syntax errors will be silently ignored.</p>
     */
    public void addProducerForStringRepresentation(String srep) {
    /*
      NOTE: This method is called from the constructor to add the fixed producer
      (i.e. the very first producer) to normalProducers, so this method must not
      assume normalProducers is non-empty
    */

        String name = "";
        int pos = srep.lastIndexOf(':');
        if (pos >= 0) {
            name = srep.substring(0, pos).trim();
            srep = srep.substring(pos + 1);
        }

        FormulaDominoTree tree;
        try {
            tree = recursiveBuildTree(new StringBuffer(srep), new HashMap<>(), null, -1, UnboundIdAssigner.newAssigner());
        } catch (Exception x) {
            return;
        }

        EnhancedDominoTileProducer newProducer = new NormalEnhancedDominoTileProducer(tree);
        smallestSortOrderIndex--;
        ProducerMenuItem newItem = new ProducerMenuItem(smallestSortOrderIndex, newProducer, name, true);
        normalProducers.add(newItem);
    }

    /**
     * Throws IllegalArgumentException if stg is wrong.
     */
    protected FormulaDominoTree recursiveBuildTree(StringBuffer rest, HashMap<String, BonusRef> formulaToBonusRef, FormulaDominoTree parent, int parentIndex, UnboundIdAssigner assign) {
        int pos = rest.toString().indexOf(';');
        if (pos < 0) pos = rest.length();

        String srep = rest.substring(0, pos).trim();
        rest.delete(0, pos + 1);

        if (srep.isEmpty()) return null;

        List<String> parts = new ArrayList<>();
        while (0 <= (pos = srep.indexOf(','))) {
            parts.add(srep.substring(0, pos));
            srep = srep.substring(pos + 1);
        }
        parts.add(srep);

        Vector subParts = new Vector(); // TODO difficult to generify, can contain strings and lists of strings
        for (String part : parts) {
            srep = part;
            Vector subPart = new Vector();
            while (0 <= (pos = srep.indexOf('['))) {
                int pos2 = srep.indexOf(']');
                if (pos2 <= pos + 1) throw new RuntimeException();
                String formula = srep.substring(pos + 1, pos2);
                if (!isFormulaSafe(formula, true)) throw new RuntimeException();
                subPart.add(formula);
                srep = srep.substring(0, pos) + " " + srep.substring(pos2 + 1);
            }
            if (!isFormulaSafe(srep, true)) throw new RuntimeException();
            subPart.add(srep);
            subParts.add(subPart);
        }

        //now subParts is a Vector of Vectors of Strings that describe infix formulas
        //the last String in each (sub-)Vector is the root or leaf, the other strings are
        //bonuses. The first Vector describes the root, the others are leaves.

        FormulaDominoTree tree;

        Vector part1 = (Vector) subParts.firstElement(); // TODO if replaced with List, this prevents the app from starting??
        String root = Utils.upperCaseNoSpace((String) part1.lastElement());

        if (subParts.size() == 1) //only a root without leaves
        {
            try {
                BonusRef bonusRef = formulaToBonusRef.get(root);
                if (bonusRef == null) {
                    String[] lb = {root};
                    tree = FormulaDominoTree.newFromInfix(root, lb, lb);
                    tree.useBonusSquare(0, 0);
                    if (parent != null) tree.attachToSubtreeConnection(parent, parentIndex);
                } else {
                    parent.useBonusSquare(parentIndex, bonusRef.getOwner(), bonusRef.getSubtreeIndex(), bonusRef.getNewBonusForSubtreeIndex());
                    tree = null;
                }
            } catch (Exception x) {
                throw new RuntimeException();
            }
        } else //at least one leaf
        {
            String[] globalBonus = new String[part1.size() - 1];
            for (int j = 0; j < part1.size() - 1; ++j)
                globalBonus[j] = Utils.upperCaseNoSpace((String) part1.get(j));
            String[] leaves = new String[subParts.size() - 1];
            String[][] bonus = new String[subParts.size() - 1][];
            for (int i = 1; i < subParts.size(); ++i) {
                Vector<String> subPart = (Vector) subParts.get(i);
                leaves[i - 1] = Utils.upperCaseNoSpace(subPart.lastElement());
                bonus[i - 1] = new String[subPart.size() - 1];
                for (int j = 0; j < subPart.size() - 1; ++j)
                    bonus[i - 1][j] = Utils.upperCaseNoSpace(subPart.get(j));
            }

            try {
                tree = FormulaDominoTree.newFromInfix(root, leaves, globalBonus);

                if (parent != null) tree.attachToSubtreeConnection(parent, parentIndex);

                for (int i = 0; i < leaves.length; ++i)
                    if (bonus[i].length > 0) throw new RuntimeException("per-branch bonuses not supported");

                for (int i = 0; i < tree.numberOfSubtreeConnections(); ++i) {
                    HashMap<String, BonusRef> newMap = new HashMap<>(formulaToBonusRef);
                    for (int num = 0; num < globalBonus.length; ++num) {
                        newMap.put(globalBonus[num], new BonusRef(tree, i, num));
                    }

                    recursiveBuildTree(rest, newMap, tree, i, assign);
                }
            } catch (Exception x) {
                throw new RuntimeException();
            }
        }

        return tree;
    }

    /**
     * <p>-1 is the extra producer. {@link BonusEnhancedDominoTileProducer}s will be represented by an empty string.</p>
     * <p>Note, that if the producers name is an empty string, the returned string
     * will not contain a ":".</p>
     *
     * @throws IllegalArgumentException if there is no producer with index {@code i}.
     */
    public String getStringRepresentationForProducer(int i) {
        ProducerMenuItem prod = getProducer(i);
        String name = prod.name();
        if (name != null && !name.isEmpty()) name += ": ";
        EnhancedDominoTile edt = prod.producer().produce();
        String ret = name + edt.toStringRepresentation();
        edt.removeIdAssignments();
        return ret;
    }

    /**
     * <p>Returns the number of producers including the fixed producer but
     * excluding the extra producer.</p>
     */
    public int numberOfProducers() {
        return normalProducers.size();
    }

    /**
     * <p>Returns the producer with index {@code i}. -1 is the extra producer.</p>
     *
     * @throws IllegalArgumentException if there is no producer with index {@code i}.
     */
    protected ProducerMenuItem getProducer(int index) {
        if (index < -1 || (index == -1 && extraProducer == null) || (index >= normalProducers.size() && index > fixedProducerIndex))
            throw new IllegalArgumentException("producer not found");

        if (index == -1)
            return extraProducer;

        if (index == fixedProducerIndex || index >= normalProducers.size() || (index < fixedProducerIndex && index >= normalProducers.size() - 1))
            index = 0;
        else {
            if (index < fixedProducerIndex) ++index;
        }
        return normalProducers.get(index);
    }

    /**
     * <p>Sets {@code prod} as the new extra producer.</p>
     * <p>The old extra producer (if any) will be removed. Unless {@code prod}
     * is {@code null}, it will become the new active producer. If {@code prod}
     * is {@code null}, another producer will be activated.</p>
     */
    public void setExtraProducer(EnhancedDominoTileProducer producer) {
        if (producer == null) {
            if (activeProducer == -1) activateNextEnabledProducer();
            extraProducer = null;
        } else {
            smallestSortOrderIndex--;
            extraProducer = new ProducerMenuItem(smallestSortOrderIndex, producer, "", true);
            activeProducer = -1;
        }
    }

    /**
     * <p>Activates the producer {@code i >= 0} if it is enabled.</p>
     * <p>If {@code i} is invalid, nothing happens.</p>
     *
     * @return true if producer has been enabled successfully
     */
    public boolean activateProducerIfEnabled(int index) {
        try {
            if (getProducer(index).isEnabled()) {
                activeProducer = index;
                return true;
            }
        } catch (IllegalArgumentException ignored) {
        }
        return false;
    }

    public void activateNextEnabledProducer() {
        activateNextPrevProducer(+1, getProducer(activeProducer).sortOrderIndex());
    }

    public void activateFirstEnabledProducer() {
        activateNextPrevProducer(+1, smallestSortOrderIndex - 1);
    }

    /**
     * <p>If {@code np==+1} activates the next, if {@code np==-1}
     * activates the previous producer in the sort order, relative to the
     * producer with sort order index {@code idx}.</p>
     */
    public void activateNextPrevProducer(int np, int idx) {
        int smallestGreater = Integer.MAX_VALUE;
        int smallestGreaterProducer = Integer.MAX_VALUE;
        int smallest = idx;
        int smallestProducer = activeProducer;
        if (extraProducer != null) {
            smallest = getProducer(-1).sortOrderIndex();
            smallestProducer = -1;
            if (np * smallest > np * idx) {
                smallestGreater = smallest;
                smallestGreaterProducer = smallestProducer;
            }
        }

        for (int i = 0; i < numberOfProducers(); ++i) {
            ProducerMenuItem prod = getProducer(i);
            if (!prod.isEnabled()) continue;

            if (np * prod.sortOrderIndex() < np * smallest) {
                smallest = prod.sortOrderIndex();
                smallestProducer = i;
            }
            if ((np * prod.sortOrderIndex() > np * idx) &&
                    (np * prod.sortOrderIndex() < np * smallestGreater)) {
                smallestGreater = prod.sortOrderIndex();
                smallestGreaterProducer = i;
            }
        }
        if (smallestGreater == Integer.MAX_VALUE)
            activeProducer = smallestProducer;
        else
            activeProducer = smallestGreaterProducer;
    }

    /**
     * <p>Puts the currently active producer to the front of the sort order.
     * This only affects {@link #activateNextEnabledProducer()} but not
     * {@link #activateProducerIfEnabled(int)}.</p>
     */
    public void sortActiveToFront() {
        smallestSortOrderIndex--;
        getProducer(activeProducer).setSortOrderIndex(smallestSortOrderIndex);
    }

    /**
     * <p>Returns a tile produced by the currently active producer.</p>
     */
    public EnhancedDominoTile produce() {
        return getProducer(activeProducer).producer().produce();
    }

    public List<ProducerMenuItem> getNormalProducers() {
        return normalProducers;
    }
}
