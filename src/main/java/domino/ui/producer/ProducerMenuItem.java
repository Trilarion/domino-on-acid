/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.producer;

import javax.swing.*;

/**
 * Item in the Tiles menu
 */
public class ProducerMenuItem extends JCheckBoxMenuItem { // TODO decouple ProducerMenuItem from JCheckBoxMenuItem

    private final EnhancedDominoTileProducer producer;
    private int index;
    private boolean fixed;

    public ProducerMenuItem(int sortOrderIndex, EnhancedDominoTileProducer producer, String name, boolean selected) {
        super(name, selected);
        this.producer = producer;
        index = sortOrderIndex;
    }

    public EnhancedDominoTileProducer producer() {
        return producer;
    }

    public String name() {
        return getText();
    }

    public int sortOrderIndex() {
        return index;
    }

    public void setSortOrderIndex(int newIndex) {
        index = newIndex;
    }

    public boolean isFixed() {
        return fixed;
    }

    public void setFixed() {
        fixed = true;
    }

    public boolean isEnabled() {
        return isSelected();
    }

}
