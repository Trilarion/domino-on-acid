/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.mouse;

import java.awt.*;

public class MoveBoard extends MouseEventProcessorState {
    private final MouseEventProcessor mouseEventProcessor;
    private Point viewportPosition;

    public MoveBoard(MouseEventProcessor mouseEventProcessor) {
        this.mouseEventProcessor = mouseEventProcessor;
    }

    public void init() {
        viewportPosition = mouseEventProcessor.getWorld().graphicalRepresentation().getViewport().getViewPosition();
    }

    public void handleMouseMotion() {
        if (mouseEventProcessor.getModifiers() == 0 || mouseEventProcessor.isDragInhibitor()) return;

        int curx = mouseEventProcessor.getMousePos().x;
        int cury = mouseEventProcessor.getMousePos().y;
        int difx = mouseEventProcessor.getDragStart().x - curx;
        int dify = mouseEventProcessor.getDragStart().y - cury;
        mouseEventProcessor.getWorld().graphicalRepresentation().moveViewportAbsolute(viewportPosition.x + difx, viewportPosition.y + dify);
        if (mouseEventProcessor.isLastIdleStateTracksMovement()) mouseEventProcessor.getLastIdleState().trackMovement();
    }

    public void handleModifierChange() {
        mouseEventProcessor.setState(mouseEventProcessor.getLastIdleState());
        if (mouseEventProcessor.isLastIdleStateTracksMovement()) mouseEventProcessor.getLastIdleState().trackMovement();
    }
}
