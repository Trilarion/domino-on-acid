/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.mouse;

import domino.ui.UIConstants;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 *
 */
public class Zoom extends MouseEventProcessorState { // TODO make full zoom corresponding to about half the screen height
    // TODO Zoom is achieved with shift and ctrl (intended?)
    private final MouseEventProcessor mouseEventProcessor;
    private boolean goingright;
    private boolean goingdown;
    private boolean havedirection;
    private int startSquareSize;
    private Point viewportCenter;
    private Point2D.Double startD;

    public Zoom(MouseEventProcessor mouseEventProcessor) {
        this.mouseEventProcessor = mouseEventProcessor;
    }

    public void init() {
        havedirection = false;
        startSquareSize = mouseEventProcessor.getWorld().getSquareSize();

        Dimension d = mouseEventProcessor.getWorld().graphicalRepresentation().getViewport().getExtentSize();
        viewportCenter = new Point(d.width >> 1, d.height >> 1);

        if (mouseEventProcessor.isZoomingIn())
            startD = mouseEventProcessor.getWorld().graphicalRepresentation().translateToBoardNormalized(mouseEventProcessor.getDragStart());
        else
            startD = mouseEventProcessor.getWorld().graphicalRepresentation().translateToBoardNormalized(viewportCenter);
    }

    public void handleMouseMotion() {
        if (mouseEventProcessor.getModifiers() == 0 || mouseEventProcessor.isDragInhibitor()) return;

        int curx = mouseEventProcessor.getMousePos().x;
        int cury = mouseEventProcessor.getMousePos().y;
        long difx = Math.abs(curx - mouseEventProcessor.getDragStart().x);
        long dify = Math.abs(cury - mouseEventProcessor.getDragStart().y);

        difx -= UIConstants.ZOOM_DRAG_THRESHOLD;
        if (difx < 0) difx = 0;
        dify -= UIConstants.ZOOM_DRAG_THRESHOLD;
        if (dify < 0) dify = 0;

        if (difx + dify != 0) {
            if (!havedirection) {
                goingright = (curx > mouseEventProcessor.getDragStart().x);
                goingdown = (cury > mouseEventProcessor.getDragStart().y);
                havedirection = true;
            }

            if ((curx > mouseEventProcessor.getDragStart().x) != goingright) difx = 0;
            if ((cury > mouseEventProcessor.getDragStart().y) != goingdown) dify = 0;

            double dif = Math.sqrt(difx * difx + dify * dify);

            if (mouseEventProcessor.isZoomingIn())
                mouseEventProcessor.getWorld().setSquareSize(startSquareSize + (int) dif / 2, startD.x, startD.y, curx, cury);
            else
                mouseEventProcessor.getWorld().setSquareSize(startSquareSize - (int) dif / 2, startD.x, startD.y, viewportCenter.x, viewportCenter.y);
        }

        if (mouseEventProcessor.isLastIdleStateTracksMovement()) mouseEventProcessor.getLastIdleState().trackMovement();
    }

    public void handleModifierChange() {
        mouseEventProcessor.setState(mouseEventProcessor.getLastIdleState());
        if (mouseEventProcessor.isLastIdleStateTracksMovement()) mouseEventProcessor.getLastIdleState().trackMovement();
    }

}
