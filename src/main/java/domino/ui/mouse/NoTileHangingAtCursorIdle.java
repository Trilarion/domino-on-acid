/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.mouse;

import domino.logic.tree.FormulaDominoTree;
import domino.ui.UIConstants;
import domino.ui.block.BonusDominoBlock;
import domino.ui.block.DominoBlock;
import domino.ui.iso.IsoBlock;
import domino.ui.iso.JIsoWorld;
import domino.ui.producer.BonusEnhancedDominoTileProducer;
import domino.ui.producer.EnhancedDominoTileProducer;
import domino.ui.producer.NormalEnhancedDominoTileProducer;
import domino.model.tile.DominoTile;
import domino.model.tile.EnhancedDominoTile;
import domino.utils.VectorInt3D;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Collection;

public class NoTileHangingAtCursorIdle extends MouseEventProcessorState {

    private final MouseEventProcessor mouseEventProcessor;

    public NoTileHangingAtCursorIdle(MouseEventProcessor mouseEventProcessor) {
        this.mouseEventProcessor = mouseEventProcessor;
    }

    public void init() {
        mouseEventProcessor.setLastIdleState(mouseEventProcessor.getNoTileHangingAtCursorIdle());
        mouseEventProcessor.setLastIdleStateTracksMovement(false);
    }

    public void handleMouseMotion() {
        if (mouseEventProcessor.getModifiers() == 0 || mouseEventProcessor.isDragInhibitor()) return;

        int mod = mouseEventProcessor.getModifiers();

        if (MouseEventProcessor.ctrlKey(mod) || MouseEventProcessor.shiftKey(mod)) {
            if (!(MouseEventProcessor.leftButton(mod) || MouseEventProcessor.rightButton(mod) || MouseEventProcessor.middleButton(mod)))
                return;
            mouseEventProcessor.setDragStart(mouseEventProcessor.getLastModChangeMousePos());
            mouseEventProcessor.setZoomingIn(MouseEventProcessor.ctrlKey(mod));
            mouseEventProcessor.setState(mouseEventProcessor.getZoom());
            mouseEventProcessor.getState().handleMouseMotion();
        } else if (MouseEventProcessor.leftButton(mod) && (!MouseEventProcessor.altKey(mod))) {
            mouseEventProcessor.setDragStart(mouseEventProcessor.getLastModChangeMousePos());
            mouseEventProcessor.setState(mouseEventProcessor.getMoveBoard());
            mouseEventProcessor.getState().handleMouseMotion();
        } else if (MouseEventProcessor.rightButton(mod) || MouseEventProcessor.middleButton(mod) || (MouseEventProcessor.leftButton(mod) && MouseEventProcessor.altKey(mod))) {
            Point p = new Point(mouseEventProcessor.getMousePos());
            p.x -= mouseEventProcessor.getLastModChangeMousePos().x;
            p.y -= mouseEventProcessor.getLastModChangeMousePos().y;
            int dif = (int) Math.round(Math.sqrt(p.x * (double) p.x + p.y * (double) p.y));
            if (dif > UIConstants.TILE_TEAR_OFF_THRESHOLD)
                attachTileToMouse(true);
        }
    }

    public void handleMouseClick(MouseEvent e) {
        if (mouseEventProcessor.getModifiers() != 0 && (!MouseEventProcessor.altKey(mouseEventProcessor.getModifiers())))
            return;
        attachTileToMouse(false);
    }

    public void handleKeyPress(KeyEvent e) {
        int kc = e.getKeyCode();
        if (kc == MouseEventProcessor.KC_PICKUP_TILE) attachTileToMouse(false);
        else if (kc >= MouseEventProcessor.KC_0 && kc <= MouseEventProcessor.KC_9) {
            if (mouseEventProcessor.getProducerManager().activateProducerIfEnabled(kc - MouseEventProcessor.KC_0 - 1))
                mouseEventProcessor.produceHangingTile();
        }
    }

    public void attachTileToMouse(boolean tearOff) {
        //NOTE: This function is also called on right-drag if the TEAR_OFF_THRESHOLD
        //has been exceeded (see mouseDragged(MouseEvent) )

        int mod = mouseEventProcessor.getModifiers();

        if (true) //used to be if (rightButton(mod) but I decided to remove this restriction
        {
            Point clickpos = mouseEventProcessor.getMousePos();
            if (tearOff) clickpos = mouseEventProcessor.getLastModChangeMousePos();

            JIsoWorld jIsoWorld = mouseEventProcessor.getWorld().graphicalRepresentation();
            //try with z==1 first to give high blocks precedence
            VectorInt3D clickLocation = jIsoWorld.translateToIsoWorld(clickpos, 1).getA();
            Collection<IsoBlock> blocks = mouseEventProcessor.getWorld().blocksAtLocation(clickLocation);
            if (blocks == null) {
                clickLocation = jIsoWorld.translateToIsoWorld(clickpos, 0).getA();
                blocks = mouseEventProcessor.getWorld().blocksAtLocation(clickLocation);
            }

            boolean produceHangingTile = false;

            if (blocks == null) //user clicked empty space
            {
                if (!tearOff) produceHangingTile = true;
            } else //user clicked on an IsoBlock
            {
                EnhancedDominoTileProducer newCurrentExtraProducer = null;
                DominoBlock clickedBlock =
                        (DominoBlock) blocks.iterator().next();
                DominoTile clickedTile = (DominoTile) clickedBlock.parentObject();

                if (tearOff && clickedTile == mouseEventProcessor.getMasterTile()) return;

                boolean clickedBonusBlock = (clickedBlock instanceof BonusDominoBlock);
                boolean bonusBlockIsPartOfBiggerTile = clickedTile.numberOfBlocks() > 1;

                if (clickedBonusBlock && (!tearOff || !bonusBlockIsPartOfBiggerTile)) { //user clicked on a bonus block and is either not dragging or the
                    //bonus block is a tile of its own
                    if (clickedBlock.isActive()) {
                        BonusDominoBlock bonusBlock =
                                (BonusDominoBlock) clickedBlock;
                        newCurrentExtraProducer = new BonusEnhancedDominoTileProducer(
                                bonusBlock.formulaDominoTree(),
                                bonusBlock.subtreeConnectionIndex(),
                                bonusBlock.newBonusForSubtreeConnectionIndex());

                        if (tearOff) ((EnhancedDominoTile) clickedTile).dieWithChildren();
                    }
                } else //user clicked on a non-bonus block or is dragging on a bonus block
                {    //that is part of a bigger tile
                    FormulaDominoTree tree = (FormulaDominoTree) clickedTile.rootTree().cloneDT();
                    tree.detachFromParent();
                    newCurrentExtraProducer = new NormalEnhancedDominoTileProducer(tree);

                    if (tearOff) ((EnhancedDominoTile) clickedTile).dieWithChildren();
                }

                if (newCurrentExtraProducer != null) {
                    mouseEventProcessor.getProducerManager().setExtraProducer(newCurrentExtraProducer);
                    produceHangingTile = true;
                }
            }

            if (produceHangingTile) {
                if (tearOff) mouseEventProcessor.setDragInhibitor(true);
                mouseEventProcessor.produceHangingTile();
            }
        }
    }

}
