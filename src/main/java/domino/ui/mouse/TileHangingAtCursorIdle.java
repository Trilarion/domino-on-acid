/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.mouse;

import domino.ui.MainFrame;
import domino.ui.UIConstants;
import domino.ui.block.DominoBlock;
import domino.ui.iso.IsoBlock;
import domino.ui.iso.JIsoWorld;
import domino.utils.Pair;
import domino.utils.VectorDouble3D;
import domino.utils.VectorInt3D;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Iterator;

public class TileHangingAtCursorIdle extends MouseEventProcessorState {
    private final MouseEventProcessor mouseEventProcessor;
    private final MainFrame mainFrame;
    private boolean validDropLocation;
    private VectorInt3D dropLocation;

    public TileHangingAtCursorIdle(MouseEventProcessor mouseEventProcessor, MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        this.mouseEventProcessor = mouseEventProcessor;
    }

    public void init() {
        if (mouseEventProcessor.getLastIdleState() != this) {
            validDropLocation = false;
            dropLocation = null;
            mouseEventProcessor.setLastIdleState(mouseEventProcessor.getTileHangingAtCursorIdle());
            mouseEventProcessor.setLastIdleStateTracksMovement(true);
        }
    }

    public void handleMouseMotion() {
        if (mouseEventProcessor.getModifiers() == 0 || mouseEventProcessor.isDragInhibitor()) {
            trackMovement();
            return;
        }

        int mod = mouseEventProcessor.getModifiers();

        if (MouseEventProcessor.ctrlKey(mod) || MouseEventProcessor.shiftKey(mod)) {
            if (!(MouseEventProcessor.leftButton(mod) || MouseEventProcessor.rightButton(mod) || MouseEventProcessor.middleButton(mod))) {
                trackMovement();
                return;
            }
            mouseEventProcessor.setDragStart(mouseEventProcessor.getLastModChangeMousePos());
            mouseEventProcessor.setZoomingIn(MouseEventProcessor.ctrlKey(mod));
            mouseEventProcessor.setState(mouseEventProcessor.getZoom());
            mouseEventProcessor.getState().handleMouseMotion();
        } else if (MouseEventProcessor.leftButton(mod) && (!MouseEventProcessor.altKey(mod))) {
            mouseEventProcessor.setDragStart(mouseEventProcessor.getLastModChangeMousePos());
            mouseEventProcessor.setState(mouseEventProcessor.getMoveBoard());
            mouseEventProcessor.getState().handleMouseMotion();
        } else if (MouseEventProcessor.rightButton(mod) || MouseEventProcessor.middleButton(mod) || (MouseEventProcessor.leftButton(mod) && MouseEventProcessor.altKey(mod))) {
            mouseEventProcessor.setDragStart(mouseEventProcessor.getLastModChangeMousePos());
            mouseEventProcessor.setState(mouseEventProcessor.getRotateTile());
            mouseEventProcessor.getState().handleMouseMotion();
        } else trackMovement();
    }

    public void trackMovement() {
        Point p = mouseEventProcessor.getMousePos();
        JIsoWorld jIsoWorld = mouseEventProcessor.getWorld().graphicalRepresentation();
        Pair<VectorInt3D, VectorDouble3D> temp = jIsoWorld.translateToIsoWorld(p, 0);
        VectorInt3D clickLocation = temp.getA();
        VectorDouble3D clickShift = temp.getB();

        if (!clickLocation.equals(dropLocation)) {
            dropLocation = clickLocation;

            if (validDropLocation) {
                mouseEventProcessor.getHangingTile().deunifyIfNecessary();
                validDropLocation = false;
            }

            if (mouseEventProcessor.getHangingTile().unifyWithNearestTile(clickLocation)) {
                validDropLocation = true;
            }
        }

        //adjust to get shift for middle of top side rather than upper left corner
        clickShift = VectorDouble3D.newX(clickShift, clickShift.x - 0.5);
        if (clickShift.x < 0) {
            clickShift = VectorDouble3D.newX(clickShift, clickShift.x + 1.0);
            clickLocation = VectorInt3D.newX(clickLocation, clickLocation.x - 1);
        }
        clickShift = VectorDouble3D.newY(clickShift, clickShift.y - 0.5);
        if (clickShift.y < 0) {
            clickShift = VectorDouble3D.newY(clickShift, clickShift.y + 1.0);
            clickLocation = VectorInt3D.newY(clickLocation, clickLocation.y - 1);
        }
        mouseEventProcessor.getWorld().moveObject(mouseEventProcessor.getHangingTile(), clickLocation, clickShift);
    }

    public void handleMouseClick(MouseEvent e) {
        int mod = mouseEventProcessor.getModifiers();

        if (mouseEventProcessor.leftButton(e) && (!MouseEventProcessor.altKey(mod)))
            dropTile();
        else if (mouseEventProcessor.rightButton(e) || (mouseEventProcessor.leftButton(e) && MouseEventProcessor.altKey(mod))) {
            mouseEventProcessor.getProducerManager().activateNextEnabledProducer();
            createNewTile();
        } else if (mouseEventProcessor.middleButton(e)) rotateRight();
    }

    protected void createNewTile() {
        VectorDouble3D sh = mouseEventProcessor.getHangingTile().getShift();
        VectorInt3D loc = mouseEventProcessor.getHangingTile().getReferenceBlockLocation();
        mouseEventProcessor.getHangingTile().deunifyIfNecessary();
        mouseEventProcessor.getWorld().removeObject(mouseEventProcessor.getHangingTile());
        mouseEventProcessor.getHangingTile().removeIdAssignments();
        validDropLocation = false;
        dropLocation = null;

        mouseEventProcessor.setHangingTile(mouseEventProcessor.getProducerManager().produce());
        mouseEventProcessor.getHangingTile().setOrientation(mouseEventProcessor.getHangingTileOrientation());
        mouseEventProcessor.getWorld().addObject(mouseEventProcessor.getHangingTile(), loc, sh);
        mouseEventProcessor.getWorld().putInTopLayer(mouseEventProcessor.getHangingTile());

        trackMovement(); //to recheck if location is valid drop location and unify
        //if necessary
    }

    protected void dropTile() {
        if (!validDropLocation) {
            mouseEventProcessor.getHangingTile().deunifyIfNecessary();

            if ((MouseEventProcessor.ctrlKey(mouseEventProcessor.getModifiers()) || MouseEventProcessor.shiftKey(mouseEventProcessor.getModifiers())) && mouseEventProcessor.getHangingTile().hasNoAdjacentBlocks(dropLocation))
                validDropLocation = true;
            else {
                mouseEventProcessor.getWorld().removeObject(mouseEventProcessor.getHangingTile());
                mouseEventProcessor.getHangingTile().removeIdAssignments();
            }
        }

        if (validDropLocation) {
            mouseEventProcessor.getHangingTile().attachToUnified();
            mouseEventProcessor.getWorld().moveObject(mouseEventProcessor.getHangingTile(), dropLocation, VectorDouble3D.ZERO);
            mouseEventProcessor.getWorld().putInDefaultLayer(mouseEventProcessor.getHangingTile());

            mouseEventProcessor.getProducerManager().sortActiveToFront();

            if (mouseEventProcessor.getMasterTile().rootTree().numberOfOpenLeaves() == 0) {
                // level solved
                SwingUtilities.invokeLater(mainFrame::victory);
                mouseEventProcessor.setModifiers(0);
            }

            /*
             * check if the newly added tile has an IsoBlock very close to (or even
             * beyond) the edge of the board and increase the board size if necessary
             */
            int left = -mouseEventProcessor.getWorld().minusXDim() + UIConstants.FREE_SQUARES_AROUND_BOARD;
            int right = mouseEventProcessor.getWorld().plusXDim() - UIConstants.FREE_SQUARES_AROUND_BOARD;
            int top = -mouseEventProcessor.getWorld().minusYDim() + UIConstants.FREE_SQUARES_AROUND_BOARD;
            int bottom = mouseEventProcessor.getWorld().plusYDim() - UIConstants.FREE_SQUARES_AROUND_BOARD;
            Iterator<DominoBlock> iterator = mouseEventProcessor.getHangingTile().blocksIterator();
            while (iterator.hasNext()) {
                VectorInt3D location = ((IsoBlock) iterator.next()).getAbsoluteLocation();
                if (location.x > right) mouseEventProcessor.getWorld().addLines(1, location.x - right);
                if (location.x < left) mouseEventProcessor.getWorld().addLines(0, left - location.x);
                if (location.y > bottom) mouseEventProcessor.getWorld().addLines(3, location.y - bottom);
                if (location.y < top) mouseEventProcessor.getWorld().addLines(2, top - location.y);
            }
        }

        mouseEventProcessor.setState(mouseEventProcessor.getNoTileHangingAtCursorIdle());
    }

    public void handleKeyPress(KeyEvent e) {
        int kc = e.getKeyCode();
        if (kc == MouseEventProcessor.KC_ROTATE_RIGHT1 || kc == MouseEventProcessor.KC_ROTATE_RIGHT2 || kc == MouseEventProcessor.KC_ROTATE_RIGHT3)
            rotateRight();
        else if (kc == MouseEventProcessor.KC_ROTATE_LEFT) rotateLeft();
        else if (kc == MouseEventProcessor.KC_DROP_TILE) dropTile();
        else if (kc >= MouseEventProcessor.KC_0 && kc <= MouseEventProcessor.KC_9) {
            if (mouseEventProcessor.getProducerManager().activateProducerIfEnabled(kc - MouseEventProcessor.KC_0 - 1))
                createNewTile();
        }
    }

    protected void rotateRight() {
        mouseEventProcessor.getHangingTile().deunifyIfNecessary();
        validDropLocation = false;
        dropLocation = null;
        mouseEventProcessor.setHangingTileOrientation((mouseEventProcessor.getHangingTileOrientation() + 1) & 3);
        mouseEventProcessor.getHangingTile().setOrientation(mouseEventProcessor.getHangingTileOrientation());
        mouseEventProcessor.getWorld().putInTopLayer(mouseEventProcessor.getHangingTile());
        trackMovement();
    }

    protected void rotateLeft() {
        mouseEventProcessor.getHangingTile().deunifyIfNecessary();
        validDropLocation = false;
        dropLocation = null;
        mouseEventProcessor.setHangingTileOrientation((mouseEventProcessor.getHangingTileOrientation() + 3) & 3);
        mouseEventProcessor.getHangingTile().setOrientation(mouseEventProcessor.getHangingTileOrientation());
        mouseEventProcessor.getWorld().putInTopLayer(mouseEventProcessor.getHangingTile());
        trackMovement();
    }

}
