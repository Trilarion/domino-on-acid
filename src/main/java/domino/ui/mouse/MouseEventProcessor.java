/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.mouse;

import domino.ui.MainFrame;
import domino.ui.iso.IsoWorld;
import domino.ui.producer.TileProducerManager;
import domino.model.tile.DominoTile;
import domino.model.tile.EnhancedDominoTile;
import domino.utils.VectorDouble3D;
import domino.utils.VectorInt3D;

import java.awt.*;
import java.awt.event.*;

public class MouseEventProcessor implements MouseListener, MouseMotionListener, KeyListener {

    protected static final int KC_CTRL_R = 17;
    protected static final int KC_CTRL_L = 17;
    protected static final int KC_SHIFT_R = 16;
    protected static final int KC_SHIFT_L = 16;
    protected static final int KC_ALT_R = 65406;
    protected static final int KC_ALT_L = 18;
    protected static final int KC_SPACE = 32;
    protected static final int KC_0 = 48;
    protected static final int KC_9 = 57;

    protected static final int KC_ROTATE_RIGHT1 = 87;
    protected static final int KC_ROTATE_RIGHT2 = 69;
    protected static final int KC_ROTATE_RIGHT3 = 82;
    protected static final int KC_ROTATE_LEFT = 81;
    protected static final int KC_DROP_TILE = KC_SPACE;
    protected static final int KC_PICKUP_TILE = KC_SPACE;

    protected static final int MOD_LEFT_BUTTON = 1;
    protected static final int MOD_RIGHT_BUTTON = 2;
    protected static final int MOD_MIDDLE_BUTTON = 4;
    protected static final int MOD_CTRL = 8;
    protected static final int MOD_SHIFT = 16;
    protected static final int MOD_ALT = 32;
    protected static final int MOD_SPACE = 128;

    private final MouseEventProcessorState noTileHangingAtCursorIdle;
    private final MouseEventProcessorState zoom;
    private final MouseEventProcessorState moveBoard;
    private final MouseEventProcessorState tileHangingAtCursorIdle;
    private final MouseEventProcessorState rotateTile;

    private final IsoWorld world;
    private MouseEventProcessorState state;
    private Point dragStart;
    private Point lastModChangeMousePos;
    private Point mousePos = new Point(0, 0);
    private int modifiers;
    private boolean zoomingIn;
    private MouseEventProcessorState lastIdleState;
    private boolean lastIdleStateTracksMovement;
    private boolean dragInhibitor;
    private EnhancedDominoTile hangingTile;
    private int hangingTileOrientation;
    private TileProducerManager producerManager;
    private DominoTile masterTile; // this tile must not be removed

    public MouseEventProcessor(IsoWorld world, TileProducerManager producerManager, DominoTile masterTile, MainFrame mainFrame) {
        this.world = world;
        this.masterTile = masterTile;
        this.producerManager = producerManager;

        tileHangingAtCursorIdle = new TileHangingAtCursorIdle(this, mainFrame);
        noTileHangingAtCursorIdle = new NoTileHangingAtCursorIdle(this);
        zoom = new Zoom(this);
        moveBoard = new MoveBoard(this);
        rotateTile = new RotateTile(this);

        dragInhibitor = false;

        // set initial state
        setState(noTileHangingAtCursorIdle);

        // setup event processing
        this.world.graphicalRepresentation().addMouseListener(this);
        this.world.graphicalRepresentation().addMouseMotionListener(this);
        this.world.graphicalRepresentation().addKeyListener(this);
    }

    protected static boolean leftButton(int modifiers) {
        return (modifiers & MOD_LEFT_BUTTON) != 0;
    }

    protected static boolean rightButton(int modifiers) {
        return (modifiers & MOD_RIGHT_BUTTON) != 0;
    }

    protected static boolean middleButton(int modifiers) {
        return (modifiers & MOD_MIDDLE_BUTTON) != 0;
    }

    protected static boolean ctrlKey(int modifiers) {
        return (modifiers & MOD_CTRL) != 0;
    }

    protected static boolean shiftKey(int modifiers) {
        return (modifiers & MOD_SHIFT) != 0;
    }

    protected static boolean altKey(int modifiers) {
        return (modifiers & MOD_ALT) != 0;
    }

    protected boolean leftButton(MouseEvent e) {
        return (e.getModifiers() & InputEvent.BUTTON1_MASK) != 0;
    }

    protected boolean rightButton(MouseEvent e) {
        return (e.getModifiers() & InputEvent.BUTTON3_MASK) != 0;
    }

    protected boolean middleButton(MouseEvent e) {
        return (e.getModifiers() & InputEvent.BUTTON2_MASK) != 0;
    }

    protected boolean ctrlKey(MouseEvent e) {
        return (e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0;
    }

    protected boolean shiftKey(MouseEvent e) {
        return (e.getModifiersEx() & InputEvent.SHIFT_DOWN_MASK) != 0;
    }

    protected boolean altKey(MouseEvent e) {
        return (e.getModifiersEx() & InputEvent.ALT_DOWN_MASK) != 0;
    }

    public void reset(DominoTile newMasterTile) {
        producerManager.setExtraProducer(null);

        if (lastIdleState == tileHangingAtCursorIdle) {
            hangingTile.deunifyIfNecessary();
            world.removeObject(hangingTile);
            hangingTile.removeIdAssignments();
        }

        masterTile = newMasterTile;

        dragInhibitor = false;
        modifiers = 0;
        setState(noTileHangingAtCursorIdle);
    }

    public void setProducerManager(TileProducerManager newProducerManager) {
        producerManager = newProducerManager;
    }

    protected void setState(MouseEventProcessorState newState) {
        state = newState;
        state.init();
    }

    protected void checkKeyModifiers(MouseEvent e) {
        modifiers &= ~(MOD_CTRL | MOD_SHIFT | MOD_ALT);
        if (ctrlKey(e)) modifiers |= MOD_CTRL;
        if (shiftKey(e)) modifiers |= MOD_SHIFT;
        if (altKey(e)) modifiers |= MOD_ALT;
    }

    public void mousePressed(MouseEvent e) {
        int oldMods = modifiers;

        if (leftButton(e)) {
            modifiers |= MOD_LEFT_BUTTON;
        }
        if (rightButton(e)) {
            modifiers |= MOD_RIGHT_BUTTON;
        }
        if (middleButton(e)) {
            modifiers |= MOD_MIDDLE_BUTTON;
        }

        checkKeyModifiers(e);

        if (modifiers != oldMods) {
            if (modifiers == 0) dragInhibitor = false;
            lastModChangeMousePos = e.getPoint();
            state.handleModifierChange();
        }
    }

    public void mouseReleased(MouseEvent e) {
        int oldMods = modifiers;

        if (leftButton(e)) {
            modifiers &= ~MOD_LEFT_BUTTON;
        }
        if (rightButton(e)) {
            modifiers &= ~MOD_RIGHT_BUTTON;
        }
        if (middleButton(e)) {
            modifiers &= ~MOD_MIDDLE_BUTTON;
        }

        checkKeyModifiers(e);

        if (modifiers != oldMods) {
            if (modifiers == 0) dragInhibitor = false;
            lastModChangeMousePos = e.getPoint();
            state.handleModifierChange();
        }
    }

    public void keyPressed(KeyEvent e) {
        int oldMods = modifiers;
        switch (e.getKeyCode()) {
            //case KC_CTRL_R:
            case KC_CTRL_L:
                modifiers |= MOD_CTRL;
                break;
            //case KC_SHIFT_R:
            case KC_SHIFT_L:
                modifiers |= MOD_SHIFT;
                break;
            case KC_ALT_R:
            case KC_ALT_L:
                modifiers |= MOD_ALT;
                break;
            case KC_SPACE:
                modifiers |= MOD_SPACE;
                break;
        }
        if (modifiers != oldMods) {
            if (modifiers == 0) dragInhibitor = false;
            lastModChangeMousePos = mousePos;
            state.handleModifierChange();
        }
        state.handleKeyPress(e);
    }

    public void keyReleased(KeyEvent e) {
        int oldMods = modifiers;
        switch (e.getKeyCode()) {
            //case KC_CTRL_R:
            case KC_CTRL_L:
                modifiers &= ~MOD_CTRL;
                break;
            //case KC_SHIFT_R:
            case KC_SHIFT_L:
                modifiers &= ~MOD_SHIFT;
                break;
            case KC_ALT_R:
            case KC_ALT_L:
                modifiers &= ~MOD_ALT;
                break;
            case KC_SPACE:
                modifiers &= ~MOD_SPACE;
                break;
        }
        if (modifiers != oldMods) {
            if (modifiers == 0) dragInhibitor = false;
            lastModChangeMousePos = mousePos;
            state.handleModifierChange();
        }
        state.handleKeyRelease(e);
    }

    public void keyTyped(KeyEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
        world.graphicalRepresentation().requestFocus();
        modifiers = 0;
    }

    public void mouseExited(MouseEvent e) {
        modifiers = 0;
    }

    public void mouseClicked(MouseEvent e) {
        state.handleMouseClick(e);
    }

    public void mouseDragged(MouseEvent e) {
        mousePos = e.getPoint();
        state.handleMouseMotion();
    }

    public void mouseMoved(MouseEvent e) {
        mousePos = e.getPoint();
        state.handleMouseMotion();
    }

    protected void produceHangingTile() {
        hangingTile = producerManager.produce();
        hangingTile.setOrientation(hangingTileOrientation);
        //clickLocation,new IsoShift(0,0,0) is only approximate location, the call
        //state.handleMouseMotion() later will reposition the tile exactly
        world.addObject(hangingTile, VectorInt3D.ZERO, VectorDouble3D.ZERO);
        world.putInTopLayer(hangingTile);

        setState(tileHangingAtCursorIdle);
        state.handleMouseMotion(); //to position the tile properly
    }

    public IsoWorld getWorld() {
        return world;
    }

    public MouseEventProcessorState getState() {
        return state;
    }

    public Point getDragStart() {
        return dragStart;
    }

    public void setDragStart(Point dragStart) {
        this.dragStart = dragStart;
    }

    public Point getLastModChangeMousePos() {
        return lastModChangeMousePos;
    }

    public Point getMousePos() {
        return mousePos;
    }

    public int getModifiers() {
        return modifiers;
    }

    public void setModifiers(int modifiers) {
        this.modifiers = modifiers;
    }

    public boolean isZoomingIn() {
        return zoomingIn;
    }

    public void setZoomingIn(boolean zoomingIn) {
        this.zoomingIn = zoomingIn;
    }

    public MouseEventProcessorState getLastIdleState() {
        return lastIdleState;
    }

    public void setLastIdleState(MouseEventProcessorState lastIdleState) {
        this.lastIdleState = lastIdleState;
    }

    public boolean isLastIdleStateTracksMovement() {
        return lastIdleStateTracksMovement;
    }

    public void setLastIdleStateTracksMovement(boolean lastIdleStateTracksMovement) {
        this.lastIdleStateTracksMovement = lastIdleStateTracksMovement;
    }

    public boolean isDragInhibitor() {
        return dragInhibitor;
    }

    public void setDragInhibitor(boolean dragInhibitor) {
        this.dragInhibitor = dragInhibitor;
    }

    public EnhancedDominoTile getHangingTile() {
        return hangingTile;
    }

    public void setHangingTile(EnhancedDominoTile hangingTile) {
        this.hangingTile = hangingTile;
    }

    public int getHangingTileOrientation() {
        return hangingTileOrientation;
    }

    public void setHangingTileOrientation(int hangingTileOrientation) {
        this.hangingTileOrientation = hangingTileOrientation;
    }

    public TileProducerManager getProducerManager() {
        return producerManager;
    }

    public DominoTile getMasterTile() {
        return masterTile;
    }

    public MouseEventProcessorState getNoTileHangingAtCursorIdle() {
        return noTileHangingAtCursorIdle;
    }

    public MouseEventProcessorState getZoom() {
        return zoom;
    }

    public MouseEventProcessorState getMoveBoard() {
        return moveBoard;
    }

    public MouseEventProcessorState getTileHangingAtCursorIdle() {
        return tileHangingAtCursorIdle;
    }

    public MouseEventProcessorState getRotateTile() {
        return rotateTile;
    }
}
