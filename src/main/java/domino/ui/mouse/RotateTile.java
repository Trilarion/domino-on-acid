/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.mouse;

import domino.ui.UIConstants;

public class RotateTile extends MouseEventProcessorState {

    private final MouseEventProcessor mouseEventProcessor;
    private boolean haveStartAngle;
    private double startAngle;
    private int startOrientation;

    public RotateTile(MouseEventProcessor mouseEventProcessor) {
        this.mouseEventProcessor = mouseEventProcessor;
    }

    public void init() {
        haveStartAngle = false;
        startOrientation = mouseEventProcessor.getHangingTileOrientation();
    }

    public void handleMouseMotion() {
        if (mouseEventProcessor.getModifiers() == 0 || mouseEventProcessor.isDragInhibitor()) return;

        int curx = mouseEventProcessor.getMousePos().x;
        int cury = mouseEventProcessor.getMousePos().y;
        long difx = curx - mouseEventProcessor.getDragStart().x;
        long dify = cury - mouseEventProcessor.getDragStart().y;

        double dif = Math.sqrt(difx * difx + dify * dify);
        if (dif >= UIConstants.ROTATE_DRAG_THRESHOLD) {
            double angle = Math.atan2(dify, difx);
            if (!haveStartAngle) {
                startAngle = angle;
                haveStartAngle = true;
            } else //if (haveStartAngle)
            {
                angle -= startAngle;
                if (angle < 0) angle += (2 * Math.PI);
                int steps = (int) Math.round(UIConstants.NUM_ROTATE_STEPS * angle / (2 * Math.PI));
                mouseEventProcessor.setHangingTileOrientation((startOrientation + steps) & 3);
                mouseEventProcessor.getHangingTile().setOrientation(mouseEventProcessor.getHangingTileOrientation());
                mouseEventProcessor.getWorld().putInTopLayer(mouseEventProcessor.getHangingTile());
            }
        }
    }

    public void handleModifierChange() {
        mouseEventProcessor.setState(mouseEventProcessor.getTileHangingAtCursorIdle());
        mouseEventProcessor.getState().handleMouseMotion();
    }

}
