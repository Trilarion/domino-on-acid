/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui;

import domino.Version;
import domino.io.LevelManager;
import domino.logic.ProofFormatter;
import domino.logic.syntaxnet.ConflictingBindException;
import domino.logic.tree.FormulaDominoTree;
import domino.ui.block.DominoBlock;
import domino.ui.i18n.Messages;
import domino.ui.iso.IsoWorld;
import domino.ui.iso.JIsoWorld;
import domino.ui.mouse.MouseEventProcessor;
import domino.ui.producer.ProducerMenuItem;
import domino.ui.producer.TileProducerManager;
import domino.model.tile.DominoTile;
import domino.utils.Browser;
import domino.utils.Utils;
import domino.utils.VectorDouble3D;
import domino.utils.VectorInt3D;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;

public class MainFrame extends JFrame {

    private final JCheckBoxMenuItem autoSimplifyMenuItem = new JCheckBoxMenuItem("Auto-Simplify", true);
    private final IsoWorld isoWorld;
    private final MouseEventProcessor mouseEventProcessor;
    private final JFileChooser saveProofChooser;
    private final JFileChooser loadSaveTileChooser;
    private final LevelManager levelManager;
    private final JMenu tileMenu;
    private DominoTile startTile;
    private String currentLevel;
    private TileProducerManager tileProducerManager;
    private JFrame proofFrame;

    public MainFrame() { // TODO icon for the application

        setLocationByPlatform(true);

        isoWorld = new IsoWorld(UIConstants.WORLD_XDIM, UIConstants.WORLD_XDIM, UIConstants.WORLD_YDIM, UIConstants.WORLD_YDIM, UIConstants.WORLD_SQUARESIZE, -0.16, -0.16);
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        isoWorld.graphicalRepresentation().setPreferredSize(new Dimension(Math.min(UIConstants.WINDOW_WIDTH, screenSize.width), Math.min(UIConstants.WINDOW_HEIGHT, screenSize.height)));
        setContentPane(isoWorld.graphicalRepresentation());

        tileProducerManager = new TileProducerManager(UIConstants.BORING_ELONGATOR_INDEX, "Boring Elongator: X0,X0");

        // level manager
        levelManager = new LevelManager(UIConstants.LEVELS_URL);
        currentLevel = levelManager.getLevel(UIConstants.INITIAL_LEVEL);
        if (currentLevel == null) currentLevel = "((A->_|_)->_|_)->A"; // fallback
        createStartTileForLevel(currentLevel);

        // put start tile in world
        isoWorld.addObject(startTile, VectorInt3D.ZERO, VectorDouble3D.ZERO);
        fixWorldSizeForStartTile();

        mouseEventProcessor = new MouseEventProcessor(isoWorld, tileProducerManager, startTile, this);

        // world should always have focus
        addWindowListener(new WindowAdapter() {
            public void windowActivated(WindowEvent e) {
                isoWorld.graphicalRepresentation().requestFocusInWindow();
            }
        });

        // TODO save proof chooser can be constructed new every time again (do not have them as global variable)
        saveProofChooser = new JFileChooser();
        saveProofChooser.setDialogTitle("Save Proof"); // TODO internationalize
        FileFilter txtFileFilter = new TxtFileFilter(new String[]{".txt", ".proof"});
        saveProofChooser.addChoosableFileFilter(txtFileFilter);
        saveProofChooser.setFileFilter(txtFileFilter);

        // load file chooser
        loadSaveTileChooser = new JFileChooser();
        txtFileFilter = new TxtFileFilter(new String[]{".tiles"});
        loadSaveTileChooser.addChoosableFileFilter(txtFileFilter);
        loadSaveTileChooser.setFileFilter(txtFileFilter);

        // Create the menu bar and menus
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        // File menu
        JMenu fileMenu = new JMenu(Messages.MENU_FILE.toString());
        fileMenu.setMnemonic(KeyEvent.VK_F);
        menuBar.add(fileMenu);

        JMenuItem menuItem = new JMenuItem("Show Proof", KeyEvent.VK_S);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
        menuItem.addActionListener(e -> showProof());
        fileMenu.add(menuItem);

        menuItem = new JMenuItem(Messages.MENU_FILE_QUIT.toString(), KeyEvent.VK_Q);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK));
        menuItem.addActionListener(e -> System.exit(0)); // TODO better just try to close window
        fileMenu.add(menuItem);

        // Level menu
        JMenu levelMenu = new JMenu(Messages.MENU_LEVEL.toString());
        levelMenu.setMnemonic(KeyEvent.VK_L);
        menuBar.add(levelMenu);

        menuItem = new JMenuItem("About", KeyEvent.VK_A);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK));
        menuItem.addActionListener(e -> aboutLevel());
        levelMenu.add(menuItem);

        menuItem = new JMenuItem(Messages.MENU_LEVEL_RESTART.toString(), KeyEvent.VK_R);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK));
        menuItem.addActionListener(e -> restartLevel());
        levelMenu.add(menuItem);

        menuItem = new JMenuItem("Advance", KeyEvent.VK_D);
        levelMenu.add(menuItem);
        menuItem.addActionListener(e -> nextLevel());

        menuItem = new JMenuItem("Select New", KeyEvent.VK_N);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
        levelMenu.add(menuItem);
        menuItem.addActionListener(e -> newLevel());

        levelMenu.addSeparator();
        autoSimplifyMenuItem.setMnemonic(KeyEvent.VK_U);
        levelMenu.add(autoSimplifyMenuItem);

        // tile menu
        tileMenu = new JMenu("Tile");
        tileMenu.setMnemonic(KeyEvent.VK_T);
        menuBar.add(tileMenu);

        // add custom tile to menu
        menuItem = new JMenuItem("Add Custom Tile", KeyEvent.VK_A);
        menuItem.addActionListener(e -> {
            String name = JOptionPane.showInputDialog(getContentPane(),
                    "Enter a name for the new tile",
                    "Custom Tile",
                    JOptionPane.QUESTION_MESSAGE);
            if (name == null) return;
            tileProducerManager.extraProducerToRegularProducerIfPossible(name);
            updateTileMenu();
        });
        tileMenu.add(menuItem);

        tileMenu.addSeparator();

        menuItem = new JMenuItem("Load Tiles", KeyEvent.VK_L);
        tileMenu.add(menuItem);
        menuItem.addActionListener(e -> {
            loadSaveTileChooser.setDialogTitle("Load Tiles");
            int retval = loadSaveTileChooser.showOpenDialog(getContentPane());
            if (retval == JFileChooser.APPROVE_OPTION) {
                try {
                    URL fileURL = loadSaveTileChooser.getSelectedFile().toURI().toURL();
                    loadTiles(fileURL);
                } catch (Exception x) {
                    JOptionPane.showMessageDialog(getContentPane(), "File error: " + x.getMessage(), "File error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        menuItem = new JMenuItem("Save Tiles", KeyEvent.VK_S);
        tileMenu.add(menuItem);
        menuItem.addActionListener(e -> saveTiles());

        tileMenu.addSeparator();

        menuItem = new JMenuItem("Remove Unselected");
        tileMenu.add(menuItem);
        menuItem.addActionListener
                (e -> {
                            tileProducerManager.removeDisabledProducers();
                            updateTileMenu();
                        }
                );

        menuItem = new JMenuItem("Restore Default");
        tileMenu.add(menuItem);
        menuItem.addActionListener(e -> loadTiles(UIConstants.DEFAULT_TILES_URL));

        tileMenu.addSeparator();

        // help menu
        JMenu helpMenu = new JMenu(Messages.MENU_HELP.toString());
        helpMenu.setMnemonic(KeyEvent.VK_H);
        menuBar.add(helpMenu);

        menuItem = new JMenuItem("Manual", KeyEvent.VK_M);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, InputEvent.CTRL_DOWN_MASK));
        helpMenu.add(menuItem);
        menuItem.addActionListener(e -> {
            Browser browser = new Browser(Messages.MANUAL_WINDOW_TITLE.toString());
            browser.browse(UIConstants.MANUAL_URL);
            browser.show();
        });

        menuItem = new JMenuItem(Messages.MENU_HELP_ABOUT.toString(), KeyEvent.VK_A);
        helpMenu.add(menuItem);
        menuItem.addActionListener(e -> aboutGame());

        loadTiles(UIConstants.DEFAULT_TILES_URL);

        pack();

        focusOnStartTile();
    }

    /**
     * <p>Removes all producer entries from {@code tileMenu} and then adds
     * entries to the end of the menu for all producers except for the extra
     * producer and the fixed producer.</p>
     */
    public void updateTileMenu() {
        for (Component item : tileMenu.getMenuComponents()) {
            if (item instanceof ProducerMenuItem)
                tileMenu.remove(item);
        }

        for (ProducerMenuItem normalProducer : tileProducerManager.getNormalProducers()) {
            if (!normalProducer.isFixed()) tileMenu.add(normalProducer);
        }
    }

    public void focusOnStartTile() { // TODO that doesn't seem to work well
        JIsoWorld jIsoWorld = isoWorld.graphicalRepresentation();
        Dimension dimension = jIsoWorld.getViewport().getExtentSize();
        Point viewportCenter = new Point(dimension.width >> 1, dimension.height >> 1);
        isoWorld.setSquareSize(UIConstants.WORLD_SQUARESIZE, 0.5, 0.5, viewportCenter.x, viewportCenter.y);
        isoWorld.graphicalRepresentation().requestFocus();
    }

    private void newLevel() {
        String level;
        do {
            level = JOptionPane.showInputDialog(getContentPane(),
                    "Enter a level number (1-" + levelManager.numberOfLevels() + ")\nor an infix formula",
                    "Start New Level",
                    JOptionPane.QUESTION_MESSAGE);
            if (level == null) return;

            try {
                int lnum = Integer.parseInt(level.trim()) - 1; //-1 because LevelManager starts count at 0
                String newLevel = levelManager.getLevel(lnum);
                if (newLevel != null) level = newLevel;
            } catch (Exception ignored) {
            }
        } while (!createStartTileForLevel(level));

        currentLevel = level;
        restartLevelWithoutConfirmation();
    }

    private void nextLevel() {
        int levelNumber = levelManager.getLevelNumberFor(currentLevel);
        if (levelNumber < 0) levelNumber = 0;
        ++levelNumber;
        if (levelNumber >= levelManager.numberOfLevels()) levelNumber = 0;

        String level = levelManager.getLevel(levelNumber);
        if (level == null) level = currentLevel;

        currentLevel = level;
        restartLevelWithoutConfirmation();
    }

    public void victory() {
        Object[] options = {"Advance", "Restart", "Show Proof", "Continue"};
        int retval = JOptionPane.showOptionDialog(getContentPane(),
                "Congratulations!",
                "You did it!",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.INFORMATION_MESSAGE,
                null,
                options,
                options[0]);
        switch (retval) {
            case 0:
                nextLevel();
                break;
            case 1:
                restartLevelWithoutConfirmation();
                break;
            case 2:
                showProof();
                break;
        }
    }

    private void restartLevel() {
        int retval = JOptionPane.showConfirmDialog(getContentPane(),
                "Restart level?",
                "Restart level?",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (retval != JOptionPane.YES_OPTION) return;

        restartLevelWithoutConfirmation();
    }

    protected void fixWorldSizeForStartTile() { // TODO is this working?
        Iterator<DominoBlock> iterator = startTile.blocksIterator();
        VectorInt3D minloc = new VectorInt3D(-UIConstants.WORLD_XDIM / 2, -UIConstants.WORLD_YDIM / 2, 0); // TODO should be VectorInt2D
        VectorInt3D maxloc = new VectorInt3D(UIConstants.WORLD_XDIM / 2, UIConstants.WORLD_YDIM / 2, 0);
        while (iterator.hasNext()) {
            VectorInt3D loc = iterator.next().getAbsoluteLocation();
            if (loc.x < minloc.x) minloc = new VectorInt3D(loc.x, minloc.y, minloc.z);
            if (loc.x > maxloc.x) maxloc = new VectorInt3D(loc.x, maxloc.y, maxloc.z);
            if (loc.y < minloc.y) minloc = new VectorInt3D(minloc.x, loc.y, minloc.z);
            if (loc.y > maxloc.y) maxloc = new VectorInt3D(maxloc.x, loc.y, maxloc.z);
        }
        minloc = new VectorInt3D(minloc.x - UIConstants.WORLD_XDIM / 2, minloc.y - UIConstants.WORLD_YDIM / 2, minloc.z);
        maxloc = new VectorInt3D(maxloc.x + UIConstants.WORLD_XDIM / 2, maxloc.y + UIConstants.WORLD_YDIM / 2, maxloc.z);
        int worldXdim = -minloc.x;
        if (worldXdim < maxloc.x) worldXdim = maxloc.x;
        int worldYdim = -minloc.y;
        if (worldYdim < maxloc.y) worldYdim = maxloc.y;
        isoWorld.addLines(0, worldXdim - isoWorld.minusXDim());
        isoWorld.addLines(1, worldXdim - isoWorld.plusXDim());
        isoWorld.addLines(2, worldYdim - isoWorld.minusYDim());
        isoWorld.addLines(3, worldYdim - isoWorld.plusYDim());
    }

    protected void restartLevelWithoutConfirmation() {
        createStartTileForLevel(currentLevel);
        isoWorld.removeAllIsoObjects();
        DominoTile.removeIdAssignmentsForAllTiles();
        isoWorld.addObject(startTile, VectorInt3D.ZERO, VectorDouble3D.ZERO);
        mouseEventProcessor.reset(startTile);
        fixWorldSizeForStartTile();
        focusOnStartTile();
    }

    protected void aboutLevel() {
        int level = levelManager.getLevelNumberFor(currentLevel) + 1; //+1 because we want to number from 1
        String level_description = (level == 0 ? "Custom Level" : ("Level " + level + " of " + levelManager.numberOfLevels()));
        level_description = level_description + "\n" + currentLevel;
        JOptionPane.showMessageDialog(getContentPane(), level_description, "About this level", JOptionPane.INFORMATION_MESSAGE);
    }

    protected void aboutGame() { // TODO adapt this, possibly read from a resource
        JOptionPane.showMessageDialog(getContentPane(),
                "Domino Version " + Version.VERSION + "\n" +
                        "Copyright (c) 2002,2003 Matthias S. Benkmann\n" +
                        "\n" +
                        "Send bug reports and comments to <matthias@winterdrache.de>\n" +
                        "The most recent version can be found at\nhttp://www.winterdrache.de/freeware/domino/index.html",
                "Game Information",
                JOptionPane.INFORMATION_MESSAGE);
    }

    private void loadTiles(URL tileFile) { // TODO possibly outsource to io
        InputStream instream = null;

        try {
            URLConnection ucon = tileFile.openConnection();
            ucon.setDoInput(true);
            ucon.setDoOutput(false);
            //ucon.connect(); should be done implicitly by getInputStream()
            instream = ucon.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(instream));

            TileProducerManager newTileProducerManager = new TileProducerManager(tileProducerManager);
            newTileProducerManager.removeAllProducers(); //redundant because TileProducerManager(TileProducerManager) is not a true copy constructor, however it might become one in the future

            String line;
            while ((line = in.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty()) continue;
                newTileProducerManager.addProducerForStringRepresentation(line);
            }
            instream.close();

            tileProducerManager = newTileProducerManager;
            mouseEventProcessor.setProducerManager(tileProducerManager);
            updateTileMenu();
            tileProducerManager.activateFirstEnabledProducer();
        } catch (Exception x) {
            JOptionPane.showMessageDialog(getContentPane(),
                    "File error: " + x.getMessage(),
                    "File error",
                    JOptionPane.ERROR_MESSAGE);
            try {
                if (instream != null) instream.close();
            } catch (Exception ignored) {
            }
        }
    }

    private void saveTiles() { // TODO possibly outsource to io
        loadSaveTileChooser.setDialogTitle("Save Tiles");
        int retval = loadSaveTileChooser.showSaveDialog(getContentPane());
        if (retval == JFileChooser.APPROVE_OPTION) {
            File file = loadSaveTileChooser.getSelectedFile();
            if (file.exists()) {
                retval = JOptionPane.showConfirmDialog(getContentPane(),
                        file + " exists!\nOverwrite?",
                        "Overwrite?",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);
                if (retval != JOptionPane.YES_OPTION) return;
            }

            FileWriter out = null;
            try {
                out = new FileWriter(file);
                for (int i = 0; i < tileProducerManager.numberOfProducers() - 1; ++i)
                    if (i != UIConstants.BORING_ELONGATOR_INDEX)
                        out.write(tileProducerManager.getStringRepresentationForProducer(i) + "\n");

                if (tileProducerManager.numberOfProducers() - 1 > UIConstants.BORING_ELONGATOR_INDEX)
                    out.write(tileProducerManager.getStringRepresentationForProducer(tileProducerManager.numberOfProducers() - 1) + "\n");
                out.close();
            } catch (Exception x) {
                JOptionPane.showMessageDialog(getContentPane(),
                        "File error: " + x.getMessage(),
                        "File error",
                        JOptionPane.ERROR_MESSAGE);
                try {
                    out.close();
                } catch (Exception ignored) {
                }
            }
        }
    }

    private void showProof() { // TODO show that in a nicer view and maybe without the nowrapeditorkit
        if (proofFrame != null && proofFrame.isShowing()) {
            proofFrame.setVisible(false);
            proofFrame.dispose();
        }
        proofFrame = new JFrame("Proof");
        proofFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        final Container contentPane = proofFrame.getContentPane();

        String[] diag = ProofFormatter.naturalDeductionDiagram(startTile.rootTree());
        for (int i = 1; i < diag.length; ++i) {
            diag[0] = diag[0] + "\n" + diag[i];
            diag[i] = null;
        }
        final JEditorPane proofEditorPane = new JEditorPane("text/plain", "");
        proofEditorPane.setEditorKit(new NoWrapEditorKit());
        proofEditorPane.setText(diag[0]);
        proofEditorPane.setFont(new Font("Monospaced", Font.PLAIN, proofEditorPane.getFont().getSize() + 2));
        JScrollPane proofScrollPane = new JScrollPane(proofEditorPane);


        // Create the menu bar and menus

        JMenuBar menuBar = new JMenuBar();
        proofFrame.getRootPane().setJMenuBar(menuBar);

        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        menuBar.add(fileMenu);

        JMenuItem menuItem = new JMenuItem("Save As...", KeyEvent.VK_S);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
        fileMenu.add(menuItem);
        menuItem.addActionListener(e -> saveJTextComponent(contentPane, proofEditorPane));

        menuItem = new JMenuItem("Close Window", KeyEvent.VK_C);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));
        fileMenu.add(menuItem);
        menuItem.addActionListener
                (e -> {
                            proofFrame.setVisible(false);
                            proofFrame.dispose();
                        }
                );

        // edit menu
        JMenu editMenu = new JMenu("Edit");
        editMenu.setMnemonic(KeyEvent.VK_E);
        menuBar.add(editMenu);

        menuItem = new JMenuItem("Cut", KeyEvent.VK_T);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK));
        editMenu.add(menuItem);
        menuItem.addActionListener(e -> proofEditorPane.cut());

        menuItem = new JMenuItem("Copy", KeyEvent.VK_C);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK));
        editMenu.add(menuItem);
        menuItem.addActionListener(e -> proofEditorPane.copy());

        menuItem = new JMenuItem("Paste", KeyEvent.VK_P);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_DOWN_MASK));
        editMenu.add(menuItem);
        menuItem.addActionListener(e -> proofEditorPane.paste());

        menuItem = new JMenuItem("Select All", KeyEvent.VK_A);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK));
        editMenu.add(menuItem);
        menuItem.addActionListener
                (e -> {
                            proofEditorPane.selectAll();
                            proofEditorPane.getCaret().setSelectionVisible(true);
                        }
                );

        contentPane.add(proofScrollPane);
        proofFrame.pack();
        proofFrame.setVisible(true);
    }

    private void saveJTextComponent(Component rootComponent, JTextComponent text) {
        int retval = saveProofChooser.showSaveDialog(rootComponent);
        if (retval == JFileChooser.APPROVE_OPTION) {
            File file = saveProofChooser.getSelectedFile();
            if (file.exists()) {
                retval = JOptionPane.showConfirmDialog(rootComponent,
                        file + " exists!\nOverwrite?",
                        "Overwrite?",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);
                if (retval != JOptionPane.YES_OPTION) return;
            }

            FileWriter out = null;
            try {
                out = new FileWriter(file);
                text.write(out);
                out.close();
            } catch (Exception x) {
                JOptionPane.showMessageDialog(rootComponent,
                        "File error: " + x.getMessage(),
                        "File error",
                        JOptionPane.ERROR_MESSAGE);
                try {
                    out.close();
                } catch (Exception ignored) {
                }
            }
        }
    }

    private boolean createStartTileForLevel(String level) { // TODO make this static, return the start tile and move to TileProduceManager
        if (!TileProducerManager.isFormulaSafe(level, false)) return false;

        //because the check above has made sure that operands are valid,
        //we can now safely remove whitespace without changing the semantics of
        //level. Whitespace removal is necessary because (so far) the
        //newFromInfix*() functions don't chew whitespace.
        level = Utils.upperCaseNoSpace(level);

        FormulaDominoTree tautologyTree;
        try {
            tautologyTree = FormulaDominoTree.newFromInfix(level, new String[]{level}, null);
        } catch (IllegalArgumentException x) {
            return false;
        }

        //attach as many ->I blocks as possible so that the player doesn't have to do it
        if (autoSimplifyMenuItem.isSelected()) {
            String[] leaves = {"X1"};
            String[] bonus = {"X0"};
            FormulaDominoTree arrowIntroTree = FormulaDominoTree.newFromInfix("X0->X1", leaves, bonus);
            try {
                while (true) {
                    arrowIntroTree.cloneDT().attachTo(tautologyTree, 0);
                }
            } catch (ConflictingBindException ignored) {
            }
        }

        startTile = new DominoTile(tautologyTree);
        startTile.deactivateRootBlock();

        return true;
    }
}
