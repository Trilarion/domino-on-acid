/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.iso;

import domino.ui.block.DominoBlock;
import domino.utils.VectorDouble3D;
import domino.utils.VectorInt3D;

import javax.swing.*;
import java.util.*;

/**
 * <p>Manages a world with an isometric view.</p>
 */
public class IsoWorld {
    /**
     * <p>manages the actual graphics.</p>
     */
    private final JIsoWorld jIsoWorld;

    /**
     * <p>Maps a {@link VectorInt3D}s to {@link Collection}s of {@link IsoBlock}s
     * found at that location.
     * If a block has a non-zero {@link IsoBlock#getAbsoluteShift() shift},
     * it will be contained in multiple Collections.</p>
     * <p>This map does not contain empty {@link Collection}s. If a {@link Collection}
     * becomes empty, its mapping is removed.</p>
     */
    private final Map<VectorInt3D, Collection<IsoBlock>> blocks = new HashMap<>();

    /**
     * <p>Creates a new IsoWorld.</p>
     *
     * @param minusxdim  {@code -minusxdim} is the index of the left-most column of
     *                   the board (a negative number, because {@code minusxdim>=0}).
     * @param plusxdim   the index of the right-most column of
     *                   the board.
     * @param minusydim  {@code -minusydim} is the index of the down-most row of
     *                   the board (a negative number, because {@code minusydim>=0}).
     * @param plusydim   the index of the up-most row of the board.
     * @param squareSide initial side length of the bottom and top side square of a block.
     * @param xoffsetRel {@code xoffsetRel_*squareSide} is the number of pixels,
     *                   the top side of the block is shifted to the right in comparison to the
     *                   bottom side.
     * @param yoffsetRel {@code yoffsetRel_*squareSide} is the number of pixels,
     *                   the top side of the block is shifted down in comparison to the
     *                   bottom side.
     */
    public IsoWorld(int minusxdim, int plusxdim, int minusydim, int plusydim, int squareSide, double xoffsetRel, double yoffsetRel) {
        jIsoWorld = new JIsoWorld(minusxdim, plusxdim, minusydim, plusydim, squareSide, xoffsetRel, yoffsetRel);
    }

    /**
     * <p>Returns 0 if {@code abs(c1-c2)<=SHIFT_PRECISION},
     * 1 if {@code c1-c2>SHIFT_PRECISION},
     * -1 if {@code c2-c1>SHIFT_PRECISION}.
     * </p>
     *
     * <p>Comparisons of {@code IsoShift} coordinates should consider differences
     * less than this value to be 0.</p>
     */
    public static int shiftCompare(double c1, double c2) { // TODO refactor (inline)
        final double SHIFT_PRECISION = 0.001;
        c1 -= c2;
        if (c1 + SHIFT_PRECISION < 0) return -1;
        if (c1 - SHIFT_PRECISION > 0) return +1;
        return 0;
    }

    /**
     * <p>Adds or removes rows or columns from the board.</p>
     * <p>This method only increases or decreases the visible part of the board.
     * Removing rows/columns will not remove any blocks located there and will not
     * prevent new blocks from being added there.</p>
     *
     * @param leftrighttopbottom Where to add/remove:
     *                           {@code 0=left, 1=right, 2=top, 3=bottom}
     * @param numLines           how much space to add. Negative values remove space.
     */
    public void addLines(int leftrighttopbottom, int numLines) {
        jIsoWorld.addLines(leftrighttopbottom, numLines);
    }

    /**
     * <p>Returns the index of the right-most column of the board.</p>
     */
    public int plusXDim() {
        return jIsoWorld.plusXDim();
    }

    /**
     * <p>Returns abs() of the index of the left-most column of the board.</p>
     */
    public int minusXDim() {
        return jIsoWorld.minusXDim();
    }

    /**
     * <p>Returns the index of the up-most row of the board.</p>
     */
    public int plusYDim() {
        return jIsoWorld.plusYDim();
    }

    /**
     * <p>Returns abs() of the index of the down-most row of the board.</p>
     */
    public int minusYDim() {
        return jIsoWorld.minusYDim();
    }

    /**
     * <p>Returns {@code true} if {@code object} would cause a collision if
     * its reference block were put at {@code location} with the given
     * {@code shift}. {@code object} may or may not be part of
     * {@code this IsoWorld}. Collisions of {@code object} with itself do
     * not count.</p>
     */
    public boolean collision(IsoObject object, VectorInt3D location, VectorDouble3D shift) {
        Iterator<DominoBlock> iterator = object.blocksIterator();
        while (iterator.hasNext()) {
            IsoBlock block = iterator.next();
            if (!block.isMassive()) continue;
            VectorInt3D loc = block.getRelativeLocation();
            loc = VectorInt3D.add(loc, location);

            int xm = 0;
            if (shift.x > 0) xm = 1;
            int ym = 0;
            if (shift.y > 0) ym = 1;
            int zm = 0;
            if (shift.z > 0) zm = 1;

            for (int x = loc.x; x <= loc.x + xm; ++x) {
                for (int y = loc.y; y <= loc.y + ym; ++y) {
                    for (int z = loc.z; z <= loc.z + zm; ++z) {
                        Collection<IsoBlock> c = blocksAtLocation(new VectorInt3D(x, y, z));
                        if (c == null) continue;
                        for (IsoBlock otherBlock : c) {
                            if (otherBlock.parentObject() == object) continue;
                            if (otherBlock.isMassive()) return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * <p>Adds {@code block} to the {@link Collection} of {@link IsoBlock}s
     * at {@code location} (see {@link #blocksAtLocation(VectorInt3D)}), creating
     * this {@link Collection} if necessary.</p>
     *
     * @see #removeFromBlocks(VectorInt3D, IsoBlock)
     */
    protected void addToBlocks(VectorInt3D location, IsoBlock block) {
        Collection<IsoBlock> c = blocksAtLocation(location);
        if (c == null) {
            c = new LinkedList<>();
            blocks.put(location, c);
        }
        c.add(block);
    }

    /**
     * <p>Removes {@code block} from the {@link Collection} of {@link IsoBlock}s
     * at {@code location} (see {@link #blocksAtLocation(VectorInt3D)}), deleting
     * this {@link Collection} if it is empty afterwards.</p>
     *
     * @see #addToBlocks(VectorInt3D, IsoBlock)
     */
    protected void removeFromBlocks(VectorInt3D location, IsoBlock block) {
        Collection<IsoBlock> c = blocksAtLocation(location);
        if (c != null) {
            c.remove(block);
            if (c.isEmpty()) blocks.remove(location);
        }
    }

    /**
     * <p>Returns -1 if when displaying an {@link IsoBlock} the top side has a
     * lower y coordinate than the bottom side, +1 if the top side has the same or
     * a higher y coordinate than the bottom side.</p>
     *
     * @see #xOffsetSign2()
     */
    public int yOffsetSign2() {
        int i = graphicalRepresentation().yOffset();
        if (i < 0) return -1;
        return +1;
    }

    /**
     * <p>Returns -1 if when displaying an {@link IsoBlock} the top side has a
     * lower x coordinate than the bottom side, +1 if the top side has the same or
     * a higher x coordinate than the bottom side.</p>
     *
     * @see #yOffsetSign2()
     */
    public int xOffsetSign2() {
        int i = graphicalRepresentation().xOffset();
        if (i < 0) return -1;
        return +1;
    }

    /**
     * <p>Adjust the paint order so that {@code block} is painted correctly.</p>
     * <p>The layer on which a block is painted only depends on its {@link VectorInt3D}.
     * However, more than 1 {@link IsoBlock} can occupy the same location, so
     * additional paint order tuning has to be performed based on the blocks'
     * {@link VectorDouble3D}s. That is the purpose of this function.</p>
     * <p>{@code block} must have been added to the world before using this
     * function.</p>
     */
    protected void insertInPaintOrder(IsoBlock block) {
        VectorInt3D loc = block.getAbsoluteLocation();
        VectorDouble3D shift1 = block.getAbsoluteShift();
        Collection<IsoBlock> blocks = blocksAtLocation(loc);
        int paintOrderPosition = Integer.MAX_VALUE;
        int numOtherBlocks = 0;
        for (IsoBlock otherBlock : blocks) {
            if (block == otherBlock) continue;

            //only consider blocks whose main location is the same as that of block
            if (!otherBlock.getAbsoluteLocation().equals(loc)) continue;

            ++numOtherBlocks;

            VectorDouble3D shift2 = otherBlock.getAbsoluteShift();

            int cmp2;
            int cmp1 = shiftCompare(shift1.y, shift2.y);
            if (cmp1 == 0) {
                cmp1 = shiftCompare(shift1.x, shift2.x);
                if (cmp1 == 0) {
                    cmp1 = shiftCompare(shift1.z, shift2.z);
                    cmp2 = -1;
                } else cmp2 = xOffsetSign2();
            } else cmp2 = yOffsetSign2();

            if (cmp1 != cmp2) {
                int pop = otherBlock.getPaintOrderPosition();
                if (pop < paintOrderPosition) paintOrderPosition = pop;
            }
        }

        if (paintOrderPosition == Integer.MAX_VALUE)
            paintOrderPosition = numOtherBlocks;
        else {
            for (IsoBlock otherBlock : blocks) {
                if (block == otherBlock) continue;

                //only consider blocks whose main location is the same as that of block
                if (!otherBlock.getAbsoluteLocation().equals(loc)) continue;

                int pop = otherBlock.getPaintOrderPosition();
                if (pop >= paintOrderPosition) {
                    otherBlock.setPaintOrderPosition(pop + 1);
                    jIsoWorld.setPaintOrderPosition(otherBlock.graphicalRepresentation().toJComponent(), pop + 1);
                }
            }
        }

        block.setPaintOrderPosition(paintOrderPosition);
        jIsoWorld.setPaintOrderPosition(block.graphicalRepresentation().toJComponent(), paintOrderPosition);
    }

    /**
     * <p>Removes block's paint order assignment, adjust other blocks' paint order
     * positions to close the gap.</p>
     */
    protected void removeFromPaintOrder(IsoBlock block) {
        VectorInt3D loc = block.getAbsoluteLocation();
        Collection<IsoBlock> blocks = blocksAtLocation(loc);
        if (blocks != null) {
            int paintOrderPosition = block.getPaintOrderPosition();
            block.setPaintOrderPosition(-1);
            jIsoWorld.setPaintOrderPosition(block.graphicalRepresentation().toJComponent(), -1);
            Iterator<IsoBlock> iterator = blocks.iterator();
            while (iterator.hasNext()) {
                IsoBlock otherBlock = iterator.next();
                if (block == otherBlock) continue;

                //only consider blocks whose main location is the same as that of block
                if (!otherBlock.getAbsoluteLocation().equals(loc)) continue;

                int pop = otherBlock.getPaintOrderPosition();
                if (pop > paintOrderPosition) otherBlock.setPaintOrderPosition(pop - 1);
            }

            iterator = blocks.iterator();
            while (iterator.hasNext()) {
                IsoBlock otherBlock = iterator.next();
                if (block == otherBlock) continue;

                //only consider blocks whose main location is the same as that of block
                if (!otherBlock.getAbsoluteLocation().equals(loc)) continue;

                int pop = otherBlock.getPaintOrderPosition();
                if (pop >= paintOrderPosition) // >= because it may have been decremented above
                    jIsoWorld.setPaintOrderPosition(otherBlock.graphicalRepresentation().toJComponent(), pop);
            }
        }
    }

    /**
     * <p>Puts {@code object}'s {@link IsoBlock}s into the top layers,
     * regardless of their {@link VectorInt3D}s. The relative layer order of the
     * blocks is preserved.</p>
     *
     * @see #putInDefaultLayer(IsoObject)
     */
    public void putInTopLayer(IsoObject object) {
        class CompObj implements Comparator<CompObj> {
            public final IsoBlock block;
            public final int layer;

            public CompObj(IsoBlock b, int l) {
                block = b;
                layer = l;
            }

            public int compare(CompObj o1, CompObj o2) {
                return Integer.compare(o1.layer, o2.layer);
            }

        }

        CompObj[] c = new CompObj[object.numberOfBlocks()];
        int i = 0;
        if (c.length > 0) {
            Iterator<DominoBlock> blocks = object.blocksIterator();
            while (blocks.hasNext()) {
                IsoBlock block = blocks.next();
                c[i] = new CompObj(block, jIsoWorld.getLayer(block.graphicalRepresentation().toJComponent()));
                i++;
            }

            Arrays.sort(c, c[0]);
        }

        int l = Integer.MAX_VALUE;
        while (i > 0) {
            i--;
            jIsoWorld.setLayer(c[i].block.graphicalRepresentation().toJComponent(), l);
            l--;
        }
    }

    /**
     * <p>Puts {@code object} into the layer that is appropriate for its
     * {@link VectorInt3D}.</p>
     *
     * @see #putInTopLayer(IsoObject)
     */
    public void putInDefaultLayer(IsoObject object) {
        Iterator<DominoBlock> blocks = object.blocksIterator();
        while (blocks.hasNext()) {
            IsoBlock block = blocks.next();
            jIsoWorld.setLayer(block.graphicalRepresentation().toJComponent(), block.getAbsoluteLocation());
        }
    }

    /**
     * <p>Calls {@link JIsoWorld#setBlock(JIsoBlock, VectorInt3D, VectorDouble3D)} for every
     * block in this IsoWorld.</p>
     */
    protected void setBlocks() {
        Set<Map.Entry<VectorInt3D, Collection<IsoBlock>>> coordsAndBlocks = blocks.entrySet();
        for (Map.Entry<VectorInt3D, Collection<IsoBlock>> coordsAndBlock : coordsAndBlocks) {
            Collection<IsoBlock> blocksAtPosition = coordsAndBlock.getValue();
            for (IsoBlock block : blocksAtPosition) {
                jIsoWorld.setBlock(block.graphicalRepresentation(), block.getAbsoluteLocation(), block.getAbsoluteShift());
            }
        }
    }

    /**
     * <p>Changes the size of the squares that form the top and bottom sides of the
     * blocks.</p>
     *
     * @param sideLengthInPixels new side length of a square.
     * @param fixPointX          x coordinate of fixpoint (normalized board coordinate). This
     *                           board location will still be visible at the same position in the viewport
     *                           after the resize.
     * @param fixPointY          y coordinate of fixpoint (normalized board coordinate).
     * @see #setSquareSize(int)
     * @see #setSquareSize(int, double, double, int, int)
     */
    public void setSquareSize(int sideLengthInPixels, double fixPointX, double fixPointY) {
        graphicalRepresentation().setSquareSize(sideLengthInPixels, fixPointX, fixPointY);
        setBlocks();
    }

    /**
     * <p>Changes the size of the squares that form the top and bottom sides of the
     * blocks.</p>
     *
     * @param sideLengthInPixels new side length of a square.
     * @param fixPointX          x coordinate of fixpoint (normalized board coordinate).
     * @param fixPointY          y coordinate of fixpoint (normalized board coordinate).
     * @param targetX            x coordinate where fixpoint should be located after resize
     *                           (viewport coordinate).
     * @param targetY            y coordinate where fixpoint should be located after resize
     *                           (viewport coordinate).
     * @see #setSquareSize(int)
     * @see #setSquareSize(int, double, double)
     */
    public void setSquareSize(int sideLengthInPixels, double fixPointX, double fixPointY, int targetX, int targetY) {
        graphicalRepresentation().setSquareSize(sideLengthInPixels, fixPointX, fixPointY, targetX, targetY);
        setBlocks();
    }

    /**
     * <p>Returns the current side length (in pixels) of the top and bottom square of
     * a block.</p>
     *
     * @see #setSquareSize(int, double, double, int, int)
     */
    public int getSquareSize() {
        return graphicalRepresentation().getSquareSize();
    }

    /**
     * <p>Changes the size of the squares that form the top and bottom sides of the
     * blocks.</p>
     * <p>The center of the viewport will be a fixpoint, i.e. it will still show
     * the same location on the board after the resize.</p>
     *
     * @param sideLengthInPixels new side length of a square.
     * @see #setSquareSize(int, double, double)
     * @see #setSquareSize(int, double, double, int, int)
     */
    public void setSquareSize(int sideLengthInPixels) {
        graphicalRepresentation().setSquareSize(sideLengthInPixels);
        setBlocks();
    }

    /**
     * <p>Returns the viewport to the {@code IsoWorld}.</p>
     */
    public JIsoWorld graphicalRepresentation() {
        return jIsoWorld;
    }

    /**
     * <p>Adds {@code object} to the world at the given {@code location}
     * with the given {@code shift}.</p>
     * <p>After being added to the world, the object must not change its shape, i.e.
     * its {@link IsoBlock}s must not change their properties.</p>
     * <p>It is possible to add objects at locations that exceed the visible board
     * coordinates. In that case, the object will not be drawn (completely).</p>
     *
     * @throws IsoObjectCollisionException if adding {@code object} would cause a collision.
     *                                     In that case, {@code object} will not be added to the world.
     * @see #addObjectNoCollision(IsoObject, VectorInt3D, VectorDouble3D)
     */
    public void addObject(IsoObject object, VectorInt3D location, VectorDouble3D shift) {
        if (collision(object, location, shift)) throw new IsoObjectCollisionException();
        addObjectNoCollision(object, location, shift);
    }

    /**
     * <p>Like {@link #addObject(IsoObject object, VectorInt3D location, VectorDouble3D shift)}
     * but without collision check.</p>
     */
    public void addObjectNoCollision(IsoObject object, VectorInt3D location, VectorDouble3D shift) {
        object.setWorld(this);
        object.setReferenceBlockLocation(location);
        object.setShift(shift, null);

        Iterator<DominoBlock> iterator = object.blocksIterator();
        while (iterator.hasNext()) {
            IsoBlock block = iterator.next();
            VectorInt3D loc = block.getAbsoluteLocation();
            VectorDouble3D sh = block.getAbsoluteShift();

            int xm = 0;
            if (sh.x > 0) xm = 1;
            int ym = 0;
            if (sh.y > 0) ym = 1;
            int zm = 0;
            if (sh.z > 0) zm = 1;

            for (int x = loc.x; x <= loc.x + xm; ++x) {
                for (int y = loc.y; y <= loc.y + ym; ++y) {
                    for (int z = loc.z; z <= loc.z + zm; ++z) {
                        addToBlocks(new VectorInt3D(x, y, z), block);
                    }
                }
            }

            jIsoWorld.addBlock(block.graphicalRepresentation(), loc, shift);
            insertInPaintOrder(block);
        }
    }

    /**
     * <p>Moves {@code object} (which must already be part of the world) to
     * {@code newLocation} with {@code newShift}.</p>
     *
     * @throws IsoObjectCollisionException if moving {@code object} would cause a collision.
     *                                     In that case, {@code object} will not be moved.
     * @see #removeObject(IsoObject)
     * @see #addObject(IsoObject, VectorInt3D, VectorDouble3D)
     */
    public void moveObject(IsoObject object, VectorInt3D newLocation, VectorDouble3D newShift) {
        if (collision(object, newLocation, newShift)) throw new IsoObjectCollisionException();

        //could be made more efficient! If this is improved to not remove the object,
        //then don't forget to adjust the layer of every block

        int layer = -1;
        if (object.numberOfBlocks() > 0) {
            IsoBlock block = object.blocksIterator().next();
            layer = jIsoWorld.getLayer(block.graphicalRepresentation().toJComponent());
        }
        removeObject(object);
        addObjectNoCollision(object, newLocation, newShift);
        if (layer >= Integer.MAX_VALUE - object.numberOfBlocks()) putInTopLayer(object);
    }

    /**
     * <p>Removes {@code object} from the world.</p>
     *
     * @see #moveObject(IsoObject, VectorInt3D, VectorDouble3D)
     * @see #addObject(IsoObject, VectorInt3D, VectorDouble3D)
     */
    public void removeObject(IsoObject object) {
        Iterator<DominoBlock> iterator = object.blocksIterator();
        while (iterator.hasNext()) {
            IsoBlock block = iterator.next();
            VectorInt3D loc = block.getAbsoluteLocation();
            VectorDouble3D sh = block.getAbsoluteShift();

            int xm = 0;
            if (sh.x > 0) xm = 1;
            int ym = 0;
            if (sh.y > 0) ym = 1;
            int zm = 0;
            if (sh.z > 0) zm = 1;

            for (int x = loc.x; x <= loc.x + xm; ++x) {
                for (int y = loc.y; y <= loc.y + ym; ++y) {
                    for (int z = loc.z; z <= loc.z + zm; ++z) {
                        removeFromBlocks(new VectorInt3D(x, y, z), block);
                    }
                }
            }
            removeFromPaintOrder(block);
            jIsoWorld.removeBlock(block.graphicalRepresentation());
        }
        object.setWorld(null);
    }

    /**
     * <p>Removes all {@code IsoObject}s from the world.</p>
     */
    public void removeAllIsoObjects() {
        Iterator<Map.Entry<VectorInt3D, Collection<IsoBlock>>> iterator = blocks.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<VectorInt3D, Collection<IsoBlock>> ent = iterator.next();
            Collection<IsoBlock> c = ent.getValue();
            removeObject(c.iterator().next().parentObject());
            iterator = blocks.entrySet().iterator();
        }
    }

    /**
     * <p>Adds {@code object} to the viewport as an object that does not
     * interact with the blocks from the world.</p>
     *
     * @see #removeNonWorldObject(JComponent)
     */
    public void addNonWorldObject(JComponent object) {
        graphicalRepresentation().getContentPane().add(object);
    }

    /**
     * <p>Removes {@code object} which must have been added using
     * {@link #addNonWorldObject(JComponent)}.</p>
     */
    public void removeNonWorldObject(JComponent object) {
        graphicalRepresentation().getContentPane().remove(object);
    }

    /**
     * <p>Returns the {@link Collection} of all {@link IsoBlock}s at
     * {@code location} or {@code null} if there are none (meaning that
     * the returned {@link Collection} will never be empty). The returned
     * {@code Collection} includes blocks that overlap with {@code location}
     * due to shift.</p>
     * <p>Note, that for efficiency reasons, the returned {@code Collection}
     * may not be a copy
     * but a reference to the actual internal structure used. DO NOT MODIFY IT!</p>
     */
    public Collection<IsoBlock> blocksAtLocation(VectorInt3D location) {
        return blocks.get(location);
    }

}

