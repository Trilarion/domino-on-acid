/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.iso;

import domino.ui.block.DominoBlock;
import domino.utils.VectorDouble3D;
import domino.utils.VectorInt3D;

import java.util.Collection;
import java.util.Iterator;

/**
 * <p>An {@code IsoObject} consists of 0 or more {@link IsoBlock}s and can
 * be added to an {@link IsoWorld}.</p>
 */
public interface IsoObject {

    /**
     * <p>Returns the location of the reference block within the {@link IsoWorld}
     * {@code this} object is part of.</p>
     * <p>If {@code this} object is not part of an {@link IsoWorld}, the result
     * is undefined.</p>
     * <p>Note, that the {@code IsoObject} does not necessarily have an actual
     * {@link IsoBlock} at the location returned by this method. The location is just
     * the point of reference for {@link IsoBlock#getAbsoluteLocation()}.</p>
     *
     * @see #getShift()
     */
    VectorInt3D getReferenceBlockLocation();

    /**
     * <p>Sets the position of the reference block in the {@link IsoWorld}. This
     * method is intended to be used only by the {@link IsoWorld} managing this object.
     * Use {@link IsoWorld#moveObject(IsoObject, VectorInt3D, VectorDouble3D)} to move the
     * {@code IsoObject}.</p>
     * <p>This method creates a copy of the {@code location} argument.</p>
     *
     * @see #setShift(VectorDouble3D, Collection)
     */
    void setReferenceBlockLocation(VectorInt3D location);

    /**
     * <p>Sets the shift for the {@code IsoObject} as a whole. This shift is
     * added to each {@link IsoBlock}'s
     * {@link IsoBlock#getRelativeShift() relative shift} value. Because no
     * shift coordinate may exceed 1.0, this may cause the position of an
     * {@link IsoBlock} to change to bring the shift back into the permitted range.</p>
     * <p>This method is intended to be called by {@link IsoWorld} only. Use
     * {@link IsoWorld#moveObject(IsoObject, VectorInt3D, VectorDouble3D)} to move
     * an {@code IsoObject} around within the {@link IsoWorld}.</p>
     *
     * @param shift            the new shift for the object. The argument is copied, so later
     *                         changes to this object will not affect {@code this IsoObject}.
     * @param posChangedBlocks if non-{@code null}, all {@link IsoBlock}s that
     *                         change position due to the new shift will be added to this
     *                         {@link Collection}.
     * @see #getShift()
     * @see #setReferenceBlockLocation(VectorInt3D)
     */
    void setShift(VectorDouble3D shift, Collection<IsoBlock> posChangedBlocks);

    /**
     * <p>Returns the current shift value of {@code this IsoObject}.</p>
     *
     * @see #setShift(VectorDouble3D, Collection)
     * @see #getReferenceBlockLocation()
     */
    VectorDouble3D getShift();

    /**
     * <p>Returns the number of {@link IsoBlock}s, {@code this} object consists of.</p>
     * <p>Note that {@code IsoObject}s with 0 {@link IsoBlock}s are possible.</p> // TODO why are they possible??
     */
    int numberOfBlocks();

    /**
     * <p>Returns an iterator to iterate over all {@link IsoBlock}s {@code this}
     * object consists of.</p>
     * <p>Note that {@code IsoObject}s without any {@link IsoBlock}s are
     * possible.</p>
     */
    Iterator<DominoBlock> blocksIterator();

    /**
     * <p>Returns the {@link IsoWorld} {@code this} object is part of or
     * {@code null} if this object is not part of a world.</p>
     *
     * @see #setWorld(IsoWorld)
     */
    IsoWorld getWorld(); // TODO does the isoobject really need to know about the world, maybe more functionality should go from the object to the world

    /**
     * <p>Called by {@link IsoWorld} {@code world} when {@code this}
     * {@code IsoObject} is added to it.</p>
     * <p>When the {@code IsoObject} is removed again, this will be called with
     * {@code world==null}.
     *
     * @see #getWorld()
     */
    void setWorld(IsoWorld world);
}



