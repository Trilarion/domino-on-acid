/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.iso;

import domino.utils.VectorDouble3D;
import domino.utils.VectorInt3D;

/**
 * <p>{@code IsoBlock}s are the fundamental building blocks of an {@link IsoWorld}.
 * Every {@code IsoBlock} is part of exactly one {@link IsoObject}.</p>
 */
public interface IsoBlock {

    /**
     * <p>Returns the {@link IsoObject} this block belongs to.</p>
     */
    IsoObject parentObject();

    /**
     * <p>Returns the location of {@code this} block relative to the parent
     * {@link IsoObject}'s reference block (which has a relative location of (0,0,0)).</p>
     * <p>Unlike {@link #getAbsoluteLocation()} this method does not take shift
     * into account.</p>
     *
     * @see #getAbsoluteLocation()
     */
    VectorInt3D getRelativeLocation();

    /**
     * <p>Returns the location of {@code this} block within the {@link IsoWorld}.</p>
     * <p>This method takes shift into account. If {@code this} block has a
     * shift relative to the parent object and the parent object has been shifted so
     * that {@code this} block's shift would have a coordinate &gt;=1.0, then
     * the value returned by {@code getAbsoluteLocation()} will be adjusted to
     * get the shift into the legal range again.</p>
     *
     * @see #getRelativeLocation()
     */
    VectorInt3D getAbsoluteLocation();

    /**
     * <p>Returns the shift of {@code this} block relative to the parent object.</p>
     *
     * @see #getAbsoluteShift()
     */
    VectorDouble3D getRelativeShift();

    /**
     * <p>Returns the shift of {@code this} block relative to the location returned
     * by {@link #getAbsoluteLocation()}.</p>
     *
     * @see #getAbsoluteLocation()
     */
    VectorDouble3D getAbsoluteShift();

    /**
     * <p>Returns the value set with the most recent call to {@link #setPaintOrderPosition(int)}.
     * This method is meant to be used by a container managing {@code IsoBlocks}
     * such as {@link IsoWorld}.</p>
     */
    int getPaintOrderPosition();

    /**
     * <p>Sets the value to be returned by {@link #getPaintOrderPosition()}.</p>
     * <p>This method is meant to be used by a
     * container managing {@code IsoBlocks} such as {@link IsoWorld}.</p>
     */
    void setPaintOrderPosition(int position);

    /**
     * <p>Returns {@code true} if {@code this} block can not share the same
     * {@link VectorInt3D} with another {@code IsoBlock}.</p>
     */
    boolean isMassive();

    /**
     * <p>Returns the component to be used for rendering {@code this} block on
     * the screen.</p>
     */
    JIsoBlock graphicalRepresentation(); // TODO not sure if the rendering should be part of the iso block
}

