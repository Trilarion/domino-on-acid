/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.iso;

import domino.ui.UIConstants;
import domino.utils.Pair;
import domino.utils.VectorDouble3D;
import domino.utils.VectorInt3D;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;

/**
 * <p>{@code JIsoWorld} is the class responsible for the graphical
 * representation of an {@link IsoWorld}.</p>
 */
public class JIsoWorld extends JScrollPane {

    private final double xoffsetRel;
    private final double yoffsetRel;
    private final JIsoWorldPane boardPane;
    private int xoffset;
    private int yoffset;
    private int squareSide;
    private int plusxdim;
    private int minusxdim;
    private int plusydim;
    private int minusydim;
    private int leftpadding;
    private int toppadding;
    private int rightpadding;
    private int bottompadding;
    private int boardWidth;
    private int boardHeight;

    /**
     * <p>Creates a new JIsoWorld.</p>
     *
     * @param minusxdim  {@code -minusxdim} is the index of the left-most column of
     *                   the board (a negative number, because {@code minusxdim>=0}).
     * @param plusxdim   the index of the right-most column of
     *                   the board.
     * @param minusydim  {@code -minusydim} is the index of the down-most row of
     *                   the board (a negative number, because {@code minusydim>=0}).
     * @param plusydim   the index of the up-most row of the board.
     * @param squareSide initial side length of the bottom and top side square of a block.
     * @param xoffsetRel {@code xoffsetRel_*squareSide} is the number of pixels,
     *                   the top side of the block is shifted to the right in comparison to the
     *                   bottom side.
     * @param yoffsetRel {@code yoffsetRel_*squareSide} is the number of pixels,
     *                   the top side of the block is shifted down in comparison to the
     *                   bottom side.
     */
    public JIsoWorld(int minusxdim, int plusxdim, int minusydim, int plusydim, int squareSide, double xoffsetRel, double yoffsetRel) {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        //the following assignments must be performed before creating boardPane_
        this.xoffsetRel = xoffsetRel; // TODO could be a 2D vector
        this.yoffsetRel = yoffsetRel;
        this.squareSide = squareSide;
        xoffset = (int) Math.round(xoffsetRel * squareSide);
        yoffset = (int) Math.round(yoffsetRel * squareSide);
        this.minusxdim = minusxdim;
        this.plusxdim = plusxdim;
        this.minusydim = minusydim;
        this.plusydim = plusydim;
        leftpadding = 0;
        toppadding = 0;
        rightpadding = 0;
        bottompadding = 0;

        boardPane = new JIsoWorldPane(this);
        setViewportView(boardPane);
        setBorder(null);
    }

    /**
     * <p>Returns the number of pixels the top side of a block is moved to the right
     * when drawing, relative to the bottom side. This number changes if the squares
     * are resized.</p>
     *
     * @see #yOffset()
     */
    public int xOffset() {
        return xoffset;
    }


    /**
     * <p>Returns the number of pixels the top side of a block is moved down
     * when drawing, relative to the bottom side. This number changes if the squares
     * are resized.</p>
     *
     * @see #xOffset()
     */
    public int yOffset() {
        return yoffset;
    }

    /**
     * <p>Returns the index of the right-most column of the board.</p>
     */
    public int plusXDim() {
        return plusxdim;
    }

    /**
     * <p>Returns abs() of the index of the left-most column of the board.</p>
     */
    public int minusXDim() {
        return minusxdim;
    }

    /**
     * <p>Returns the index of the up-most row of the board.</p>
     */
    public int plusYDim() {
        return plusydim;
    }

    /**
     * <p>Returns abs() of the index of the down-most row of the board.</p>
     */
    public int minusYDim() {
        return minusydim;
    }

    /**
     * <p>Returns the canvas that contains the graphics objects for the blocks.</p>
     */
    public JComponent getContentPane() {
        return boardPane;
    }

    /**
     * <p>Translates coordinates relative to the viewport to
     * coordinates relative to the board. Padding is regarded as separate from the board,
     * so points in the padding area have negative board coordinates.</p>
     *
     * @see #translateToViewport(Point)
     * @see #translateNormalizedToViewport(Point2D.Double)
     * @see #translateToBoardNormalized(Point)
     * @see #translateToIsoWorld(Point, int)
     */
    public Point translateToBoard(Point p) {
        Point viewPos = getViewport().getViewPosition();
        return new Point(p.x + viewPos.x - leftpadding, p.y + viewPos.y - toppadding);
    }

    /**
     * <p>Translates coordinates relative to the board to
     * coordinates relative to the viewport.
     * Padding is regarded as separate from the board,
     * so points in the padding area have negative board coordinates.</p>
     *
     * @see #translateToBoard(Point)
     * @see #translateToBoardNormalized(Point)
     * @see #translateNormalizedToViewport(Point2D.Double)
     * @see #translateToIsoWorld(Point, int)
     */
    public Point translateToViewport(Point p) {
        Point viewPos = getViewport().getViewPosition();
        return new Point(p.x - viewPos.x + leftpadding, p.y - viewPos.y + toppadding);
    }

    /**
     * <p>Translates normalized board coordinates to
     * (pixel) coordinates relative to the viewport.
     * Padding is regarded as separate from the board,
     * so points in the padding area have negative board coordinates.</p>
     *
     * @see #translateToViewport(Point)
     * @see #translateToBoard(Point)
     * @see #translateToBoardNormalized(Point)
     * @see #translateToIsoWorld(Point, int)
     */
    public Point translateNormalizedToViewport(Point2D.Double p) {
        Point viewPos = getViewport().getViewPosition();
        return new Point((int) Math.round(p.x * boardWidth) - viewPos.x + leftpadding, (int) Math.round(p.y * boardHeight) - viewPos.y + toppadding);
    }

    /**
     * <p>Like {@link #translateToBoard(Point)} but returns coordinates relative to
     * the board's width and height.</p>
     *
     * @see #translateToViewport(Point)
     * @see #translateNormalizedToViewport(Point2D.Double)
     * @see #translateToBoard(Point)
     * @see #translateToIsoWorld(Point, int)
     */
    public Point2D.Double translateToBoardNormalized(Point p) {
        p = translateToBoard(p);
        return new Point2D.Double((double) p.x / boardWidth, (double) p.y / boardHeight);
    }

    /**
     * <p>Translates coordinates relative to the viewport to the {@link VectorInt3D}
     * with z coordinate {@code z} that an {@link IsoBlock} would need to have for
     * its top side to be at that location.</p>
     */

    /**
     * <p>Translates coordinates relative to the viewport to the {@link VectorInt3D}
     * with z coordinate {@code z} that an {@link IsoBlock} would need to have for
     * its top side to be at that location. If {@code sh!=null}, it will be
     * set to reflect the {@link VectorDouble3D} the block would need to have in order to
     * have the upper left corner of the top side exactly at point {@code p}.</p>
     *
     * @see #translateToViewport(Point)
     * @see #translateNormalizedToViewport(Point2D.Double)
     * @see #translateToBoard(Point)
     * @see #translateToBoardNormalized(Point)
     */
    public Pair<VectorInt3D, VectorDouble3D> translateToIsoWorld(Point p, int z) {
        p = translateToBoard(p);
        int width = squareSide + Math.abs(xoffset);
        int height = squareSide + Math.abs(yoffset);
        //no xzadd and yzadd in the following formulas!
        double x = (p.x - (1 + z - UIConstants.MAXZ_TO_TAKE_INTO_ACCOUNT_FOR_SIZING_BOARD) * xoffset - minusxdim * squareSide) / (double) squareSide;
        double y = (p.y - (1 + z - UIConstants.MAXZ_TO_TAKE_INTO_ACCOUNT_FOR_SIZING_BOARD) * yoffset - minusydim * squareSide) / (double) squareSide;
        int xi = (int) Math.floor(x);
        int yi = (int) Math.floor(y);
        return new Pair<>(new VectorInt3D(xi, yi, z), new VectorDouble3D(x - xi, y - yi, 0));
    }

    /**
     * <p>Moves the viewport {@code xdif} pixels to the right and
     * {@code ydif} pixels down.</p>
     */
    public void moveViewportRelative(int xdif, int ydif) { // TODO never used?
        JViewport viewport = getViewport();
        Point p = viewport.getViewPosition();
        p.x += xdif;
        p.y += ydif;
        moveViewportAbsolute(p.x, p.y);
    }

    /**
     * <p>Moves the viewport so that the coordinates {@code (x,y)} of the world
     * appear in the upper left corner.</p>
     */
    public void moveViewportAbsolute(int x, int y) {
        JViewport viewport = getViewport();
        Dimension d = viewport.getExtentSize();
        Dimension s = viewport.getViewSize();
        int right = x + d.width - s.width;
        int bottom = y + d.height - s.height;
        if (right > 0) x -= right;
        if (bottom > 0) y -= bottom;
        if (x < 0) x = 0;
        if (y < 0) y = 0;
        viewport.setViewPosition(new Point(x, y));
    }

    /**
     * <p>Returns the current side length (in pixels) of the top and bottom square of
     * a block.</p>
     *
     * @see #setSquareSize(int, double, double, int, int)
     */
    public int getSquareSize() {
        return squareSide;
    }

    /**
     * <p>Changes the size of the squares that form the top and bottom sides of the
     * blocks.</p>
     * <p>This method does not affect blocks already on the board. You have to call
     * {@link #setBlock(JIsoBlock, VectorInt3D, VectorDouble3D)} for each of these blocks, to
     * update its position and size.</p>
     * <p>The center of the viewport will be a fixpoint, i.e. it will still show
     * the same location on the board after the resize.</p>
     *
     * @param sideLengthInPixels new side length of a square.
     * @see #setSquareSize(int, double, double)
     * @see #setSquareSize(int, double, double, int, int)
     */
    public void setSquareSize(int sideLengthInPixels) {
        boardPane.setSquareSize(sideLengthInPixels);
    }

    /**
     * <p>Adds or removes rows or columns from the board.</p>
     * <p>This method only increases or decreases the visible part of the board.
     * Removing rows/columns will not remove any blocks located there and will not
     * prevent new blocks from being added there.</p>
     *
     * @param leftrighttopbottom Where to add/remove:
     *                           {@code 0=left, 1=right, 2=top, 3=bottom}
     * @param numLines           how much space to add. Negative values remove space.
     */
    public void addLines(int leftrighttopbottom, int numLines) {
        Dimension d = getViewport().getExtentSize();
        Point viewportMiddle = new Point(d.width >> 1, d.height >> 1);
        Point boardP = translateToBoard(viewportMiddle);

        int old;
        int xplus = 0;
        int yplus = 0;
        switch (leftrighttopbottom) {
            case 0:
                old = minusxdim;
                minusxdim += numLines;
                if (minusxdim < 0) minusxdim = 0;
                xplus = (minusxdim - old) * squareSide;
                boardP.x += xplus;
                break;

            case 1:
                plusxdim += numLines;
                if (plusxdim < 0) plusxdim = 0;
                break;

            case 2:
                old = minusydim;
                minusydim += numLines;
                if (minusydim < 0) minusydim = 0;
                yplus = (minusydim - old) * squareSide;
                boardP.y += yplus;
                break;

            case 3:
                plusydim += numLines;
                if (plusydim < 0) plusydim = 0;
                break;

            default:
                throw new IllegalArgumentException();
        }

        boardPane.setPreferredSize();
        boardPane.refocusViewport(xplus, yplus, boardP.x / (double) boardWidth, boardP.y / (double) boardHeight, viewportMiddle.x, viewportMiddle.y);
    }

    /**
     * <p>Adds {@code block} to the {@code JIsoWorld}.</p>
     */
    public void addBlock(JIsoBlock block, VectorInt3D location, VectorDouble3D shift) {
        boardPane.addBlock(block, location, shift);
    }

    /**
     * <p>Removes {@code block} from the {@code JIsoWorld}.</p>
     */
    public void removeBlock(JIsoBlock block) {
        boardPane.removeBlock(block);
    }

    /**
     * <p>Changes the size of the squares that form the top and bottom sides of the
     * blocks.</p>
     * <p>This method does not affect blocks already on the board. You have to call
     * {@link #setBlock(JIsoBlock, VectorInt3D, VectorDouble3D)} for each of these blocks, to
     * update its position and size.</p>
     *
     * @param sideLengthInPixels new side length of a square.
     * @param fixPointX          x coordinate of fixpoint (normalized board coordinate). This
     *                           board location will still be visible at the same position in the viewport
     *                           after the resize.
     * @param fixPointY          y coordinate of fixpoint (normalized board coordinate).
     * @see #setSquareSize(int)
     * @see #setSquareSize(int, double, double, int, int)
     */
    public void setSquareSize(int sideLengthInPixels, double fixPointX, double fixPointY) {
        boardPane.setSquareSize(sideLengthInPixels, fixPointX, fixPointY);
    }

    /**
     * <p>Changes the size of the squares that form the top and bottom sides of the
     * blocks.</p>
     * <p>This method does not affect blocks already on the board. You have to call
     * {@link #setBlock(JIsoBlock, VectorInt3D, VectorDouble3D)} for each of these blocks, to
     * update its position and size.</p>
     *
     * @param sideLengthInPixels new side length of a square.
     * @param fixPointX          x coordinate of fixpoint (normalized board coordinate).
     * @param fixPointY          y coordinate of fixpoint (normalized board coordinate).
     * @param targetX            x coordinate where fixpoint should be located after resize
     *                           (viewport coordinate).
     * @param targetY            y coordinate where fixpoint should be located after resize
     *                           (viewport coordinate).
     * @see #setSquareSize(int)
     * @see #setSquareSize(int, double, double)
     */
    public void setSquareSize(int sideLengthInPixels, double fixPointX, double fixPointY, int targetX, int targetY) {
        boardPane.setSquareSize(sideLengthInPixels, fixPointX, fixPointY, targetX, targetY);
    }

    /**
     * <p>Recomputes padding, repositions viewport and {@link Component}s.</p>
     *
     * @param xplus     value to add to {@link Component}s' x coordinates.
     * @param yplus     value to add to {@link Component}s' y coordinates.
     * @param fixPointX x coordinate of fixpoint (normalized board coordinate).
     * @param fixPointY y coordinate of fixpoint (normalized board coordinate).
     * @param targetX   x coordinate where fixpoint should be located after resize
     *                  (viewport coordinate).
     * @param targetY   y coordinate where fixpoint should be located after resize
     *                  (viewport coordinate).
     */
    public void refocusViewport(int xplus, int yplus, double fixPointX, double fixPointY, int targetX, int targetY) { // TODO never used
        boardPane.refocusViewport(xplus, yplus, fixPointX, fixPointY, targetX, targetY);
    }

    /**
     * <p>Adjusts position and size of {@code block} after changing the square
     * size, then causes a repaint.</p>
     *
     * @param block    the block to adjust. This block must have been added to this
     *                 {@code JIsoWorld}.
     * @param location the location of {@code block} within the world.
     * @param shift    {@code block}'s shift.
     * @see #setSquareSize(int, double, double, int, int)
     */
    public void setBlock(JIsoBlock block, VectorInt3D location, VectorDouble3D shift) {
        boardPane.setBlock(block, location, shift);
    }

    /**
     * <p>see {@link JLayeredPane#getLayer(Component)}.</p>
     */
    public int getLayer(Component component) {
        return boardPane.getLayer(component);
    }

    /**
     * <p>see {@link JLayeredPane#setLayer(Component, int)}.</p>
     */
    public void setLayer(Component component, int layer) {
        boardPane.setLayer(component, layer);
    }

    /**
     * <p>Like {@link #setLayer(Component, int)} but instead of specifying the layer
     * explicitly, it is computed to be appropriate for a block at the given
     * {@code location}.</p>
     */
    public void setLayer(Component component, VectorInt3D location) {
        boardPane.setLayer(component, boardPane.layerFor(location));
    }

    /**
     * <p>See {@link JLayeredPane#setPosition(Component, int)}.</p>
     */
    public void setPaintOrderPosition(Component component, int paintOrderPosition) {
        boardPane.setPosition(component, paintOrderPosition);
    }

    /**
     * <p>{@code xoffsetRel_*squareSide_} is the number of pixels, the top side of
     * the block is shifted to the right relative to the bottom side.</p>
     */
    public double getXoffsetRel() {
        return xoffsetRel;
    }

    /**
     * <p>{@code yoffsetRel_*squareSide_} is the number of pixels, the top side of
     * the block is shifted down relative to the bottom side.</p>
     */
    public double getYoffsetRel() {
        return yoffsetRel;
    }

    /**
     * <p>The actual scroll area that contains the graphics objects for the blocks.</p>
     */
    public JIsoWorldPane getBoardPane() {
        return boardPane;
    }

    /**
     * <p>{@code xoffsetRel_*squareSide_}</p>
     */
    public int getXoffset() {
        return xoffset;
    }

    public void setXoffset(int xoffset) {
        this.xoffset = xoffset;
    }

    /**
     * <p>{@code yoffsetRel_*squareSide_}</p>
     */
    public int getYoffset() {
        return yoffset;
    }

    public void setYoffset(int yoffset) {
        this.yoffset = yoffset;
    }

    /**
     * <p>Side length of the bottom and top side square of a block.</p>
     */
    public int getSquareSide() {
        return squareSide;
    }

    public void setSquareSide(int squareSide) {
        this.squareSide = squareSide;
    }

    /**
     * <p>{@code plusxdim_} is the index of the right-most column of the board.</p>
     */
    public int getPlusxdim() {
        return plusxdim;
    }

    /**
     * <p>{@code -minusxdim_} is the index of the left-most column of the board
     * (a negative number, because {@code minusxdim_>=0}).</p>
     */
    public int getMinusxdim() {
        return minusxdim;
    }

    /**
     * <p>{@code plusydim_} is the index of the up-most row of the board.</p>
     */
    public int getPlusydim() {
        return plusydim;
    }

    /**
     * <p>{@code -minusydim_} is the index of the down-most row of the board
     * (a negative number, because {@code minusydim_>=0}).</p>
     */
    public int getMinusydim() {
        return minusydim;
    }

    /**
     * Number of pixels added at the left side of the board to satisfy the fixpoint
     * restrictions from the last call to {@link #setSquareSize(int, double, double, int, int)}.
     */
    public int getLeftpadding() {
        return leftpadding;
    }

    public void setLeftpadding(int leftpadding) {
        this.leftpadding = leftpadding;
    }

    /**
     * Number of pixels added at the top of the board to satisfy the fixpoint
     * restrictions from the last call to {@link #setSquareSize(int, double, double, int, int)}.
     */
    public int getToppadding() {
        return toppadding;
    }

    public void setToppadding(int toppadding) {
        this.toppadding = toppadding;
    }

    /**
     * Number of pixels added at the right side of the board to satisfy the fixpoint
     * restrictions from the last call to {@link #setSquareSize(int, double, double, int, int)}.
     */
    public int getRightpadding() {
        return rightpadding;
    }

    public void setRightpadding(int rightpadding) {
        this.rightpadding = rightpadding;
    }

    /**
     * Number of pixels added at the bottom of the board to satisfy the fixpoint
     * restrictions from the last call to {@link #setSquareSize(int, double, double, int, int)}.
     */
    public int getBottompadding() {
        return bottompadding;
    }

    public void setBottompadding(int bottompadding) {
        this.bottompadding = bottompadding;
    }

    /**
     * <p>Width of the board without padding.</p>
     */
    public int getBoardWidth() {
        return boardWidth;
    }

    public void setBoardWidth(int boardWidth) {
        this.boardWidth = boardWidth;
    }

    /**
     * <p>Height of the board without padding.</p>
     */
    public int getBoardHeight() {
        return boardHeight;
    }

    public void setBoardHeight(int boardHeight) {
        this.boardHeight = boardHeight;
    }
}

