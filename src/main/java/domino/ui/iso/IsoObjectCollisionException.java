/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.iso;

/**
 * {@code IsoObjectCollision} is thrown if an attempt is made to put a
 * massive {@link IsoBlock} at a position already occupied by another
 * massive {@link IsoBlock}.
 */
public class IsoObjectCollisionException extends RuntimeException {
    private static final long serialVersionUID = -1018203880078255676L;

    /**
     * Constructs an {@code IsoObjectCollision} with no detail message.
     */
    public IsoObjectCollisionException() {
    }

    /**
     * Constructs an {@code IsoObjectCollision} with the specified
     * detail message
     */
    public IsoObjectCollisionException(String s) {
        super(s);
    }

}

