/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.iso;

import javax.swing.*;

/**
 * <p>A {@code JIsoBlock} is the graphical representation for an {@code IsoBlock}.</p>
 */
public interface JIsoBlock {

    /**
     * <p>Sets the offset (in pixels) of the top side of the block relative to the bottom side.
     * If {@code xoffset} and {@code yoffset} are both negative, the top side of the block will
     * be drawn up and left of the bottom side of the block.</p>
     */
    void setIsoOffsets(int xoffset, int yoffset);

    /**
     * <p>Returns a {@link JComponent} to perform the actual drawing.</p>
     */
    JComponent toJComponent();
}

