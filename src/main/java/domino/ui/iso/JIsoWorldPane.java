/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.iso;

import domino.ui.UIConstants;
import domino.utils.VectorDouble3D;
import domino.utils.VectorInt3D;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.Point2D;

/**
 * <p>The content pane for a {@link JIsoWorld}.</p>
 */
public class JIsoWorldPane extends JLayeredPane implements Scrollable {

    private final JIsoWorld jIsoWorld;

    /**
     * Creates a new {@code JIsoWorldPane}.
     */
    public JIsoWorldPane(JIsoWorld jIsoWorld) {
        this.jIsoWorld = jIsoWorld;
        setPreferredSize();

        /*
         * We need to grow and shrink with our JIsoWorld to avoid ugly effects like
         * IsoObjects being cut off in the middle.
         */
        jIsoWorld.addComponentListener
                (
                        new ComponentAdapter() {
                            public void componentResized(ComponentEvent e) {
                                Dimension d = jIsoWorld.getViewport().getExtentSize();
                                Point2D.Double p = jIsoWorld.translateToBoardNormalized(new Point(d.width >> 1, d.height >> 1));
                                refocusViewport(0, 0, p.x, p.y, d.width >> 1, d.height >> 1);
                            }

                        }
                );
    }

    public Dimension getPreferredScrollableViewportSize() {
        return getPreferredSize();
    }

    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        return jIsoWorld.getSquareSide();
    }

    public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
        return jIsoWorld.getSquareSide();
    }

    public boolean getScrollableTracksViewportWidth() {
        return false;
    }

    public boolean getScrollableTracksViewportHeight() {
        return false;
    }

    /**
     * <p>Computes the size of the canvas and sets it as preferred size.</p>
     */
    protected void setPreferredSize() {
        int cols = jIsoWorld.getMinusxdim() + jIsoWorld.getPlusxdim() + 1;
        int rows = jIsoWorld.getMinusydim() + jIsoWorld.getPlusydim() + 1;
        jIsoWorld.setBoardWidth(jIsoWorld.getSquareSide() * cols + Math.abs(UIConstants.MAXZ_TO_TAKE_INTO_ACCOUNT_FOR_SIZING_BOARD * jIsoWorld.getXoffset()));
        jIsoWorld.setBoardHeight(jIsoWorld.getSquareSide() * rows + Math.abs(UIConstants.MAXZ_TO_TAKE_INTO_ACCOUNT_FOR_SIZING_BOARD * jIsoWorld.getYoffset()));
        setPreferredSize(new Dimension(jIsoWorld.getBoardWidth() + jIsoWorld.getLeftpadding() + jIsoWorld.getRightpadding(), jIsoWorld.getBoardHeight() + jIsoWorld.getToppadding() + jIsoWorld.getBottompadding()));
    }

    /**
     * <p>Adjusts position and size of {@code block} after changing the square
     * size, then causes a repaint.</p>
     *
     * @param block    the block to adjust. This block must have been added to this
     *                 {@code JIsoWorld}.
     * @param location the location of {@code block} within the world.
     * @param shift    {@code block}'s shift.
     * @see #setSquareSize(int, double, double, int, int)
     */
    public void setBlock(JIsoBlock block, VectorInt3D location, VectorDouble3D shift) {
        int width = jIsoWorld.getSquareSide() + Math.abs(jIsoWorld.getXoffset());
        int height = jIsoWorld.getSquareSide() + Math.abs(jIsoWorld.getYoffset());
        int xzadd = (jIsoWorld.getXoffset() < 0) ? 1 : 0;
        int yzadd = (jIsoWorld.getYoffset() < 0) ? 1 : 0;
        int xpixel = (int) Math.round(jIsoWorld.getLeftpadding() + ((location.x + jIsoWorld.getMinusxdim()) + shift.x) * jIsoWorld.getSquareSide() + ((xzadd + location.z - UIConstants.MAXZ_TO_TAKE_INTO_ACCOUNT_FOR_SIZING_BOARD) + shift.z) * jIsoWorld.getXoffset());
        int ypixel = (int) Math.round(jIsoWorld.getToppadding() + ((location.y + jIsoWorld.getMinusydim()) + shift.y) * jIsoWorld.getSquareSide() + ((yzadd + location.z - UIConstants.MAXZ_TO_TAKE_INTO_ACCOUNT_FOR_SIZING_BOARD) + shift.z) * jIsoWorld.getYoffset());
        JComponent component = block.toJComponent();
        component.setBounds(xpixel, ypixel, width, height);
        block.setIsoOffsets(jIsoWorld.getXoffset(), jIsoWorld.getYoffset());
        //block.revalidate();
        component.repaint();
    }

    /**
     * <p>Returns the appropriate layer for a block at {@code location}.</p>
     */
    public int layerFor(VectorInt3D location) {
        return (location.y + UIConstants.YDIM_MAX / 2) * UIConstants.XDIM_MAX + (location.x + UIConstants.XDIM_MAX / 2) * UIConstants.ZDIM_MAX + location.z + UIConstants.ZDIM_MAX / 2;
    }

    /**
     * <p>Adds {@code block} to the {@code JIsoWorld}.</p>
     */
    public void addBlock(JIsoBlock block, VectorInt3D location, VectorDouble3D shift) {
        add(block.toJComponent(), layerFor(location));
        setBlock(block, location, shift);
    }

    /**
     * <p>Removes {@code block} from the {@code JIsoWorld}.</p>
     */
    public void removeBlock(JIsoBlock block) {
        Rectangle r = block.toJComponent().getBounds(null);
        remove(block.toJComponent());
        repaint(r);
    }

    /**
     * <p>Changes the size of the squares that form the top and bottom sides of the
     * blocks.</p>
     * <p>This method does not affect blocks already on the board. You have to call
     * {@link #setBlock(JIsoBlock, VectorInt3D, VectorDouble3D)} for each of these blocks, to
     * update its position and size.</p>
     * <p>The center of the viewport will be a fixpoint, i.e. it will still show
     * the same location on the board after the resize.</p>
     *
     * @param sideLengthInPixels new side length of a square.
     * @see #setSquareSize(int, double, double)
     * @see #setSquareSize(int, double, double, int, int)
     */
    public void setSquareSize(int sideLengthInPixels) {
        Dimension d = jIsoWorld.getViewport().getExtentSize();
        Point2D.Double p = jIsoWorld.translateToBoardNormalized(new Point(d.width >> 1, d.height >> 1));
        setSquareSize(sideLengthInPixels, p.x, p.y);
    }

    /**
     * <p>Changes the size of the squares that form the top and bottom sides of the
     * blocks.</p>
     * <p>This method does not affect blocks already on the board. You have to call
     * {@link #setBlock(JIsoBlock, VectorInt3D, VectorDouble3D)} for each of these blocks, to
     * update its position and size.</p>
     *
     * @param sideLengthInPixels new side length of a square.
     * @param fixPointX          x coordinate of fixpoint (normalized board coordinate). This
     *                           board location will still be visible at the same position in the viewport
     *                           after the resize.
     * @param fixPointY          y coordinate of fixpoint (normalized board coordinate).
     * @see #setSquareSize(int)
     * @see #setSquareSize(int, double, double, int, int)
     */
    public void setSquareSize(int sideLengthInPixels, double fixPointX, double fixPointY) {
        Point p = jIsoWorld.translateToViewport(new Point((int) Math.round(fixPointX * jIsoWorld.getBoardWidth()), (int) Math.round(fixPointY * jIsoWorld.getBoardHeight())));
        setSquareSize(sideLengthInPixels, fixPointX, fixPointY, p.x, p.y);
    }

    /**
     * <p>Changes the size of the squares that form the top and bottom sides of the
     * blocks.</p>
     * <p>This method does not affect blocks already on the board. You have to call
     * {@link #setBlock(JIsoBlock, VectorInt3D, VectorDouble3D)} for each of these blocks, to
     * update its position and size.</p>
     *
     * @param sideLengthInPixels new side length of a square.
     * @param fixPointX          x coordinate of fixpoint (normalized board coordinate).
     * @param fixPointY          y coordinate of fixpoint (normalized board coordinate).
     * @param targetX            x coordinate where fixpoint should be located after resize
     *                           (viewport coordinate).
     * @param targetY            y coordinate where fixpoint should be located after resize
     *                           (viewport coordinate).
     * @see #setSquareSize(int)
     * @see #setSquareSize(int, double, double)
     */
    public void setSquareSize(int sideLengthInPixels, double fixPointX, double fixPointY, int targetX, int targetY) {
        if (sideLengthInPixels < UIConstants.MIN_SQUARE_SIZE) sideLengthInPixels = UIConstants.MIN_SQUARE_SIZE;
        double scale = ((double) sideLengthInPixels) / jIsoWorld.getSquareSide();
        jIsoWorld.setBoardWidth((int)(jIsoWorld.getBoardWidth() * scale));
        jIsoWorld.setBoardHeight((int)(jIsoWorld.getBoardHeight() * scale));
        double newFixPointX = fixPointX * jIsoWorld.getBoardWidth();
        double newFixPointY = fixPointY * jIsoWorld.getBoardHeight();
        JViewport viewport = jIsoWorld.getViewport();
        Point viewPos = new Point();
        viewPos.x = (int) Math.round(newFixPointX - targetX);
        viewPos.y = (int) Math.round(newFixPointY - targetY);
        jIsoWorld.setLeftpadding(0);
        jIsoWorld.setToppadding(0);
        if (viewPos.x < 0) {
            jIsoWorld.setLeftpadding(-viewPos.x);
            viewPos.x = 0;
        }
        if (viewPos.y < 0) {
            jIsoWorld.setToppadding(-viewPos.y);
            viewPos.y = 0;
        }

        Dimension d = viewport.getExtentSize();
        jIsoWorld.setRightpadding(viewPos.x + d.width - jIsoWorld.getBoardWidth());
        jIsoWorld.setBottompadding(viewPos.y + d.height - jIsoWorld.getBoardHeight());
        if (jIsoWorld.getRightpadding() < 0) jIsoWorld.setRightpadding(0);
        if (jIsoWorld.getBottompadding() < 0) jIsoWorld.setBottompadding(0);

        jIsoWorld.setSquareSide(sideLengthInPixels);
        jIsoWorld.setXoffset((int) Math.round(jIsoWorld.getXoffsetRel() * jIsoWorld.getSquareSide()));
        jIsoWorld.setYoffset((int) Math.round(jIsoWorld.getYoffsetRel() * jIsoWorld.getSquareSide()));
        setPreferredSize();

        revalidate();
        viewport.validate(); //force viewport to acknowledge new size of this
        //so that it will not crop the new viewPos coordinates
        viewport.setViewPosition(viewPos);
    }

    /**
     * <p>Recomputes padding, repositions viewport and {@link Component}s.</p>
     *
     * @param xplus     value to add to {@link Component}s' x coordinates.
     * @param yplus     value to add to {@link Component}s' y coordinates.
     * @param fixPointX x coordinate of fixpoint (normalized board coordinate).
     * @param fixPointY y coordinate of fixpoint (normalized board coordinate).
     * @param targetX   x coordinate where fixpoint should be located after resize
     *                  (viewport coordinate).
     * @param targetY   y coordinate where fixpoint should be located after resize
     *                  (viewport coordinate).
     */
    public void refocusViewport(int xplus, int yplus, double fixPointX, double fixPointY, int targetX, int targetY) {
        double newFixPointX = fixPointX * jIsoWorld.getBoardWidth();
        double newFixPointY = fixPointY * jIsoWorld.getBoardHeight();

        Point viewPos = new Point();
        viewPos.x = (int) Math.round(newFixPointX - targetX);
        viewPos.y = (int) Math.round(newFixPointY - targetY);
        int oldleftpadding = jIsoWorld.getLeftpadding();
        int oldtoppadding = jIsoWorld.getToppadding();
        jIsoWorld.setLeftpadding(0);
        jIsoWorld.setToppadding(0);
        if (viewPos.x < 0) {
            jIsoWorld.setLeftpadding(-viewPos.x);
            viewPos.x = 0;
        }
        if (viewPos.y < 0) {
            jIsoWorld.setToppadding(-viewPos.y);
            viewPos.y = 0;
        }
        xplus += (jIsoWorld.getLeftpadding() - oldleftpadding);
        yplus += (jIsoWorld.getToppadding() - oldtoppadding);

        if (xplus != 0 || yplus != 0) {
            Component[] comp = jIsoWorld.getBoardPane().getComponents();
            for (Component component : comp) {
                Point p = component.getLocation();
                component.setLocation(p.x + xplus, p.y + yplus);
            }
        }

        JViewport viewport = jIsoWorld.getViewport();
        Dimension d = viewport.getExtentSize();
        jIsoWorld.setRightpadding(viewPos.x + d.width - jIsoWorld.getBoardWidth());
        jIsoWorld.setBottompadding(viewPos.y + d.height - jIsoWorld.getBoardHeight());
        if (jIsoWorld.getRightpadding() < 0) jIsoWorld.setRightpadding(0);
        if (jIsoWorld.getBottompadding() < 0) jIsoWorld.setBottompadding(0);

        setPreferredSize();

        revalidate();
        viewport.validate(); //force viewport to acknowledge new size of this
        //so that it will not crop the new viewPos coordinates
        viewport.setViewPosition(viewPos);
    }

}
