/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class TxtFileFilter extends FileFilter {
    private final String[] extensions_;

    public TxtFileFilter(String[] extensions) {
        extensions_ = new String[extensions.length];
        System.arraycopy(extensions, 0, extensions_, 0, extensions_.length);
    }

    public boolean accept(File f) {
        if (f.isDirectory()) return true;
        for (String s : extensions_) if (f.getName().endsWith(s)) return true;
        return false;
    }

    public String getDescription() {
        StringBuilder desc = new StringBuilder("Text Files (");
        for (int i = 0; i < extensions_.length - 1; ++i) desc.append("*").append(extensions_[i]).append(", ");
        desc.append("*").append(extensions_[extensions_.length - 1]).append(")");
        return desc.toString();
    }

}
