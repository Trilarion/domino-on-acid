/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui;

import domino.model.tile.DominoTile;

import java.net.URL;

/**
 *
 */
public final class UIConstants {

    public static final int WORLD_XDIM = 20;
    public static final int WORLD_YDIM = 20;
    public static final int WORLD_SQUARESIZE = 32; //24; // 24 for generating documentation pics
    public static final int BORING_ELONGATOR_INDEX = 3;
    public static final int WINDOW_WIDTH = 900;
    public static final int WINDOW_HEIGHT = 700;
    public static final URL DEFAULT_TILES_URL = UIConstants.class.getResource("/domino/core/tiles.tiles");
    public static final URL LEVELS_URL = UIConstants.class.getResource("/domino/core/levels.txt");
    public static final int INITIAL_LEVEL = 0;
    public static final URL MANUAL_URL = UIConstants.class.getResource("/domino/documentation/index.html");
    public static final int ZOOM_DRAG_THRESHOLD = 5;
    /**
     * <p>Number of empty rows/columns to keep at all sides of the board. If the adding
     * of a {@link DominoTile} occurs in this area, it will be increased.</p>
     */
    public static final int FREE_SQUARES_AROUND_BOARD = 16;
    public static final int ROTATE_DRAG_THRESHOLD = 32;
    public static final int NUM_ROTATE_STEPS = 4;
    public static final int TILE_TEAR_OFF_THRESHOLD = 16;
    /**
     * <p>{@code XDIM_MAX, YDIM_MAX and ZDIM_MAX} specify the maximum number of blocks
     * along the respective axis. The product of the 3 must not exceed
     * {@link Integer#MAX_VALUE} because a unique layer is assigned to each block.</p>
     * <p>x coordinates of blocks in the {@code JIsoWorld} must be
     * in the range {@code ]-XDIM_MAX/2,XDIM_MAX/2[}. The same applies to
     * y coordinates and {@code YDIM_MAX} and z coordinates and
     * {@code ZDIM_MAX}.</p>
     */
    public static final int XDIM_MAX = 4096;
    /**
     * <p>see {@link #XDIM_MAX}.</p>
     */
    public static final int YDIM_MAX = 4096;
    /**
     * <p>see {@link #XDIM_MAX}.</p>
     */
    public static final int ZDIM_MAX = 64;
    /**
     * <p>When determining the size of the drawing canvas, space needs to be added to
     * make sure the top sides (which are offset due to the isometric view) of blocks
     * at the sides are still on the canvas. Blocks with a z coordinate that exceeds this
     * constants value will not be completely visible anymore if placed at the very side
     * of the board.</p>
     */
    public static final int MAXZ_TO_TAKE_INTO_ACCOUNT_FOR_SIZING_BOARD = 4;
    /**
     * <p>The minimum side length (in pixels) of a block's top and bottom side square.</p>
     */
    public static final int MIN_SQUARE_SIZE = 10;

    private UIConstants() {}
}
