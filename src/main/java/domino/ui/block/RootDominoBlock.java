/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.block;

import domino.logic.square.FormulaDominoSquare;
import domino.logic.tree.FormulaDominoTree;
import domino.model.tile.DominoTile;
import domino.utils.VectorInt3D;

import java.awt.*;

/**
 * <p>Class for {@link DominoBlock}s labelled with a root formula.</p>
 */
public class RootDominoBlock extends DominoBlock {
    private final DominoTile dominoTile;
    /**
     * <p>The tree that has the formula as root.</p>
     */
    private final FormulaDominoTree tree_;

    /**
     * <p>Creates a new {@code RootDominoBlock}.</p>
     *
     * @param relativeLocation value to be returned by {@link #getRelativeLocation()}.
     * @param active           {@code false} if this block is just a dummy for displaying.
     * @param tree             the subtree of {@link DominoTile}'s tree that provides the bonus.
     */
    public RootDominoBlock(DominoTile dominoTile, VectorInt3D relativeLocation, boolean active, FormulaDominoTree tree) {
        super(dominoTile);
        this.dominoTile = dominoTile;
        setRelativeLocation_(relativeLocation);
        setActive_(active);
        tree_ = tree;
        FormulaDominoSquare sq = (FormulaDominoSquare) tree.rootSquare();
        setFormula_(sq.toPrefixRepresentation(DominoTile.globalUnboundIdAssigner));
    }

    /**
     * <p>Returns the {@link FormulaDominoTree} whose root formula this tile
     * represents.</p>
     */
    public FormulaDominoTree formulaDominoTree() {
        return tree_;
    }

    /**
     * <p>Returns the color used to paint the side of the block parallel to the x axis.</p>
     */
    public Color xColor() {
        if (!isActive()) return super.xColor();
        else return (dominoTile.numberOfBlocks() > 1) ? new Color(220, 50, 20) : DominoTile.BONUS_BLUE;
    }

    /**
     * <p>Returns the color used to paint the side of the block parallel to the y axis.</p>
     */
    public Color yColor() {
        if (!isActive()) return super.yColor();
        else return (dominoTile.numberOfBlocks() > 1) ? new Color(180, 40, 15) : DominoTile.BONUS_BLUE_DARK;
    }

    //see DominoBlock
    public void updateFormula() {
        FormulaDominoSquare sq = (FormulaDominoSquare) tree_.rootSquare();
        String newFormula = sq.toPrefixRepresentation(DominoTile.globalUnboundIdAssigner);
        if (!newFormula.equals(getFormula_())) {
            setFormula_(newFormula);
            repaint();
        }
    }

}
