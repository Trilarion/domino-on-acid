/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.block;

import domino.logic.square.FormulaDominoSquare;
import domino.logic.syntaxnet.FormulaSyntaxNet;
import domino.ui.iso.IsoBlock;
import domino.ui.iso.IsoObject;
import domino.ui.iso.JIsoBlock;
import domino.model.tile.DominoTile;
import domino.utils.VectorDouble3D;
import domino.utils.VectorInt3D;

import javax.swing.*;
import java.awt.*;

/**
 * <p>The class of the blocks that form a {@link DominoTile}.</p>
 */
public abstract class DominoBlock extends JPanel implements IsoBlock, JIsoBlock {
    private final DominoTile dominoTile;
    private VectorInt3D relativeLocation_;
    private int paintOrderPosition_;
    private boolean active_;
    private String formula_;

    private int xoffset_;
    private int yoffset_;

    /**
     * <p>default constructor.</p>
     */
    protected DominoBlock(DominoTile dominoTile) {
        super(null);
        this.dominoTile = dominoTile;
        setOpaque(false); //because of the non-rectangular shape
    }

    /**
     * <p>If this is {@code false}, then {@code this DominoBlock} is just a
     * filler for displaying purposes that can not be attached to another
     * {@code DominoBlock}.</p>
     */
    public boolean isActive() {
        return active_;
    }

    /**
     * <p>Deactivates this block.</p>
     *
     * @see #isActive()
     */
    public void deactivate() {
        active_ = false;
        repaint();
    }


    /**
     * <p>Returns the formula whose graphical representation currently labels
     * {@code this DominoBlock}.</p>
     */
    public String currentFormula() {
        return formula_;
    }

    /**
     * <p>Checks if the formula that labels this block has changed in the
     * underlying {@code FormulaDominoTree} and updates and repaints this
     * block if necessary.</p>
     * <p>Multi-threading issues are discussed at {@link DominoTile#updateFormulas()}.</p>
     */
    public abstract void updateFormula();

    /**
     * <p>Returns the color used to paint the side of the block parallel to the x axis.</p>
     */
    public Color xColor() {
        return Color.gray;
    }

    /**
     * <p>Returns the color used to paint the side of the block parallel to the y axis.</p>
     */
    public Color yColor() {
        return Color.gray;
    }

    /**
     * <p>see {@link JComponent#paintComponent(Graphics)}.</p>
     */
    protected void paintComponent(Graphics g) {
        Insets insets = getInsets();
        int currentWidth = getWidth() - insets.left - insets.right;
        int currentHeight = getHeight() - insets.top - insets.bottom;
        int x = insets.left;
        int y = insets.top;

        //yes, I know that having different width and height for a square doesn't make
        //sense, but I don't want to force the owner of this object to display it with a
        //1:1 aspect ratio
        int squareWidth = currentWidth - Math.abs(xoffset_);
        int squareHeight = currentHeight - Math.abs(yoffset_);

        int x0, y0, x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6;
        if (xoffset_ <= 0) {
            if (yoffset_ <= 0) {
                x0 = x;
                y0 = y;
                x1 = x + (squareWidth - 1);
                y1 = y;
                x2 = x1;
                y2 = y1 + (squareHeight - 1);
                x3 = x0;
                y3 = y2;

                x4 = x1 - xoffset_;
                y4 = y1 - yoffset_;
                x5 = x4;
                y5 = y4 + (squareHeight - 1);
                x6 = x5 - (squareWidth - 1);
                y6 = y5;
            } else //if (yoffset_>0)
            {
                throw new RuntimeException("unsupported");
            }
        } else //if  (xoffset_>0)
        {
            throw new RuntimeException("unsupported");
        }

        int[] xPoints1 = {x3, x2, x5, x6};
        int[] yPoints1 = {y3, y2, y5, y6};
        int[] xPoints2 = {x1, x4, x5, x2};
        int[] yPoints2 = {y1, y4, y5, y2};

        g.setColor(xColor());
        g.fillPolygon(xPoints1, yPoints1, 4);
        g.setColor(yColor());
        g.fillPolygon(xPoints2, yPoints2, 4);

        g.setColor(DominoTile.OUTLINE_COLOR);
        g.drawPolygon(xPoints1, yPoints1, 4);
        g.drawPolygon(xPoints2, yPoints2, 4);

        paintLabel(g, x0, y0, squareWidth, squareHeight);
    }

    /**
     * <p>Paints the top side (the label) of the block.</p>
     *
     * @param g      the graphics context for drawing.
     * @param x      the x coordinate of the upper-left corner of the label rectangle.
     * @param y      the y coordinate of the upper-left corner of the label rectangle.
     * @param width  the width of the label rectangle.
     * @param height the height of the label rectangle.
     */
    protected void paintLabel(Graphics g, int x, int y, int width, int height) {
        paintRectangle(currentFormula(), 0, g, x, y, width, height, true);
    }

    /**
     * <p>Paints a framed rectangle filled with one or more colors.</p>
     *
     * @param g      the graphics context for drawing.
     * @param x      the x coordinate of the upper-left corner of the rectangle.
     * @param y      the y coordinate of the upper-left corner of the rectangle.
     * @param width  the width of the rectangle.
     * @param height the height of the rectangle.
     * @param color  "_|_" or propositional variable as used in strings returned by
     *               {@link FormulaDominoSquare#toPrefixRepresentation()}. This string will be
     *               used to determine color(s) for the rectangle.
     * @see #paintRectangle(String, int, Graphics, int, int, int, int, boolean)
     */
    protected void paintRectangle(Graphics g, int x, int y, int width, int height, String color) {

        Color[] col;

        if ("_|_".equals(color)) {
            col = new Color[1];
            col[0] = Color.red;
        } else {
            boolean unbound = true;
            if (color.charAt(0) != 'X' || color.length() < 2)
                unbound = false;
            else {
                for (int i = 1; i < color.length(); ++i)
                    if (!Character.isDigit(color.charAt(i))) {
                        unbound = false;
                        break;
                    }
            }

            if (unbound) {
                int num = color.length() - 1; //-1 because of 1st character 'X'
                col = new Color[num];
                for (int i = 0; i < num; ++i)
                    col[i] = DominoTile.GRAY_COLORS[Character.digit(color.charAt(num - i), 10)];
            } else {
                int num = color.length();
                col = new Color[num];
                for (int i = 0; i < num; ++i)
                    col[i] = DominoTile.NON_GRAY_COLORS[color.charAt(i) - 'A'];
            }
        }

        int rectWidth = (width / col.length) - 4; //-4 to account for black and white frame
        int rectHeight = (height / col.length) - 4; //-4 to account for black and white frame
        if (rectWidth < 2) rectWidth = 2;
        if (rectHeight < 2) rectHeight = 2;

        for (Color value : col) {
            if (width <= 0 || height <= 0) break;
            g.setColor(Color.black);
            g.drawRect(x, y, width - 1, height - 1);
            x++;
            y++;
            width -= 2;
            height -= 2;
            if (width <= 0 || height <= 0) break;
            g.setColor(Color.white);
            g.drawRect(x, y, width - 1, height - 1);
            x++;
            y++;
            width -= 2;
            height -= 2;
            if (width <= 0 || height <= 0) break;

            g.setColor(value);
            g.fillRect(x, y, width, height);
            x += rectWidth / 2;
            y += rectHeight / 2;
            width -= rectWidth;
            height -= rectHeight;
        }
    }

    /**
     * <p>Paints a rectangle with a pattern based on a formula.</p>
     *
     * @param f             a prefix formula as returned by
     *                      {@link FormulaDominoSquare#toPrefixRepresentation()}.
     * @param i             index in {@code f} of the subformula to use for determining the
     *                      pattern for the rectangle.
     * @param g             the graphics context for drawing.
     * @param x             the x coordinate of the upper-left corner of the rectangle.
     * @param y             the y coordinate of the upper-left corner of the rectangle.
     * @param width         the width of the rectangle.
     * @param height        the height of the rectangle.
     * @param verticalSplit if the subformula to use for the pattern is an
     *                      implication, then 2 rectangles need to be drawn. If
     *                      {@code verticalSplit} is {@code true}, then the 2 rectangles
     *                      will be the top and bottom half of the rectangle
     *                      {@code (x,y,width,height)}. If {@code verticalSplit} is
     *                      {@code false} the split will be into left and right half.
     * @return the index of the character after the subformula that has been
     * processed.
     * @see #paintRectangle(Graphics, int, int, int, int, String)
     */
    protected int paintRectangle(String f, int i, Graphics g, int x, int y, int width, int height, boolean verticalSplit) {
        if (i < f.length()) {
            if (f.charAt(i) == '-')  //->
            {
                i += 2;
                if (verticalSplit) {
                    i = paintRectangle(f, i, g, x, y, width, height / 2, !verticalSplit);
                    i = paintRectangle(f, i, g, x, y + height / 2, width, height - (height / 2), !verticalSplit);
                } else //if horizontal split
                {
                    i = paintRectangle(f, i, g, x, y, width / 2, height, !verticalSplit);
                    i = paintRectangle(f, i, g, x + width / 2, y, width - (width / 2), height, !verticalSplit);
                }
            } else // must be "("
            {
                i++;
                int j = i;
                while (f.charAt(i) != ')') ++i;
                paintRectangle(g, x, y, width, height, f.substring(j, i));
                i++;
            }
        }

        return i;
    }

    // see IsoBlock
    public IsoObject parentObject() {
        return dominoTile;
    }

    //see IsoBlock
    public VectorDouble3D getAbsoluteShift() {
        return dominoTile.getShift();
    }

    //see IsoBlock
    public VectorDouble3D getRelativeShift() {
        return VectorDouble3D.ZERO;
    }

    //see IsoBlock
    public int getPaintOrderPosition() {
        return paintOrderPosition_;
    }

    //see IsoBlock
    public void setPaintOrderPosition(int position) {
        paintOrderPosition_ = position;
    }

    //see IsoBlock
    public VectorInt3D getAbsoluteLocation() {
        //shift does not have to be taken into account because DominoBlocks always
        //have a (0,0,0) relative shift and the parent object's shift is always
        //between 0.0 and 1.0
        return VectorInt3D.add(getRelativeLocation(), dominoTile.getReferenceBlockLocation());
    }

    //see IsoBlock
    public VectorInt3D getRelativeLocation() {
        switch (dominoTile.getOrientation()) {
            //NOTE: greater y coordinates are further SOUTH!
            case 1:
                return new VectorInt3D(-relativeLocation_.y, relativeLocation_.x, relativeLocation_.z);
            case 2:
                return new VectorInt3D(-relativeLocation_.x, -relativeLocation_.y, relativeLocation_.z);
            case 3:
                return new VectorInt3D(relativeLocation_.y, -relativeLocation_.x, relativeLocation_.z);
        }
        return relativeLocation_;
    }

    /**
     * <p>Changes the value returned by {@link #getRelativeLocation}.</p>
     * <p>{@code location} is copied, so later modifications will not affect
     * {@code this}.</p>
     */
    public void setRelativeLocation(VectorInt3D location) {
        relativeLocation_ = location;
    }

    //see IsoBlock
    public boolean isMassive() {
        return dominoTile.isMassive_();
    }

    //see IsoBlock
    public JIsoBlock graphicalRepresentation() { // TODO inline?
        return this;
    }

    //see JIsoBlock
    public JComponent toJComponent() { // TODO inline?
        return this;
    }

    //see JIsoBlock
    public void setIsoOffsets(int xoffset, int yoffset) {
        xoffset_ = xoffset;
        yoffset_ = yoffset;
    }

    public DominoTile getDominoTile() {
        return dominoTile;
    }

    /**
     * <p>location relative to the reference block.</p>
     */
    public VectorInt3D getRelativeLocation_() {
        return relativeLocation_;
    } // TODO never used?

    public void setRelativeLocation_(VectorInt3D relativeLocation_) {
        this.relativeLocation_ = relativeLocation_;
    }

    /**
     * <p>see {@link #getPaintOrderPosition()}.</p>
     */
    public int getPaintOrderPosition_() {
        return paintOrderPosition_;
    } // TODO never used?

    public void setPaintOrderPosition_(int paintOrderPosition_) {
        this.paintOrderPosition_ = paintOrderPosition_;
    } // TODO never used?

    /**
     * <p>see {@link #isActive()}.</p>
     */
    public boolean isActive_() {
        return active_;
    } // TODO never used?

    public void setActive_(boolean active_) {
        this.active_ = active_;
    }

    /**
     * <p>Caches the formula whose graphical representation labels this block.
     * This is done for efficiency and to allow changing the underlying
     * {@link FormulaSyntaxNet} from a different thread.</p>
     */
    public String getFormula_() {
        return formula_;
    }

    public void setFormula_(String formula_) {
        this.formula_ = formula_;
    }

    /**
     * <p>number of pixels, the top side of the block is shifted to the right
     * relative to the bottom side.</p>
     */
    public int getXoffset_() {
        return xoffset_;
    } // TODO never used?

    public void setXoffset_(int xoffset_) {
        this.xoffset_ = xoffset_;
    }

    /**
     * <p>number of pixels, the top side of the block is shifted down
     * relative to the bottom side.</p>
     */
    public int getYoffset_() {
        return yoffset_;
    } // TODO never used?

    public void setYoffset_(int yoffset_) {
        this.yoffset_ = yoffset_;
    }
}
