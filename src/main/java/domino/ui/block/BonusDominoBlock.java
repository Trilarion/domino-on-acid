/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.block;

import domino.logic.square.FormulaDominoSquare;
import domino.logic.tree.DominoTree;
import domino.logic.tree.FormulaDominoTree;
import domino.model.tile.DominoTile;
import domino.utils.VectorInt3D;

import java.awt.*;

/**
 * <p>Class for {@link DominoBlock}s labelled with a bonus formula.</p>
 */
public class BonusDominoBlock extends DominoBlock {
    /**
     * <p>The tree that provides the bonus.</p>
     */
    private final FormulaDominoTree tree_;
    /**
     * <p>The subtree connection index for which the bonus is valid.</p>
     */
    private final int subtreeIndex_;
    /**
     * <p>The index of the bonus in the list of bonuses.</p>
     */
    private final int newBonusForSubtreeIndex_;
    /**
     * <p>If this block has been attached to a tree, this is the
     * {@link FormulaDominoTree} that represents the attached bonus square, i.e. the
     * tree returned by {@link FormulaDominoTree#useBonusSquare(int, int)}.</p>
     */
    private FormulaDominoTree bonusTree_;

    /**
     * <p>Creates a new {@code BonusDominoBlock}.</p>
     *
     * @param relativeLocation        value to be returned by {@link #getRelativeLocation()}.
     * @param active                  {@code false} if this block is just a dummy for displaying.
     * @param tree                    the subtree of {@link DominoTile}'s tree that provides the bonus.
     * @param subtreeIndex            the subtree connection index for which the bonus is valid.
     * @param newBonusForSubtreeIndex the index of the bonus in the list of bonuses.
     */
    public BonusDominoBlock(DominoTile dominoTile, VectorInt3D relativeLocation, boolean active, FormulaDominoTree tree, int subtreeIndex, int newBonusForSubtreeIndex) {
        super(dominoTile);
        setRelativeLocation_(relativeLocation);
        setActive_(active);
        tree_ = tree;
        subtreeIndex_ = subtreeIndex;
        newBonusForSubtreeIndex_ = newBonusForSubtreeIndex;
        FormulaDominoSquare sq = (FormulaDominoSquare) tree.newBonusSquareForSubtreeConnection(subtreeIndex, newBonusForSubtreeIndex);
        setFormula_(sq.toPrefixRepresentation(DominoTile.globalUnboundIdAssigner));
    }

    /**
     * <p>If this block has been {@link #attachBonusTree(DominoTree, int) attached to a tree},
     * this function returns the
     * {@link FormulaDominoTree} that represents the attached bonus square (the
     * tree returned by {@link FormulaDominoTree#useBonusSquare(int, int)}), otherwise
     * returns {@code null}.</p>
     */
    public FormulaDominoTree getBonusTree() {
        return bonusTree_;
    }

    /**
     * <p>See {@link FormulaDominoTree#useBonusSquare(int, DominoTree, int, int)}.</p>
     */
    public void attachBonusTree(DominoTree toTree, int subtreeConnectionIndex) {
        bonusTree_ = (FormulaDominoTree) toTree.useBonusSquare(subtreeConnectionIndex, formulaDominoTree(), subtreeConnectionIndex(), newBonusForSubtreeConnectionIndex());
    }

    /**
     * <p>See {@link FormulaDominoTree#detachFromParent()}.</p>
     */
    public boolean detachBonusTree() {
        if (bonusTree_ == null) return false;
        boolean ret = bonusTree_.detachFromParent();
        bonusTree_ = null;
        return ret;
    }

    /**
     * <p>Returns the {@link FormulaDominoTree} that owns the bonus this tile
     * represents.</p>
     */
    public FormulaDominoTree formulaDominoTree() {
        return tree_;
    }

    /**
     * <p>Returns the index of the subtree connection in
     * {@link #formulaDominoTree()} for which the bonus is valid.</p>
     */
    public int subtreeConnectionIndex() {
        return subtreeIndex_;
    }

    /**
     * <p>Returns the number of the bonus in the list of bonuses that
     * {@link #formulaDominoTree()} <em>adds to</em> the the bonuses valid for
     * {@link #subtreeConnectionIndex()}.</p>
     */
    public int newBonusForSubtreeConnectionIndex() {
        return newBonusForSubtreeIndex_;
    }

    /**
     * <p>Returns the color used to paint the side of the block parallel to the x axis.</p>
     */
    public Color xColor() {
        return DominoTile.BONUS_BLUE;
    }

    /**
     * <p>Returns the color used to paint the side of the block parallel to the y axis.</p>
     */
    public Color yColor() {
        return DominoTile.BONUS_BLUE_DARK;
    }

    //see DominoBlock
    public void updateFormula() {
        FormulaDominoSquare sq = (FormulaDominoSquare) tree_.newBonusSquareForSubtreeConnection(subtreeIndex_, newBonusForSubtreeIndex_);
        String newFormula = sq.toPrefixRepresentation(DominoTile.globalUnboundIdAssigner);
        if (!newFormula.equals(getFormula_())) {
            setFormula_(newFormula);
            repaint();
        }
    }

}
