/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.block;

import domino.logic.square.FormulaDominoSquare;
import domino.logic.tree.FormulaDominoTree;
import domino.model.tile.DominoTile;
import domino.utils.VectorInt3D;

import java.awt.*;

/**
 * <p>Class for {@link DominoBlock}s labelled with a subtree connection formula.</p>
 */
public class SubtreeConnectionDominoBlock extends DominoBlock {

    /**
     * <p>The tree that owns the subtree connection.</p>
     */
    private final FormulaDominoTree tree_;
    /**
     * <p>The index of the subtree connection.</p>
     */
    private final int subtreeConnectionIndex_;

    /**
     * <p>Creates a new {@code SubtreeConnectionDominoBlock}.</p>
     *
     * @param relativeLocation       value to be returned by {@link #getRelativeLocation()}.
     * @param active                 {@code false} if this block is just a dummy for displaying.
     * @param tree                   the subtree of {@link DominoTile}'s tree that owns the subtree connection.
     * @param subtreeConnectionIndex the index of the subtree connection.
     */
    public SubtreeConnectionDominoBlock(DominoTile dominoTile, VectorInt3D relativeLocation, boolean active, FormulaDominoTree tree, int subtreeConnectionIndex) {
        super(dominoTile);
        setRelativeLocation_(relativeLocation);
        setActive_(active);
        tree_ = tree;
        subtreeConnectionIndex_ = subtreeConnectionIndex;
        FormulaDominoSquare sq = (FormulaDominoSquare) tree.subtreeConnectionSquare(subtreeConnectionIndex_);
        setFormula_(sq.toPrefixRepresentation(DominoTile.globalUnboundIdAssigner));
    }

    /**
     * <p>Returns the {@link FormulaDominoTree} that owns the subtree connection
     * this tile represents.</p>
     */
    public FormulaDominoTree formulaDominoTree() {
        return tree_;
    }

    /**
     * <p>Returns the index of this subtree connection in
     * {@link #formulaDominoTree()}.</p>
     */
    public int subtreeConnectionIndex() {
        return subtreeConnectionIndex_;
    }

    /**
     * <p>Returns the color used to paint the side of the block parallel to the x axis.</p>
     */
    public Color xColor() {
        return new Color(50, 200, 50);
    }

    /**
     * <p>Returns the color used to paint the side of the block parallel to the y axis.</p>
     */
    public Color yColor() {
        return new Color(40, 150, 40);
    }

    //see DominoBlock
    public void updateFormula() {
        FormulaDominoSquare sq = (FormulaDominoSquare) tree_.subtreeConnectionSquare(subtreeConnectionIndex_);
        String newFormula = sq.toPrefixRepresentation(DominoTile.globalUnboundIdAssigner);
        if (!newFormula.equals(getFormula_())) {
            setFormula_(newFormula);
            repaint();
        }
    }

}
