/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.ui.block;


import domino.model.tile.DominoTile;
import domino.utils.VectorInt3D;

import java.awt.*;

/**
 * <p>An unlabelled {@link DominoBlock}.</p>
 */
public class EmptyDominoBlock extends DominoBlock {

    /**
     * <p>Creates a new {@code EmptyDominoBlock}.</p>
     *
     * @param relativeLocation value to be returned by {@link #getRelativeLocation()}.
     */
    public EmptyDominoBlock(DominoTile dominoTile, VectorInt3D relativeLocation) {
        super(dominoTile);
        setRelativeLocation_(relativeLocation);
        setActive_(false);
        setFormula_(null);
    }

    //see DominoBlock, does nothing
    public void updateFormula() {
    }

    /**
     * <p>See DominoBlock#paintLabel(Graphics, int, int, int, int).
     */
    protected void paintLabel(Graphics g, int x, int y, int width, int height) {
        g.setColor(DominoTile.TOP_COLOR);
        g.fillRect(x, y, width, height);
        g.setColor(DominoTile.OUTLINE_COLOR);
        width--;
        height--;
        if (width >= 0 && height >= 0) g.drawRect(x, y, width, height);
    }

}
