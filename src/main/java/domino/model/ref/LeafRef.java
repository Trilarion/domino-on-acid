/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.model.ref;

import domino.logic.tree.FormulaDominoTree;

import java.util.Collection;

/**
 * <p>A reference to a leaf (more precisely: a subtree connection) of a
 * {@link FormulaDominoTree}.</p>
 */
public class LeafRef {
    private FormulaDominoTree owner;
    private int index;
    private Collection<BonusRef> bonus;

    /**
     * <p>The tree that owns the subtree connection.</p>
     */
    public FormulaDominoTree getOwner() {
        return owner;
    }

    public void setOwner(FormulaDominoTree owner) {
        this.owner = owner;
    }

    /**
     * <p>The index of the subtree connection.</p>
     */
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * <p>A {@link Collection} of {@link BonusRef}s with references to all bonus
     * that may be attached to this leaf.</p>
     */
    public Collection<BonusRef> getBonus() {
        return bonus;
    }

    public void setBonus(Collection<BonusRef> bonus) {
        this.bonus = bonus;
    }
}
