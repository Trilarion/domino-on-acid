/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.model.ref;

import domino.logic.tree.FormulaDominoTree;

/**
 * <p>Reference to a bonus square in a {@link FormulaDominoTree}.</p>
 */
public class BonusRef {
    private final FormulaDominoTree owner;
    private final int subtreeIndex;
    private final int newBonusForSubtreeIndex;
    private String formula;

    public BonusRef(FormulaDominoTree tree, int s, int b) {
        owner = tree;
        subtreeIndex = s;
        newBonusForSubtreeIndex = b;
    }

    /**
     * <p>The tree that owns the bonus square.</p>
     */
    public FormulaDominoTree getOwner() {
        return owner;
    }

    /**
     * <p>The index of the subtree connection for which the bonus square is valid.</p>
     */
    public int getSubtreeIndex() {
        return subtreeIndex;
    }

    /**
     * <p>The index of the bonus square in the list of bonus squares valid for
     * the respective subtree connection.</p>
     */
    public int getNewBonusForSubtreeIndex() {
        return newBonusForSubtreeIndex;
    }

    /**
     * <p>The formula of the bonus square.</p>
     */
    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }
}
