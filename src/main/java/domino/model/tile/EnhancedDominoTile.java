/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.model.tile;

import domino.logic.tree.FormulaDominoTree;
import domino.ui.block.DominoBlock;
import domino.ui.iso.IsoBlock;
import domino.utils.Pair;
import domino.utils.VectorInt3D;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Iterator;

public abstract class EnhancedDominoTile extends DominoTile {
    protected EnhancedDominoTile(FormulaDominoTree tree) {
        super(tree);
    }

    protected EnhancedDominoTile(FormulaDominoTree tree, int subtreeIndex, int newBonusForSubtreeIndex) {
        super(tree, subtreeIndex, newBonusForSubtreeIndex);
    }

    public abstract boolean unifyWithNearestTile(VectorInt3D location);

    public abstract void deunifyIfNecessary();

    public abstract void attachToUnified();

    /**
     * <p>Returns an {@link IsoBlock} that would be horizontally or vertically adjacent
     * to a block of {@code this} tile if {@code this} tile was put at
     * {@code location}.</p>
     * <p>The {@link IsoBlock} and the adjacent block from {@code this} tile is returned as
     * {@code Pair<IsoBlock, IsoBlock>}.</p>
     * <p>Returns {@code null} if there is no adjacent block.</p>
     * <p>If {@code onlyone} is {@code true}, {@code null} is
     * returned if there is more than one adjacent block.</p>
     */
    public @Nullable Pair<IsoBlock, IsoBlock> nearestIsoBlock(VectorInt3D location, boolean onlyone) {
        VectorInt3D[] offset = {new VectorInt3D(-1, 0, 0), new VectorInt3D(1, -1, 0),
                new VectorInt3D(1, 1, 0), new VectorInt3D(-1, 1, 0)};
        Pair<IsoBlock, IsoBlock> found = null;
        Iterator<DominoBlock> iterator = blocksIterator();
        while (iterator.hasNext()) {
            IsoBlock block2 = iterator.next();
            VectorInt3D blockLoc = block2.getRelativeLocation();
            blockLoc = VectorInt3D.add(blockLoc, location);
            for (int i = 0; i < 4; ++i) {
                blockLoc = VectorInt3D.add(blockLoc, offset[i]);
                Collection<IsoBlock> blocks = getWorld().blocksAtLocation(blockLoc);
                if (blocks != null) {
                    for (IsoBlock block : blocks) {
                        if (block.parentObject() != this) {
                            if (found != null) return null;
                            found = new Pair<>(block, block2);
                            if (!onlyone) return found;
                        }
                    }
                }
            }
        }
        return found;
    }

    /**
     * <p>Returns {@code true} if there exists no {@link IsoBlock} that would be
     * horizontally or vertically adjacent
     * to a block of {@code this} tile if {@code this} tile was put at
     * {@code location}.</p>
     */
    public boolean hasNoAdjacentBlocks(VectorInt3D location) {
        return nearestIsoBlock(location, false) == null;
    }

    public void dieWithChildren() {
        DominoTile con = connectedToTile();
        detach();
        if (rootTree() != null) rootTree().detachFromParent();
        recursiveDeath();
        if (con != null) con.updateFormulas();
    }

    protected void recursiveDeath() {
        for (int i = 0; i < numberOfTileConnections(); ++i) {
            EnhancedDominoTile subTile = (EnhancedDominoTile) tileConnection(i);
            if (subTile != null) subTile.recursiveDeath();
        }
        getWorld().removeObject(this);
        removeIdAssignments();
    }

}
