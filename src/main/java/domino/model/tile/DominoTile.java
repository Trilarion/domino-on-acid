/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.model.tile;


import domino.logic.UnboundIdAssigner;
import domino.logic.UnboundIdAssignerImpl;
import domino.logic.square.FormulaDominoSquare;
import domino.logic.tree.FormulaDominoTree;
import domino.ui.block.*;
import domino.ui.iso.IsoBlock;
import domino.ui.iso.IsoObject;
import domino.ui.iso.IsoWorld;
import domino.model.ref.BonusRef;
import domino.model.ref.LeafRef;
import domino.utils.VectorDouble3D;
import domino.utils.VectorInt3D;

import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * <p>Representation of a {@link FormulaDominoTree} in an {@link IsoWorld}.</p>
 */
public class DominoTile implements IsoObject {
    /**
     * <p>The color used to paint the top side of empty (unlabelled)
     * {@code DominoBlock}s.</p>
     */
    public static final Color TOP_COLOR = Color.lightGray;
    /**
     * <p>The color used for the lines drawn around the block's faces.</p>
     */
    public static final Color OUTLINE_COLOR = Color.black;
    /**
     * <p>The color used for the lighter side of a bonus block or an inactive root
     * block.</p>
     */
    public static final Color BONUS_BLUE = new Color(60, 100, 230);
    /**
     * <p>The color used for the darker side of a bonus block or an inactive root
     * block.</p>
     */
    public static final Color BONUS_BLUE_DARK = new Color(50, 90, 190);
    /**
     * <p>The colors used to represent unbound propositional variables.</p>
     */
    public static final Color[] GRAY_COLORS = {new Color(128, 128, 128), new Color(96, 96, 96), new Color(198, 198, 198), new Color(152, 152, 152), new Color(32, 32, 32), new Color(250, 250, 250), new Color(226, 226, 226), new Color(172, 172, 172), new Color(212, 212, 212), new Color(238, 238, 238)};
    /**
     * <p>The colors used to represent bound propositional variables.</p>
     */
    public static final Color[] NON_GRAY_COLORS =
            {new Color(0, 192, 255), new Color(255, 64, 240), new Color(255, 255, 0),
                    new Color(128, 255, 224), new Color(144, 0, 224), new Color(192, 0, 96),
                    new Color(255, 160, 80), new Color(0, 160, 96), new Color(160, 192, 0),
                    new Color(192, 0, 128), new Color(160, 255, 0), new Color(192, 128, 255),
                    new Color(0, 96, 160), new Color(0, 224, 224), new Color(48, 0, 192),
                    new Color(160, 48, 0), new Color(192, 144, 0), new Color(0, 128, 128),
                    new Color(255, 192, 255), new Color(224, 255, 144), new Color(224, 255, 192),
                    new Color(255, 224, 224), new Color(208, 255, 255), new Color(255, 255, 192),
                    new Color(224, 192, 112), new Color(255, 234, 255)};
    /**
     * <p>Used for consistent labelling of all {@code DominoTile}s.
     * After a {@code DominoTile} is removed from the world,
     * {@link #removeIdAssignments()} needs to be called to free the labels used
     * for that tile.</p>
     */
    public static final UnboundIdAssignerImpl globalUnboundIdAssigner = UnboundIdAssigner.newAssigner();
    private final boolean massive_ = false;
    /**
     * <p>Contains all {@link IsoBlock}s this tile consists of.</p>
     * <p>The first entry is always the root block (which need not always be
     * a {@link RootDominoBlock}).</p>
     */
    private final List<DominoBlock> blocks_;
    /**
     * <p>For each block of {@code this} tile where another {@code DominoTile}
     * can be connected, there is one entry in this array, pointing to the connected
     * tile.</p>
     */
    private final DominoTile[] leafConnections_;
    /**
     * <p>The {@link FormulaDominoTree}, {@code this DominoTile} represents.</p>
     */
    private final FormulaDominoTree tree_;
    /**
     * <p>If {@code this DominoTile} is connected to another, then this
     * variable is a reference to the connected tile.</p>
     */
    private DominoTile rootConnection_;
    /**
     * <p>{@code rootConnection_.leafConnections_[rootConnectionLeafIndex_]==this}</p>
     */
    private int rootConnectionLeafIndex_ = -1;
    /**
     * <p>Location of the reference block within the {@link IsoWorld}.</p>
     */
    private VectorInt3D refBlockLocation_;
    /**
     * <p>Shift value to be added to the individual shifts of all blocks of this tile.</p>
     */
    private VectorDouble3D shift_ = VectorDouble3D.ZERO;
    /**
     * <p>The {@link IsoWorld} this tile is in.</p>
     */
    private IsoWorld world_;
    /**
     * <p>0: root block points north, 1: root block points east,
     * 2: root block points south, 3 root block points west.</p>
     * <p>{@code orientation_} does not affect shift!</p>
     */
    private int orientation_;

    /**
     * <p>Creates a {@code DominoTile} to represent the bonus square with number
     * {@code newBonusForSubtreeIndex} that {@code tree} adds to the bonus squares that may
     * be used in the subtree of {@code tree} with index {@code subtreeIndex}.</p>
     * <p>{@code tree} is not copied, it is linked to the {@code DominoTile}.
     * However, to make the {@code DominoTile} reflect changes made to the tree,
     * {@link #updateFormulas()} has to be called.</p>
     */
    public DominoTile(FormulaDominoTree tree, int subtreeIndex, int newBonusForSubtreeIndex) {
        tree_ = tree;
        leafConnections_ = new DominoTile[0];
        blocks_ = new ArrayList<>(1);
        VectorInt3D location = VectorInt3D.ZERO;
        blocks_.add(new BonusDominoBlock(this, location, true, tree, subtreeIndex, newBonusForSubtreeIndex));
    }

    /**
     * <p>Creates a {@code DominoTile} to represent {@code tree}.</p>
     * <p>{@code tree} is not copied, it is linked to the {@code DominoTile}.
     * However, to make the {@code DominoTile} reflect changes made to the tree,
     * {@link #updateFormulas()} has to be called.</p>
     */
    public DominoTile(FormulaDominoTree tree) {
        tree_ = tree;

        Collection<LeafRef> leafRefs = collectLeafRefs(tree, new LinkedList<>(), UnboundIdAssigner.newAssigner());
        leafConnections_ = new DominoTile[leafRefs.size()];

        //move all BonusRefs that occur in all LeafRefs to commonBonus
        Collection<BonusRef> commonBonus = new LinkedList<>();  //Collection of BonusRefs
        extractCommonBonus(leafRefs, commonBonus);

    /*
      now we have to construct a DominoTile from the following:

      1) 1x RootDominoBlock  (tree root, active)
      2) commonBonus.size() x BonusDominoBlock  (inactive)
      3) commonBonus.size() x BonusDominoBlock  (active, on top of blocks from 2)
      4) leafRefs.size() x a series of BonusDominoBlocks (inactive)
      5) leafRefs.size() x a series of BonusDominoBlocks (active, on top of 4)
      6) leafRefs.size() x SubtreeConnectionDominoBlock (active)
      7) n x EmptyDominoBlock (inactive, filler)

      NOTE: It is possible that leafRefs.isEmpty(), i.e. there are no open leaves.
    */

        int numLeafBonusBlocks = 0; //number of bonus blocks that are not common to all leaves
        Iterator<LeafRef> iterator = leafRefs.iterator();
        while (iterator.hasNext()) numLeafBonusBlocks += iterator.next().getBonus().size();

        int numLeaves = leafRefs.size();
        int fillerNum = 0;

        if (numLeaves >= 4) fillerNum = 1 + (numLeaves - 3) * 2;
        if (commonBonus.isEmpty() && numLeaves > 1 && fillerNum == 0) fillerNum = 1;

        //if there is at least one non-common bonus block, at least 1 filler is needed so
        //that the non-common bonus block can not be mistaken for a common bonus block
        if (numLeafBonusBlocks != 0 && fillerNum == 0) fillerNum = 1;

        int numBlocks = 1 + 2 * commonBonus.size() + 2 * numLeafBonusBlocks + leafRefs.size() + fillerNum;

        blocks_ = new ArrayList<>(numBlocks);

        VectorInt3D location = VectorInt3D.ZERO;
        blocks_.add(new RootDominoBlock(this, location, true, tree));
        location = new VectorInt3D(location.x, location.y + 1, location.z);

        for (BonusRef bonusRef : commonBonus) {
            //add inactive bonus block
            blocks_.add(new BonusDominoBlock(this, location, false, bonusRef.getOwner(), bonusRef.getSubtreeIndex(), bonusRef.getNewBonusForSubtreeIndex()));
            //add active bonus block on top
            location = VectorInt3D.newZ(location, location.z + 1);
            blocks_.add(new BonusDominoBlock(this, location, true, bonusRef.getOwner(), bonusRef.getSubtreeIndex(), bonusRef.getNewBonusForSubtreeIndex()));
            location = new VectorInt3D(location.x, location.y + 1, location.z - 1);
        }

        //leftx is coordinate left of left-most filler
        int leftx = location.x - (fillerNum / 2) - 1;
        //rightx is coordinate right of right-most filler
        int rightx = leftx + (fillerNum == 0 ? 1 : fillerNum) + 1;

        if (fillerNum > 0) {
            location = VectorInt3D.newX(location, leftx);
            for (int i = 0; i < fillerNum; ++i) {
                location = VectorInt3D.newX(location, location.x + 1);
                blocks_.add(new EmptyDominoBlock(this, location));
            }
            location = VectorInt3D.newY(location, location.y + 1);
        }

        if (numLeaves >= 2) {
            location = new VectorInt3D(leftx, location.y - 1, location.z);
            Iterator<LeafRef> iter3 = leafRefs.iterator();
            int sign = -1;
            for (int i = 0; i < 2; ++i) {
                LeafRef lRef = iter3.next();
                iter3.remove();
                for (BonusRef bonus : lRef.getBonus()) {
                    //add inactive bonus block
                    blocks_.add(new BonusDominoBlock(this, location, false, bonus.getOwner(), bonus.getSubtreeIndex(), bonus.getNewBonusForSubtreeIndex()));
                    //add active bonus block on top
                    location = new VectorInt3D(location.x, location.y, location.z + 1);
                    blocks_.add(new BonusDominoBlock(this, location, true, bonus.getOwner(), bonus.getSubtreeIndex(), bonus.getNewBonusForSubtreeIndex()));
                    location = new VectorInt3D(location.x + sign, location.y, location.z - 1);
                }
                blocks_.add(new SubtreeConnectionDominoBlock(this, location, true, lRef.getOwner(), lRef.getIndex()));
                location = new VectorInt3D(rightx, location.y, location.z);
                sign = 1;
            }
            numLeaves -= 2;
            location = new VectorInt3D(location.x, location.y + 1, location.z);
        }

        if (numLeaves > 0) {
            location = VectorInt3D.newX(location, leftx + 1);
            int ystart = location.y;
            for (LeafRef lRef : leafRefs) {
                for (BonusRef bonus : lRef.getBonus()) {
                    //add inactive bonus block
                    blocks_.add(new BonusDominoBlock(this, location, false, bonus.getOwner(), bonus.getSubtreeIndex(), bonus.getNewBonusForSubtreeIndex()));
                    //add active bonus block on top
                    location = VectorInt3D.newZ(location, location.z + 1);
                    blocks_.add(new BonusDominoBlock(this, location, true, bonus.getOwner(), bonus.getSubtreeIndex(), bonus.getNewBonusForSubtreeIndex()));
                    location = new VectorInt3D(location.x, location.y + 1, location.z - 1);
                }
                blocks_.add(new SubtreeConnectionDominoBlock(this, location, true, lRef.getOwner(), lRef.getIndex()));
                location = new VectorInt3D(location.x + 2, ystart, location.z);
            }
        }

    /*
      Currently the RootDominoBlock is the reference block. Now we compute the
      center of the DominoTile's mass (ignoring blocks with z!=0)
      and make the block closest to this center
      the new reference block. Note that the center may not contain an actual
      *DominoBlock.

      NOTE: It is possible that leafRefs.isEmpty(), i.e. there are no open leaves.
    */

        location = VectorInt3D.ZERO;

        Iterator<DominoBlock> iter5 = blocksIterator();
        while (iter5.hasNext()) {
            VectorInt3D adder = ((IsoBlock) iter5.next()).getRelativeLocation();
            if (adder.z == 0) location = VectorInt3D.add(location, adder);
        }
        location = new VectorInt3D((int) -Math.round((double) location.x / numberOfBlocks()),
                (int) -Math.round((double) location.y / numberOfBlocks()),
                (int) -Math.round((double) location.z / numberOfBlocks()));

        Iterator<DominoBlock> iter6 = blocksIterator();
        while (iterator.hasNext()) {
            DominoBlock block = iter6.next();
            VectorInt3D relLocation = block.getRelativeLocation();
            relLocation = VectorInt3D.add(relLocation, location);
            block.setRelativeLocation(relLocation);
        }
    }

    /**
     * <p>Removes all id assignments for all {@code DominoTile}s.</p>
     * <p>Note that the tiles will not be redrawn.</p>
     */
    public static void removeIdAssignmentsForAllTiles() {
        globalUnboundIdAssigner.reset();
    }

    /**
     * <p>Returns a {@link Collection} that contains a {@link LeafRef} for every
     * open leaf in {@code tree}.</p>
     *
     * @param tree           the tree whose leaves to collect.
     * @param inheritedBonus pass a new {@link LinkedList}.
     * @param tempAssigner   pass a new {@link UnboundIdAssigner}.
     */
    protected static Collection<LeafRef> collectLeafRefs(FormulaDominoTree tree, Collection<BonusRef> inheritedBonus, UnboundIdAssignerImpl tempAssigner) {
        Collection<LeafRef> leafRefs = new LinkedList<>();

        for (int i = 0; i < tree.numberOfSubtreeConnections(); ++i) {
            Collection<BonusRef> accumulatedBonus = new LinkedList<>(inheritedBonus);

            addNewBonusRefs:
            for (int j = 0; j < tree.numberOfNewBonusSquaresForSubtreeConnection(i); ++j) {
                FormulaDominoSquare b = (FormulaDominoSquare) tree.newBonusSquareForSubtreeConnection(i, j);
                String bonusFormula = b.toPrefixRepresentation(tempAssigner);
                for (BonusRef bonus : accumulatedBonus) {
                    if (bonus.getFormula().equals(bonusFormula))
                        continue addNewBonusRefs;
                }

                BonusRef bonusRef = new BonusRef(tree, i, j);
                bonusRef.setFormula(bonusFormula);
                accumulatedBonus.add(bonusRef);
            }

            FormulaDominoTree sub = (FormulaDominoTree) tree.subtree(i);
            if (sub == null) {
                LeafRef leafRef = new LeafRef();
                leafRef.setOwner(tree);
                leafRef.setIndex(i);
                leafRef.setBonus(new ArrayList<>(accumulatedBonus));
                leafRefs.add(leafRef);
            } else {
                leafRefs.addAll(collectLeafRefs(sub, accumulatedBonus, tempAssigner));
            }
        }

        return leafRefs;
    }

    protected static void updateFormulasDown(DominoTile tile) {
        Iterator<DominoBlock> iterator = tile.blocksIterator();
        while (iterator.hasNext()) {
            DominoBlock block = iterator.next();
            block.updateFormula();
        }

        for (int i = 0; i < tile.leafConnections_.length; ++i)
            if (tile.leafConnections_[i] != null) updateFormulasDown(tile.leafConnections_[i]);
    }

    /**
     * <p>After a {@code DominoTile} is removed from the world, this method
     * has to be called to free the label colors used for that tile.</p>
     * <p>See {@link UnboundIdAssigner#removeMappingsStrictlyFor(FormulaDominoTree)}
     * for more information.</p>
     */
    public void removeIdAssignments() {
        if (rootTree() != null)
            globalUnboundIdAssigner.removeMappingsStrictlyFor(rootTree());
    }

    /**
     * <p>Frees the label colors that are not used because they are superseded by
     * another color due to unification.</p>
     */
    public void removeUnneededIdAssignments() {
        globalUnboundIdAssigner.removeUnneededMappingsFor(tree_);
    }

    /**
     * <p>Attaches {@code this} tile to {@code t}, connecting with
     * leaf block {@code leafIndex}.</p>
     * <p>This method does NOT perform any unifications/connections of the
     * underlying {@link FormulaDominoTree}s.</p>
     *
     * @throws IllegalArgumentException if {@code this} is already attached to
     *                                  a {@code DominoTile} or if {@code leafIndex} is not a valid
     *                                  index.
     * @see #detach()
     * @see #attachTo(SubtreeConnectionDominoBlock)
     */
    public void attachTo(DominoTile t, int leafIndex) {
        if (leafIndex < 0 || leafIndex >= t.leafConnections_.length ||
                t.leafConnections_[leafIndex] != null) throw new IllegalArgumentException();
        t.leafConnections_[leafIndex] = this;
        rootConnection_ = t;
        rootConnectionLeafIndex_ = leafIndex;
    }

    /**
     * <p>Attaches {@code this} tile to {@code block}.</p>
     * <p>This method does NOT perform any unifications/connections of the
     * underlying {@link FormulaDominoTree}s.</p>
     *
     * @throws IllegalArgumentException if {@code this} is already attached to
     *                                  a {@code DominoTile} or if {@code leafIndex} is not a valid
     *                                  index.
     * @see #detach()
     */
    public void attachTo(SubtreeConnectionDominoBlock block) {
        DominoTile tile = (DominoTile) block.parentObject();
        Iterator<DominoBlock> iterator = tile.blocksIterator();
        int i = 0;
        while (true) {
            IsoBlock block1 = iterator.next();
            if (block1 instanceof SubtreeConnectionDominoBlock) {
                if (block1 == block) break;
                else i++;
            }
        }
        attachTo(tile, i);
    }

    /**
     * <p>Detaches {@code this} tile from the {@code DominoTile} it is
     * attached to (if there is one).</p>
     * <p>This method does NOT perform any deunificating/detaching of the
     * underlying {@link FormulaDominoTree}s.</p>
     *
     * @see #attachTo(DominoTile, int)
     */
    public void detach() {
        if (rootConnection_ != null) {
            rootConnection_.leafConnections_[rootConnectionLeafIndex_] = null;
            rootConnection_ = null;
        }
    }

    /**
     * <p>0: root block points north, 1: root block points east,
     * 2: root block points south, 3 root block points west.</p>
     * <p>The orientation does not affect shift!</p>
     *
     * @see #setOrientation(int)
     */
    public int getOrientation() {
        return orientation_;
    }

    /**
     * <p>0: root block points north, 1: root block points east,
     * 2: root block points south, 3 root block points west.</p>
     * <p>The orientation does not affect shift!</p>
     * <p>If {@code this DominoTile} is already part of an {@link IsoWorld},
     * the new orientation will only be set if it does not cause a collision.</p>
     * <p>If the orientation is changed, the object is removed from and added again to
     * the {@link IsoWorld} it is part of (if any). This means that the
     * {@code DominoTile}'s layer will be reset and
     * {@link IsoWorld#putInTopLayer(IsoObject)} may need to be called again.</p>
     *
     * @param newOrientation is ANDed with 3 to get the new orientation.
     * @see #getOrientation()
     */
    public void setOrientation(int newOrientation) {
        newOrientation &= 3;
        if (newOrientation == orientation_) return;
        int oldOrientation = orientation_;
        orientation_ = newOrientation;
        if (world_ != null) {
            VectorInt3D loc = refBlockLocation_;
            VectorDouble3D sh = shift_;

            //The following is a bit unclean: We test whether the new orientation is
            //permissible by calling IsoWorld.collision() for this with the new orientation
            //already set. In theory this could break horribly because the docs to
            //IsoWorld.addObject() say that after being added to the IsoWorld, the
            //object must not change its shape. However, being the implementor I know that
            //collision() does not break and its unlikely it will ever be changed in a way
            //that the following would stop working.
            if (world_.collision(this, loc, sh)) {
                orientation_ = oldOrientation;
            } else {
                //must restore orientation before calling removeObject() because removeObject()
                //WILL break if the object being removed has changed shape.
                orientation_ = oldOrientation;
                //need to save reference to world because it will be nulled
                IsoWorld oldWorld = world_;
                world_.removeObject(this);
                orientation_ = newOrientation;
                oldWorld.addObjectNoCollision(this, loc, sh);
            }
        }
    }

    /**
     * <p>Puts the root block of the tile into inactive state so that no other tiles
     * can be connected to it.</p>
     */
    public void deactivateRootBlock() {
        blocks_.get(0).deactivate();
    }

    /**
     * <p>Returns the {@link FormulaDominoTree} this tile represents.</p>
     * <p>Note that the return value will be {@code null} if the tile represents a
     * bonus square that is not currently attached to a tree.</p>
     */
    public FormulaDominoTree rootTree() {
        DominoBlock rootBlock = rootBlock();
        if (rootBlock instanceof BonusDominoBlock) {
            return ((BonusDominoBlock) rootBlock).getBonusTree();
        } else return tree_;
    }

    /**
     * <p>Returns the {@link DominoBlock} that can be connected to
     * a leaf of another {@code DominoBlock}.</p>
     */
    public DominoBlock rootBlock() {
        return blocks_.get(0);
    }

    /**
     * <p>Returns a string representation of this tile.</p>
     */
    public String toStringRepresentation() {
        if (rootBlock() instanceof BonusDominoBlock) {
            BonusDominoBlock block = (BonusDominoBlock) rootBlock();
            return ((FormulaDominoSquare) block.formulaDominoTree().newBonusSquareForSubtreeConnection(block.subtreeConnectionIndex(), block.newBonusForSubtreeConnectionIndex())).toInfixRepresentation();
        } else {
            StringBuffer rep = new StringBuffer();
            recursiveTreeToString(rootTree(), rep, UnboundIdAssigner.newAssigner());
            return rep.toString();
        }
    }

    /**
     * <p>Appends a string representation of {@code tree} to {@code rep}.</p>
     */
    protected void recursiveTreeToString(FormulaDominoTree tree, StringBuffer rep, UnboundIdAssignerImpl assigner) {
        if (tree == null)
            rep.append(";  ");
        else {
            rep.append(((FormulaDominoSquare) tree.rootSquare()).toInfixRepresentation(assigner));
      /*
        NOTE: The following code assumes that all subtree connections of a
        FormulaDominoTree have the same bonus squares. This is true for the
        current implementation but is not specified in the interface. While it
        would be possible to write this function in a general way, this would
        break UITest.java:TileProducerManager.addProducerForStringRepresentation()
        because it does not support per-branch bonuses.
      */
            if (tree.numberOfSubtreeConnections() > 0) {
                for (int i = 0; i < tree.numberOfNewBonusSquaresForSubtreeConnection(0); ++i) {
                    rep.append(" [");
                    rep.append(((FormulaDominoSquare) tree.newBonusSquareForSubtreeConnection(0, i)).toInfixRepresentation(assigner));
                    rep.append(']');
                }

                for (int i = 0; i < tree.numberOfSubtreeConnections(); ++i) {
                    rep.append(", ");
                    rep.append(((FormulaDominoSquare) tree.subtreeConnectionSquare(i)).toInfixRepresentation(assigner));
                }
            }

            rep.append(";  ");

            for (int i = 0; i < tree.numberOfSubtreeConnections(); ++i) {
                recursiveTreeToString((FormulaDominoTree) tree.subtree(i), rep, assigner);
            }
        }
    }

    /**
     * <p>Returns the number of leaves {@code this} tile has for other tiles to
     * connect to.</p>
     */
    public int numberOfTileConnections() {
        return leafConnections_.length;
    }

    /**
     * <p>If a tile is connected to leaf {@code i}, it is returned, otherwise
     * the result is {@code null}.</p>
     * <p>Constraints for {@code i}:
     * {@code 0<=i<numberOfTileConnections()}.</p>
     *
     * @throws IllegalArgumentException if {@code i} is not a valid index.
     */
    public DominoTile tileConnection(int i) {
        if (i < 0 || i >= numberOfTileConnections()) throw new IllegalArgumentException();
        return leafConnections_[i];
    }

    /**
     * <p>If {@code this} tile is connected to another tile, that tile is returned,
     * otherwise the result is {@code null}.</p>
     */
    public DominoTile connectedToTile() {
        return rootConnection_;
    }

    /**
     * <p>Removes the {@link BonusRef}s common to all {@link LeafRef}s from
     * {@code leafRefs} and stores them in {@code commonBonus}.</p>
     */
    protected void extractCommonBonus(Collection<LeafRef> leafRefs, Collection<BonusRef> commonBonus) {
        if (leafRefs.isEmpty()) return;
        //if (leafRefs.size()==1) return; //activate this line to get back the longish tiles
        LeafRef lRef = leafRefs.iterator().next();
        Iterator<BonusRef> bonusRefIterator = lRef.getBonus().iterator();

        moveAllCommonBonusRefsToCommonBonus:
        while (bonusRefIterator.hasNext()) {
            BonusRef bonusRef = bonusRefIterator.next();
            Iterator<LeafRef> iterator = leafRefs.iterator();
            iterator.next(); //skip lRef

            checkIfBrefPresentInAllLeafRefs:
            while (iterator.hasNext()) {
                LeafRef lRef2 = iterator.next();
                for (BonusRef o : lRef2.getBonus()) {
                    if (o.getFormula().equals(bonusRef.getFormula()))
                        continue checkIfBrefPresentInAllLeafRefs;
                }
                continue moveAllCommonBonusRefsToCommonBonus;
            }

            commonBonus.add(bonusRef);
            bonusRefIterator.remove(); //do this separately, not in the loop below, otherwise bonusRefIterator would be invalidated
            iterator = leafRefs.iterator();
            iterator.next(); //skip lRef
            while (iterator.hasNext()) {
                LeafRef lRef2 = iterator.next();
                lRef2.getBonus().removeIf(bonusRef1 -> bonusRef1.getFormula().equals(bonusRef.getFormula()));
            }
        }
    }

    /**
     * <p>After making changes to the underlying {@link FormulaDominoTree}, this
     * method has to be called to update the {@code DominoTile}.</p>
     * <p>This method will not only work on the {@code DominoTile} for which
     * it is called but also on all {@code DominoTile}s connected to it.</p>
     * <p>This method has to be called from the event-dispatching thread, so you should
     * use {@link javax.swing.SwingUtilities#invokeAndWait(Runnable) invokeAndWait()} or
     * {@link javax.swing.SwingUtilities#invokeLater(Runnable) invokeLater()}. In any case you
     * have to make sure that no thread accesses the {@link FormulaDominoTree}
     * {@code this DominoTile} is linked to until {@code updateFormulas()}
     * has finished.</p>
     */
    public void updateFormulas() {
        DominoTile tile = this;
        while (tile.rootConnection_ != null) tile = tile.rootConnection_;
        updateFormulasDown(tile);
    }

    //see IsoObject
    public VectorInt3D getReferenceBlockLocation() {
        return refBlockLocation_;
    }


    //see IsoObject
    public void setReferenceBlockLocation(VectorInt3D location) {
        refBlockLocation_ = location;
    }


    //see IsoObject
    public VectorDouble3D getShift() {
        return shift_;
    }

    //see IsoObject
    public void setShift(VectorDouble3D shift, Collection<IsoBlock> posChangedBlocks) {
        // debugging test
        if (shift.x < 0 || shift.y < 0 || shift.z < 0 || shift.x >= 1.0 || shift.y >= 1.0
                || shift.z >= 1.0) throw new RuntimeException("illegal shift");

        shift_ = shift;
        //Because all DominoBlocks have a (0,0,0) shift, no DominoBlock can change
        //position. Therefore, posChangedBlocks does not need to be considered.
    }


    //see IsoObject
    public int numberOfBlocks() {
        return blocks_.size();
    }

    //see IsoObject
    public Iterator<DominoBlock> blocksIterator() {
        return blocks_.iterator();
    }

    //see IsoObject
    public IsoWorld getWorld() {
        return world_;
    }

    //see IsoObject
    public void setWorld(IsoWorld world) {
        world_ = world;
    }

    /**
     * <p>Determines if the {@link DominoBlock}s that form {@code this} tile return
     * {@code true} for {@link DominoBlock#isMassive()}.</p>
     */
    public boolean isMassive_() {
        return massive_;
    }
}



