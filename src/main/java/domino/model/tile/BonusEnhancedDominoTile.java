/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.model.tile;


import domino.logic.IllegalNodeTypeException;
import domino.logic.syntaxnet.ConflictingBindException;
import domino.logic.tree.DominoTree;
import domino.logic.tree.FormulaDominoTree;
import domino.ui.block.BonusDominoBlock;
import domino.ui.block.SubtreeConnectionDominoBlock;
import domino.ui.iso.IsoBlock;
import domino.utils.Pair;
import domino.utils.VectorInt3D;


public class BonusEnhancedDominoTile extends EnhancedDominoTile {
    private SubtreeConnectionDominoBlock leafBlock;

    public BonusEnhancedDominoTile(FormulaDominoTree tree, int subtreeIndex, int newBonusForSubtreeIndex) {
        super(tree, subtreeIndex, newBonusForSubtreeIndex);
    }

    public boolean unifyWithNearestTile(VectorInt3D location) {
        BonusDominoBlock bonusBlock;
        SubtreeConnectionDominoBlock lBlock;

        Pair<IsoBlock, IsoBlock> compadres = nearestIsoBlock(location, true);
        if (compadres == null) return false;

        if ((compadres.getA() instanceof SubtreeConnectionDominoBlock) &&
                (compadres.getB() instanceof BonusDominoBlock)) {
            bonusBlock = (BonusDominoBlock) compadres.getB();
            // debugging test
            if (bonusBlock != rootBlock()) throw new RuntimeException("weird!");

            lBlock = (SubtreeConnectionDominoBlock) compadres.getA();
        } else return false;

        if (!(lBlock.isActive() && bonusBlock.isActive())) return false;

        DominoTree tree1 = lBlock.formulaDominoTree();
        int tree1idx = lBlock.subtreeConnectionIndex();

        if (tree1.subtree(tree1idx) != null) return false;

        try {
            bonusBlock.attachBonusTree(tree1, tree1idx);
        } catch (ConflictingBindException | IllegalNodeTypeException x) {
            return false;
        }
        //NOTE: IllegalNodeTypeException is caught because lBlock could already be
        //connected to another tile on the board


        leafBlock = lBlock;

        ((DominoTile) bonusBlock.parentObject()).updateFormulas();
        ((DominoTile) leafBlock.parentObject()).updateFormulas();

        return true;
    }

    public void deunifyIfNecessary() {
        BonusDominoBlock bonusBlock = (BonusDominoBlock) rootBlock();
        if (bonusBlock.detachBonusTree()) {
            ((DominoTile) bonusBlock.parentObject()).updateFormulas();
            ((DominoTile) leafBlock.parentObject()).updateFormulas();

            //avoid keeping an out of date reference to avoid errors and gc problems
            leafBlock = null;
        }
    }

    public void attachToUnified() {
        BonusDominoBlock bonusBlock = (BonusDominoBlock) rootBlock();

        if (bonusBlock.getBonusTree() == null) return;

        DominoTile connectingTile1 = (DominoTile) bonusBlock.parentObject();
        connectingTile1.attachTo(leafBlock);

        DominoTile connectingTile2 = (DominoTile) leafBlock.parentObject();

        /*
         * The unification makes a lot of color assignments for unbound
         * nodes obsolete because they are superseded by the colors of the unified tree.
         * For that reason, we remove the assignments here. In some cases this may lead to
         * reassignments of visible colors, but this is extremely hard to avoid.
         */
        connectingTile1.removeUnneededIdAssignments();
        connectingTile2.removeUnneededIdAssignments();
        connectingTile1.updateFormulas(); //because some colors might have changed
        connectingTile2.updateFormulas(); //because some colors might have changed

        //set reference to null because we are now attached to leafBlock.parentObject()
        //and don't want to risk keeping a reference after an detach()
        leafBlock = null;
    }

}
