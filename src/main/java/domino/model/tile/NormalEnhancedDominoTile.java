/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.model.tile;

import domino.logic.IllegalNodeTypeException;
import domino.logic.syntaxnet.ConflictingBindException;
import domino.logic.tree.FormulaDominoTree;
import domino.ui.block.RootDominoBlock;
import domino.ui.block.SubtreeConnectionDominoBlock;
import domino.ui.iso.IsoBlock;
import domino.utils.Pair;
import domino.utils.VectorInt3D;

/**
 *
 */
public class NormalEnhancedDominoTile extends EnhancedDominoTile {
    private RootDominoBlock rootBlock;
    private SubtreeConnectionDominoBlock leafBlock;

    public NormalEnhancedDominoTile(FormulaDominoTree tree) {
        super(tree);
    }

    public void deunifyIfNecessary() {
        if (rootBlock != null && rootBlock.formulaDominoTree().detachFromParent()) {
            ((DominoTile) rootBlock.parentObject()).updateFormulas();
            ((DominoTile) leafBlock.parentObject()).updateFormulas();

            //avoid keeping out of date references to prevent errors and gc problems
            rootBlock = null;
            leafBlock = null;
        }
    }

    public boolean unifyWithNearestTile(VectorInt3D location) {
        RootDominoBlock rBlock;
        SubtreeConnectionDominoBlock lBlock;

        Pair<IsoBlock, IsoBlock> compadres = nearestIsoBlock(location, true);
        if (compadres == null) return false;

        if ((compadres.getA() instanceof SubtreeConnectionDominoBlock) &&
                (compadres.getB() instanceof RootDominoBlock)) {
            rBlock = (RootDominoBlock) compadres.getB();
            lBlock = (SubtreeConnectionDominoBlock) compadres.getA();
        } else if ((compadres.getB() instanceof SubtreeConnectionDominoBlock) &&
                (compadres.getA() instanceof RootDominoBlock)) {
            rBlock = (RootDominoBlock) compadres.getA();
            lBlock = (SubtreeConnectionDominoBlock) compadres.getB();
        } else return false;

        if (!(rBlock.isActive() && lBlock.isActive())) return false;

        FormulaDominoTree tree1 = lBlock.formulaDominoTree();
        int tree1idx = lBlock.subtreeConnectionIndex();
        FormulaDominoTree tree2 = rBlock.formulaDominoTree();
        try {
            tree2.attachToSubtreeConnection(tree1, tree1idx);
        } catch (ConflictingBindException | IllegalNodeTypeException x) {
            return false;
        }
        //NOTE: IllegalNodeTypeException is caught because lBlock could already be
        //connected to another tile on the board


        rootBlock = rBlock;
        leafBlock = lBlock;
        ((DominoTile) rootBlock.parentObject()).updateFormulas();
        ((DominoTile) leafBlock.parentObject()).updateFormulas();

        return true;
    }

    public void attachToUnified() {
        if (rootBlock == null || rootBlock.formulaDominoTree().parent() == null) return;
        DominoTile connectingTile1 = (DominoTile) rootBlock.parentObject();
        connectingTile1.attachTo(leafBlock);

        DominoTile connectingTile2 = (DominoTile) leafBlock.parentObject();

        /*
         * The unification makes a lot of color assignments for unbound
         * nodes obsolete because they are superseded by the colors of the unified tree.
         * For that reason, we remove the assignments here. In some cases this may lead to
         * reassignments of visible colors, but this is extremely hard to avoid.
         */
        connectingTile1.removeUnneededIdAssignments();
        connectingTile2.removeUnneededIdAssignments();
        connectingTile1.updateFormulas(); //because some colors might have changed
        connectingTile2.updateFormulas(); //because some colors might have changed

        //set references to null because now this tile is attached to another tile and
        //these references are maintained by the DominoTile superclass, so that redundant
        //references should be avoided to prevent inconsistencies.
        rootBlock = null;
        leafBlock = null;
    }

}
