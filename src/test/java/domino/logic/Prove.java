/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic;

import domino.logic.rules.*;
import domino.logic.square.FormulaDominoSquare;
import domino.logic.syntaxnet.ConflictingBindException;
import domino.logic.tree.DominoTree;
import domino.logic.tree.FormulaDominoTree;
import domino.logic.tree.SubCon;

import java.util.*;

public class Prove {
    public final static boolean eliminateRedundancy = true;
    public final static boolean verbose = false; // TODO verbose to LOG
    public final static boolean simplifyProof = true;
    //difficulty points per for each tile between the use and the provider of a bonus square
    public static final int BONUS_SQUARE_DISTANCE_TO_PROVIDER = 1;
    //difficulty points for every connection of a tile that is not a simplifier tile to a connection point of the form X->Y
    public static final int X_ARROW_Y_NOT_CONNECTED_TO_SIMPLIFIER = 10;

    //difficulty points for every simplifier used
    public static final int SIMPLIFIER_BASE = 1;

    //difficulty points for every redness inducer used
    public static final int REDNESS_INDUCER_BASE = 10;

    //additional difficulty points if a redness inducer is used in a branch that has no bonus square with _|_ in the lower-right corner
    public static final int REDNESS_INDUCER_WITHOUT_BONUS_IN_SAME_BRANCH = 20;

    //difficulty points for every complexifier used
    public static final int COMPLEXIFIER_BASE = 5;

    //additional difficulty points if a complexifier branch is not connected to a bonus square
    public static final int COMPLEXIFIER_CONNECTION_WITH_NO_BONUS = 10;

    protected static final DeductionRule[] rules = {new CloseRule(),
            new PERule(),
            new TNDRule(),
            new MPRule()
            /*,new ComboRule1()
            ,new ComboRule2()*/}; //the combo rules caused OOM on some formulas, so I aborted tests with them

    /**
     * <p>Returns those bonuses used for leaves at tree that can not be provided from
     * inside tree. Leaves will be fixed to use bonuses provided inside tree where
     * possible.</p>
     */
    protected static void returnAndFixNeededBonusesFromAbove(FormulaDominoTree tree, Collection<String> c, UnboundIdAssignerImpl assign, HashMap bonusesToFix) {
        if (tree == null) return;

        if (tree.numberOfSubtreeConnections() == 0) //tree is bonus square
        {
            String formula = ((FormulaDominoSquare) tree.rootSquare()).toInfixRepresentation(assign);
            BonusDescriptor desc = (BonusDescriptor) bonusesToFix.get(formula);
            if (desc == null)
                c.add(formula);
            else {
                DominoTree parent = tree.parent();
                int parentIndex = tree.parentSubtreeIndex();
                tree.detachFromParent();
                parent.useBonusSquare(parentIndex, desc.bonusProvider, desc.subtree, desc.newBonusForSubtreeIndex);
            }
        } else //if (tree.numberOfSubtreeConnections()>0)
        {
            for (int i = 0; i < tree.numberOfSubtreeConnections(); ++i) {
                HashMap newFix = new HashMap(bonusesToFix);
                for (int num = 0; num < tree.numberOfNewBonusSquaresForSubtreeConnection(i); ++num) {
                    String formula = ((FormulaDominoSquare) tree.newBonusSquareForSubtreeConnection(i, num)).toInfixRepresentation(assign);
                    newFix.put(formula, new BonusDescriptor(null, -1, tree, i, num));
                }
                returnAndFixNeededBonusesFromAbove((FormulaDominoTree) tree.subtree(i), c, assign, newFix);
            }
        }
    }

    protected static void getAndFixBonusDescriptors(FormulaDominoTree tree, Map<String, BonusDescriptor> mapBonusStringToDescriptor, Collection<BonusDescriptor> c, UnboundIdAssignerImpl assign) {
        if (tree == null) return;

        if (tree.numberOfSubtreeConnections() == 0) //tree is bonus square
        {
            String formula = ((FormulaDominoSquare) tree.rootSquare()).toInfixRepresentation(assign);
            BonusDescriptor desc = mapBonusStringToDescriptor.get(formula);
            if (desc != null) {
                c.add(new BonusDescriptor((FormulaDominoTree) tree.parent(), tree.parentSubtreeIndex(), desc.bonusProvider, desc.subtree, desc.newBonusForSubtreeIndex));
            }
        } else //if (tree.numberOfSubtreeConnections()>0)
        {
            for (int i = 0; i < tree.numberOfSubtreeConnections(); ++i) {
                getAndFixBonusDescriptors((FormulaDominoTree) tree.subtree(i), mapBonusStringToDescriptor, c, assign);
            }
        }
    }

    /**
     * <p>Returns true if tree or one of its subtrees has been moved up and
     * pullUpBranches() has to be called again for the top root.</p>
     */
    protected static boolean pullUpBranches(FormulaDominoTree tree, UnboundIdAssignerImpl assign) {
        if (tree == null) return false;

        HashSet<String> needed = new HashSet<>();
        returnAndFixNeededBonusesFromAbove(tree, needed, assign, new HashMap());

        LinkedList subtreeIndex = new LinkedList();
        DominoTree root = tree;
        while (root.parent() != null) {
            subtreeIndex.addFirst(root.parentSubtreeIndex());
            root = root.parent();
        }

        DominoTree closestAncestorThatAddedANeededBonusSquare = null;
        HashMap mapBonusStringToDescriptor = new HashMap();
        Iterator iter = subtreeIndex.iterator();
        while (root != tree) {
            int i = (Integer) iter.next();
            for (int num = 0; num < root.numberOfNewBonusSquaresForSubtreeConnection(i); ++num) {
                String bstr = ((FormulaDominoSquare) root.newBonusSquareForSubtreeConnection(i, num)).toInfixRepresentation(assign);
                if (needed.contains(bstr)) {
                    mapBonusStringToDescriptor.put(bstr, new BonusDescriptor(null, -1, (FormulaDominoTree) root, i, num));
                    closestAncestorThatAddedANeededBonusSquare = root;
                    needed.remove(bstr);
                }
            }

            root = root.subtree(i);
        }

        String formula = ((FormulaDominoSquare) tree.rootSquare()).toInfixRepresentation(assign);

        DominoTree puller = null;
        int pullIndex = -1;
        DominoTree ancestor = tree;
        while (ancestor.parent() != null) {
            int k = ancestor.parentSubtreeIndex();
            ancestor = ancestor.parent();
            String ancestorformula = ((FormulaDominoSquare) ancestor.subtreeConnectionSquare(k)).toInfixRepresentation(assign);
            if (formula.equals(ancestorformula)) {
                puller = ancestor;
                pullIndex = k;
            }

            if (ancestor == closestAncestorThatAddedANeededBonusSquare) break;
        }

        if (puller != tree.parent()) {
            //collect bonus descriptors BEFORE detaching
            Collection<BonusDescriptor> bonusDescs = new ArrayList<>();
            getAndFixBonusDescriptors(tree, mapBonusStringToDescriptor, bonusDescs, assign);

            DominoTree oldTreeParent = tree.parent();
            puller.subtree(pullIndex).detachFromParent();
            tree.detachFromParent();
            if (tree.numberOfSubtreeConnections() > 0) //tree is NOT a bonus square
                tree.attachToSubtreeConnection(puller, pullIndex);
            //else tree is contained in bonusDescs

            iter = bonusDescs.iterator();
            while (iter.hasNext()) {
                BonusDescriptor desc = (BonusDescriptor) iter.next();
                if (desc.bonusParent == oldTreeParent) {
                    desc.bonusParent = (FormulaDominoTree) puller;
                    desc.bonusParentIndex = pullIndex;
                }
                if (desc.bonusParent.subtree(desc.bonusParentIndex) == null)
                    desc.bonusParent.useBonusSquare(desc.bonusParentIndex, desc.bonusProvider, desc.subtree, desc.newBonusForSubtreeIndex);
            }

            return true;
        }

        for (int i = 0; i < tree.numberOfSubtreeConnections(); ++i) {
            if (pullUpBranches((FormulaDominoTree) tree.subtree(i), assign)) return true;
        }

        return false;
    }

    /**
     * <p>Removes redundant parts of the proof, creating an equivalent but smaller
     * proof.</p>
     */
    protected static void simplifyProof(FormulaDominoTree tree) {
        FormulaDominoTree masterRoot = tree;
        while (masterRoot.parent() != null) masterRoot = (FormulaDominoTree) masterRoot.parent();

        UnboundIdAssignerImpl assign = UnboundIdAssigner.newAssigner();
        while (pullUpBranches(masterRoot, assign)) ;

        if (masterRoot.numberOfOpenLeaves() > 0) throw new RuntimeException("Oops! I broke the proof :-(");
    }

    protected static void addMappings(BranchStringTree bst, FormulaDominoTree tree, HashMap mapBranchStringToTreeVector) {
        loop:
        for (int i = 0; i < bst.branch.length; ++i) {
            String b = bst.branch[i];
            for (int j = 0; j < i; ++j) if (b.equals(bst.branch[j])) continue loop;

            Vector v = (Vector) mapBranchStringToTreeVector.get(b);
            if (v == null) {
                v = new Vector(1);
                mapBranchStringToTreeVector.put(b, v);
            }
            v.add(tree);
        }
    }

    protected static void removeMappings(BranchStringTree bst, FormulaDominoTree tree, HashMap mapBranchStringToTreeVector) {
        loop:
        for (int i = 0; i < bst.branch.length; ++i) {
            String b = bst.branch[i];
            for (int j = 0; j < i; ++j) if (b.equals(bst.branch[j])) continue loop;

            Vector v = (Vector) mapBranchStringToTreeVector.get(b);
            if (v == null || !v.remove(tree)) throw new RuntimeException("mapping not found");
            if (v.isEmpty()) mapBranchStringToTreeVector.remove(b);
        }
    }

    /**
     * <p>The tree for bst must not be in trees or it will be removed, too. </p>
     */
    protected static void eliminateRedundantTrees(BranchStringTree bst, HashMap mapBranchStringToTreeVector, Collection trees) {
        Vector[] va = new Vector[bst.branch.length];
        for (int i = 0; i < bst.branch.length; ++i) {
            Vector v = (Vector) mapBranchStringToTreeVector.get(bst.branch[i]);
            if (v == null) return;
            int j = i - 1;
            while (j >= 0 && va[j].size() > v.size()) --j;
            ++j;
            int len = i - j;
            if (len > 0) System.arraycopy(va, j, va, j + 1, len);
            va[j] = v;
        }

        //va is now array of Vectors with candidates for being redundant because they are
        //no better than the tree from which bst was generated
        //va is sorted by Vector size, i.e. va[0] is the Vector with the fewest elements
        int i = 0;
        va[0] = new Vector(va[0]); //copy to avoid deleting elements from original
        computeintersection:
        while (i < va[0].size()) {
            FormulaDominoTree tree = (FormulaDominoTree) va[0].get(i);
            for (int j = 1; j < va.length; ++j) {
                boolean found = false;
                for (int k = 0; k < va[j].size(); ++k) {
                    if (tree == va[j].get(k)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    va[0].remove(i);
                    if (va[0].isEmpty()) return;
                    continue computeintersection;
                }
            }

            ++i;
        }

        //now va[0] contains all those trees that have to be checked against bst and have
        //to be removed if they are not better than or incomparable to bst
        for (i = 0; i < va[0].size(); ++i) {
            FormulaDominoTree tree2 = (FormulaDominoTree) va[0].get(i);
            BranchStringTree bst2 = new BranchStringTree(tree2);
            int cmp = bst.compareTo(bst2); //-1 bst is better, +1 bst2 is better, 0 incomparable
            if (cmp < 0) {
                removeMappings(bst2, tree2, mapBranchStringToTreeVector);
                if (!trees.remove(tree2)) throw new RuntimeException("phantom object");
            }
        }
    }

    protected static void getOpenSubtreeConnections(DominoTree tree, Vector subCons) {
        for (int i = 0; i < tree.numberOfSubtreeConnections(); ++i) {
            DominoTree s = tree.subtree(i);
            if (s == null)
                subCons.add(new SubCon((FormulaDominoTree) tree, i));
            else
                getOpenSubtreeConnections(s, subCons);
        }
    }

    /**
     * <p>Returns true, if tree has at least one subtree connection that has the same
     * formula as a subtree connection of an ancestor of tree with no new bonus squares
     * usable for this leaf.</p>
     */
    public static boolean noProgress(FormulaDominoTree tree) {
        if (tree.numberOfSubtreeConnections() == 0) //if tree is bonus square
        {
            tree = (FormulaDominoTree) tree.parent();  //then examine parent
            if (tree.numberOfOpenLeaves() == 0) return false;
        }

        DominoTree root = tree;
        LinkedList<Integer> subtreeIndex = new LinkedList<>();
        while (root.parent() != null) {
            subtreeIndex.addFirst(root.parentSubtreeIndex());
            root = root.parent();
        }

        UnboundIdAssignerImpl assign = UnboundIdAssigner.newAssigner();
        DominoTree closestAncestorThatAddedANewBonusSquare = null;
        HashSet<String> bonus = new HashSet<>();
        Iterator<Integer> iterator = subtreeIndex.iterator();
        while (root != tree) {
            int i = iterator.next();
            int bonusNumBefore = bonus.size();
            for (int num = 0; num < root.numberOfNewBonusSquaresForSubtreeConnection(i); ++num) {
                String bstr = ((FormulaDominoSquare) root.newBonusSquareForSubtreeConnection(i, num)).toInfixRepresentation(assign);
                bonus.add(bstr);
            }

            if (bonus.size() > bonusNumBefore) closestAncestorThatAddedANewBonusSquare = root;

            root = root.subtree(i);
        }

        check_subtree_connections:
        for (int i = 0; i < tree.numberOfSubtreeConnections(); ++i) {
            //ignore subtree connections without open leaves
            if (tree.subtree(i) != null && tree.subtree(i).numberOfOpenLeaves() == 0) continue;

            //if a new bonus is added, then this subtree connection makes progress,
            //so first check if a new bonus is added
            for (int num = 0; num < tree.numberOfNewBonusSquaresForSubtreeConnection(i); ++num) {
                String bstr = ((FormulaDominoSquare) tree.newBonusSquareForSubtreeConnection(i, num)).toInfixRepresentation(assign);
                if (!bonus.contains(bstr)) continue check_subtree_connections;
            }

      /*
        If we get here, the subtree connection does not add any new bonus squares, so
        we need to check if its formula is the same as that of a subtree connection
        of an ancestor of tree that is at least as close as
        closestAncestorThatAddedANewBonusSquare.
      */
            String formula = ((FormulaDominoSquare) tree.subtreeConnectionSquare(i)).toInfixRepresentation(assign);

            DominoTree ancestor = tree;
            while (ancestor.parent() != null) {
                int k = ancestor.parentSubtreeIndex();
                ancestor = ancestor.parent();
                String ancestorformula = ((FormulaDominoSquare) ancestor.subtreeConnectionSquare(k)).toInfixRepresentation(assign);
                if (formula.equals(ancestorformula)) return true;

                if (ancestor == closestAncestorThatAddedANewBonusSquare) break;
            }
        }
        return false;
    }

    public static int maxSubconParentheses(FormulaDominoTree t) {
        if (t.numberOfSubtreeConnections() == 0) //if tree is bonus square
        {
            t = (FormulaDominoTree) t.parent();     //examine parent
            if (t.numberOfOpenLeaves() == 0) return 0;
        }

        int maxPLevel = 0;
        for (int k = 0; k < t.numberOfSubtreeConnections(); ++k) {
            //ignore connections with attached subtree without open leaves
            if (t.subtree(k) != null && t.subtree(k).numberOfOpenLeaves() == 0) continue;

            int pLevel = 0;
            String formula = ((FormulaDominoSquare) t.subtreeConnectionSquare(k)).toInfixRepresentation();
            for (int i = 0; i < formula.length(); ++i) {
                char ch = formula.charAt(i);
                if (ch == '(') {
                    pLevel++;
                    if (pLevel > maxPLevel) maxPLevel = pLevel;
                } else if (ch == ')') {
                    pLevel--;
                }
            }
        }
        return maxPLevel;
    }

    public static void main(String[] args) { // TODO maybe produce some tests from it?
        if (args.length != 1) {
            System.err.println("USAGE: Prove <tautology>");
            System.exit(1);
        }

        //protection against future modifications that seem plausible but don't actually
        //work because of the current implementation
        for (int i = 1; i < rules.length; ++i)
            if (rules[i].requiredBonusSquares() != 0) throw new RuntimeException("currently not supported!");
        if (rules[0].requiredBonusSquares() != 1) throw new RuntimeException("currently not supported!");

        String tautRepresentation = args[0];
        tautRepresentation = "(" + tautRepresentation + ")";
        System.out.println("Proving " + tautRepresentation);

        HashSet<String> subFormulae = new HashSet<>(); //all subformulae in positive and negated form
        Deque<Integer> pStack = new ArrayDeque<>();
        int maxPLevel = 0;
        int pLevel = 0;
        for (int i = 0; i < tautRepresentation.length(); ++i) {
            char ch = tautRepresentation.charAt(i);
            if (ch == '(') {
                pLevel++;
                if (pLevel > maxPLevel) maxPLevel = pLevel;
                pStack.push(i);
            } else if (ch == ')') {
                pLevel--;
                int j = pStack.pop();
                String f = tautRepresentation.substring(j, i + 1);
                subFormulae.add(f);
                subFormulae.add("(" + f + "->_|_)");
            } else if (Character.isLetterOrDigit(ch) || ch == '|' || ch == '_') {
                int j = i + 1;
                while (
                        j < tautRepresentation.length() &&
                                (Character.isLetterOrDigit(tautRepresentation.charAt(j)) ||
                                        tautRepresentation.charAt(j) == '|' ||
                                        tautRepresentation.charAt(j) == '_'
                                )
                ) ++j;
                String f = tautRepresentation.substring(i, j);
                subFormulae.add(f);
                subFormulae.add("(" + f + "->_|_)");
                i = j - 1;
            }
        }

        FormulaDominoTree tree = FormulaDominoTree.newFromInfix(tautRepresentation, new String[]{tautRepresentation}, null);

        List<Integer> emptyVector = new ArrayList<>(0);
        List<Integer> bsVector = new ArrayList<>(1);

        try {
            DeductionRule peRule = new PERule();
            while (true) peRule.applyTo(tree, 0, emptyVector);
        } catch (ConflictingBindException ignored) {
        }

        class FDTComparator implements Comparator {
            protected int maxDepth(DominoTree tree) {
                if (tree == null) return 0;
                int d = 0;
                for (int i = 0; i < tree.numberOfSubtreeConnections(); ++i) {
                    int d2 = maxDepth(tree.subtree(i));
                    if (d2 > d) d = d2;
                }
                return d + 1;
            }

            public int compare(Object o1, Object o2) {
                FormulaDominoTree t1 = (FormulaDominoTree) o1;
                FormulaDominoTree t2 = (FormulaDominoTree) o2;

                if (t1.numberOfOpenLeaves() < t2.numberOfOpenLeaves()) return -1;
                if (t1.numberOfOpenLeaves() > t2.numberOfOpenLeaves()) return +1;

                int d1 = maxDepth(t1);
                int d2 = maxDepth(t2);
                if (d1 < d2) return -1;
                if (d1 > d2) return +1;

                if (t1.numberOfLeaves() < t2.numberOfLeaves()) return -1;
                if (t1.numberOfLeaves() > t2.numberOfLeaves()) return +1;

                if (t1 == t2) return 0;

                int numBonus1 = 0;
                for (int i = 0; i < t1.numberOfLeaves(); ++i)
                    numBonus1 += t1.numberOfBonusSquares(i);

                int numBonus2 = 0;
                for (int i = 0; i < t2.numberOfLeaves(); ++i)
                    numBonus2 += t2.numberOfBonusSquares(i);

                if (numBonus1 > numBonus2) return -1; //give precedence to the one with MORE bonus squares
                if (numBonus1 < numBonus2) return +1;

                int c1 = t1.hashCode();
                int c2 = t2.hashCode();

                if (c1 < c2) return -1;
                if (c1 > c2) return +1;

                return -1; //not very clean, because it violates the sgn() requirements
                //but it should do for our purposes
            }

        }

        SortedSet trees = new TreeSet(new FDTComparator());
        HashMap mapBranchStringToTreeVector = new HashMap();

        trees.add(tree);
        BranchStringTree bst = new BranchStringTree(tree);
        addMappings(bst, tree, mapBranchStringToTreeVector);

        processnexttree:
        while (true) {
            if (trees.size() == 0) {
                System.err.println("Cannot prove formula! Maybe it's not a tautology?");
                System.exit(1);
            }
            tree = (FormulaDominoTree) trees.first();
            trees.remove(tree);
            if (eliminateRedundancy) {
                bst = new BranchStringTree(tree);
                removeMappings(bst, tree, mapBranchStringToTreeVector);
                eliminateRedundantTrees(bst, mapBranchStringToTreeVector, trees);
            }

            if (verbose) {
                BranchStringTree br = new BranchStringTree(tree);
                System.out.print("----------------- ");
                for (int i = 0; i < br.branch.length; ++i) {
                    System.out.print(br.branch[i] + "/" + br.bonus.get(i).size() + " ");
                }
                System.out.println(" -----------------");
            }

            Vector subCons = new Vector();
            getOpenSubtreeConnections(tree, subCons);

            int[] maxRule = new int[subCons.size()];
            for (int i = 0; i < maxRule.length; ++i) {
                SubCon s = (SubCon) subCons.get(i);
                maxRule[i] = rules.length - 1 + s.tree.numberOfBonusSquares(s.tree.leafIndexForSubtreeConnection(s.index));
            }

            for (int index = 0; index < maxRule.length; ++index) {
                SubCon s = (SubCon) subCons.get(index);

                for (int rule = 0; rule < maxRule[index]; ++rule) {
                    //rules 0 to maxRule[index]-(rules.length-1) (excl.) are bonus squares
                    //rules maxRule[index]-(rules.length-1) to maxRule[index] (excl.) map to rules[1], rules[2], rules[3],...

                    try {
                        DominoTree t;

                        if (rule < maxRule[index] - (rules.length - 1)) //bonus square
                        {
                            //test if the subtree connection to connect to has at least
                            //one bound part and reject it if not
                            String conFormula = ((FormulaDominoSquare) s.tree.subtreeConnectionSquare(s.index)).toPrefixRepresentation();
                            boolean okay = false;
                            int k = conFormula.indexOf(')');
                            while (k >= 0) {
                                if (!Character.isDigit(conFormula.charAt(k - 1))) {
                                    okay = true;
                                    break;
                                }
                                k = conFormula.indexOf(')', k + 1);
                            }
                            if (!okay) throw new ConflictingBindException();

                            bsVector.clear();
                            bsVector.add(rule);
                            t = rules[0].applyTo(s.tree, s.tree.leafIndexForSubtreeConnection(s.index), bsVector);

                            if (verbose) {
                                BranchStringTree br = new BranchStringTree(tree);
                                for (int i = 0; i < br.branch.length; ++i) {
                                    System.out.print(br.branch[i] + "/" + br.bonus.get(i).size() + " ");
                                }
                            }
                        } else //if (rule>=maxRule[index]-(rules.length-1))
                        {
                            t = rules[rule - (maxRule[index] - (rules.length - 1)) + 1].applyTo(s.tree, s.tree.leafIndexForSubtreeConnection(s.index), emptyVector);

                            if (verbose) {
                                BranchStringTree br = new BranchStringTree(tree);
                                for (int i = 0; i < br.branch.length; ++i) {
                                    System.out.print(br.branch[i] + "/" + br.bonus.get(i).size() + " ");
                                }
                            }

                            for (int i = 0; i < t.numberOfNewBonusSquaresForSubtreeConnection(0); ++i) {
                                FormulaDominoSquare sq = (FormulaDominoSquare) t.newBonusSquareForSubtreeConnection(0, i);
                                String formula = sq.toInfixRepresentation();
                                if (!subFormulae.contains(formula)) {
                                    if (verbose) System.out.println("rejected because of " + formula);
                                    t.detachFromParent();
                                    throw new ConflictingBindException();
                                }
                            }

                        }

                        if (noProgress((FormulaDominoTree) t)) {
                            if (verbose) System.out.println("rejected because no progress made");
                            t.detachFromParent();
                            throw new ConflictingBindException();
                        }

                        if (maxSubconParentheses((FormulaDominoTree) t) > maxPLevel) {
                            if (verbose) System.out.println("rejected because parentheses limit exceeded");
                            t.detachFromParent();
                            throw new ConflictingBindException();
                        }

                        if (verbose) System.out.println();

                        if (tree.numberOfOpenLeaves() == 0) break processnexttree;

                        FormulaDominoTree cloneTree = (FormulaDominoTree) tree.cloneDT();
                        if (eliminateRedundancy) {
                            bst = new BranchStringTree(cloneTree);
                            eliminateRedundantTrees(bst, mapBranchStringToTreeVector, trees);
                            addMappings(bst, cloneTree, mapBranchStringToTreeVector);
                        }
                        trees.add(cloneTree);

                        t.detachFromParent();

                    } catch (ConflictingBindException ignored) {
                    }
                }
            }
        }

        if (simplifyProof) simplifyProof(tree);

        String[] diagram = ProofFormatter.naturalDeductionDiagram(tree);
        for (String s : diagram) System.out.println(s);
        System.out.println("Difficulty: " + difficulty(tree));
        System.out.println();
    }

    /**
     * <p>Judges the difficulty of a proof.</p>
     */
    protected static int difficulty(FormulaDominoTree tree) {
        return difficulty(tree, UnboundIdAssigner.newAssigner());
    }

    protected static int difficulty(FormulaDominoTree tree, UnboundIdAssignerImpl assign) {
        if (tree == null) return 0;
        int diffy = 0;

        String rootFormula = ((FormulaDominoSquare) tree.rootSquare()).toPrefixRepresentation(assign);

        if (tree.numberOfSubtreeConnections() == 0) //tree is bonus square
        {
            int distance = 0;
            DominoTree scan = tree;
            find_distance:
            while (true) {
                int pidx = scan.parentSubtreeIndex();
                scan = scan.parent();
                ++distance;
                for (int i = 0; i < scan.numberOfNewBonusSquaresForSubtreeConnection(pidx); ++i) {
                    String bonus = ((FormulaDominoSquare) scan.newBonusSquareForSubtreeConnection(pidx, i)).toPrefixRepresentation(assign);
                    if (bonus.equals(rootFormula)) break find_distance;
                }
            }

            diffy += distance * BONUS_SQUARE_DISTANCE_TO_PROVIDER;
        } else //tree is NOT bonus square
        {
            int type = 0; //0 => mp tile, 1 => TND tile, 2 => ->I tile, 3 => sthg else
            if (tree.numberOfSubtreeConnections() == 1) {
                if (tree.numberOfNewBonusSquaresForSubtreeConnection(0) == 0)
                    type = 3;
                else {
                    String bonus = ((FormulaDominoSquare) tree.newBonusSquareForSubtreeConnection(0, 0)).toPrefixRepresentation(assign);
                    String leaf = ((FormulaDominoSquare) tree.subtreeConnectionSquare(0)).toPrefixRepresentation(assign);
                    if (leaf.equals("(_|_)") && bonus.equals("->" + rootFormula + "(_|_)"))
                        type = 1;
                    else
                        type = 2;
                }
            }

            if (type != 2 && tree.parent() != null && rootFormula.startsWith("->"))
                diffy += X_ARROW_Y_NOT_CONNECTED_TO_SIMPLIFIER;

            if (type == 0) {
                diffy += COMPLEXIFIER_BASE;
                for (int i = 0; i < tree.numberOfSubtreeConnections(); ++i)
                    if (tree.subtree(i) != null && tree.subtree(i).numberOfSubtreeConnections() > 0)
                        diffy += COMPLEXIFIER_CONNECTION_WITH_NO_BONUS;
            }

            if (type == 2) diffy += SIMPLIFIER_BASE;

            if (type == 1) {
                diffy += REDNESS_INDUCER_BASE;
                boolean foundBonus = false;
                DominoTree scan = tree;
                find_bonus:
                while (scan.parent() != null) {
                    int pidx = scan.parentSubtreeIndex();
                    scan = scan.parent();

                    for (int i = 0; i < scan.numberOfNewBonusSquaresForSubtreeConnection(pidx); ++i) {
                        String bonus = ((FormulaDominoSquare) scan.newBonusSquareForSubtreeConnection(pidx, i)).toPrefixRepresentation(assign);
                        if (bonus.length() >= 5 && bonus.lastIndexOf("(_|_)") == bonus.length() - 5) {
                            foundBonus = true;
                            break find_bonus;
                        }
                    }
                }

                if (!foundBonus) diffy += REDNESS_INDUCER_WITHOUT_BONUS_IN_SAME_BRANCH;
            }
        }

        for (int i = 0; i < tree.numberOfSubtreeConnections(); ++i) {
            diffy += difficulty((FormulaDominoTree) tree.subtree(i), assign);
        }

        return diffy;
    }

}
