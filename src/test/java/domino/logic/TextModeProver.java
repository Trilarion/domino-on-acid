/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic;

import domino.logic.rules.*;
import domino.logic.square.FormulaDominoSquare;
import domino.logic.syntaxnet.ConflictingBindException;
import domino.logic.tree.DominoTree;
import domino.logic.tree.FormulaDominoTree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

/**
 * <p>This class is a test program for testing {@link FormulaDominoTree},
 * {@link TautologyGenerator} and related classes.</p>
 */
public class TextModeProver {
    protected static final DeductionRule[] rules = {new TNDRule(), new PERule(), new MPRule(), new CloseRule()};

    public static void main(String[] args) {
        List<Double> prob = new ArrayList<>();
        prob.add(1.0); //TND
        prob.add(3.0); //->I
        prob.add(6.0); //->E
        prob.add(5.0); //[assumpt]
        Random rand0 = new Random();
        long seed = rand0.nextLong();
        System.out.println("Random seed: " + seed);
        rand0 = new Random(seed);
        System.out.print("Generating tautology to prove...");
        String tautRepresentation = ((FormulaDominoSquare) new TautologyGenerator(rand0).randomTautologyProof(3, prob, true).rootSquare()).toInfixRepresentation();
        System.out.println(tautRepresentation);
        System.out.println();

        String[] leaves = new String[1];
        leaves[0] = tautRepresentation;
        FormulaDominoTree tree = FormulaDominoTree.newFromInfix(tautRepresentation, leaves, null);

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        UnboundIdAssignerImpl assigner = UnboundIdAssigner.newAssigner();
        List option = new ArrayList(); //format of option: Integer rule, Integer leaf, Vector bsIndex // TODO can contain different elements, difficult to generify
        while (tree.numberOfOpenLeaves() > 0) {
            option.clear();
            assigner.reset();

            for (int leaf = 0; leaf < tree.numberOfLeaves(); ++leaf) {
                if (!tree.isOpenLeaf(leaf)) continue;
                System.out.println();
                System.out.print("Goal: " + ((FormulaDominoSquare) tree.leafSquare(leaf)).toInfixRepresentation(assigner) + "   ");
                int bnum = tree.numberOfBonusSquares(leaf);
                for (int j = 0; j < bnum; ++j)
                    System.out.print("[" + ((FormulaDominoSquare) tree.bonusSquare(leaf, j)).toInfixRepresentation(assigner) + "] ");
                System.out.println();

                for (int rule = 0; rule < rules.length; ++rule) {
                    int needBS = rules[rule].requiredBonusSquares();
                    if (needBS > 1) throw new RuntimeException("Unsupported rule");
                    int bs = 0;
                    List<Integer> bsIndex = new ArrayList<>();
                    do {
                        bsIndex.clear();
                        if (needBS > 0) bsIndex.add(bs);

                        try {
                            DominoTree t = rules[rule].applyTo(tree, leaf, bsIndex);
                            option.add(rule);
                            option.add(leaf);
                            option.add(bsIndex);
                            bsIndex = new ArrayList<>(); //so that we don't overwrite the Vector in option
                            System.out.println(option.size() / 3 + ". " + rules[rule].name(((FormulaDominoSquare) t.rootSquare()).toInfixRepresentation(assigner)));
                            if (t.numberOfOpenLeaves() == 0) System.out.println("\t<done>");
                            else {
                                for (int i = 0; i < t.numberOfLeaves(); ++i) {
                                    if (!t.isOpenLeaf(i)) continue;

                                    System.out.print("\t" + ((FormulaDominoSquare) t.leafSquare(i)).toInfixRepresentation(assigner) + "   ");
                                    bnum = t.numberOfBonusSquares(i);
                                    for (int j = 0; j < bnum; ++j)
                                        System.out.print("[" + ((FormulaDominoSquare) t.bonusSquare(i, j)).toInfixRepresentation(assigner) + "] ");
                                    System.out.println();
                                }
                            }
                            t.detachFromParent();
                        } catch (ConflictingBindException ignored) {
                        }

                        ++bs;

                    } while (needBS > 0 && bs < tree.numberOfBonusSquares(leaf));
                }
            }

            if (option.isEmpty()) throw new RuntimeException("No applicable rules!");

            while (true) {
                System.out.println();
                System.out.print("> ");
                try {
                    int i = Integer.parseInt(in.readLine());
                    System.err.println(i);
                    i = (i - 1) * 3;
                    if (i < 0 || i >= option.size()) throw new NumberFormatException();
                    int rule = (Integer) option.get(i);
                    int leaf = (Integer) option.get(i + 1);
                    Vector<Integer> bsIndex = (Vector<Integer>) option.get(i + 2);
                    rules[rule].applyTo(tree, leaf, bsIndex);
                    System.out.println();
                    break;
                } catch (NumberFormatException x) {
                    System.err.println("Illegal input!");
                } catch (IOException x) {
                    System.err.println("Read error!");
                    System.exit(1);
                }
            }
        }

        String[] diagram = ProofFormatter.naturalDeductionDiagram(tree);
        System.out.println();
        for (String s : diagram) System.out.println(s);
        System.out.println();
    }

}

