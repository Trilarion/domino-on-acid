/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.tree;

import domino.logic.syntaxnet.FormulaSyntaxNet;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class FormulaDominoTreeTest extends TestCase {

    /**
     * <p>Tests the functions of FormulaDominoTree.</p>
     */

    public void testFormulaDominoTree() {
        Random rand0 = new Random(0);
        domino.logic.TautologyGenerator tgen = new domino.logic.TautologyGenerator(rand0);
        List<Double> prob = new ArrayList<>();
        prob.add(1.0);
        prob.add(2.0);
        prob.add(3.0);
        prob.add(4.0);
        FormulaDominoTree randomTautProof = tgen.randomTautologyProof(2, prob, true);
        FormulaDominoTree toBeDetachedParent = (FormulaDominoTree) randomTautProof.subtree(0).subtree(0);
        FormulaDominoTree toBeDetached = (FormulaDominoTree) toBeDetachedParent.subtree(0);
        System.out.println("randomTautProof.numberOfLeaves()=" + randomTautProof.numberOfLeaves());
        System.out.println("randomTautProof.numberOfOpenLeaves()=" + randomTautProof.numberOfOpenLeaves());
        System.out.println("toBeDetached.numberOfLeaves()=" + toBeDetached.numberOfLeaves());
        System.out.println("toBeDetached.numberOfOpenLeaves()=" + toBeDetached.numberOfOpenLeaves());
        System.out.println("toBeDetached.detachFromParent():");
        toBeDetached.detachFromParent();
        System.out.println("randomTautProof.isOpenLeaf(0)=" + randomTautProof.isOpenLeaf(0));
        System.out.println("toBeDetached.isOpenLeaf(0)=" + toBeDetached.isOpenLeaf(0));
        System.out.println("randomTautProof.numberOfLeaves()=" + randomTautProof.numberOfLeaves());
        System.out.println("randomTautProof.numberOfOpenLeaves()=" + randomTautProof.numberOfOpenLeaves());
        System.out.println("toBeDetached.numberOfLeaves()=" + toBeDetached.numberOfLeaves());
        System.out.println("toBeDetached.numberOfOpenLeaves()=" + toBeDetached.numberOfOpenLeaves());
        System.out.println("toBeDetached.attachTo(...):");
        toBeDetached.attachTo(toBeDetachedParent, 0);
        System.out.println("randomTautProof.numberOfLeaves()=" + randomTautProof.numberOfLeaves());
        System.out.println("randomTautProof.numberOfOpenLeaves()=" + randomTautProof.numberOfOpenLeaves());
        System.out.println("toBeDetached.numberOfLeaves()=" + toBeDetached.numberOfLeaves());
        System.out.println("toBeDetached.numberOfOpenLeaves()=" + toBeDetached.numberOfOpenLeaves());

        System.out.println("toBeDetached.root_=" + toBeDetached.root_.toInfixFormulaString());
        System.out.println("toBeDetached.leaves_[0]=" + toBeDetached.leaves_[0].toInfixFormulaString());

        HashSet<FormulaSyntaxNet> allReach = new HashSet<>();
        toBeDetached.root_.getAllReachableNodes(allReach);
        System.out.println("toBeDetached.root_.getAllReachableNodes().size()=" + allReach.size());

        System.out.println("cloneTree=toBeDetached.cloneDT():");
        FormulaDominoTree cloneTree = (FormulaDominoTree) toBeDetached.cloneDT();
        System.out.println("cloneTree.root_=" + cloneTree.root_.toInfixFormulaString());
        System.out.println("cloneTree.leaves_[0]=" + cloneTree.leaves_[0].toInfixFormulaString());

        toBeDetached.root_.getAllReachableNodes(allReach);
        System.out.println("toBeDetached.root_.getAllReachableNodes().size()=" + allReach.size());
        cloneTree.root_.getAllReachableNodes(allReach);
        System.out.println("cloneTree.root_.getAllReachableNodes().size()=" + allReach.size());

        System.out.println("cloneTree.numberOfLeaves()=" + cloneTree.numberOfLeaves());
        System.out.println("cloneTree.numberOfOpenLeaves()=" + cloneTree.numberOfOpenLeaves());

        FormulaDominoTree t = randomTautProof;
        while (t != null && t.numberOfSubtreeConnections() != 0) t = (FormulaDominoTree) t.subtree(0);
        if (t != null) t.detachFromParent();
        t.root_.getAllReachableNodes(allReach);
        System.out.println("t.root_.getAllReachableNodes().size()=" + allReach.size());
    }
}