/*
 * Domino On Acid
 * Copyright (C) 2021 The Domino On Acid Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License (ONLY THIS VERSION).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package domino.logic.syntaxnet;


import domino.logic.fsn.FSNIterator;
import junit.framework.TestCase;

import java.util.Collection;
import java.util.HashSet;

import static domino.logic.syntaxnet.FormulaSyntaxNet.*;

public class FormulaSyntaxNetTest extends TestCase {

    /**
     * This function tests the <code>FormulaSyntaxNet</code> methods.
     */
    public void testFormulaSyntaxNet() {
        System.out.println("Testing FormulaSyntaxNet");

        System.out.println("Creating ((A->B)->C) -> (D->(E->F))");

        String owner = "owner";
        FormulaSyntaxNet A = newLeaf(owner, "A");
        FormulaSyntaxNet B = newLeaf(owner, "B");
        FormulaSyntaxNet C = newLeaf(owner, "C");
        FormulaSyntaxNet D = newLeaf(owner, "D");
        FormulaSyntaxNet E = newLeaf(owner, "E");
        FormulaSyntaxNet F = newLeaf(owner, "F");

        System.out.println("UIDs for A,B,C,D,E,F: " + A.uid() + ", " + B.uid() + ", " + C.uid() + ", " + D.uid() + ", " + E.uid() + ", " + F.uid());

        FormulaSyntaxNet net = newBranch(owner,
                newBranch(owner, newBranch(owner, A, B), C),
                newBranch(owner, D, newBranch(owner, E, F))
        );

        System.out.println("A.id()=" + A.id());
        System.out.println("A.isBound()=" + A.isBound() + "  " +
                "A.isBranch()=" + A.isBranch() + "  " +
                "A.isBoundOrBranch()=" + A.isBoundOrBranch() + "  " +
                "A.isUnbound()=" + A.isUnbound() + "  " +
                "A.isForward()=" + A.isForward() + "  " +
                "A.isUnboundOrForward()=" + A.isUnboundOrForward()
        );

        System.out.println("infix string: " + net.toInfixFormulaString());

        final String[] orderStr = {"in", "pre", "post"};
        FSNIterator[] itAr = new FSNIterator[3];
        itAr[0] = net.inorderIterator();
        itAr[1] = net.preorderIterator();
        itAr[2] = net.postorderIterator();
        for (int i = 0; i < 3; ++i) {
            System.out.print(orderStr[i] + "order walk: ");
            while (itAr[i].hasNext()) {
                FormulaSyntaxNet n = itAr[i].next();
                if (n.isBranch()) System.out.print("->");
                else if (n.isBound()) System.out.print("(" + n.id() + ")");
                else if (n.isUnboundOrForward()) System.out.println("(unbound)");
                else throw new RuntimeException("Huh? What's this node?");
            }
            System.out.println();
        }

        String nastyString1 = "(A->X0)->X0";
        String nastyString2 = "(((A))->(B))->(C)";
        FormulaSyntaxNet nasty1 = newFromInfixFormulaString(owner, nastyString1);
        FormulaSyntaxNet nasty2 = newFromInfixFormulaString(owner, nastyString2);

        System.out.println(nastyString1 + " parsed to " + nasty1.toInfixFormulaString());
        System.out.println(nastyString2 + " parsed to " + nasty2.toInfixFormulaString());
        System.out.print("unifying " + nastyString1 + " with " + nastyString2 + "...");
        try {
            unify(nasty1, nasty2);
            System.out.println("succeeds (This is BAD!)");
        } catch (ConflictingBindException x) {
            System.out.println("fails (This is GOOD!)");
        }

        System.out.println(nastyString1 + " has become " + nasty1.toInfixFormulaString() + " (should be unchanged)");
        System.out.println(nastyString2 + " has become " + nasty2.toInfixFormulaString() + " (should be unchanged)");

        FormulaSyntaxNet nasty3 = newBranch(owner, newBranch(owner, A, B), newLeaf(owner, "B"));
        String nastyString3 = nasty3.toInfixFormulaString();

        System.out.print("unifying " + nastyString1 + " with " + nastyString3 + "...");
        try {
            unify(nasty3, nasty1);
            System.out.println("succeeds (This is GOOD!)");
        } catch (ConflictingBindException x) {
            System.out.println("fails (This is BAD!)");
        }

        System.out.println(nastyString3 + " has become " + nasty3.toInfixFormulaString() + " (should be unchanged)");
        System.out.println(nastyString1 + " has become " + nasty1.toInfixFormulaString() + " (should be same as ^^^^^)");
        System.out.println("unifying last net with itself");
        unify(nasty1, nasty1);

        String testStr1 = "A->(X0->(B->X3))";
        String testStr2 = "X1->((C->D)->(B->X10))";
        FormulaSyntaxNet test1 = newFromInfixFormulaString(owner, testStr1);
        FormulaSyntaxNet test2 = newFromInfixFormulaString(owner, testStr2);
        System.out.println("T1:" + testStr1 + " parsed to " + test1.toInfixFormulaString());
        System.out.println("T2:" + testStr2 + " parsed to " + test2.toInfixFormulaString());
        unify(test1, test2);
        System.out.println("unify(" + testStr1 + "," + testStr2 + ") == (" + test1.toInfixFormulaString() + "," + test2.toInfixFormulaString() + ")");

        String testStr3 = "X1->(X2->(B->C))";
        FormulaSyntaxNet test3 = newFromInfixFormulaString(owner, testStr3);
        System.out.println("T3:" + testStr3 + " parsed to " + test3.toInfixFormulaString());

        System.out.println("unifying T1 with T3");
        unify(test1, test3);
        System.out.println("T1 has become " + test1.toInfixFormulaString());
        System.out.println("T3 has become " + test3.toInfixFormulaString());
        System.out.println("T2 has become " + test2.toInfixFormulaString() + " (should be same as T1 and T3)");

        System.out.println("creating unbound leaves u1, u2, u3 and u4");
        FormulaSyntaxNet u1 = newLeaf(owner, null);
        FormulaSyntaxNet u2 = newLeaf(owner, null);
        FormulaSyntaxNet u3 = newLeaf(owner, null);
        FormulaSyntaxNet u4 = newLeaf(owner, null);
        System.out.println("unifying u1 and u2");
        unify(u1, u2);
        System.out.println("unifying u3 and u4");
        unify(u3, u4);
        System.out.println("unifying u1 and u3");
        unify(u1, u3);
        System.out.println("unifying u4 and A");
        unify(u4, A);
        System.out.println("u1, u2, u3, u4 should all be 'A' now: " + u1.toInfixFormulaString() + ", " + u2.toInfixFormulaString() + ", " + u3.toInfixFormulaString() + ", " + u4.toInfixFormulaString());

        FormulaSyntaxNet[] cn = new FormulaSyntaxNet[10];
        cn[1] = newBranch(owner, newLeaf(owner, null), newLeaf(owner, null));
        cn[2] = newBranch(owner,
                newLeaf(owner, "A"),
                newBranch(owner,
                        newLeaf(owner, null),
                        newLeaf(owner, null)
                )
        );
        cn[3] = newLeaf(owner, null);
        cn[7] = newLeaf(owner, null);
        cn[8] = newLeaf(owner, null);

        String owner2 = "owner2";
        cn[4] = newLeaf(owner2, null);
        cn[5] = newBranch(owner2,
                newLeaf(owner2, "A"),
                newBranch(owner2,
                        newLeaf(owner2, "B"),
                        newLeaf(owner2, "B")
                )
        );
        cn[6] = newBranch(owner2,
                newLeaf(owner2, "A"),
                newBranch(owner2,
                        newLeaf(owner2, null),
                        newLeaf(owner2, "B")
                )
        );

        for (int i = 1; i <= 8; ++i) {
            System.out.println("cn[" + i + "]=" + cn[i].toInfixFormulaString());
        }

        System.out.println("unify(cn[3],cn[7]);");
        unify(cn[3], cn[7]);
        System.out.println("unify(cn[8],cn[7]);");
        unify(cn[8], cn[7]);
        System.out.println("cn[3]=" + cn[3].toInfixFormulaString());
        System.out.println("cn[7]=" + cn[7].toInfixFormulaString());
        System.out.println("cn[8]=" + cn[8].toInfixFormulaString());

        System.out.println("unify(cn[1].right(),cn[3])");
        unify(cn[1].right(), cn[3]);
        System.out.println("cn[1]=" + cn[1].toInfixFormulaString());
        System.out.println("cn[3]=" + cn[3].toInfixFormulaString());

        System.out.println("unify(cn[3],cn[2])");
        unify(cn[3], cn[2]);
        for (int i = 1; i <= 3; ++i) {
            System.out.println("cn[" + i + "]=" + cn[i].toInfixFormulaString());
        }

        System.out.println("unify(cn[1].left(),cn[2].right().left())");
        unify(cn[1].left(), cn[2].right().left());
        for (int i = 1; i <= 3; ++i) {
            System.out.println("cn[" + i + "]=" + cn[i].toInfixFormulaString());
        }

        System.out.println("unify(cn[4],cn[5])");
        unify(cn[4], cn[5]);
        for (int i = 1; i <= 5; ++i) {
            System.out.println("cn[" + i + "]=" + cn[i].toInfixFormulaString());
        }

        System.out.println("unify(cn[4],cn[6])");
        unify(cn[4], cn[6]);
        for (int i = 1; i <= 6; ++i) {
            System.out.println("cn[" + i + "]=" + cn[i].toInfixFormulaString());
        }

        Collection allNodesReachableFromCN4 = new HashSet();
        cn[4].getAllReachableNodes(allNodesReachableFromCN4);
        System.out.println("number of elements reachable from cn[4]=" + allNodesReachableFromCN4.size());
        Collection allNodesReachableFromCN1 = new HashSet();
        cn[1].getAllReachableNodes(allNodesReachableFromCN1);
        System.out.println("number of elements reachable from cn[1]=" + allNodesReachableFromCN1.size());

        System.out.println("unify(cn[4],cn[1].right())");
        unify(cn[4], cn[1].right());

        for (int i = 1; i <= 8; ++i) {
            System.out.println("cn[" + i + "]=" + cn[i].toInfixFormulaString());
        }

        Collection owners_owner2 = new HashSet();
        owners_owner2.add(owner2);
        System.out.println("Splitting off cn[4],cn[5],cn[6]:");
        System.out.println("  extractByOwner(cn[4],owners_owner2)");
        extractByOwner(cn[4], owners_owner2);
        for (int i = 1; i <= 8; ++i) {
            System.out.println("cn[" + i + "]=" + cn[i].toInfixFormulaString());
        }

        Collection allNodesReachableFromCN4_2 = new HashSet();
        cn[4].getAllReachableNodes(allNodesReachableFromCN4_2);
        System.out.println("number of elements reachable from cn[4]=" + allNodesReachableFromCN4_2.size());
        Collection allNodesReachableFromCN1_2 = new HashSet();
        cn[1].getAllReachableNodes(allNodesReachableFromCN1_2);
        System.out.println("number of elements reachable from cn[1]=" + allNodesReachableFromCN1_2.size());

        if (
                !allNodesReachableFromCN4.equals(allNodesReachableFromCN4_2) ||
                        !allNodesReachableFromCN1.equals(allNodesReachableFromCN1_2)
        )
            throw new RuntimeException("disentangling c[1] and c[4] FAILED!");

        System.out.print("unify(cn[2].right().right(),cn[1])...");
        try {
            unify(cn[2].right().right(), cn[1]);
            System.out.println("succeeds (This is BAD!)");
        } catch (ConflictingBindException x) {
            System.out.println("fails (This is GOOD!)");
        }

        System.out.println("removeAllForwards(cn[1])");
        removeAllForwards(cn[1]);
        for (int i = 1; i <= 8; ++i) {
            System.out.println("cn[" + i + "]=" + cn[i].toInfixFormulaString());
        }

        System.out.println();
        FormulaSyntaxNet sleaf = newLeaf(owner, null);
        FormulaSyntaxNet fakebranch = newBranch(owner, sleaf, sleaf);
        FormulaSyntaxNet weird = newBranch(owner, fakebranch, fakebranch);
        FormulaSyntaxNet apyra = newFromInfixFormulaString(owner, "(X0->A)->(X1->X0)");
        System.out.println("weird=" + weird.toInfixFormulaString());
        System.out.println("apyra=" + apyra.toInfixFormulaString());
        System.out.println("unify(apyra,weird)");
        unify(apyra, weird);
        System.out.println("weird=" + weird.toInfixFormulaString());
        System.out.println("apyra=" + apyra.toInfixFormulaString());

        String[] formulaStrings = {"X0->X1", "(X1->(X1->X2))", "X2->X0"};
        FormulaSyntaxNet[] formulas = newFromInfixFormulaStrings(owner, formulaStrings);

        if (formulas.length != formulaStrings.length) throw new RuntimeException("Oops!");

        for (int i = 0; i < formulas.length; ++i) {
            System.out.println("formulaStrings[" + i + "]=" + formulaStrings[i] + " parsed to " + formulas[i].toInfixFormulaString());
        }

        System.out.println("unify(A,formulas[0].left())");
        unify(A, formulas[0].left());
        for (int i = 0; i < formulas.length; ++i) {
            System.out.println("formulas[" + i + "] has become " + formulas[i].toInfixFormulaString());
        }

        System.out.println("unify(B,formulas[2].left())");
        unify(B, formulas[2].left());
        for (int i = 0; i < formulas.length; ++i) {
            System.out.println("formulas[" + i + "] has become " + formulas[i].toInfixFormulaString());
        }

        System.out.println("unify(C,formulas[1].left())");
        unify(C, formulas[1].left());
        for (int i = 0; i < formulas.length; ++i) {
            System.out.println("formulas[" + i + "] has become " + formulas[i].toInfixFormulaString());
        }
    }
}
