# Domino On Acid

Domino On Acid is a natural deduction visualization in the form of a game of unusual dominoes.

It is written in Java and the code is released under the open source [GPL-2.0 license](LICENSE).
It is based on an earlier version published in 2009 (see [history](History.md)).

Currently, there is no official release.